OS := $(shell uname -s)

ifeq ($(build),sanitize)
    $(info *** compiling with debug turned on and address sanitizer (only for linux))
    SANITIZE = -g -fsanitize=address
    BUILD    = debug
else ifeq ($(build),debug)
    $(info *** compiling with debug turned on)
    BUILD = debug
else ifeq ($(build),release)
    $(info *** compiling in release mode -O2)
    BUILD = release
else
    $(info use one of these options:)
    $(info make build=release)
    $(info make build=debug)
    $(info make build=sanitize)
    $(info make clean)
    $(info make clean clean_all)
    $(error error)
endif

ifeq ($(OS),Linux)
	ifeq ($(BUILD),release)
		CC = gcc -O2 -DNDEBUG -DLUA_USE_LINUX -Wno-stringop-overflow -c $< -o $@
		LD = gcc -o $@ $^ -ldl -lpthread -lm -lreadline
	else
		CC = gcc -DDEBUG -DLUA_USE_LINUX $(SANITIZE) -W -Wall -Wno-implicit-fallthrough -c $< -o $@
		LD = gcc -o $@ $^ $(SANITIZE) -lm -lreadline
	endif

else ifeq ($(findstring MINGW64,$(OS)), MINGW64)
	ifeq ($(BUILD),release)
		CC = gcc -O2 -DNDEBUG -D__USE_MINGW_ANSI_STDIO=1 -DLUA_USE_WINDOWS -Wno-stringop-overflow -D__MSVCRT_VERSION__=0x0800 -I/usr/local/include -c $< -o $@
		LD = gcc -static -o $@ $^
	else
		CC = gcc -DDEBUG -D__USE_MINGW_ANSI_STDIO=1 -DLUA_USE_WINDOWS -W -Wall -Wno-unused-variable -Wno-cast-function-type -Wno-implicit-fallthrough -D__MSVCRT_VERSION__=0x0800 -Wno-unused-parameter -I/usr/local/include -c $< -o $@
		LD = gcc -static -o $@ $^
	endif

else
    $(error error: unknown build system)
endif

#-------------------------------------------------------------------------------

LUA      = lua-536.exe
LUAC     = luac-536.exe
LUAS     = luas-536.exe
CONVERT  = convert.exe
EMBEDDED = dkjson/dkjson.out dkjson/dkjson.h xml2lua/xml2lua.out xml2lua/xml2lua.h
OBJ1     = obj/luac.o obj/convert.o
EXE1     = $(LUAC) $(CONVERT)
EXE2     = $(LUA) $(LUAS)

all: $(EXE1) $(EMBEDDED) $(EXE2)

#-------------------------------------------------------------------------------

obj/luac.o: luac.c lua536.h
	$(CC)

$(LUAC): obj/luac.o
	$(LD)

obj/convert.o: convert.c
	$(CC)

$(CONVERT): obj/convert.o
	$(LD)

#-------------------------------------------------------------------------------

dkjson/dkjson.out: dkjson/dkjson.lua
	./$(LUAC) -s -o dkjson/dkjson.out dkjson/dkjson.lua

dkjson/dkjson.h: dkjson/dkjson.out
	./$(CONVERT) dkjson/dkjson.out DKJSON > dkjson/dkjson.h

xml2lua/xml2lua.out: xml2lua/xml2lua.lua
	./$(LUAC) -s -o xml2lua/xml2lua.out xml2lua/xml2lua.lua

xml2lua/xml2lua.h: xml2lua/xml2lua.out
	./$(CONVERT) xml2lua/xml2lua.out XML2LUA > xml2lua/xml2lua.h

#-------------------------------------------------------------------------------

obj/lua536.o: lua536.c lua536.h
	$(CC) -DLUA_USE_LMATHX -DLUA_USE_LFS -DLUA_USE_LSQLITE3 -DLUA_USE_LPEG -DLUA_USE_XML2LUA -DLUA_USE_DKJSON

obj/sqlite3.o: lsqlite3/sqlite3.c
	$(CC)

obj/lua.o: lua.c lua536.h
	$(CC)

$(LUA): obj/lua.o obj/lua536.o obj/sqlite3.o
	$(LD)

obj/luas.o: luas.c lua536.h
	$(CC)

$(LUAS): obj/luas.o obj/lua536.o obj/sqlite3.o
	$(LD)

#-------------------------------------------------------------------------------
test: $(LUA)
	./$(LUA) tests/bitwise.lua
	./$(LUA) tests/calls.lua
	./$(LUA) tests/constructs.lua
	./$(LUA) tests/db.lua
	./$(LUA) tests/defer.lua
	./$(LUA) tests/errors.lua
	./$(LUA) tests/events.lua
	./$(LUA) tests/gc.lua
	./$(LUA) tests/goto.lua
	./$(LUA) tests/literals.lua
	./$(LUA) tests/locals.lua
	./$(LUA) tests/math.lua
	./$(LUA) tests/nextvar.lua
	./$(LUA) tests/os.lua
	./$(LUA) tests/pm.lua
	./$(LUA) tests/read_lua.lua
	./$(LUA) tests/sort.lua
	./$(LUA) tests/strings.lua
	./$(LUA) tests/tpack.lua
	./$(LUA) tests/utf8.lua
	./$(LUA) tests/vararg.lua
	./$(LUA) tests/verybig.lua
	./$(LUA) tests/lfs.lua
	./$(LUA) tests/lmathx.lua
	./$(LUA) tests/lpeg.lua
	./$(LUA) tests/sqlite.lua
	./$(LUA) tests/dkjson.lua
	./$(LUA) tests/xml2lua.lua
	rm test.db

test_lfs: $(LUA)
	./$(LUA) tests/lfs.lua

test_lmathx: $(LUA)
	./$(LUA) tests/lmathx.lua

test_lpeg: $(LUA)
	./$(LUA) tests/lpeg.lua

test_sqlite: $(LUA)
	./$(LUA) tests/sqlite.lua
	rm test.db

test_luac: $(LUA) $(LUAC)
	rm -f luac.out
	./$(LUAC) tests/sqlite.lua
	./$(LUA) luac.out

test_luas: $(LUAC) $(LUAS)
	rm -f luac.out
	./$(LUAC) tests/sqlite.lua
	./$(LUAS) --output test1.exe --input luac.out
	./$(LUAS) --luas luas-536.exe --output test2.exe --input tests/sqlite.lua
	./test1.exe
	./test2.exe

test_embedded: $(LUA)
	./$(LUA) tests/dkjson.lua
	./$(LUA) tests/xml2lua.lua

#-------------------------------------------------------------------------------

installw: $(LUA) $(LUAC) $(LUAS) /C/bin
	strip -p $(LUA)
	strip -p $(LUAC)
	strip -p $(LUAS)
	cp -f $(LUA) /C/bin/$(LUA)
	cp -f $(LUAC) /C/bin/$(LUAC)
	cp -f $(LUAS) /C/bin/$(LUAS)

installx: $(LUA) $(LUAC) $(LUAS) ~/bin
	strip -p $(LUA)
	strip -p $(LUAC)
	strip -p $(LUAS)
	cp -f $(LUA) ~/bin/lua-536
	cp -f $(LUAC) ~/bin/luac-536
	cp -f $(LUAS) ~/bin/luas-536

clean:
	rm -f $(LUA536) $(LUA) $(LUAC) $(LUAS) obj/* *.out *.db *.exe

clean_all:
	rm -f dkjson/*.h
	rm -f dkjson/*.out
	rm -f xml2lua/*.h
	rm -f xml2lua/*.out

#-------------------------------------------------------------------------------

SRC_FILES=\
	536/lapi.c \
	536/lapi.h \
	536/lauxlib.c \
	536/lauxlib.h \
	536/lbaselib.c \
	536/lbitlib.c \
	536/lcode.c \
	536/lcode.h \
	536/lcorolib.c \
	536/lctype.c \
	536/lctype.h \
	536/ldblib.c \
	536/ldebug.c \
	536/ldebug.h \
	536/ldo.c \
	536/ldo.h \
	536/ldump.c \
	536/lfunc.c \
	536/lfunc.h \
	536/lgc.c \
	536/lgc.h \
	536/linitx.c \
	536/liolib.c \
	536/llex.c \
	536/llex.h \
	536/llimits.h \
	536/lmathlib.c \
	536/lmem.c \
	536/lmem.h \
	536/loadlib.c \
	536/lobject.c \
	536/lobject.h \
	536/lopcodes.c \
	536/lopcodes.h \
	536/loslib.c \
	536/lparser.c \
	536/lparser.h \
	536/lprefix.h \
	536/lstate.c \
	536/lstate.h \
	536/lstring.c \
	536/lstring.h \
	536/lstrlib.c \
	536/ltable.c \
	536/ltable.h \
	536/ltablib.c \
	536/ltm.c \
	536/ltm.h \
	536/lua.h \
	536/luaconf.h \
	536/lualib.h \
	536/lundump.c \
	536/lundump.h \
	536/lutf8lib.c \
	536/lvm.c \
	536/lvm.h \
	536/lzio.c \
	536/lzio.h \
	lmathx/lmathx.c \
	lfs/lfs.c \
	lsqlite3/lsqlite3.c \
	lpeg/lptypes.h \
	lpeg/lpcap.h \
	lpeg/lptree.h \
	lpeg/lpvm.h \
	lpeg/lpcode.h \
	lpeg/lpprint.h \
	lpeg/lpcap.c \
	lpeg/lptree.c \
	lpeg/lpvm.c \
	lpeg/lpcode.c \
	lpeg/lpprint.c \
	dkjson/dkjson.h \
	xml2lua/xml2lua.h \

 lua536.c: $(SRC_FILES)
	echo "#include \"lua536.h\"" > tmp.c
	cat 536/lprefix.h >> tmp.c
	cat 536/llimits.h >> tmp.c
	cat 536/lctype.h >> tmp.c
	cat 536/lopcodes.h >> tmp.c
	cat 536/lmem.h >> tmp.c
	cat 536/lzio.h >> tmp.c
	cat 536/lobject.h >> tmp.c
	cat 536/lparser.h >> tmp.c
	cat 536/llex.h >> tmp.c
	cat 536/ltm.h >> tmp.c
	cat 536/lstate.h >> tmp.c
	cat 536/ldebug.h >> tmp.c
	cat 536/lapi.h >> tmp.c
	cat 536/lcode.h >> tmp.c
	cat 536/ldo.h >> tmp.c
	cat 536/lfunc.h >> tmp.c
	cat 536/lgc.h >> tmp.c
	cat 536/lstring.h >> tmp.c
	cat 536/ltable.h >> tmp.c
	cat 536/lundump.h >> tmp.c
	cat 536/lvm.h >> tmp.c
	cat 536/lapi.c >> tmp.c
	cat 536/lcode.c >> tmp.c
	cat 536/lctype.c >> tmp.c
	cat 536/ldebug.c >> tmp.c
	cat 536/ldo.c >> tmp.c
	cat 536/ldump.c >> tmp.c
	cat 536/lfunc.c >> tmp.c
	cat 536/lgc.c >> tmp.c
	cat 536/llex.c >> tmp.c
	cat 536/lmem.c >> tmp.c
	cat 536/lobject.c >> tmp.c
	cat 536/lopcodes.c >> tmp.c
	cat 536/lparser.c >> tmp.c
	cat 536/lstate.c >> tmp.c
	cat 536/lstring.c >> tmp.c
	cat 536/ltable.c >> tmp.c
	cat 536/ltm.c >> tmp.c
	cat 536/lundump.c >> tmp.c
	cat 536/lvm.c >> tmp.c
	cat 536/lzio.c >> tmp.c
	cat 536/lauxlib.c >> tmp.c
	cat 536/lbaselib.c >> tmp.c
	cat 536/lcorolib.c >> tmp.c
	cat 536/ldblib.c >> tmp.c
	cat 536/liolib.c >> tmp.c
	cat 536/lmathlib.c >> tmp.c
	cat 536/loadlib.c >> tmp.c
	cat 536/loslib.c >> tmp.c
	cat 536/lstrlib.c >> tmp.c
	cat 536/ltablib.c >> tmp.c
	cat 536/lutf8lib.c >> tmp.c
	cat lmathx/lmathx.c >> tmp.c
	cat lfs/lfs.c >> tmp.c
	cat lsqlite3/lsqlite3.c >> tmp.c
	cat lpeg/lptypes.h >> tmp.c
	cat lpeg/lpcap.h >> tmp.c
	cat lpeg/lptree.h >> tmp.c
	cat lpeg/lpvm.h >> tmp.c
	cat lpeg/lpcode.h >> tmp.c
	cat lpeg/lpprint.h >> tmp.c
	cat lpeg/lpcap.c >> tmp.c
	cat lpeg/lpcode.c >> tmp.c
	cat lpeg/lpprint.c >> tmp.c
	cat lpeg/lptree.c >> tmp.c
	cat lpeg/lpvm.c >> tmp.c
	cat dkjson/dkjson.h >> tmp.c
	cat xml2lua/xml2lua.h >> tmp.c
	cat 536/linitx.c >> tmp.c
	cat tmp.c | sed -nf scripts/remove_comments.sh | awk NF > lua536.c
	rm tmp.c
