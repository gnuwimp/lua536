#include "lua536.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#else
    #include <sys/stat.h>
    #include <unistd.h>
#endif

typedef struct Buf {
    char*  p;
    size_t size;
    size_t adler;
    size_t code;
    size_t code_size;
} Buf;

static const char* const MAGIC_STRING1 = "828c54c4";
static const size_t      MAGIC_SIZE1   = 8;
static const char* const MAGIC_STRING2 = "-718f-465b-a0e6-";
static const size_t      MAGIC_SIZE2   = 16;
static const char* const MAGIC_STRING3 = "65f01fa19ef7";
static const size_t      MAGIC_SIZE3   = 12;

#ifdef _WIN32
    unsigned long GetModuleFileNameA(void* hModule, const char* lpFilename, unsigned long nSize);
#endif

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
unsigned adler32(size_t size, const char buf[size]) {
    unsigned char* ubuf = (unsigned char*) buf;
    unsigned       a    = 1;
    unsigned       b    = 0;

    for (size_t f = 0; f < size; f++) {
        a = (a + ubuf[f]) % 65521;
        b = (b + a) % 65521;
    }

    return (b << 16) | a;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
char* programName(const char* name) {
    char path[1001];

#ifdef _WIN32
    unsigned long size = GetModuleFileNameA(NULL, path, 1000);
    return (size == 1000) ? strdup(name) : strdup(path);
#else
    if (strchr(name, '/') != NULL) {
        return strdup(name);
    }
    #ifdef __FreeBSD__
        ssize_t size = readlink("/proc/curproc/file", path, 1000); // Untested!!!
    #else
        ssize_t size = readlink("/proc/self/exe", path, 1000);
    #endif

    if (size == -1) {
        return strdup(name);
    }

    path[size] = 0;
    return strdup(path);
#endif
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
Buf load(const char* file, size_t minsize) {
    FILE*  fp   = fopen(file, "rb");
    size_t size = 0;
    char*  buf  = NULL;

    #ifdef DEBUG
        printf("debug: file = %s\n", file);
    #endif

    if (fp == NULL) {
       printf("error: can't open file %s for reading\n", file);
        exit(EXIT_FAILURE);
    }

    fseek(fp, 0L, SEEK_END);
    size = ftell(fp);

    #ifdef DEBUG
        printf("debug: size = %d bytes\n", (int) size);
    #endif

    if (size < minsize) {
        fclose(fp);
       printf("error: file size to small (%d bytes) for %s\n", (int) size, file);
        exit(EXIT_FAILURE);
    }

    rewind(fp);
    buf = calloc(size, 1);

    if (buf == NULL) {
        fclose(fp);
        printf("error: can't allocate memory (%d bytes)\n", (int) size);
        exit(EXIT_FAILURE);
    }

    size_t read = fread(buf, 1, size, fp);
    fclose(fp);

    if (read != size) {
        free(buf);
        printf("error: failed to read all file (%d < %d)\n", (int) read, (int) size);
        exit(EXIT_FAILURE);
    }

    return (Buf) { .p = buf, .size = size, .code = 0, .adler = 0, };
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void run (int argc, char **argv) {
    Buf        buf    = load(argv[0], 100000);
    lua_State* L      = NULL;
    unsigned   adler1 = 0;
    unsigned   adler2 = 0;

    { // Find lua code
        for (size_t f = buf.size - 10; f > 200000; f--) {
            if (memcmp(buf.p + f, MAGIC_STRING1, MAGIC_SIZE1) == 0 &&
                memcmp(buf.p + f + MAGIC_SIZE1, MAGIC_STRING2, MAGIC_SIZE2) == 0 &&
                memcmp(buf.p + f + MAGIC_SIZE1 + MAGIC_SIZE2, MAGIC_STRING3, MAGIC_SIZE3) == 0) {

                buf.adler     = f + MAGIC_SIZE1 + MAGIC_SIZE2 + MAGIC_SIZE3;
                buf.code      = buf.adler + 11;
                buf.code_size = buf.size - buf.code;
                break;
            }
        }

        if (buf.code == 0) {
            free(buf.p);
            printf("error: failed to find code\n");
            printf("\n");
            printf("Usage:\n");
            printf("    %s --input [lua_src_or_byte_code] --output [name_of_executable] --luas [optional_path_of_this_program]\n", argv[0]);
            exit(EXIT_FAILURE);
        }

        adler1 = (unsigned) atoi(buf.p + buf.adler);
        adler2 = adler32(buf.code_size, buf.p + buf.code);

        if (adler1 != adler2) {
            free(buf.p);
            printf("error: checksum failure %u != %u\n", adler1, adler2);
            exit(EXIT_FAILURE);
        }
    }

    { // Create lua instance
        L = luaL_newstate();

        if (L == NULL) {
            printf("error: failed to create lua object\n");
            free(buf.p);
            exit(EXIT_FAILURE);
        }

        luaL_openlibs(L);
    }

    { // Copy arguments to lua
        lua_newtable(L);
        for (int f = 0; f < argc; f++) {
            lua_pushstring(L, argv[f]);
            lua_rawseti(L, -2, f);
        }
        lua_setglobal(L, "arg");
    }

    { // Run lua code
        if (luaL_loadbuffer(L, buf.p + buf.code, buf.size - buf.code, argv[0]) != LUA_OK) {
            free(buf.p);
            printf("error: failed to load lua code\n%s\n", lua_tostring(L, -1));
            lua_close(L);
            exit(EXIT_FAILURE);
        }

        if (lua_pcall(L, 0, 0, 0) != LUA_OK) {
            free(buf.p);
            printf("error: failed to run lua code\n%s\n", lua_tostring(L, -1));
            lua_close(L);
            exit(EXIT_FAILURE);
        }

        free(buf.p);
        lua_close(L);
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void create(const char* lua, const char* output, const char* input) {
    Buf      exe   = load(lua, 100000);
    Buf      code  = load(input, 1);
    unsigned adler = adler32(code.size, code.p);
    FILE*    fp    = fopen(output, "wb");
    char     checksum[20];

    snprintf(checksum, 20, "%010u", adler);

    if (fp == NULL) {
        printf("error: can't open file %s for writing\n", output);
        free(exe.p);
        free(code.p);
        exit(EXIT_FAILURE);
    }

    if (fwrite(exe.p, 1, exe.size, fp) != exe.size ||
        fwrite(MAGIC_STRING1, 1, MAGIC_SIZE1, fp) != MAGIC_SIZE1 ||
        fwrite(MAGIC_STRING2, 1, MAGIC_SIZE2, fp) != MAGIC_SIZE2 ||
        fwrite(MAGIC_STRING3, 1, MAGIC_SIZE3, fp) != MAGIC_SIZE3 ||
        fwrite(checksum, 1, 11, fp) != 11 ||
        fwrite(code.p, 1, code.size, fp) != code.size) {
        fclose(fp);
        free(exe.p);
        free(code.p);
        remove(output);
        exit(EXIT_FAILURE);
    }

    fclose(fp);
    free(exe.p);
    free(code.p);

#ifdef _WIN32
#else
    chmod(output, 0700);
#endif
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int main (int argc, char **argv) {
    char*       exe    = programName(argv[0]);
    const char* exe2   = exe;
    const char* output = NULL;
    const char* input  = NULL;

    argv[0] = exe;

    for (int f = 1; f < argc; f++) {
        if (strcmp("--output", argv[f]) == 0 && f < argc - 1) {
            output = argv[f + 1];
        }
        else if (strcmp("--input", argv[f]) == 0 && f < argc - 1) {
            input = argv[f + 1];
        }
        else if (strcmp("--luas", argv[f]) == 0 && f < argc - 1) {
            exe2 = argv[f + 1];
        }
    }

    if (output != NULL && input != NULL) {
        create(exe2, output, input);
    }
    else {
        run(argc, argv);
    }

    free(exe);
    return EXIT_SUCCESS;
}

