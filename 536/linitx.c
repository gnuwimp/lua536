/*
** $Id: linit.c,v 1.39.1.1 2017/04/19 17:20:42 roberto Exp $
** Initialization of libraries for lua.c and other clients
** See Copyright Notice in lua.h
*/


#define linit_c
#define LUA_LIB

/*
** If you embed Lua in your program and need to open the standard
** libraries, call luaL_openlibs in your program. If you need a
** different set of libraries, copy this file to your project and edit
** it to suit your needs.
**
** You can also *preload* libraries, so that a later 'require' can
** open the library, which is already linked to the application.
** For that, do the following code:
**
**  luaL_getsubtable(L, LUA_REGISTRYINDEX, LUA_PRELOAD_TABLE);
**  lua_pushcfunction(L, luaopen_modname);
**  lua_setfield(L, -2, modname);
**  lua_pop(L, 1);  // remove PRELOAD table
*/

//#include "lprefix.h"


#include <stddef.h>

//#include "lua.h"

//#include "lualib.h"
//#include "lauxlib.h"


/*
** these libs are loaded by lua.c and are readily available to any Lua
** program
*/
static const luaL_Reg loadedlibs[] = {
    {"_G", luaopen_base},
    {LUA_LOADLIBNAME, luaopen_package},
    {LUA_COLIBNAME, luaopen_coroutine},
    {LUA_TABLIBNAME, luaopen_table},
    {LUA_IOLIBNAME, luaopen_io},
    {LUA_OSLIBNAME, luaopen_os},
    {LUA_STRLIBNAME, luaopen_string},
    {LUA_MATHLIBNAME, luaopen_math},
    {LUA_UTF8LIBNAME, luaopen_utf8},
    {LUA_DBLIBNAME, luaopen_debug},
#if defined(LUA_COMPAT_BITLIB)
    {LUA_BITLIBNAME, luaopen_bit32},
#endif

#ifdef LUA_USE_LMATHX
    {LMATHX_NAME, luaopen_mathx},
#endif

#ifdef LUA_USE_LFS
    {LFS_LIBNAME, luaopen_lfs},
#endif

#ifdef LUA_USE_LSQLITE3
    {"sqlite3", luaopen_lsqlite3},
#endif

#ifdef LUA_USE_LPEG
    {"lpeg", luaopen_lpeg},
#endif

    {NULL, NULL}
};

int linit_do_string(lua_State *L, const char* descr, const char* str) {
    const int ret = luaL_dostring(L, str);

    if (ret != LUA_OK) {
        fprintf(stderr, "error luaL_dostring: %s: %s\n", descr, lua_tostring(L, -1));
        lua_pop(L, 1);
        return 1;
    }

    return 0;
}

int linit_do_bytecode(lua_State *L, const char* descr, const char* str, size_t size) {
    int ret = luaL_loadbuffer (L, str, size, descr);

    if (ret != LUA_OK) {
        fprintf(stderr, "error luaL_loadbuffer: %s: %s\n", descr, lua_tostring(L, -1));
        lua_pop(L, 1);
        return 1;
    }

    if (lua_pcall(L, 0, 0, 0) != LUA_OK) {
        fprintf(stderr, "error lua_pcall: %s: %s\n", descr, lua_tostring(L, -1));
        lua_pop(L, 1);
        return 1;
    }

    return 0;
}

void luaL_openlibs (lua_State *L) {
    const luaL_Reg *lib;
    /* "require" functions from 'loadedlibs' and set results to global table */
    for (lib = loadedlibs; lib->func; lib++) {
        luaL_requiref(L, lib->name, lib->func, 1);
        lua_pop(L, 1);  /* remove lib */
    }

#ifdef LUA_USE_XML2LUA
    linit_do_bytecode(L, "xml2lua", (const char*) XML2LUA, XML2LUA_SIZE);
    // linit_do_string(L, "xml2lua", (const char*) XML2LUA);
#endif

#ifdef LUA_USE_DKJSON
    linit_do_bytecode(L, "dkjson", (const char*) DKJSON, DKJSON_SIZE);
    // linit_do_string(L, "dkjson", (const char*) DKJSON);
#endif

}

