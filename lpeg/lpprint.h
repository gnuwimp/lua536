/*
** $Id: lpprint.h $
*/

#ifdef LUA_USE_LPEG

#if !defined(lpprint_h)
#define lpprint_h


//#include "lptree.h"
//#include "lpvm.h"


#if defined(LPEG_DEBUG)

void printpatt (Instruction2 *p, int n);
void printtree (TTree *tree, int ident);
void printktable (lua_State *L, int idx);
void printcharset (const byte *st);
void printcaplist (Capture *cap, Capture *limit);
void printinst (const Instruction2 *op, const Instruction2 *p);

#else

#define printktable(L,idx)  \
	luaL_error(L, "function only implemented in debug mode")
#define printtree(tree,i)  \
	luaL_error(L, "function only implemented in debug mode")
#define printpatt(p,n)  \
	luaL_error(L, "function only implemented in debug mode")

#endif


#endif

#endif
