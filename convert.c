#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const size_t MAX_SIZE = 1000000;

int main (int argc, char **argv) {
    FILE*  file = NULL;
    size_t read = 0;
    char buffer[MAX_SIZE + 1];

    if (argc < 3) {
        fprintf(stderr, "error: missing arguments\n");
        goto ERROR;
    }

    file = fopen(argv[1], "rb");

    if (file == NULL) {
        fprintf(stderr, "error: can't open file <%s>\n", argv[1]);
        goto ERROR;
    }

    read = fread(buffer, 1, MAX_SIZE, file);
    fclose(file);

    if (read == 0 || read == MAX_SIZE) {
        fprintf(stderr, "error: can't read file or it is to large (%d bytes)\n", (int) read);
        return EXIT_FAILURE;
    }

    fprintf(stderr, "read %d bytes from %s\n", (int) read, argv[1]);
    fflush(stderr);

    fprintf(stdout, "#ifdef LUA_USE_%s\n", argv[2]);
    fprintf(stdout, "static const size_t %s_SIZE = %d;\n", argv[2], (int) read);
    fprintf(stdout, "static const unsigned char %s[%d] = {\n", argv[2], (int) read + 1);

    for (size_t f = 0; f < read; f++) {
        fprintf(stdout, "%d", (unsigned char) buffer[f]);
        fprintf(stdout, ",");
        if (f && f % 64 == 0) fprintf(stdout, "\n");
    }

    fprintf(stdout, "0};\n");
    fprintf(stdout, "#endif\n");
    fflush(stdout);

    return EXIT_SUCCESS;

ERROR:
    fflush(stderr);
    return EXIT_FAILURE;
}

