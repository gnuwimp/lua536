#include "lua536.h"
#ifndef lprefix_h
#define lprefix_h
#if !defined(LUA_USE_C89)	
#if !defined(_XOPEN_SOURCE)
#define _XOPEN_SOURCE           600
#elif _XOPEN_SOURCE == 0
#undef _XOPEN_SOURCE  
#endif
#if !defined(LUA_32BITS) && !defined(_FILE_OFFSET_BITS)
#define _LARGEFILE_SOURCE       1
#define _FILE_OFFSET_BITS       64
#endif
#endif				
#if defined(_WIN32) 	
#if !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS  
#endif
#endif			
#endif
#ifndef llimits_h
#define llimits_h
#include <limits.h>
#include <stddef.h>
#if defined(LUAI_MEM)		
typedef LUAI_UMEM lu_mem;
typedef LUAI_MEM l_mem;
#elif LUAI_BITSINT >= 32	
typedef size_t lu_mem;
typedef ptrdiff_t l_mem;
#else  	
typedef unsigned long lu_mem;
typedef long l_mem;
#endif				
typedef unsigned char lu_byte;
#define MAX_SIZET	((size_t)(~(size_t)0))
#define MAX_SIZE	(sizeof(size_t) < sizeof(lua_Integer) ? MAX_SIZET \
                          : (size_t)(LUA_MAXINTEGER))
#define MAX_LUMEM	((lu_mem)(~(lu_mem)0))
#define MAX_LMEM	((l_mem)(MAX_LUMEM >> 1))
#define MAX_INT		INT_MAX  
#define point2uint(p)	((unsigned int)((size_t)(p) & UINT_MAX))
#if defined(LUAI_USER_ALIGNMENT_T)
typedef LUAI_USER_ALIGNMENT_T L_Umaxalign;
#else
typedef union {
  lua_Number n;
  double u;
  void *s;
  lua_Integer i;
  long l;
} L_Umaxalign;
#endif
typedef LUAI_UACNUMBER l_uacNumber;
typedef LUAI_UACINT l_uacInt;
#if defined(lua_assert)
#define check_exp(c,e)		(lua_assert(c), (e))
#define lua_longassert(c)	((c) ? (void)0 : lua_assert(0))
#else
#define lua_assert(c)		((void)0)
#define check_exp(c,e)		(e)
#define lua_longassert(c)	((void)0)
#endif
#if !defined(luai_apicheck)
#define luai_apicheck(l,e)	lua_assert(e)
#endif
#define api_check(l,e,msg)	luai_apicheck(l,(e) && msg)
#if !defined(UNUSED)
#define UNUSED(x)	((void)(x))
#endif
#define cast(t, exp)	((t)(exp))
#define cast_void(i)	cast(void, (i))
#define cast_byte(i)	cast(lu_byte, (i))
#define cast_num(i)	cast(lua_Number, (i))
#define cast_int(i)	cast(int, (i))
#define cast_uchar(i)	cast(unsigned char, (i))
#if !defined(l_castS2U)
#define l_castS2U(i)	((lua_Unsigned)(i))
#endif
#if !defined(l_castU2S)
#define l_castU2S(i)	((lua_Integer)(i))
#endif
#if defined(__GNUC__)
#define l_noret		void __attribute__((noreturn))
#elif defined(_MSC_VER) && _MSC_VER >= 1200
#define l_noret		void __declspec(noreturn)
#else
#define l_noret		void
#endif
#if !defined(LUAI_MAXCCALLS)
#define LUAI_MAXCCALLS		200
#endif
#if LUAI_BITSINT >= 32
typedef unsigned int Instruction;
#else
typedef unsigned long Instruction;
#endif
#if !defined(LUAI_MAXSHORTLEN)
#define LUAI_MAXSHORTLEN	40
#endif
#if !defined(MINSTRTABSIZE)
#define MINSTRTABSIZE	128
#endif
#if !defined(STRCACHE_N)
#define STRCACHE_N		53
#define STRCACHE_M		2
#endif
#if !defined(LUA_MINBUFFER)
#define LUA_MINBUFFER	32
#endif
#if !defined(lua_lock)
#define lua_lock(L)	((void) 0)
#define lua_unlock(L)	((void) 0)
#endif
#if !defined(luai_threadyield)
#define luai_threadyield(L)	{lua_unlock(L); lua_lock(L);}
#endif
#if !defined(luai_userstateopen)
#define luai_userstateopen(L)		((void)L)
#endif
#if !defined(luai_userstateclose)
#define luai_userstateclose(L)		((void)L)
#endif
#if !defined(luai_userstatethread)
#define luai_userstatethread(L,L1)	((void)L)
#endif
#if !defined(luai_userstatefree)
#define luai_userstatefree(L,L1)	((void)L)
#endif
#if !defined(luai_userstateresume)
#define luai_userstateresume(L,n)	((void)L)
#endif
#if !defined(luai_userstateyield)
#define luai_userstateyield(L,n)	((void)L)
#endif
#if !defined(luai_numidiv)
#define luai_numidiv(L,a,b)     ((void)L, l_floor(luai_numdiv(L,a,b)))
#endif
#if !defined(luai_numdiv)
#define luai_numdiv(L,a,b)      ((a)/(b))
#endif
#if !defined(luai_nummod)
#define luai_nummod(L,a,b,m)  \
  { (m) = l_mathop(fmod)(a,b); if ((m)*(b) < 0) (m) += (b); }
#endif
#if !defined(luai_numpow)
#define luai_numpow(L,a,b)      ((void)L, l_mathop(pow)(a,b))
#endif
#if !defined(luai_numadd)
#define luai_numadd(L,a,b)      ((a)+(b))
#define luai_numsub(L,a,b)      ((a)-(b))
#define luai_nummul(L,a,b)      ((a)*(b))
#define luai_numunm(L,a)        (-(a))
#define luai_numeq(a,b)         ((a)==(b))
#define luai_numlt(a,b)         ((a)<(b))
#define luai_numle(a,b)         ((a)<=(b))
#define luai_numisnan(a)        (!luai_numeq((a), (a)))
#endif
#if !defined(HARDSTACKTESTS)
#define condmovestack(L,pre,pos)	((void)0)
#else
#define condmovestack(L,pre,pos)  \
	{ int sz_ = (L)->stacksize; pre; luaD_reallocstack((L), sz_); pos; }
#endif
#if !defined(HARDMEMTESTS)
#define condchangemem(L,pre,pos)	((void)0)
#else
#define condchangemem(L,pre,pos)  \
	{ if (G(L)->gcrunning) { pre; luaC_fullgc(L, 0); pos; } }
#endif
#endif
#ifndef lctype_h
#define lctype_h
#if !defined(LUA_USE_CTYPE)
#if 'A' == 65 && '0' == 48
#define LUA_USE_CTYPE	0
#else
#define LUA_USE_CTYPE	1
#endif
#endif
#if !LUA_USE_CTYPE	
#include <limits.h>
#define ALPHABIT	0
#define DIGITBIT	1
#define PRINTBIT	2
#define SPACEBIT	3
#define XDIGITBIT	4
#define MASK(B)		(1 << (B))
#define testprop(c,p)	(luai_ctype_[(c)+1] & (p))
#define lislalpha(c)	testprop(c, MASK(ALPHABIT))
#define lislalnum(c)	testprop(c, (MASK(ALPHABIT) | MASK(DIGITBIT)))
#define lisdigit(c)	testprop(c, MASK(DIGITBIT))
#define lisspace(c)	testprop(c, MASK(SPACEBIT))
#define lisprint(c)	testprop(c, MASK(PRINTBIT))
#define lisxdigit(c)	testprop(c, MASK(XDIGITBIT))
#define ltolower(c)	((c) | ('A' ^ 'a'))
LUAI_DDEC const lu_byte luai_ctype_[UCHAR_MAX + 2];
#else			
#include <ctype.h>
#define lislalpha(c)	(isalpha(c) || (c) == '_')
#define lislalnum(c)	(isalnum(c) || (c) == '_')
#define lisdigit(c)	(isdigit(c))
#define lisspace(c)	(isspace(c))
#define lisprint(c)	(isprint(c))
#define lisxdigit(c)	(isxdigit(c))
#define ltolower(c)	(tolower(c))
#endif			
#endif
#ifndef lopcodes_h
#define lopcodes_h
enum OpMode {iABC, iABx, iAsBx, iAx};  
#define SIZE_C		9
#define SIZE_B		9
#define SIZE_Bx		(SIZE_C + SIZE_B)
#define SIZE_A		8
#define SIZE_Ax		(SIZE_C + SIZE_B + SIZE_A)
#define SIZE_OP		6
#define POS_OP		0
#define POS_A		(POS_OP + SIZE_OP)
#define POS_C		(POS_A + SIZE_A)
#define POS_B		(POS_C + SIZE_C)
#define POS_Bx		POS_C
#define POS_Ax		POS_A
#if SIZE_Bx < LUAI_BITSINT-1
#define MAXARG_Bx        ((1<<SIZE_Bx)-1)
#define MAXARG_sBx        (MAXARG_Bx>>1)         
#else
#define MAXARG_Bx        MAX_INT
#define MAXARG_sBx        MAX_INT
#endif
#if SIZE_Ax < LUAI_BITSINT-1
#define MAXARG_Ax	((1<<SIZE_Ax)-1)
#else
#define MAXARG_Ax	MAX_INT
#endif
#define MAXARG_A        ((1<<SIZE_A)-1)
#define MAXARG_B        ((1<<SIZE_B)-1)
#define MAXARG_C        ((1<<SIZE_C)-1)
#define MASK1(n,p)	((~((~(Instruction)0)<<(n)))<<(p))
#define MASK0(n,p)	(~MASK1(n,p))
#define GET_OPCODE(i)	(cast(OpCode, ((i)>>POS_OP) & MASK1(SIZE_OP,0)))
#define SET_OPCODE(i,o)	((i) = (((i)&MASK0(SIZE_OP,POS_OP)) | \
		((cast(Instruction, o)<<POS_OP)&MASK1(SIZE_OP,POS_OP))))
#define getarg(i,pos,size)	(cast(int, ((i)>>pos) & MASK1(size,0)))
#define setarg(i,v,pos,size)	((i) = (((i)&MASK0(size,pos)) | \
                ((cast(Instruction, v)<<pos)&MASK1(size,pos))))
#define GETARG_A(i)	getarg(i, POS_A, SIZE_A)
#define SETARG_A(i,v)	setarg(i, v, POS_A, SIZE_A)
#define GETARG_B(i)	getarg(i, POS_B, SIZE_B)
#define SETARG_B(i,v)	setarg(i, v, POS_B, SIZE_B)
#define GETARG_C(i)	getarg(i, POS_C, SIZE_C)
#define SETARG_C(i,v)	setarg(i, v, POS_C, SIZE_C)
#define GETARG_Bx(i)	getarg(i, POS_Bx, SIZE_Bx)
#define SETARG_Bx(i,v)	setarg(i, v, POS_Bx, SIZE_Bx)
#define GETARG_Ax(i)	getarg(i, POS_Ax, SIZE_Ax)
#define SETARG_Ax(i,v)	setarg(i, v, POS_Ax, SIZE_Ax)
#define GETARG_sBx(i)	(GETARG_Bx(i)-MAXARG_sBx)
#define SETARG_sBx(i,b)	SETARG_Bx((i),cast(unsigned int, (b)+MAXARG_sBx))
#define CREATE_ABC(o,a,b,c)	((cast(Instruction, o)<<POS_OP) \
			| (cast(Instruction, a)<<POS_A) \
			| (cast(Instruction, b)<<POS_B) \
			| (cast(Instruction, c)<<POS_C))
#define CREATE_ABx(o,a,bc)	((cast(Instruction, o)<<POS_OP) \
			| (cast(Instruction, a)<<POS_A) \
			| (cast(Instruction, bc)<<POS_Bx))
#define CREATE_Ax(o,a)		((cast(Instruction, o)<<POS_OP) \
			| (cast(Instruction, a)<<POS_Ax))
#define BITRK		(1 << (SIZE_B - 1))
#define ISK(x)		((x) & BITRK)
#define INDEXK(r)	((int)(r) & ~BITRK)
#if !defined(MAXINDEXRK)  
#define MAXINDEXRK	(BITRK - 1)
#endif
#define RKASK(x)	((x) | BITRK)
#define NO_REG		MAXARG_A
typedef enum {
OP_MOVE,
OP_LOADK,
OP_LOADKX,
OP_LOADBOOL,
OP_LOADNIL,
OP_GETUPVAL,
OP_GETTABUP,
OP_GETTABLE,
OP_SETTABUP,
OP_SETUPVAL,
OP_SETTABLE,
OP_NEWTABLE,
OP_SELF,
OP_ADD,
OP_SUB,
OP_MUL,
OP_MOD,
OP_POW,
OP_DIV,
OP_IDIV,
OP_BAND,
OP_BOR,
OP_BXOR,
OP_SHL,
OP_SHR,
OP_UNM,
OP_BNOT,
OP_NOT,
OP_LEN,
OP_CONCAT,
OP_JMP,
OP_EQ,
OP_LT,
OP_LE,
OP_TEST,
OP_TESTSET,
OP_CALL,
OP_TAILCALL,
OP_RETURN,
OP_FORLOOP,
OP_FORPREP,
OP_TFORCALL,
OP_TFORLOOP,
OP_SETLIST,
OP_CLOSURE,
OP_VARARG,
OP_EXTRAARG,
OP_DEFER    
} OpCode;
#define NUM_OPCODES	(cast(int, OP_DEFER) + 1)
enum OpArgMask {
  OpArgN,  
  OpArgU,  
  OpArgR,  
  OpArgK   
};
LUAI_DDEC const lu_byte luaP_opmodes[NUM_OPCODES];
#define getOpMode(m)	(cast(enum OpMode, luaP_opmodes[m] & 3))
#define getBMode(m)	(cast(enum OpArgMask, (luaP_opmodes[m] >> 4) & 3))
#define getCMode(m)	(cast(enum OpArgMask, (luaP_opmodes[m] >> 2) & 3))
#define testAMode(m)	(luaP_opmodes[m] & (1 << 6))
#define testTMode(m)	(luaP_opmodes[m] & (1 << 7))
LUAI_DDEC const char *const luaP_opnames[NUM_OPCODES+1];  
#define LFIELDS_PER_FLUSH	50
#endif
#ifndef lmem_h
#define lmem_h
#include <stddef.h>
#define luaM_reallocv(L,b,on,n,e) \
  (((sizeof(n) >= sizeof(size_t) && cast(size_t, (n)) + 1 > MAX_SIZET/(e)) \
      ? luaM_toobig(L) : cast_void(0)) , \
   luaM_realloc_(L, (b), (on)*(e), (n)*(e)))
#define luaM_reallocvchar(L,b,on,n)  \
    cast(char *, luaM_realloc_(L, (b), (on)*sizeof(char), (n)*sizeof(char)))
#define luaM_freemem(L, b, s)	luaM_realloc_(L, (b), (s), 0)
#define luaM_free(L, b)		luaM_realloc_(L, (b), sizeof(*(b)), 0)
#define luaM_freearray(L, b, n)   luaM_realloc_(L, (b), (n)*sizeof(*(b)), 0)
#define luaM_malloc(L,s)	luaM_realloc_(L, NULL, 0, (s))
#define luaM_new(L,t)		cast(t *, luaM_malloc(L, sizeof(t)))
#define luaM_newvector(L,n,t) \
		cast(t *, luaM_reallocv(L, NULL, 0, n, sizeof(t)))
#define luaM_newobject(L,tag,s)	luaM_realloc_(L, NULL, tag, (s))
#define luaM_growvector(L,v,nelems,size,t,limit,e) \
          if ((nelems)+1 > (size)) \
            ((v)=cast(t *, luaM_growaux_(L,v,&(size),sizeof(t),limit,e)))
#define luaM_reallocvector(L, v,oldn,n,t) \
   ((v)=cast(t *, luaM_reallocv(L, v, oldn, n, sizeof(t))))
LUAI_FUNC l_noret luaM_toobig (lua_State *L);
LUAI_FUNC void *luaM_realloc_ (lua_State *L, void *block, size_t oldsize,
                                                          size_t size);
LUAI_FUNC void *luaM_growaux_ (lua_State *L, void *block, int *size,
                               size_t size_elem, int limit,
                               const char *what);
#endif
#ifndef lzio_h
#define lzio_h
#define EOZ	(-1)			
typedef struct Zio ZIO;
#define zgetc(z)  (((z)->n--)>0 ?  cast_uchar(*(z)->p++) : luaZ_fill(z))
typedef struct Mbuffer {
  char *buffer;
  size_t n;
  size_t buffsize;
} Mbuffer;
#define luaZ_initbuffer(L, buff) ((buff)->buffer = NULL, (buff)->buffsize = 0)
#define luaZ_buffer(buff)	((buff)->buffer)
#define luaZ_sizebuffer(buff)	((buff)->buffsize)
#define luaZ_bufflen(buff)	((buff)->n)
#define luaZ_buffremove(buff,i)	((buff)->n -= (i))
#define luaZ_resetbuffer(buff) ((buff)->n = 0)
#define luaZ_resizebuffer(L, buff, size) \
	((buff)->buffer = luaM_reallocvchar(L, (buff)->buffer, \
				(buff)->buffsize, size), \
	(buff)->buffsize = size)
#define luaZ_freebuffer(L, buff)	luaZ_resizebuffer(L, buff, 0)
LUAI_FUNC void luaZ_init (lua_State *L, ZIO *z, lua_Reader reader,
                                        void *data);
LUAI_FUNC size_t luaZ_read (ZIO* z, void *b, size_t n);	
struct Zio {
  size_t n;			
  const char *p;		
  lua_Reader reader;		
  void *data;			
  lua_State *L;			
};
LUAI_FUNC int luaZ_fill (ZIO *z);
#endif
#ifndef lobject_h
#define lobject_h
#include <stdarg.h>
#define LUA_TPROTO	LUA_NUMTAGS		
#define LUA_TDEADKEY	(LUA_NUMTAGS+1)		
#define LUA_TOTALTAGS	(LUA_TPROTO + 2)
#define LUA_TLCL	(LUA_TFUNCTION | (0 << 4))  
#define LUA_TLCF	(LUA_TFUNCTION | (1 << 4))  
#define LUA_TCCL	(LUA_TFUNCTION | (2 << 4))  
#define LUA_TSHRSTR	(LUA_TSTRING | (0 << 4))  
#define LUA_TLNGSTR	(LUA_TSTRING | (1 << 4))  
#define LUA_TNUMFLT	(LUA_TNUMBER | (0 << 4))  
#define LUA_TNUMINT	(LUA_TNUMBER | (1 << 4))  
#define BIT_ISCOLLECTABLE	(1 << 6)
#define ctb(t)			((t) | BIT_ISCOLLECTABLE)
typedef struct GCObject GCObject;
#define CommonHeader	GCObject *next; lu_byte tt; lu_byte marked
struct GCObject {
  CommonHeader;
};
typedef union Value {
  GCObject *gc;    
  void *p;         
  int b;           
  lua_CFunction f; 
  lua_Integer i;   
  lua_Number n;    
} Value;
#define TValuefields	Value value_; int tt_
typedef struct lua_TValue {
  TValuefields;
} TValue;
#define NILCONSTANT	{NULL}, LUA_TNIL
#define val_(o)		((o)->value_)
#define rttype(o)	((o)->tt_)
#define novariant(x)	((x) & 0x0F)
#define ttype(o)	(rttype(o) & 0x3F)
#define ttnov(o)	(novariant(rttype(o)))
#define checktag(o,t)		(rttype(o) == (t))
#define checktype(o,t)		(ttnov(o) == (t))
#define ttisnumber(o)		checktype((o), LUA_TNUMBER)
#define ttisfloat(o)		checktag((o), LUA_TNUMFLT)
#define ttisinteger(o)		checktag((o), LUA_TNUMINT)
#define ttisnil(o)		checktag((o), LUA_TNIL)
#define ttisboolean(o)		checktag((o), LUA_TBOOLEAN)
#define ttislightuserdata(o)	checktag((o), LUA_TLIGHTUSERDATA)
#define ttisstring(o)		checktype((o), LUA_TSTRING)
#define ttisshrstring(o)	checktag((o), ctb(LUA_TSHRSTR))
#define ttislngstring(o)	checktag((o), ctb(LUA_TLNGSTR))
#define ttistable(o)		checktag((o), ctb(LUA_TTABLE))
#define ttisfunction(o)		checktype(o, LUA_TFUNCTION)
#define ttisclosure(o)		((rttype(o) & 0x1F) == LUA_TFUNCTION)
#define ttisCclosure(o)		checktag((o), ctb(LUA_TCCL))
#define ttisLclosure(o)		checktag((o), ctb(LUA_TLCL))
#define ttislcf(o)		checktag((o), LUA_TLCF)
#define ttisfulluserdata(o)	checktag((o), ctb(LUA_TUSERDATA))
#define ttisthread(o)		checktag((o), ctb(LUA_TTHREAD))
#define ttisdeadkey(o)		checktag((o), LUA_TDEADKEY)
#define ivalue(o)	check_exp(ttisinteger(o), val_(o).i)
#define fltvalue(o)	check_exp(ttisfloat(o), val_(o).n)
#define nvalue(o)	check_exp(ttisnumber(o), \
	(ttisinteger(o) ? cast_num(ivalue(o)) : fltvalue(o)))
#define gcvalue(o)	check_exp(iscollectable(o), val_(o).gc)
#define pvalue(o)	check_exp(ttislightuserdata(o), val_(o).p)
#define tsvalue(o)	check_exp(ttisstring(o), gco2ts(val_(o).gc))
#define uvalue(o)	check_exp(ttisfulluserdata(o), gco2u(val_(o).gc))
#define clvalue(o)	check_exp(ttisclosure(o), gco2cl(val_(o).gc))
#define clLvalue(o)	check_exp(ttisLclosure(o), gco2lcl(val_(o).gc))
#define clCvalue(o)	check_exp(ttisCclosure(o), gco2ccl(val_(o).gc))
#define fvalue(o)	check_exp(ttislcf(o), val_(o).f)
#define hvalue(o)	check_exp(ttistable(o), gco2t(val_(o).gc))
#define bvalue(o)	check_exp(ttisboolean(o), val_(o).b)
#define thvalue(o)	check_exp(ttisthread(o), gco2th(val_(o).gc))
#define deadvalue(o)	check_exp(ttisdeadkey(o), cast(void *, val_(o).gc))
#define l_isfalse(o)	(ttisnil(o) || (ttisboolean(o) && bvalue(o) == 0))
#define iscollectable(o)	(rttype(o) & BIT_ISCOLLECTABLE)
#define righttt(obj)		(ttype(obj) == gcvalue(obj)->tt)
#define checkliveness(L,obj) \
	lua_longassert(!iscollectable(obj) || \
		(righttt(obj) && (L == NULL || !isdead(G(L),gcvalue(obj)))))
#define settt_(o,t)	((o)->tt_=(t))
#define setfltvalue(obj,x) \
  { TValue *io=(obj); val_(io).n=(x); settt_(io, LUA_TNUMFLT); }
#define chgfltvalue(obj,x) \
  { TValue *io=(obj); lua_assert(ttisfloat(io)); val_(io).n=(x); }
#define setivalue(obj,x) \
  { TValue *io=(obj); val_(io).i=(x); settt_(io, LUA_TNUMINT); }
#define chgivalue(obj,x) \
  { TValue *io=(obj); lua_assert(ttisinteger(io)); val_(io).i=(x); }
#define setnilvalue(obj) settt_(obj, LUA_TNIL)
#define setfvalue(obj,x) \
  { TValue *io=(obj); val_(io).f=(x); settt_(io, LUA_TLCF); }
#define setpvalue(obj,x) \
  { TValue *io=(obj); val_(io).p=(x); settt_(io, LUA_TLIGHTUSERDATA); }
#define setbvalue(obj,x) \
  { TValue *io=(obj); val_(io).b=(x); settt_(io, LUA_TBOOLEAN); }
#define setgcovalue(L,obj,x) \
  { TValue *io = (obj); GCObject *i_g=(x); \
    val_(io).gc = i_g; settt_(io, ctb(i_g->tt)); }
#define setsvalue(L,obj,x) \
  { TValue *io = (obj); TString *x_ = (x); \
    val_(io).gc = obj2gco(x_); settt_(io, ctb(x_->tt)); \
    checkliveness(L,io); }
#define setuvalue(L,obj,x) \
  { TValue *io = (obj); Udata *x_ = (x); \
    val_(io).gc = obj2gco(x_); settt_(io, ctb(LUA_TUSERDATA)); \
    checkliveness(L,io); }
#define setthvalue(L,obj,x) \
  { TValue *io = (obj); lua_State *x_ = (x); \
    val_(io).gc = obj2gco(x_); settt_(io, ctb(LUA_TTHREAD)); \
    checkliveness(L,io); }
#define setclLvalue(L,obj,x) \
  { TValue *io = (obj); LClosure *x_ = (x); \
    val_(io).gc = obj2gco(x_); settt_(io, ctb(LUA_TLCL)); \
    checkliveness(L,io); }
#define setclCvalue(L,obj,x) \
  { TValue *io = (obj); CClosure *x_ = (x); \
    val_(io).gc = obj2gco(x_); settt_(io, ctb(LUA_TCCL)); \
    checkliveness(L,io); }
#define sethvalue(L,obj,x) \
  { TValue *io = (obj); Table *x_ = (x); \
    val_(io).gc = obj2gco(x_); settt_(io, ctb(LUA_TTABLE)); \
    checkliveness(L,io); }
#define setdeadvalue(obj)	settt_(obj, LUA_TDEADKEY)
#define setobj(L,obj1,obj2) \
	{ TValue *io1=(obj1); *io1 = *(obj2); \
	  (void)L; checkliveness(L,io1); }
#define setobjs2s	setobj
#define setobj2s	setobj
#define setsvalue2s	setsvalue
#define sethvalue2s	sethvalue
#define setptvalue2s	setptvalue
#define setobjt2t	setobj
#define setobj2n	setobj
#define setsvalue2n	setsvalue
#define setobj2t(L,o1,o2)  ((void)L, *(o1)=*(o2), checkliveness(L,(o1)))
typedef TValue *StkId;  
typedef struct TString {
  CommonHeader;
  lu_byte extra;  
  lu_byte shrlen;  
  unsigned int hash;
  union {
    size_t lnglen;  
    struct TString *hnext;  
  } u;
} TString;
typedef union UTString {
  L_Umaxalign dummy;  
  TString tsv;
} UTString;
#define getstr(ts)  \
  check_exp(sizeof((ts)->extra), cast(char *, (ts)) + sizeof(UTString))
#define svalue(o)       getstr(tsvalue(o))
#define tsslen(s)	((s)->tt == LUA_TSHRSTR ? (s)->shrlen : (s)->u.lnglen)
#define vslen(o)	tsslen(tsvalue(o))
typedef struct Udata {
  CommonHeader;
  lu_byte ttuv_;  
  struct Table *metatable;
  size_t len;  
  union Value user_;  
} Udata;
typedef union UUdata {
  L_Umaxalign dummy;  
  Udata uv;
} UUdata;
#define getudatamem(u)  \
  check_exp(sizeof((u)->ttuv_), (cast(char*, (u)) + sizeof(UUdata)))
#define setuservalue(L,u,o) \
	{ const TValue *io=(o); Udata *iu = (u); \
	  iu->user_ = io->value_; iu->ttuv_ = rttype(io); \
	  checkliveness(L,io); }
#define getuservalue(L,u,o) \
	{ TValue *io=(o); const Udata *iu = (u); \
	  io->value_ = iu->user_; settt_(io, iu->ttuv_); \
	  checkliveness(L,io); }
typedef struct Upvaldesc {
  TString *name;  
  lu_byte instack;  
  lu_byte idx;  
} Upvaldesc;
typedef struct LocVar {
  TString *varname;
  int startpc;  
  int endpc;    
} LocVar;
typedef struct Proto {
  CommonHeader;
  lu_byte numparams;  
  lu_byte is_vararg;
  lu_byte maxstacksize;  
  int sizeupvalues;  
  int sizek;  
  int sizecode;
  int sizelineinfo;
  int sizep;  
  int sizelocvars;
  int linedefined;  
  int lastlinedefined;  
  TValue *k;  
  Instruction *code;  
  struct Proto **p;  
  int *lineinfo;  
  LocVar *locvars;  
  Upvaldesc *upvalues;  
  struct LClosure *cache;  
  TString  *source;  
  GCObject *gclist;
} Proto;
typedef struct UpVal UpVal;
#define ClosureHeader \
	CommonHeader; lu_byte nupvalues; GCObject *gclist
typedef struct CClosure {
  ClosureHeader;
  lua_CFunction f;
  TValue upvalue[1];  
} CClosure;
typedef struct LClosure {
  ClosureHeader;
  struct Proto *p;
  UpVal *upvals[1];  
} LClosure;
typedef union Closure {
  CClosure c;
  LClosure l;
} Closure;
#define isLfunction(o)	ttisLclosure(o)
#define getproto(o)	(clLvalue(o)->p)
typedef union TKey {
  struct {
    TValuefields;
    int next;  
  } nk;
  TValue tvk;
} TKey;
#define setnodekey(L,key,obj) \
	{ TKey *k_=(key); const TValue *io_=(obj); \
	  k_->nk.value_ = io_->value_; k_->nk.tt_ = io_->tt_; \
	  (void)L; checkliveness(L,io_); }
typedef struct Node {
  TValue i_val;
  TKey i_key;
} Node;
typedef struct Table {
  CommonHeader;
  lu_byte flags;  
  lu_byte lsizenode;  
  unsigned int sizearray;  
  TValue *array;  
  Node *node;
  Node *lastfree;  
  struct Table *metatable;
  GCObject *gclist;
} Table;
#define lmod(s,size) \
	(check_exp((size&(size-1))==0, (cast(int, (s) & ((size)-1)))))
#define twoto(x)	(1<<(x))
#define sizenode(t)	(twoto((t)->lsizenode))
#define luaO_nilobject		(&luaO_nilobject_)
LUAI_DDEC const TValue luaO_nilobject_;
#define UTF8BUFFSZ	8
LUAI_FUNC int luaO_int2fb (unsigned int x);
LUAI_FUNC int luaO_fb2int (int x);
LUAI_FUNC int luaO_utf8esc (char *buff, unsigned long x);
LUAI_FUNC int luaO_ceillog2 (unsigned int x);
LUAI_FUNC void luaO_arith (lua_State *L, int op, const TValue *p1,
                           const TValue *p2, TValue *res);
LUAI_FUNC size_t luaO_str2num (const char *s, TValue *o);
LUAI_FUNC int luaO_hexavalue (int c);
LUAI_FUNC void luaO_tostring (lua_State *L, StkId obj);
LUAI_FUNC const char *luaO_pushvfstring (lua_State *L, const char *fmt,
                                                       va_list argp);
LUAI_FUNC const char *luaO_pushfstring (lua_State *L, const char *fmt, ...);
LUAI_FUNC void luaO_chunkid (char *out, const char *source, size_t len);
#endif
#ifndef lparser_h
#define lparser_h
typedef enum {
  VVOID,  
  VNIL,  
  VTRUE,  
  VFALSE,  
  VK,  
  VKFLT,  
  VKINT,  
  VNONRELOC,  
  VLOCAL,  
  VUPVAL,  
  VINDEXED,  
  VJMP,  
  VRELOCABLE,  
  VCALL,  
  VVARARG  
} expkind;
#define vkisvar(k)	(VLOCAL <= (k) && (k) <= VINDEXED)
#define vkisinreg(k)	((k) == VNONRELOC || (k) == VLOCAL)
typedef struct expdesc {
  expkind k;
  union {
    lua_Integer ival;    
    lua_Number nval;  
    int info;  
    struct {  
      short idx;  
      lu_byte t;  
      lu_byte vt;  
    } ind;
  } u;
  int t;  
  int f;  
} expdesc;
typedef struct Vardesc {
  short idx;  
} Vardesc;
typedef struct Labeldesc {
  TString *name;  
  int pc;  
  int line;  
  lu_byte nactvar;  
} Labeldesc;
typedef struct Labellist {
  Labeldesc *arr;  
  int n;  
  int size;  
} Labellist;
typedef struct Dyndata {
  struct {  
    Vardesc *arr;
    int n;
    int size;
  } actvar;
  Labellist gt;  
  Labellist label;   
} Dyndata;
struct BlockCnt;  
typedef struct FuncState {
  Proto *f;  
  struct FuncState *prev;  
  struct LexState *ls;  
  struct BlockCnt *bl;  
  int pc;  
  int lasttarget;   
  int jpc;  
  int nk;  
  int np;  
  int firstlocal;  
  short nlocvars;  
  lu_byte nactvar;  
  lu_byte nups;  
  lu_byte freereg;  
} FuncState;
LUAI_FUNC LClosure *luaY_parser (lua_State *L, ZIO *z, Mbuffer *buff,
                                 Dyndata *dyd, const char *name, int firstchar);
#endif
#ifndef llex_h
#define llex_h
#define FIRST_RESERVED	257
#if !defined(LUA_ENV)
#define LUA_ENV		"_ENV"
#endif
enum RESERVED {
  TK_AND = FIRST_RESERVED, TK_BREAK,
  TK_DO, TK_ELSE, TK_ELSEIF, TK_END, TK_FALSE, TK_FOR, TK_FUNCTION,
  TK_GOTO, TK_IF, TK_IN, TK_LOCAL, TK_DEFER, TK_NIL, TK_NOT, TK_OR, TK_REPEAT,
  TK_RETURN, TK_THEN, TK_TRUE, TK_UNTIL, TK_WHILE,
  TK_IDIV, TK_CONCAT, TK_DOTS, TK_EQ, TK_GE, TK_LE, TK_NE,
  TK_SHL, TK_SHR,
  TK_DBCOLON, TK_EOS,
  TK_FLT, TK_INT, TK_NAME, TK_STRING
};
#define NUM_RESERVED	(cast(int, TK_WHILE-FIRST_RESERVED+1))
typedef union {
  lua_Number r;
  lua_Integer i;
  TString *ts;
} SemInfo;  
typedef struct Token {
  int token;
  SemInfo seminfo;
} Token;
typedef struct LexState {
  int current;  
  int linenumber;  
  int lastline;  
  Token t;  
  Token lookahead;  
  struct FuncState *fs;  
  struct lua_State *L;
  ZIO *z;  
  Mbuffer *buff;  
  Table *h;  
  struct Dyndata *dyd;  
  TString *source;  
  TString *envn;  
} LexState;
LUAI_FUNC void luaX_init (lua_State *L);
LUAI_FUNC void luaX_setinput (lua_State *L, LexState *ls, ZIO *z,
                              TString *source, int firstchar);
LUAI_FUNC TString *luaX_newstring (LexState *ls, const char *str, size_t l);
LUAI_FUNC void luaX_next (LexState *ls);
LUAI_FUNC int luaX_lookahead (LexState *ls);
LUAI_FUNC l_noret luaX_syntaxerror (LexState *ls, const char *s);
LUAI_FUNC const char *luaX_token2str (LexState *ls, int token);
#endif
#ifndef ltm_h
#define ltm_h
typedef enum {
  TM_INDEX,
  TM_NEWINDEX,
  TM_GC,
  TM_MODE,
  TM_LEN,
  TM_EQ,  
  TM_ADD,
  TM_SUB,
  TM_MUL,
  TM_MOD,
  TM_POW,
  TM_DIV,
  TM_IDIV,
  TM_BAND,
  TM_BOR,
  TM_BXOR,
  TM_SHL,
  TM_SHR,
  TM_UNM,
  TM_BNOT,
  TM_LT,
  TM_LE,
  TM_CONCAT,
  TM_CALL,
  TM_N		
} TMS;
#define gfasttm(g,et,e) ((et) == NULL ? NULL : \
  ((et)->flags & (1u<<(e))) ? NULL : luaT_gettm(et, e, (g)->tmname[e]))
#define fasttm(l,et,e)	gfasttm(G(l), et, e)
#define ttypename(x)	luaT_typenames_[(x) + 1]
LUAI_DDEC const char *const luaT_typenames_[LUA_TOTALTAGS];
LUAI_FUNC const char *luaT_objtypename (lua_State *L, const TValue *o);
LUAI_FUNC const TValue *luaT_gettm (Table *events, TMS event, TString *ename);
LUAI_FUNC const TValue *luaT_gettmbyobj (lua_State *L, const TValue *o,
                                                       TMS event);
LUAI_FUNC void luaT_init (lua_State *L);
LUAI_FUNC void luaT_callTM (lua_State *L, const TValue *f, const TValue *p1,
                            const TValue *p2, TValue *p3, int hasres);
LUAI_FUNC int luaT_callbinTM (lua_State *L, const TValue *p1, const TValue *p2,
                              StkId res, TMS event);
LUAI_FUNC void luaT_trybinTM (lua_State *L, const TValue *p1, const TValue *p2,
                              StkId res, TMS event);
LUAI_FUNC int luaT_callorderTM (lua_State *L, const TValue *p1,
                                const TValue *p2, TMS event);
#endif
#ifndef lstate_h
#define lstate_h
struct lua_longjmp;  
#if !defined(l_signalT)
#include <signal.h>
#define l_signalT	sig_atomic_t
#endif
#define EXTRA_STACK   5
#define BASIC_STACK_SIZE        (2*LUA_MINSTACK)
#define KGC_NORMAL	0
#define KGC_EMERGENCY	1	
typedef struct stringtable {
  TString **hash;
  int nuse;  
  int size;
} stringtable;
typedef struct CallInfo {
  StkId func;  
  StkId	top;  
  struct CallInfo *previous, *next;  
  union {
    struct {  
      StkId base;  
      const Instruction *savedpc;
    } l;
    struct {  
      lua_KFunction k;  
      ptrdiff_t old_errfunc;
      lua_KContext ctx;  
    } c;
  } u;
  ptrdiff_t extra;
  short nresults;  
  unsigned short callstatus;
} CallInfo;
#define CIST_OAH	(1<<0)	
#define CIST_LUA	(1<<1)	
#define CIST_HOOKED	(1<<2)	
#define CIST_FRESH	(1<<3)	
#define CIST_YPCALL	(1<<4)	
#define CIST_TAIL	(1<<5)	
#define CIST_HOOKYIELD	(1<<6)	
#define CIST_LEQ	(1<<7)  
#define CIST_FIN	(1<<8)  
#define isLua(ci)	((ci)->callstatus & CIST_LUA)
#define setoah(st,v)	((st) = ((st) & ~CIST_OAH) | (v))
#define getoah(st)	((st) & CIST_OAH)
typedef struct global_State {
  lua_Alloc frealloc;  
  void *ud;         
  l_mem totalbytes;  
  l_mem GCdebt;  
  lu_mem GCmemtrav;  
  lu_mem GCestimate;  
  stringtable strt;  
  TValue l_registry;
  unsigned int seed;  
  lu_byte currentwhite;
  lu_byte gcstate;  
  lu_byte gckind;  
  lu_byte gcrunning;  
  GCObject *allgc;  
  GCObject **sweepgc;  
  GCObject *finobj;  
  GCObject *gray;  
  GCObject *grayagain;  
  GCObject *weak;  
  GCObject *ephemeron;  
  GCObject *allweak;  
  GCObject *tobefnz;  
  GCObject *fixedgc;  
  struct lua_State *twups;  
  unsigned int gcfinnum;  
  int gcpause;  
  int gcstepmul;  
  lua_CFunction panic;  
  struct lua_State *mainthread;
  const lua_Number *version;  
  TString *memerrmsg;  
  TString *tmname[TM_N];  
  struct Table *mt[LUA_NUMTAGS];  
  TString *strcache[STRCACHE_N][STRCACHE_M];  
} global_State;
struct lua_State {
  CommonHeader;
  unsigned short nci;  
  lu_byte status;
  StkId top;  
  global_State *l_G;
  CallInfo *ci;  
  const Instruction *oldpc;  
  StkId stack_last;  
  StkId stack;  
  UpVal *openupval;  
  GCObject *gclist;
  struct lua_State *twups;  
  struct lua_longjmp *errorJmp;  
  CallInfo base_ci;  
  volatile lua_Hook hook;
  ptrdiff_t errfunc;  
  int stacksize;
  int basehookcount;
  int hookcount;
  unsigned short nny;  
  unsigned short nCcalls;  
  l_signalT hookmask;
  lu_byte allowhook;
};
#define G(L)	(L->l_G)
union GCUnion {
  GCObject gc;  
  struct TString ts;
  struct Udata u;
  union Closure cl;
  struct Table h;
  struct Proto p;
  struct lua_State th;  
};
#define cast_u(o)	cast(union GCUnion *, (o))
#define gco2ts(o)  \
	check_exp(novariant((o)->tt) == LUA_TSTRING, &((cast_u(o))->ts))
#define gco2u(o)  check_exp((o)->tt == LUA_TUSERDATA, &((cast_u(o))->u))
#define gco2lcl(o)  check_exp((o)->tt == LUA_TLCL, &((cast_u(o))->cl.l))
#define gco2ccl(o)  check_exp((o)->tt == LUA_TCCL, &((cast_u(o))->cl.c))
#define gco2cl(o)  \
	check_exp(novariant((o)->tt) == LUA_TFUNCTION, &((cast_u(o))->cl))
#define gco2t(o)  check_exp((o)->tt == LUA_TTABLE, &((cast_u(o))->h))
#define gco2p(o)  check_exp((o)->tt == LUA_TPROTO, &((cast_u(o))->p))
#define gco2th(o)  check_exp((o)->tt == LUA_TTHREAD, &((cast_u(o))->th))
#define obj2gco(v) \
	check_exp(novariant((v)->tt) < LUA_TDEADKEY, (&(cast_u(v)->gc)))
#define gettotalbytes(g)	cast(lu_mem, (g)->totalbytes + (g)->GCdebt)
LUAI_FUNC void luaE_setdebt (global_State *g, l_mem debt);
LUAI_FUNC void luaE_freethread (lua_State *L, lua_State *L1);
LUAI_FUNC CallInfo *luaE_extendCI (lua_State *L);
LUAI_FUNC void luaE_freeCI (lua_State *L);
LUAI_FUNC void luaE_shrinkCI (lua_State *L);
#endif
#ifndef ldebug_h
#define ldebug_h
#define pcRel(pc, p)	(cast(int, (pc) - (p)->code) - 1)
#define getfuncline(f,pc)	(((f)->lineinfo) ? (f)->lineinfo[pc] : -1)
#define resethookcount(L)	(L->hookcount = L->basehookcount)
LUAI_FUNC l_noret luaG_typeerror (lua_State *L, const TValue *o,
                                                const char *opname);
LUAI_FUNC l_noret luaG_concaterror (lua_State *L, const TValue *p1,
                                                  const TValue *p2);
LUAI_FUNC l_noret luaG_opinterror (lua_State *L, const TValue *p1,
                                                 const TValue *p2,
                                                 const char *msg);
LUAI_FUNC l_noret luaG_tointerror (lua_State *L, const TValue *p1,
                                                 const TValue *p2);
LUAI_FUNC l_noret luaG_ordererror (lua_State *L, const TValue *p1,
                                                 const TValue *p2);
LUAI_FUNC l_noret luaG_runerror (lua_State *L, const char *fmt, ...);
LUAI_FUNC const char *luaG_addinfo (lua_State *L, const char *msg,
                                                  TString *src, int line);
LUAI_FUNC l_noret luaG_errormsg (lua_State *L);
LUAI_FUNC void luaG_traceexec (lua_State *L);
#endif
#ifndef lapi_h
#define lapi_h
#define api_incr_top(L)   {L->top++; api_check(L, L->top <= L->ci->top, \
				"stack overflow");}
#define adjustresults(L,nres) \
    { if ((nres) == LUA_MULTRET && L->ci->top < L->top) L->ci->top = L->top; }
#define api_checknelems(L,n)	api_check(L, (n) < (L->top - L->ci->func), \
				  "not enough elements in the stack")
#endif
#ifndef lcode_h
#define lcode_h
#define NO_JUMP (-1)
typedef enum BinOpr {
  OPR_ADD, OPR_SUB, OPR_MUL, OPR_MOD, OPR_POW,
  OPR_DIV,
  OPR_IDIV,
  OPR_BAND, OPR_BOR, OPR_BXOR,
  OPR_SHL, OPR_SHR,
  OPR_CONCAT,
  OPR_EQ, OPR_LT, OPR_LE,
  OPR_NE, OPR_GT, OPR_GE,
  OPR_AND, OPR_OR,
  OPR_NOBINOPR
} BinOpr;
typedef enum UnOpr { OPR_MINUS, OPR_BNOT, OPR_NOT, OPR_LEN, OPR_NOUNOPR } UnOpr;
#define getinstruction(fs,e)	((fs)->f->code[(e)->u.info])
#define luaK_codeAsBx(fs,o,A,sBx)	luaK_codeABx(fs,o,A,(sBx)+MAXARG_sBx)
#define luaK_setmultret(fs,e)	luaK_setreturns(fs, e, LUA_MULTRET)
#define luaK_jumpto(fs,t)	luaK_patchlist(fs, luaK_jump(fs), t)
LUAI_FUNC int luaK_codeABx (FuncState *fs, OpCode o, int A, unsigned int Bx);
LUAI_FUNC int luaK_codeABC (FuncState *fs, OpCode o, int A, int B, int C);
LUAI_FUNC int luaK_codek (FuncState *fs, int reg, int k);
LUAI_FUNC void luaK_fixline (FuncState *fs, int line);
LUAI_FUNC void luaK_nil (FuncState *fs, int from, int n);
LUAI_FUNC void luaK_reserveregs (FuncState *fs, int n);
LUAI_FUNC void luaK_checkstack (FuncState *fs, int n);
LUAI_FUNC int luaK_stringK (FuncState *fs, TString *s);
LUAI_FUNC int luaK_intK (FuncState *fs, lua_Integer n);
LUAI_FUNC void luaK_dischargevars (FuncState *fs, expdesc *e);
LUAI_FUNC int luaK_exp2anyreg (FuncState *fs, expdesc *e);
LUAI_FUNC void luaK_exp2anyregup (FuncState *fs, expdesc *e);
LUAI_FUNC void luaK_exp2nextreg (FuncState *fs, expdesc *e);
LUAI_FUNC void luaK_exp2val (FuncState *fs, expdesc *e);
LUAI_FUNC int luaK_exp2RK (FuncState *fs, expdesc *e);
LUAI_FUNC void luaK_self (FuncState *fs, expdesc *e, expdesc *key);
LUAI_FUNC void luaK_indexed (FuncState *fs, expdesc *t, expdesc *k);
LUAI_FUNC void luaK_goiftrue (FuncState *fs, expdesc *e);
LUAI_FUNC void luaK_goiffalse (FuncState *fs, expdesc *e);
LUAI_FUNC void luaK_storevar (FuncState *fs, expdesc *var, expdesc *e);
LUAI_FUNC void luaK_setreturns (FuncState *fs, expdesc *e, int nresults);
LUAI_FUNC void luaK_setoneret (FuncState *fs, expdesc *e);
LUAI_FUNC int luaK_jump (FuncState *fs);
LUAI_FUNC void luaK_ret (FuncState *fs, int first, int nret);
LUAI_FUNC void luaK_patchlist (FuncState *fs, int list, int target);
LUAI_FUNC void luaK_patchtohere (FuncState *fs, int list);
LUAI_FUNC void luaK_patchclose (FuncState *fs, int list, int level);
LUAI_FUNC void luaK_concat (FuncState *fs, int *l1, int l2);
LUAI_FUNC int luaK_getlabel (FuncState *fs);
LUAI_FUNC void luaK_prefix (FuncState *fs, UnOpr op, expdesc *v, int line);
LUAI_FUNC void luaK_infix (FuncState *fs, BinOpr op, expdesc *v);
LUAI_FUNC void luaK_posfix (FuncState *fs, BinOpr op, expdesc *v1,
                            expdesc *v2, int line);
LUAI_FUNC void luaK_setlist (FuncState *fs, int base, int nelems, int tostore);
#endif
#ifndef ldo_h
#define ldo_h
#define luaD_checkstackaux(L,n,pre,pos)  \
	if (L->stack_last - L->top <= (n)) \
	  { pre; luaD_growstack(L, n); pos; } else { condmovestack(L,pre,pos); }
#define luaD_checkstack(L,n)	luaD_checkstackaux(L,n,(void)0,(void)0)
#define savestack(L,p)		((char *)(p) - (char *)L->stack)
#define restorestack(L,n)	((TValue *)((char *)L->stack + (n)))
typedef void (*Pfunc) (lua_State *L, void *ud);
LUAI_FUNC int luaD_protectedparser (lua_State *L, ZIO *z, const char *name,
                                                  const char *mode);
LUAI_FUNC void luaD_hook (lua_State *L, int event, int line);
LUAI_FUNC int luaD_precall (lua_State *L, StkId func, int nresults);
LUAI_FUNC void luaD_call (lua_State *L, StkId func, int nResults);
LUAI_FUNC void luaD_callnoyield (lua_State *L, StkId func, int nResults);
LUAI_FUNC int luaD_pcall (lua_State *L, Pfunc func, void *u,
                                        ptrdiff_t oldtop, ptrdiff_t ef);
LUAI_FUNC int luaD_poscall (lua_State *L, CallInfo *ci, StkId firstResult,
                                          int nres);
LUAI_FUNC void luaD_reallocstack (lua_State *L, int newsize);
LUAI_FUNC void luaD_growstack (lua_State *L, int n);
LUAI_FUNC void luaD_shrinkstack (lua_State *L);
LUAI_FUNC void luaD_inctop (lua_State *L);
LUAI_FUNC l_noret luaD_throw (lua_State *L, int errcode);
LUAI_FUNC int luaD_rawrunprotected (lua_State *L, Pfunc f, void *ud);
LUAI_FUNC void luaD_seterrorobj (lua_State *L, int errcode, StkId oldtop);
#endif
#ifndef lfunc_h
#define lfunc_h
#define sizeCclosure(n)	(cast(int, sizeof(CClosure)) + \
                         cast(int, sizeof(TValue)*((n)-1)))
#define sizeLclosure(n)	(cast(int, sizeof(LClosure)) + \
                         cast(int, sizeof(TValue *)*((n)-1)))
#define isintwups(L)	(L->twups != L)
#define MAXUPVAL	255
struct UpVal {
  TValue *v;  
  unsigned int refcount;  
  unsigned int flags; 
  union {
    struct {  
      UpVal *next;  
      int touched;  
    } open;
    TValue value;  
  } u;
};
#define upisopen(up)	((up)->v != &(up)->u.value)
#define NOCLOSINGMETH	(-1)
#define CLOSEPROTECT	(-2)
LUAI_FUNC Proto *luaF_newproto (lua_State *L);
LUAI_FUNC CClosure *luaF_newCclosure (lua_State *L, int nelems);
LUAI_FUNC LClosure *luaF_newLclosure (lua_State *L, int nelems);
LUAI_FUNC void luaF_initupvals (lua_State *L, LClosure *cl);
LUAI_FUNC UpVal *luaF_findupval (lua_State *L, StkId level);
LUAI_FUNC int luaF_close (lua_State *L, StkId level, int status);
LUAI_FUNC void luaF_freeproto (lua_State *L, Proto *f);
LUAI_FUNC const char *luaF_getlocalname (const Proto *func, int local_number,
                                         int pc);
#endif
#ifndef lgc_h
#define lgc_h
#if !defined(GCSTEPSIZE)
#define GCSTEPSIZE	(cast_int(100 * sizeof(TString)))
#endif
#define GCSpropagate	0
#define GCSatomic	1
#define GCSswpallgc	2
#define GCSswpfinobj	3
#define GCSswptobefnz	4
#define GCSswpend	5
#define GCScallfin	6
#define GCSpause	7
#define issweepphase(g)  \
	(GCSswpallgc <= (g)->gcstate && (g)->gcstate <= GCSswpend)
#define keepinvariant(g)	((g)->gcstate <= GCSatomic)
#define resetbits(x,m)		((x) &= cast(lu_byte, ~(m)))
#define setbits(x,m)		((x) |= (m))
#define testbits(x,m)		((x) & (m))
#define bitmask(b)		(1<<(b))
#define bit2mask(b1,b2)		(bitmask(b1) | bitmask(b2))
#define l_setbit(x,b)		setbits(x, bitmask(b))
#define resetbit(x,b)		resetbits(x, bitmask(b))
#define testbit(x,b)		testbits(x, bitmask(b))
#define WHITE0BIT	0  
#define WHITE1BIT	1  
#define BLACKBIT	2  
#define FINALIZEDBIT	3  
#define WHITEBITS	bit2mask(WHITE0BIT, WHITE1BIT)
#define iswhite(x)      testbits((x)->marked, WHITEBITS)
#define isblack(x)      testbit((x)->marked, BLACKBIT)
#define isgray(x)    \
	(!testbits((x)->marked, WHITEBITS | bitmask(BLACKBIT)))
#define tofinalize(x)	testbit((x)->marked, FINALIZEDBIT)
#define otherwhite(g)	((g)->currentwhite ^ WHITEBITS)
#define isdeadm(ow,m)	(!(((m) ^ WHITEBITS) & (ow)))
#define isdead(g,v)	isdeadm(otherwhite(g), (v)->marked)
#define changewhite(x)	((x)->marked ^= WHITEBITS)
#define gray2black(x)	l_setbit((x)->marked, BLACKBIT)
#define luaC_white(g)	cast(lu_byte, (g)->currentwhite & WHITEBITS)
#define luaC_condGC(L,pre,pos) \
	{ if (G(L)->GCdebt > 0) { pre; luaC_step(L); pos;}; \
	  condchangemem(L,pre,pos); }
#define luaC_checkGC(L)		luaC_condGC(L,(void)0,(void)0)
#define luaC_barrier(L,p,v) (  \
	(iscollectable(v) && isblack(p) && iswhite(gcvalue(v))) ?  \
	luaC_barrier_(L,obj2gco(p),gcvalue(v)) : cast_void(0))
#define luaC_barrierback(L,p,v) (  \
	(iscollectable(v) && isblack(p) && iswhite(gcvalue(v))) ? \
	luaC_barrierback_(L,p) : cast_void(0))
#define luaC_objbarrier(L,p,o) (  \
	(isblack(p) && iswhite(o)) ? \
	luaC_barrier_(L,obj2gco(p),obj2gco(o)) : cast_void(0))
#define luaC_upvalbarrier(L,uv) ( \
	(iscollectable((uv)->v) && !upisopen(uv)) ? \
         luaC_upvalbarrier_(L,uv) : cast_void(0))
LUAI_FUNC void luaC_fix (lua_State *L, GCObject *o);
LUAI_FUNC void luaC_freeallobjects (lua_State *L);
LUAI_FUNC void luaC_step (lua_State *L);
LUAI_FUNC void luaC_runtilstate (lua_State *L, int statesmask);
LUAI_FUNC void luaC_fullgc (lua_State *L, int isemergency);
LUAI_FUNC GCObject *luaC_newobj (lua_State *L, int tt, size_t sz);
LUAI_FUNC void luaC_barrier_ (lua_State *L, GCObject *o, GCObject *v);
LUAI_FUNC void luaC_barrierback_ (lua_State *L, Table *o);
LUAI_FUNC void luaC_upvalbarrier_ (lua_State *L, UpVal *uv);
LUAI_FUNC void luaC_checkfinalizer (lua_State *L, GCObject *o, Table *mt);
LUAI_FUNC void luaC_upvdeccount (lua_State *L, UpVal *uv);
#endif
#ifndef lstring_h
#define lstring_h
#define sizelstring(l)  (sizeof(union UTString) + ((l) + 1) * sizeof(char))
#define sizeludata(l)	(sizeof(union UUdata) + (l))
#define sizeudata(u)	sizeludata((u)->len)
#define luaS_newliteral(L, s)	(luaS_newlstr(L, "" s, \
                                 (sizeof(s)/sizeof(char))-1))
#define isreserved(s)	((s)->tt == LUA_TSHRSTR && (s)->extra > 0)
#define eqshrstr(a,b)	check_exp((a)->tt == LUA_TSHRSTR, (a) == (b))
LUAI_FUNC unsigned int luaS_hash (const char *str, size_t l, unsigned int seed);
LUAI_FUNC unsigned int luaS_hashlongstr (TString *ts);
LUAI_FUNC int luaS_eqlngstr (TString *a, TString *b);
LUAI_FUNC void luaS_resize (lua_State *L, int newsize);
LUAI_FUNC void luaS_clearcache (global_State *g);
LUAI_FUNC void luaS_init (lua_State *L);
LUAI_FUNC void luaS_remove (lua_State *L, TString *ts);
LUAI_FUNC Udata *luaS_newudata (lua_State *L, size_t s);
LUAI_FUNC TString *luaS_newlstr (lua_State *L, const char *str, size_t l);
LUAI_FUNC TString *luaS_new (lua_State *L, const char *str);
LUAI_FUNC TString *luaS_createlngstrobj (lua_State *L, size_t l);
#endif
#ifndef ltable_h
#define ltable_h
#define gnode(t,i)	(&(t)->node[i])
#define gval(n)		(&(n)->i_val)
#define gnext(n)	((n)->i_key.nk.next)
#define gkey(n)		cast(const TValue*, (&(n)->i_key.tvk))
#define wgkey(n)		(&(n)->i_key.nk)
#define invalidateTMcache(t)	((t)->flags = 0)
#define isdummy(t)		((t)->lastfree == NULL)
#define allocsizenode(t)	(isdummy(t) ? 0 : sizenode(t))
#define keyfromval(v) \
  (gkey(cast(Node *, cast(char *, (v)) - offsetof(Node, i_val))))
LUAI_FUNC const TValue *luaH_getint (Table *t, lua_Integer key);
LUAI_FUNC void luaH_setint (lua_State *L, Table *t, lua_Integer key,
                                                    TValue *value);
LUAI_FUNC const TValue *luaH_getshortstr (Table *t, TString *key);
LUAI_FUNC const TValue *luaH_getstr (Table *t, TString *key);
LUAI_FUNC const TValue *luaH_get (Table *t, const TValue *key);
LUAI_FUNC TValue *luaH_newkey (lua_State *L, Table *t, const TValue *key);
LUAI_FUNC TValue *luaH_set (lua_State *L, Table *t, const TValue *key);
LUAI_FUNC Table *luaH_new (lua_State *L);
LUAI_FUNC void luaH_resize (lua_State *L, Table *t, unsigned int nasize,
                                                    unsigned int nhsize);
LUAI_FUNC void luaH_resizearray (lua_State *L, Table *t, unsigned int nasize);
LUAI_FUNC void luaH_free (lua_State *L, Table *t);
LUAI_FUNC int luaH_next (lua_State *L, Table *t, StkId key);
LUAI_FUNC lua_Unsigned luaH_getn (Table *t);
#if defined(LUA_DEBUG)
LUAI_FUNC Node *luaH_mainposition (const Table *t, const TValue *key);
LUAI_FUNC int luaH_isdummy (const Table *t);
#endif
#endif
#ifndef lundump_h
#define lundump_h
#define LUAC_DATA	"\x19\x93\r\n\x1a\n"
#define LUAC_INT	0x5678
#define LUAC_NUM	cast_num(370.5)
#define MYINT(s)	(s[0]-'0')
#define LUAC_VERSION	(MYINT(LUA_VERSION_MAJOR)*16+MYINT(LUA_VERSION_MINOR))
#define LUAC_FORMAT	0	
LUAI_FUNC LClosure* luaU_undump (lua_State* L, ZIO* Z, const char* name);
LUAI_FUNC int luaU_dump (lua_State* L, const Proto* f, lua_Writer w,
                         void* data, int strip);
#endif
#ifndef lvm_h
#define lvm_h
#if !defined(LUA_NOCVTN2S)
#define cvt2str(o)	ttisnumber(o)
#else
#define cvt2str(o)	0	
#endif
#if !defined(LUA_NOCVTS2N)
#define cvt2num(o)	ttisstring(o)
#else
#define cvt2num(o)	0	
#endif
#if !defined(LUA_FLOORN2I)
#define LUA_FLOORN2I		0
#endif
#define tonumber(o,n) \
	(ttisfloat(o) ? (*(n) = fltvalue(o), 1) : luaV_tonumber_(o,n))
#define tointeger(o,i) \
    (ttisinteger(o) ? (*(i) = ivalue(o), 1) : luaV_tointeger(o,i,LUA_FLOORN2I))
#define intop(op,v1,v2) l_castU2S(l_castS2U(v1) op l_castS2U(v2))
#define luaV_rawequalobj(t1,t2)		luaV_equalobj(NULL,t1,t2)
#define luaV_fastget(L,t,k,slot,f) \
  (!ttistable(t)  \
   ? (slot = NULL, 0)    \
   : (slot = f(hvalue(t), k),    \
      !ttisnil(slot)))  
#define luaV_gettable(L,t,k,v) { const TValue *slot; \
  if (luaV_fastget(L,t,k,slot,luaH_get)) { setobj2s(L, v, slot); } \
  else luaV_finishget(L,t,k,v,slot); }
#define luaV_fastset(L,t,k,slot,f,v) \
  (!ttistable(t) \
   ? (slot = NULL, 0) \
   : (slot = f(hvalue(t), k), \
     ttisnil(slot) ? 0 \
     : (luaC_barrierback(L, hvalue(t), v), \
        setobj2t(L, cast(TValue *,slot), v), \
        1)))
#define luaV_settable(L,t,k,v) { const TValue *slot; \
  if (!luaV_fastset(L,t,k,slot,luaH_get,v)) \
    luaV_finishset(L,t,k,v,slot); }
LUAI_FUNC int luaV_equalobj (lua_State *L, const TValue *t1, const TValue *t2);
LUAI_FUNC int luaV_lessthan (lua_State *L, const TValue *l, const TValue *r);
LUAI_FUNC int luaV_lessequal (lua_State *L, const TValue *l, const TValue *r);
LUAI_FUNC int luaV_tonumber_ (const TValue *obj, lua_Number *n);
LUAI_FUNC int luaV_tointeger (const TValue *obj, lua_Integer *p, int mode);
LUAI_FUNC void luaV_finishget (lua_State *L, const TValue *t, TValue *key,
                               StkId val, const TValue *slot);
LUAI_FUNC void luaV_finishset (lua_State *L, const TValue *t, TValue *key,
                               StkId val, const TValue *slot);
LUAI_FUNC void luaV_finishOp (lua_State *L);
LUAI_FUNC void luaV_execute (lua_State *L);
LUAI_FUNC void luaV_concat (lua_State *L, int total);
LUAI_FUNC lua_Integer luaV_div (lua_State *L, lua_Integer x, lua_Integer y);
LUAI_FUNC lua_Integer luaV_mod (lua_State *L, lua_Integer x, lua_Integer y);
LUAI_FUNC lua_Integer luaV_shiftl (lua_Integer x, lua_Integer y);
LUAI_FUNC void luaV_objlen (lua_State *L, StkId ra, const TValue *rb);
#endif
#define lapi_c
#define LUA_CORE
#include <stdarg.h>
#include <string.h>
const char lua_ident[] =
  "$LuaVersion: " LUA_COPYRIGHT " $"
  "$LuaAuthors: " LUA_AUTHORS " $";
#define NONVALIDVALUE		cast(TValue *, luaO_nilobject)
#define isvalid(o)	((o) != luaO_nilobject)
#define ispseudo(i)		((i) <= LUA_REGISTRYINDEX)
#define isupvalue(i)		((i) < LUA_REGISTRYINDEX)
#define isstackindex(i, o)	(isvalid(o) && !ispseudo(i))
#define api_checkvalidindex(l,o)  api_check(l, isvalid(o), "invalid index")
#define api_checkstackindex(l, i, o)  \
	api_check(l, isstackindex(i, o), "index not in the stack")
static TValue *index2addr (lua_State *L, int idx) {
  CallInfo *ci = L->ci;
  if (idx > 0) {
    TValue *o = ci->func + idx;
    api_check(L, idx <= ci->top - (ci->func + 1), "unacceptable index");
    if (o >= L->top) return NONVALIDVALUE;
    else return o;
  }
  else if (!ispseudo(idx)) {  
    api_check(L, idx != 0 && -idx <= L->top - (ci->func + 1), "invalid index");
    return L->top + idx;
  }
  else if (idx == LUA_REGISTRYINDEX)
    return &G(L)->l_registry;
  else {  
    idx = LUA_REGISTRYINDEX - idx;
    api_check(L, idx <= MAXUPVAL + 1, "upvalue index too large");
    if (ttislcf(ci->func))  
      return NONVALIDVALUE;  
    else {
      CClosure *func = clCvalue(ci->func);
      return (idx <= func->nupvalues) ? &func->upvalue[idx-1] : NONVALIDVALUE;
    }
  }
}
static void growstack (lua_State *L, void *ud) {
  int size = *(int *)ud;
  luaD_growstack(L, size);
}
LUA_API int lua_checkstack (lua_State *L, int n) {
  int res;
  CallInfo *ci = L->ci;
  lua_lock(L);
  api_check(L, n >= 0, "negative 'n'");
  if (L->stack_last - L->top > n)  
    res = 1;  
  else {  
    int inuse = cast_int(L->top - L->stack) + EXTRA_STACK;
    if (inuse > LUAI_MAXSTACK - n)  
      res = 0;  
    else  
      res = (luaD_rawrunprotected(L, &growstack, &n) == LUA_OK);
  }
  if (res && ci->top < L->top + n)
    ci->top = L->top + n;  
  lua_unlock(L);
  return res;
}
LUA_API void lua_xmove (lua_State *from, lua_State *to, int n) {
  int i;
  if (from == to) return;
  lua_lock(to);
  api_checknelems(from, n);
  api_check(from, G(from) == G(to), "moving among independent states");
  api_check(from, to->ci->top - to->top >= n, "stack overflow");
  from->top -= n;
  for (i = 0; i < n; i++) {
    setobj2s(to, to->top, from->top + i);
    to->top++;  
  }
  lua_unlock(to);
}
LUA_API lua_CFunction lua_atpanic (lua_State *L, lua_CFunction panicf) {
  lua_CFunction old;
  lua_lock(L);
  old = G(L)->panic;
  G(L)->panic = panicf;
  lua_unlock(L);
  return old;
}
LUA_API const lua_Number *lua_version (lua_State *L) {
  static const lua_Number version = LUA_VERSION_NUM;
  if (L == NULL) return &version;
  else return G(L)->version;
}
LUA_API int lua_absindex (lua_State *L, int idx) {
  return (idx > 0 || ispseudo(idx))
         ? idx
         : cast_int(L->top - L->ci->func) + idx;
}
LUA_API int lua_gettop (lua_State *L) {
  return cast_int(L->top - (L->ci->func + 1));
}
LUA_API void lua_settop (lua_State *L, int idx) {
  StkId func = L->ci->func;
  lua_lock(L);
  if (idx >= 0) {
    api_check(L, idx <= L->stack_last - (func + 1), "new top too large");
    while (L->top < (func + 1) + idx)
      setnilvalue(L->top++);
    L->top = (func + 1) + idx;
  }
  else {
    api_check(L, -(idx+1) <= (L->top - (func + 1)), "invalid new top");
    L->top += idx+1;  
  }
  lua_unlock(L);
}
static void reverse (lua_State *L, StkId from, StkId to) {
  for (; from < to; from++, to--) {
    TValue temp;
    setobj(L, &temp, from);
    setobjs2s(L, from, to);
    setobj2s(L, to, &temp);
  }
}
LUA_API void lua_rotate (lua_State *L, int idx, int n) {
  StkId p, t, m;
  lua_lock(L);
  t = L->top - 1;  
  p = index2addr(L, idx);  
  api_checkstackindex(L, idx, p);
  api_check(L, (n >= 0 ? n : -n) <= (t - p + 1), "invalid 'n'");
  m = (n >= 0 ? t - n : p - n - 1);  
  reverse(L, p, m);  
  reverse(L, m + 1, t);  
  reverse(L, p, t);  
  lua_unlock(L);
}
LUA_API void lua_copy (lua_State *L, int fromidx, int toidx) {
  TValue *fr, *to;
  lua_lock(L);
  fr = index2addr(L, fromidx);
  to = index2addr(L, toidx);
  api_checkvalidindex(L, to);
  setobj(L, to, fr);
  if (isupvalue(toidx))  
    luaC_barrier(L, clCvalue(L->ci->func), fr);
  lua_unlock(L);
}
LUA_API void lua_pushvalue (lua_State *L, int idx) {
  lua_lock(L);
  setobj2s(L, L->top, index2addr(L, idx));
  api_incr_top(L);
  lua_unlock(L);
}
LUA_API int lua_type (lua_State *L, int idx) {
  StkId o = index2addr(L, idx);
  return (isvalid(o) ? ttnov(o) : LUA_TNONE);
}
LUA_API const char *lua_typename (lua_State *L, int t) {
  UNUSED(L);
  api_check(L, LUA_TNONE <= t && t < LUA_NUMTAGS, "invalid tag");
  return ttypename(t);
}
LUA_API int lua_iscfunction (lua_State *L, int idx) {
  StkId o = index2addr(L, idx);
  return (ttislcf(o) || (ttisCclosure(o)));
}
LUA_API int lua_isinteger (lua_State *L, int idx) {
  StkId o = index2addr(L, idx);
  return ttisinteger(o);
}
LUA_API int lua_isnumber (lua_State *L, int idx) {
  lua_Number n;
  const TValue *o = index2addr(L, idx);
  return tonumber(o, &n);
}
LUA_API int lua_isstring (lua_State *L, int idx) {
  const TValue *o = index2addr(L, idx);
  return (ttisstring(o) || cvt2str(o));
}
LUA_API int lua_isuserdata (lua_State *L, int idx) {
  const TValue *o = index2addr(L, idx);
  return (ttisfulluserdata(o) || ttislightuserdata(o));
}
LUA_API int lua_rawequal (lua_State *L, int index1, int index2) {
  StkId o1 = index2addr(L, index1);
  StkId o2 = index2addr(L, index2);
  return (isvalid(o1) && isvalid(o2)) ? luaV_rawequalobj(o1, o2) : 0;
}
LUA_API void lua_arith (lua_State *L, int op) {
  lua_lock(L);
  if (op != LUA_OPUNM && op != LUA_OPBNOT)
    api_checknelems(L, 2);  
  else {  
    api_checknelems(L, 1);
    setobjs2s(L, L->top, L->top - 1);
    api_incr_top(L);
  }
  luaO_arith(L, op, L->top - 2, L->top - 1, L->top - 2);
  L->top--;  
  lua_unlock(L);
}
LUA_API int lua_compare (lua_State *L, int index1, int index2, int op) {
  StkId o1, o2;
  int i = 0;
  lua_lock(L);  
  o1 = index2addr(L, index1);
  o2 = index2addr(L, index2);
  if (isvalid(o1) && isvalid(o2)) {
    switch (op) {
      case LUA_OPEQ: i = luaV_equalobj(L, o1, o2); break;
      case LUA_OPLT: i = luaV_lessthan(L, o1, o2); break;
      case LUA_OPLE: i = luaV_lessequal(L, o1, o2); break;
      default: api_check(L, 0, "invalid option");
    }
  }
  lua_unlock(L);
  return i;
}
LUA_API size_t lua_stringtonumber (lua_State *L, const char *s) {
  size_t sz = luaO_str2num(s, L->top);
  if (sz != 0)
    api_incr_top(L);
  return sz;
}
LUA_API lua_Number lua_tonumberx (lua_State *L, int idx, int *pisnum) {
  lua_Number n;
  const TValue *o = index2addr(L, idx);
  int isnum = tonumber(o, &n);
  if (!isnum)
    n = 0;  
  if (pisnum) *pisnum = isnum;
  return n;
}
LUA_API lua_Integer lua_tointegerx (lua_State *L, int idx, int *pisnum) {
  lua_Integer res;
  const TValue *o = index2addr(L, idx);
  int isnum = tointeger(o, &res);
  if (!isnum)
    res = 0;  
  if (pisnum) *pisnum = isnum;
  return res;
}
LUA_API int lua_toboolean (lua_State *L, int idx) {
  const TValue *o = index2addr(L, idx);
  return !l_isfalse(o);
}
LUA_API const char *lua_tolstring (lua_State *L, int idx, size_t *len) {
  StkId o = index2addr(L, idx);
  if (!ttisstring(o)) {
    if (!cvt2str(o)) {  
      if (len != NULL) *len = 0;
      return NULL;
    }
    lua_lock(L);  
    luaO_tostring(L, o);
    luaC_checkGC(L);
    o = index2addr(L, idx);  
    lua_unlock(L);
  }
  if (len != NULL)
    *len = vslen(o);
  return svalue(o);
}
LUA_API size_t lua_rawlen (lua_State *L, int idx) {
  StkId o = index2addr(L, idx);
  switch (ttype(o)) {
    case LUA_TSHRSTR: return tsvalue(o)->shrlen;
    case LUA_TLNGSTR: return tsvalue(o)->u.lnglen;
    case LUA_TUSERDATA: return uvalue(o)->len;
    case LUA_TTABLE: return luaH_getn(hvalue(o));
    default: return 0;
  }
}
LUA_API lua_CFunction lua_tocfunction (lua_State *L, int idx) {
  StkId o = index2addr(L, idx);
  if (ttislcf(o)) return fvalue(o);
  else if (ttisCclosure(o))
    return clCvalue(o)->f;
  else return NULL;  
}
LUA_API void *lua_touserdata (lua_State *L, int idx) {
  StkId o = index2addr(L, idx);
  switch (ttnov(o)) {
    case LUA_TUSERDATA: return getudatamem(uvalue(o));
    case LUA_TLIGHTUSERDATA: return pvalue(o);
    default: return NULL;
  }
}
LUA_API lua_State *lua_tothread (lua_State *L, int idx) {
  StkId o = index2addr(L, idx);
  return (!ttisthread(o)) ? NULL : thvalue(o);
}
LUA_API const void *lua_topointer (lua_State *L, int idx) {
  StkId o = index2addr(L, idx);
  switch (ttype(o)) {
    case LUA_TTABLE: return hvalue(o);
    case LUA_TLCL: return clLvalue(o);
    case LUA_TCCL: return clCvalue(o);
    case LUA_TLCF: return cast(void *, cast(size_t, fvalue(o)));
    case LUA_TTHREAD: return thvalue(o);
    case LUA_TUSERDATA: return getudatamem(uvalue(o));
    case LUA_TLIGHTUSERDATA: return pvalue(o);
    default: return NULL;
  }
}
LUA_API void lua_pushnil (lua_State *L) {
  lua_lock(L);
  setnilvalue(L->top);
  api_incr_top(L);
  lua_unlock(L);
}
LUA_API void lua_pushnumber (lua_State *L, lua_Number n) {
  lua_lock(L);
  setfltvalue(L->top, n);
  api_incr_top(L);
  lua_unlock(L);
}
LUA_API void lua_pushinteger (lua_State *L, lua_Integer n) {
  lua_lock(L);
  setivalue(L->top, n);
  api_incr_top(L);
  lua_unlock(L);
}
LUA_API const char *lua_pushlstring (lua_State *L, const char *s, size_t len) {
  TString *ts;
  lua_lock(L);
  ts = (len == 0) ? luaS_new(L, "") : luaS_newlstr(L, s, len);
  setsvalue2s(L, L->top, ts);
  api_incr_top(L);
  luaC_checkGC(L);
  lua_unlock(L);
  return getstr(ts);
}
LUA_API const char *lua_pushstring (lua_State *L, const char *s) {
  lua_lock(L);
  if (s == NULL)
    setnilvalue(L->top);
  else {
    TString *ts;
    ts = luaS_new(L, s);
    setsvalue2s(L, L->top, ts);
    s = getstr(ts);  
  }
  api_incr_top(L);
  luaC_checkGC(L);
  lua_unlock(L);
  return s;
}
LUA_API const char *lua_pushvfstring (lua_State *L, const char *fmt,
                                      va_list argp) {
  const char *ret;
  lua_lock(L);
  ret = luaO_pushvfstring(L, fmt, argp);
  luaC_checkGC(L);
  lua_unlock(L);
  return ret;
}
LUA_API const char *lua_pushfstring (lua_State *L, const char *fmt, ...) {
  const char *ret;
  va_list argp;
  lua_lock(L);
  va_start(argp, fmt);
  ret = luaO_pushvfstring(L, fmt, argp);
  va_end(argp);
  luaC_checkGC(L);
  lua_unlock(L);
  return ret;
}
LUA_API void lua_pushcclosure (lua_State *L, lua_CFunction fn, int n) {
  lua_lock(L);
  if (n == 0) {
    setfvalue(L->top, fn);
    api_incr_top(L);
  }
  else {
    CClosure *cl;
    api_checknelems(L, n);
    api_check(L, n <= MAXUPVAL, "upvalue index too large");
    cl = luaF_newCclosure(L, n);
    cl->f = fn;
    L->top -= n;
    while (n--) {
      setobj2n(L, &cl->upvalue[n], L->top + n);
    }
    setclCvalue(L, L->top, cl);
    api_incr_top(L);
    luaC_checkGC(L);
  }
  lua_unlock(L);
}
LUA_API void lua_pushboolean (lua_State *L, int b) {
  lua_lock(L);
  setbvalue(L->top, (b != 0));  
  api_incr_top(L);
  lua_unlock(L);
}
LUA_API void lua_pushlightuserdata (lua_State *L, void *p) {
  lua_lock(L);
  setpvalue(L->top, p);
  api_incr_top(L);
  lua_unlock(L);
}
LUA_API int lua_pushthread (lua_State *L) {
  lua_lock(L);
  setthvalue(L, L->top, L);
  api_incr_top(L);
  lua_unlock(L);
  return (G(L)->mainthread == L);
}
static int auxgetstr (lua_State *L, const TValue *t, const char *k) {
  const TValue *slot;
  TString *str = luaS_new(L, k);
  if (luaV_fastget(L, t, str, slot, luaH_getstr)) {
    setobj2s(L, L->top, slot);
    api_incr_top(L);
  }
  else {
    setsvalue2s(L, L->top, str);
    api_incr_top(L);
    luaV_finishget(L, t, L->top - 1, L->top - 1, slot);
  }
  lua_unlock(L);
  return ttnov(L->top - 1);
}
LUA_API int lua_getglobal (lua_State *L, const char *name) {
  Table *reg = hvalue(&G(L)->l_registry);
  lua_lock(L);
  return auxgetstr(L, luaH_getint(reg, LUA_RIDX_GLOBALS), name);
}
LUA_API int lua_gettable (lua_State *L, int idx) {
  StkId t;
  lua_lock(L);
  t = index2addr(L, idx);
  luaV_gettable(L, t, L->top - 1, L->top - 1);
  lua_unlock(L);
  return ttnov(L->top - 1);
}
LUA_API int lua_getfield (lua_State *L, int idx, const char *k) {
  lua_lock(L);
  return auxgetstr(L, index2addr(L, idx), k);
}
LUA_API int lua_geti (lua_State *L, int idx, lua_Integer n) {
  StkId t;
  const TValue *slot;
  lua_lock(L);
  t = index2addr(L, idx);
  if (luaV_fastget(L, t, n, slot, luaH_getint)) {
    setobj2s(L, L->top, slot);
    api_incr_top(L);
  }
  else {
    setivalue(L->top, n);
    api_incr_top(L);
    luaV_finishget(L, t, L->top - 1, L->top - 1, slot);
  }
  lua_unlock(L);
  return ttnov(L->top - 1);
}
LUA_API int lua_rawget (lua_State *L, int idx) {
  StkId t;
  lua_lock(L);
  t = index2addr(L, idx);
  api_check(L, ttistable(t), "table expected");
  setobj2s(L, L->top - 1, luaH_get(hvalue(t), L->top - 1));
  lua_unlock(L);
  return ttnov(L->top - 1);
}
LUA_API int lua_rawgeti (lua_State *L, int idx, lua_Integer n) {
  StkId t;
  lua_lock(L);
  t = index2addr(L, idx);
  api_check(L, ttistable(t), "table expected");
  setobj2s(L, L->top, luaH_getint(hvalue(t), n));
  api_incr_top(L);
  lua_unlock(L);
  return ttnov(L->top - 1);
}
LUA_API int lua_rawgetp (lua_State *L, int idx, const void *p) {
  StkId t;
  TValue k;
  lua_lock(L);
  t = index2addr(L, idx);
  api_check(L, ttistable(t), "table expected");
  setpvalue(&k, cast(void *, p));
  setobj2s(L, L->top, luaH_get(hvalue(t), &k));
  api_incr_top(L);
  lua_unlock(L);
  return ttnov(L->top - 1);
}
LUA_API void lua_createtable (lua_State *L, int narray, int nrec) {
  Table *t;
  lua_lock(L);
  t = luaH_new(L);
  sethvalue(L, L->top, t);
  api_incr_top(L);
  if (narray > 0 || nrec > 0)
    luaH_resize(L, t, narray, nrec);
  luaC_checkGC(L);
  lua_unlock(L);
}
LUA_API int lua_getmetatable (lua_State *L, int objindex) {
  const TValue *obj;
  Table *mt;
  int res = 0;
  lua_lock(L);
  obj = index2addr(L, objindex);
  switch (ttnov(obj)) {
    case LUA_TTABLE:
      mt = hvalue(obj)->metatable;
      break;
    case LUA_TUSERDATA:
      mt = uvalue(obj)->metatable;
      break;
    default:
      mt = G(L)->mt[ttnov(obj)];
      break;
  }
  if (mt != NULL) {
    sethvalue(L, L->top, mt);
    api_incr_top(L);
    res = 1;
  }
  lua_unlock(L);
  return res;
}
LUA_API int lua_getuservalue (lua_State *L, int idx) {
  StkId o;
  lua_lock(L);
  o = index2addr(L, idx);
  api_check(L, ttisfulluserdata(o), "full userdata expected");
  getuservalue(L, uvalue(o), L->top);
  api_incr_top(L);
  lua_unlock(L);
  return ttnov(L->top - 1);
}
static void auxsetstr (lua_State *L, const TValue *t, const char *k) {
  const TValue *slot;
  TString *str = luaS_new(L, k);
  api_checknelems(L, 1);
  if (luaV_fastset(L, t, str, slot, luaH_getstr, L->top - 1))
    L->top--;  
  else {
    setsvalue2s(L, L->top, str);  
    api_incr_top(L);
    luaV_finishset(L, t, L->top - 1, L->top - 2, slot);
    L->top -= 2;  
  }
  lua_unlock(L);  
}
LUA_API void lua_setglobal (lua_State *L, const char *name) {
  Table *reg = hvalue(&G(L)->l_registry);
  lua_lock(L);  
  auxsetstr(L, luaH_getint(reg, LUA_RIDX_GLOBALS), name);
}
LUA_API void lua_settable (lua_State *L, int idx) {
  StkId t;
  lua_lock(L);
  api_checknelems(L, 2);
  t = index2addr(L, idx);
  luaV_settable(L, t, L->top - 2, L->top - 1);
  L->top -= 2;  
  lua_unlock(L);
}
LUA_API void lua_setfield (lua_State *L, int idx, const char *k) {
  lua_lock(L);  
  auxsetstr(L, index2addr(L, idx), k);
}
LUA_API void lua_seti (lua_State *L, int idx, lua_Integer n) {
  StkId t;
  const TValue *slot;
  lua_lock(L);
  api_checknelems(L, 1);
  t = index2addr(L, idx);
  if (luaV_fastset(L, t, n, slot, luaH_getint, L->top - 1))
    L->top--;  
  else {
    setivalue(L->top, n);
    api_incr_top(L);
    luaV_finishset(L, t, L->top - 1, L->top - 2, slot);
    L->top -= 2;  
  }
  lua_unlock(L);
}
LUA_API void lua_rawset (lua_State *L, int idx) {
  StkId o;
  TValue *slot;
  lua_lock(L);
  api_checknelems(L, 2);
  o = index2addr(L, idx);
  api_check(L, ttistable(o), "table expected");
  slot = luaH_set(L, hvalue(o), L->top - 2);
  setobj2t(L, slot, L->top - 1);
  invalidateTMcache(hvalue(o));
  luaC_barrierback(L, hvalue(o), L->top-1);
  L->top -= 2;
  lua_unlock(L);
}
LUA_API void lua_rawseti (lua_State *L, int idx, lua_Integer n) {
  StkId o;
  lua_lock(L);
  api_checknelems(L, 1);
  o = index2addr(L, idx);
  api_check(L, ttistable(o), "table expected");
  luaH_setint(L, hvalue(o), n, L->top - 1);
  luaC_barrierback(L, hvalue(o), L->top-1);
  L->top--;
  lua_unlock(L);
}
LUA_API void lua_rawsetp (lua_State *L, int idx, const void *p) {
  StkId o;
  TValue k, *slot;
  lua_lock(L);
  api_checknelems(L, 1);
  o = index2addr(L, idx);
  api_check(L, ttistable(o), "table expected");
  setpvalue(&k, cast(void *, p));
  slot = luaH_set(L, hvalue(o), &k);
  setobj2t(L, slot, L->top - 1);
  luaC_barrierback(L, hvalue(o), L->top - 1);
  L->top--;
  lua_unlock(L);
}
LUA_API int lua_setmetatable (lua_State *L, int objindex) {
  TValue *obj;
  Table *mt;
  lua_lock(L);
  api_checknelems(L, 1);
  obj = index2addr(L, objindex);
  if (ttisnil(L->top - 1))
    mt = NULL;
  else {
    api_check(L, ttistable(L->top - 1), "table expected");
    mt = hvalue(L->top - 1);
  }
  switch (ttnov(obj)) {
    case LUA_TTABLE: {
      hvalue(obj)->metatable = mt;
      if (mt) {
        luaC_objbarrier(L, gcvalue(obj), mt);
        luaC_checkfinalizer(L, gcvalue(obj), mt);
      }
      break;
    }
    case LUA_TUSERDATA: {
      uvalue(obj)->metatable = mt;
      if (mt) {
        luaC_objbarrier(L, uvalue(obj), mt);
        luaC_checkfinalizer(L, gcvalue(obj), mt);
      }
      break;
    }
    default: {
      G(L)->mt[ttnov(obj)] = mt;
      break;
    }
  }
  L->top--;
  lua_unlock(L);
  return 1;
}
LUA_API void lua_setuservalue (lua_State *L, int idx) {
  StkId o;
  lua_lock(L);
  api_checknelems(L, 1);
  o = index2addr(L, idx);
  api_check(L, ttisfulluserdata(o), "full userdata expected");
  setuservalue(L, uvalue(o), L->top - 1);
  luaC_barrier(L, gcvalue(o), L->top - 1);
  L->top--;
  lua_unlock(L);
}
#define checkresults(L,na,nr) \
     api_check(L, (nr) == LUA_MULTRET || (L->ci->top - L->top >= (nr) - (na)), \
	"results from function overflow current stack size")
LUA_API void lua_callk (lua_State *L, int nargs, int nresults,
                        lua_KContext ctx, lua_KFunction k) {
  StkId func;
  lua_lock(L);
  api_check(L, k == NULL || !isLua(L->ci),
    "cannot use continuations inside hooks");
  api_checknelems(L, nargs+1);
  api_check(L, L->status == LUA_OK, "cannot do calls on non-normal thread");
  checkresults(L, nargs, nresults);
  func = L->top - (nargs+1);
  if (k != NULL && L->nny == 0) {  
    L->ci->u.c.k = k;  
    L->ci->u.c.ctx = ctx;  
    luaD_call(L, func, nresults);  
  }
  else  
    luaD_callnoyield(L, func, nresults);  
  adjustresults(L, nresults);
  lua_unlock(L);
}
struct CallS {  
  StkId func;
  int nresults;
};
static void f_call (lua_State *L, void *ud) {
  struct CallS *c = cast(struct CallS *, ud);
  luaD_callnoyield(L, c->func, c->nresults);
}
LUA_API int lua_pcallk (lua_State *L, int nargs, int nresults, int errfunc,
                        lua_KContext ctx, lua_KFunction k) {
  struct CallS c;
  int status;
  ptrdiff_t func;
  lua_lock(L);
  api_check(L, k == NULL || !isLua(L->ci),
    "cannot use continuations inside hooks");
  api_checknelems(L, nargs+1);
  api_check(L, L->status == LUA_OK, "cannot do calls on non-normal thread");
  checkresults(L, nargs, nresults);
  if (errfunc == 0)
    func = 0;
  else {
    StkId o = index2addr(L, errfunc);
    api_checkstackindex(L, errfunc, o);
    func = savestack(L, o);
  }
  c.func = L->top - (nargs+1);  
  if (k == NULL || L->nny > 0) {  
    c.nresults = nresults;  
    status = luaD_pcall(L, f_call, &c, savestack(L, c.func), func);
  }
  else {  
    CallInfo *ci = L->ci;
    ci->u.c.k = k;  
    ci->u.c.ctx = ctx;  
    ci->extra = savestack(L, c.func);
    ci->u.c.old_errfunc = L->errfunc;
    L->errfunc = func;
    setoah(ci->callstatus, L->allowhook);  
    ci->callstatus |= CIST_YPCALL;  
    luaD_call(L, c.func, nresults);  
    ci->callstatus &= ~CIST_YPCALL;
    L->errfunc = ci->u.c.old_errfunc;
    status = LUA_OK;  
  }
  adjustresults(L, nresults);
  lua_unlock(L);
  return status;
}
LUA_API int lua_load (lua_State *L, lua_Reader reader, void *data,
                      const char *chunkname, const char *mode) {
  ZIO z;
  int status;
  lua_lock(L);
  if (!chunkname) chunkname = "?";
  luaZ_init(L, &z, reader, data);
  status = luaD_protectedparser(L, &z, chunkname, mode);
  if (status == LUA_OK) {  
    LClosure *f = clLvalue(L->top - 1);  
    if (f->nupvalues >= 1) {  
      Table *reg = hvalue(&G(L)->l_registry);
      const TValue *gt = luaH_getint(reg, LUA_RIDX_GLOBALS);
      setobj(L, f->upvals[0]->v, gt);
      luaC_upvalbarrier(L, f->upvals[0]);
    }
  }
  lua_unlock(L);
  return status;
}
LUA_API int lua_dump (lua_State *L, lua_Writer writer, void *data, int strip) {
  int status;
  TValue *o;
  lua_lock(L);
  api_checknelems(L, 1);
  o = L->top - 1;
  if (isLfunction(o))
    status = luaU_dump(L, getproto(o), writer, data, strip);
  else
    status = 1;
  lua_unlock(L);
  return status;
}
LUA_API int lua_status (lua_State *L) {
  return L->status;
}
LUA_API int lua_gc (lua_State *L, int what, int data) {
  int res = 0;
  global_State *g;
  lua_lock(L);
  g = G(L);
  switch (what) {
    case LUA_GCSTOP: {
      g->gcrunning = 0;
      break;
    }
    case LUA_GCRESTART: {
      luaE_setdebt(g, 0);
      g->gcrunning = 1;
      break;
    }
    case LUA_GCCOLLECT: {
      luaC_fullgc(L, 0);
      break;
    }
    case LUA_GCCOUNT: {
      res = cast_int(gettotalbytes(g) >> 10);
      break;
    }
    case LUA_GCCOUNTB: {
      res = cast_int(gettotalbytes(g) & 0x3ff);
      break;
    }
    case LUA_GCSTEP: {
      l_mem debt = 1;  
      lu_byte oldrunning = g->gcrunning;
      g->gcrunning = 1;  
      if (data == 0) {
        luaE_setdebt(g, -GCSTEPSIZE);  
        luaC_step(L);
      }
      else {  
        debt = cast(l_mem, data) * 1024 + g->GCdebt;
        luaE_setdebt(g, debt);
        luaC_checkGC(L);
      }
      g->gcrunning = oldrunning;  
      if (debt > 0 && g->gcstate == GCSpause)  
        res = 1;  
      break;
    }
    case LUA_GCSETPAUSE: {
      res = g->gcpause;
      g->gcpause = data;
      break;
    }
    case LUA_GCSETSTEPMUL: {
      res = g->gcstepmul;
      if (data < 40) data = 40;  
      g->gcstepmul = data;
      break;
    }
    case LUA_GCISRUNNING: {
      res = g->gcrunning;
      break;
    }
    default: res = -1;  
  }
  lua_unlock(L);
  return res;
}
LUA_API int lua_error (lua_State *L) {
  lua_lock(L);
  api_checknelems(L, 1);
  luaG_errormsg(L);
  return 0;  
}
LUA_API int lua_next (lua_State *L, int idx) {
  StkId t;
  int more;
  lua_lock(L);
  t = index2addr(L, idx);
  api_check(L, ttistable(t), "table expected");
  more = luaH_next(L, hvalue(t), L->top - 1);
  if (more) {
    api_incr_top(L);
  }
  else  
    L->top -= 1;  
  lua_unlock(L);
  return more;
}
LUA_API void lua_concat (lua_State *L, int n) {
  lua_lock(L);
  api_checknelems(L, n);
  if (n >= 2) {
    luaV_concat(L, n);
  }
  else if (n == 0) {  
    setsvalue2s(L, L->top, luaS_newlstr(L, "", 0));
    api_incr_top(L);
  }
  luaC_checkGC(L);
  lua_unlock(L);
}
LUA_API void lua_len (lua_State *L, int idx) {
  StkId t;
  lua_lock(L);
  t = index2addr(L, idx);
  luaV_objlen(L, L->top, t);
  api_incr_top(L);
  lua_unlock(L);
}
LUA_API lua_Alloc lua_getallocf (lua_State *L, void **ud) {
  lua_Alloc f;
  lua_lock(L);
  if (ud) *ud = G(L)->ud;
  f = G(L)->frealloc;
  lua_unlock(L);
  return f;
}
LUA_API void lua_setallocf (lua_State *L, lua_Alloc f, void *ud) {
  lua_lock(L);
  G(L)->ud = ud;
  G(L)->frealloc = f;
  lua_unlock(L);
}
LUA_API void *lua_newuserdata (lua_State *L, size_t size) {
  Udata *u;
  lua_lock(L);
  u = luaS_newudata(L, size);
  setuvalue(L, L->top, u);
  api_incr_top(L);
  luaC_checkGC(L);
  lua_unlock(L);
  return getudatamem(u);
}
static const char *aux_upvalue (StkId fi, int n, TValue **val,
                                CClosure **owner, UpVal **uv) {
  switch (ttype(fi)) {
    case LUA_TCCL: {  
      CClosure *f = clCvalue(fi);
      if (!(1 <= n && n <= f->nupvalues)) return NULL;
      *val = &f->upvalue[n-1];
      if (owner) *owner = f;
      return "";
    }
    case LUA_TLCL: {  
      LClosure *f = clLvalue(fi);
      TString *name;
      Proto *p = f->p;
      if (!(1 <= n && n <= p->sizeupvalues)) return NULL;
      *val = f->upvals[n-1]->v;
      if (uv) *uv = f->upvals[n - 1];
      name = p->upvalues[n-1].name;
      return (name == NULL) ? "(*no name)" : getstr(name);
    }
    default: return NULL;  
  }
}
LUA_API const char *lua_getupvalue (lua_State *L, int funcindex, int n) {
  const char *name;
  TValue *val = NULL;  
  lua_lock(L);
  name = aux_upvalue(index2addr(L, funcindex), n, &val, NULL, NULL);
  if (name) {
    setobj2s(L, L->top, val);
    api_incr_top(L);
  }
  lua_unlock(L);
  return name;
}
LUA_API const char *lua_setupvalue (lua_State *L, int funcindex, int n) {
  const char *name;
  TValue *val = NULL;  
  CClosure *owner = NULL;
  UpVal *uv = NULL;
  StkId fi;
  lua_lock(L);
  fi = index2addr(L, funcindex);
  api_checknelems(L, 1);
  name = aux_upvalue(fi, n, &val, &owner, &uv);
  if (name) {
    L->top--;
    setobj(L, val, L->top);
    if (owner) { luaC_barrier(L, owner, L->top); }
    else if (uv) { luaC_upvalbarrier(L, uv); }
  }
  lua_unlock(L);
  return name;
}
static UpVal **getupvalref (lua_State *L, int fidx, int n) {
  LClosure *f;
  StkId fi = index2addr(L, fidx);
  api_check(L, ttisLclosure(fi), "Lua function expected");
  f = clLvalue(fi);
  api_check(L, (1 <= n && n <= f->p->sizeupvalues), "invalid upvalue index");
  return &f->upvals[n - 1];  
}
LUA_API void *lua_upvalueid (lua_State *L, int fidx, int n) {
  StkId fi = index2addr(L, fidx);
  switch (ttype(fi)) {
    case LUA_TLCL: {  
      return *getupvalref(L, fidx, n);
    }
    case LUA_TCCL: {  
      CClosure *f = clCvalue(fi);
      api_check(L, 1 <= n && n <= f->nupvalues, "invalid upvalue index");
      return &f->upvalue[n - 1];
    }
    default: {
      api_check(L, 0, "closure expected");
      return NULL;
    }
  }
}
LUA_API void lua_upvaluejoin (lua_State *L, int fidx1, int n1,
                                            int fidx2, int n2) {
  UpVal **up1 = getupvalref(L, fidx1, n1);
  UpVal **up2 = getupvalref(L, fidx2, n2);
  if (*up1 == *up2)
    return;
  luaC_upvdeccount(L, *up1);
  *up1 = *up2;
  (*up1)->refcount++;
  if (upisopen(*up1)) (*up1)->u.open.touched = 1;
  luaC_upvalbarrier(L, *up1);
}
#define lcode_c
#define LUA_CORE
#include <math.h>
#include <stdlib.h>
#define MAXREGS		255
#define hasjumps(e)	((e)->t != (e)->f)
static int tonumeral(const expdesc *e, TValue *v) {
  if (hasjumps(e))
    return 0;  
  switch (e->k) {
    case VKINT:
      if (v) setivalue(v, e->u.ival);
      return 1;
    case VKFLT:
      if (v) setfltvalue(v, e->u.nval);
      return 1;
    default: return 0;
  }
}
void luaK_nil (FuncState *fs, int from, int n) {
  Instruction *previous;
  int l = from + n - 1;  
  if (fs->pc > fs->lasttarget) {  
    previous = &fs->f->code[fs->pc-1];
    if (GET_OPCODE(*previous) == OP_LOADNIL) {  
      int pfrom = GETARG_A(*previous);  
      int pl = pfrom + GETARG_B(*previous);
      if ((pfrom <= from && from <= pl + 1) ||
          (from <= pfrom && pfrom <= l + 1)) {  
        if (pfrom < from) from = pfrom;  
        if (pl > l) l = pl;  
        SETARG_A(*previous, from);
        SETARG_B(*previous, l - from);
        return;
      }
    }  
  }
  luaK_codeABC(fs, OP_LOADNIL, from, n - 1, 0);  
}
static int getjump (FuncState *fs, int pc) {
  int offset = GETARG_sBx(fs->f->code[pc]);
  if (offset == NO_JUMP)  
    return NO_JUMP;  
  else
    return (pc+1)+offset;  
}
static void fixjump (FuncState *fs, int pc, int dest) {
  Instruction *jmp = &fs->f->code[pc];
  int offset = dest - (pc + 1);
  lua_assert(dest != NO_JUMP);
  if (abs(offset) > MAXARG_sBx)
    luaX_syntaxerror(fs->ls, "control structure too long");
  SETARG_sBx(*jmp, offset);
}
void luaK_concat (FuncState *fs, int *l1, int l2) {
  if (l2 == NO_JUMP) return;  
  else if (*l1 == NO_JUMP)  
    *l1 = l2;  
  else {
    int list = *l1;
    int next;
    while ((next = getjump(fs, list)) != NO_JUMP)  
      list = next;
    fixjump(fs, list, l2);  
  }
}
int luaK_jump (FuncState *fs) {
  int jpc = fs->jpc;  
  int j;
  fs->jpc = NO_JUMP;  
  j = luaK_codeAsBx(fs, OP_JMP, 0, NO_JUMP);
  luaK_concat(fs, &j, jpc);  
  return j;
}
void luaK_ret (FuncState *fs, int first, int nret) {
  luaK_codeABC(fs, OP_RETURN, first, nret+1, 0);
}
static int condjump (FuncState *fs, OpCode op, int A, int B, int C) {
  luaK_codeABC(fs, op, A, B, C);
  return luaK_jump(fs);
}
int luaK_getlabel (FuncState *fs) {
  fs->lasttarget = fs->pc;
  return fs->pc;
}
static Instruction *getjumpcontrol (FuncState *fs, int pc) {
  Instruction *pi = &fs->f->code[pc];
  if (pc >= 1 && testTMode(GET_OPCODE(*(pi-1))))
    return pi-1;
  else
    return pi;
}
static int patchtestreg (FuncState *fs, int node, int reg) {
  Instruction *i = getjumpcontrol(fs, node);
  if (GET_OPCODE(*i) != OP_TESTSET)
    return 0;  
  if (reg != NO_REG && reg != GETARG_B(*i))
    SETARG_A(*i, reg);
  else {
    *i = CREATE_ABC(OP_TEST, GETARG_B(*i), 0, GETARG_C(*i));
  }
  return 1;
}
static void removevalues (FuncState *fs, int list) {
  for (; list != NO_JUMP; list = getjump(fs, list))
      patchtestreg(fs, list, NO_REG);
}
static void patchlistaux (FuncState *fs, int list, int vtarget, int reg,
                          int dtarget) {
  while (list != NO_JUMP) {
    int next = getjump(fs, list);
    if (patchtestreg(fs, list, reg))
      fixjump(fs, list, vtarget);
    else
      fixjump(fs, list, dtarget);  
    list = next;
  }
}
static void dischargejpc (FuncState *fs) {
  patchlistaux(fs, fs->jpc, fs->pc, NO_REG, fs->pc);
  fs->jpc = NO_JUMP;
}
void luaK_patchtohere (FuncState *fs, int list) {
  luaK_getlabel(fs);  
  luaK_concat(fs, &fs->jpc, list);
}
void luaK_patchlist (FuncState *fs, int list, int target) {
  if (target == fs->pc)  
    luaK_patchtohere(fs, list);  
  else {
    lua_assert(target < fs->pc);
    patchlistaux(fs, list, target, NO_REG, target);
  }
}
void luaK_patchclose (FuncState *fs, int list, int level) {
  level++;  
  for (; list != NO_JUMP; list = getjump(fs, list)) {
    lua_assert(GET_OPCODE(fs->f->code[list]) == OP_JMP &&
                (GETARG_A(fs->f->code[list]) == 0 ||
                 GETARG_A(fs->f->code[list]) >= level));
    SETARG_A(fs->f->code[list], level);
  }
}
static int luaK_code (FuncState *fs, Instruction i) {
  Proto *f = fs->f;
  dischargejpc(fs);  
  luaM_growvector(fs->ls->L, f->code, fs->pc, f->sizecode, Instruction,
                  MAX_INT, "opcodes");
  f->code[fs->pc] = i;
  luaM_growvector(fs->ls->L, f->lineinfo, fs->pc, f->sizelineinfo, int,
                  MAX_INT, "opcodes");
  f->lineinfo[fs->pc] = fs->ls->lastline;
  return fs->pc++;
}
int luaK_codeABC (FuncState *fs, OpCode o, int a, int b, int c) {
  lua_assert(getOpMode(o) == iABC);
  lua_assert(getBMode(o) != OpArgN || b == 0);
  lua_assert(getCMode(o) != OpArgN || c == 0);
  lua_assert(a <= MAXARG_A && b <= MAXARG_B && c <= MAXARG_C);
  return luaK_code(fs, CREATE_ABC(o, a, b, c));
}
int luaK_codeABx (FuncState *fs, OpCode o, int a, unsigned int bc) {
  lua_assert(getOpMode(o) == iABx || getOpMode(o) == iAsBx);
  lua_assert(getCMode(o) == OpArgN);
  lua_assert(a <= MAXARG_A && bc <= MAXARG_Bx);
  return luaK_code(fs, CREATE_ABx(o, a, bc));
}
static int codeextraarg (FuncState *fs, int a) {
  lua_assert(a <= MAXARG_Ax);
  return luaK_code(fs, CREATE_Ax(OP_EXTRAARG, a));
}
int luaK_codek (FuncState *fs, int reg, int k) {
  if (k <= MAXARG_Bx)
    return luaK_codeABx(fs, OP_LOADK, reg, k);
  else {
    int p = luaK_codeABx(fs, OP_LOADKX, reg, 0);
    codeextraarg(fs, k);
    return p;
  }
}
void luaK_checkstack (FuncState *fs, int n) {
  int newstack = fs->freereg + n;
  if (newstack > fs->f->maxstacksize) {
    if (newstack >= MAXREGS)
      luaX_syntaxerror(fs->ls,
        "function or expression needs too many registers");
    fs->f->maxstacksize = cast_byte(newstack);
  }
}
void luaK_reserveregs (FuncState *fs, int n) {
  luaK_checkstack(fs, n);
  fs->freereg += n;
}
static void freereg (FuncState *fs, int reg) {
  if (!ISK(reg) && reg >= fs->nactvar) {
    fs->freereg--;
    lua_assert(reg == fs->freereg);
  }
}
static void freeexp (FuncState *fs, expdesc *e) {
  if (e->k == VNONRELOC)
    freereg(fs, e->u.info);
}
static void freeexps (FuncState *fs, expdesc *e1, expdesc *e2) {
  int r1 = (e1->k == VNONRELOC) ? e1->u.info : -1;
  int r2 = (e2->k == VNONRELOC) ? e2->u.info : -1;
  if (r1 > r2) {
    freereg(fs, r1);
    freereg(fs, r2);
  }
  else {
    freereg(fs, r2);
    freereg(fs, r1);
  }
}
static int addk (FuncState *fs, TValue *key, TValue *v) {
  lua_State *L = fs->ls->L;
  Proto *f = fs->f;
  TValue *idx = luaH_set(L, fs->ls->h, key);  
  int k, oldsize;
  if (ttisinteger(idx)) {  
    k = cast_int(ivalue(idx));
    if (k < fs->nk && ttype(&f->k[k]) == ttype(v) &&
                      luaV_rawequalobj(&f->k[k], v))
      return k;  
  }
  oldsize = f->sizek;
  k = fs->nk;
  setivalue(idx, k);
  luaM_growvector(L, f->k, k, f->sizek, TValue, MAXARG_Ax, "constants");
  while (oldsize < f->sizek) setnilvalue(&f->k[oldsize++]);
  setobj(L, &f->k[k], v);
  fs->nk++;
  luaC_barrier(L, f, v);
  return k;
}
int luaK_stringK (FuncState *fs, TString *s) {
  TValue o;
  setsvalue(fs->ls->L, &o, s);
  return addk(fs, &o, &o);  
}
int luaK_intK (FuncState *fs, lua_Integer n) {
  TValue k, o;
  setpvalue(&k, cast(void*, cast(size_t, n)));
  setivalue(&o, n);
  return addk(fs, &k, &o);
}
static int luaK_numberK (FuncState *fs, lua_Number r) {
  TValue o;
  setfltvalue(&o, r);
  return addk(fs, &o, &o);  
}
static int boolK (FuncState *fs, int b) {
  TValue o;
  setbvalue(&o, b);
  return addk(fs, &o, &o);  
}
static int nilK (FuncState *fs) {
  TValue k, v;
  setnilvalue(&v);
  sethvalue(fs->ls->L, &k, fs->ls->h);
  return addk(fs, &k, &v);
}
void luaK_setreturns (FuncState *fs, expdesc *e, int nresults) {
  if (e->k == VCALL) {  
    SETARG_C(getinstruction(fs, e), nresults + 1);
  }
  else if (e->k == VVARARG) {
    Instruction *pc = &getinstruction(fs, e);
    SETARG_B(*pc, nresults + 1);
    SETARG_A(*pc, fs->freereg);
    luaK_reserveregs(fs, 1);
  }
  else lua_assert(nresults == LUA_MULTRET);
}
void luaK_setoneret (FuncState *fs, expdesc *e) {
  if (e->k == VCALL) {  
    lua_assert(GETARG_C(getinstruction(fs, e)) == 2);
    e->k = VNONRELOC;  
    e->u.info = GETARG_A(getinstruction(fs, e));
  }
  else if (e->k == VVARARG) {
    SETARG_B(getinstruction(fs, e), 2);
    e->k = VRELOCABLE;  
  }
}
void luaK_dischargevars (FuncState *fs, expdesc *e) {
  switch (e->k) {
    case VLOCAL: {  
      e->k = VNONRELOC;  
      break;
    }
    case VUPVAL: {  
      e->u.info = luaK_codeABC(fs, OP_GETUPVAL, 0, e->u.info, 0);
      e->k = VRELOCABLE;
      break;
    }
    case VINDEXED: {
      OpCode op;
      freereg(fs, e->u.ind.idx);
      if (e->u.ind.vt == VLOCAL) {  
        freereg(fs, e->u.ind.t);
        op = OP_GETTABLE;
      }
      else {
        lua_assert(e->u.ind.vt == VUPVAL);
        op = OP_GETTABUP;  
      }
      e->u.info = luaK_codeABC(fs, op, 0, e->u.ind.t, e->u.ind.idx);
      e->k = VRELOCABLE;
      break;
    }
    case VVARARG: case VCALL: {
      luaK_setoneret(fs, e);
      break;
    }
    default: break;  
  }
}
static void discharge2reg (FuncState *fs, expdesc *e, int reg) {
  luaK_dischargevars(fs, e);
  switch (e->k) {
    case VNIL: {
      luaK_nil(fs, reg, 1);
      break;
    }
    case VFALSE: case VTRUE: {
      luaK_codeABC(fs, OP_LOADBOOL, reg, e->k == VTRUE, 0);
      break;
    }
    case VK: {
      luaK_codek(fs, reg, e->u.info);
      break;
    }
    case VKFLT: {
      luaK_codek(fs, reg, luaK_numberK(fs, e->u.nval));
      break;
    }
    case VKINT: {
      luaK_codek(fs, reg, luaK_intK(fs, e->u.ival));
      break;
    }
    case VRELOCABLE: {
      Instruction *pc = &getinstruction(fs, e);
      SETARG_A(*pc, reg);  
      break;
    }
    case VNONRELOC: {
      if (reg != e->u.info)
        luaK_codeABC(fs, OP_MOVE, reg, e->u.info, 0);
      break;
    }
    default: {
      lua_assert(e->k == VJMP);
      return;  
    }
  }
  e->u.info = reg;
  e->k = VNONRELOC;
}
static void discharge2anyreg (FuncState *fs, expdesc *e) {
  if (e->k != VNONRELOC) {  
    luaK_reserveregs(fs, 1);  
    discharge2reg(fs, e, fs->freereg-1);  
  }
}
static int code_loadbool (FuncState *fs, int A, int b, int jump) {
  luaK_getlabel(fs);  
  return luaK_codeABC(fs, OP_LOADBOOL, A, b, jump);
}
static int need_value (FuncState *fs, int list) {
  for (; list != NO_JUMP; list = getjump(fs, list)) {
    Instruction i = *getjumpcontrol(fs, list);
    if (GET_OPCODE(i) != OP_TESTSET) return 1;
  }
  return 0;  
}
static void exp2reg (FuncState *fs, expdesc *e, int reg) {
  discharge2reg(fs, e, reg);
  if (e->k == VJMP)  
    luaK_concat(fs, &e->t, e->u.info);  
  if (hasjumps(e)) {
    int final;  
    int p_f = NO_JUMP;  
    int p_t = NO_JUMP;  
    if (need_value(fs, e->t) || need_value(fs, e->f)) {
      int fj = (e->k == VJMP) ? NO_JUMP : luaK_jump(fs);
      p_f = code_loadbool(fs, reg, 0, 1);
      p_t = code_loadbool(fs, reg, 1, 0);
      luaK_patchtohere(fs, fj);
    }
    final = luaK_getlabel(fs);
    patchlistaux(fs, e->f, final, reg, p_f);
    patchlistaux(fs, e->t, final, reg, p_t);
  }
  e->f = e->t = NO_JUMP;
  e->u.info = reg;
  e->k = VNONRELOC;
}
void luaK_exp2nextreg (FuncState *fs, expdesc *e) {
  luaK_dischargevars(fs, e);
  freeexp(fs, e);
  luaK_reserveregs(fs, 1);
  exp2reg(fs, e, fs->freereg - 1);
}
int luaK_exp2anyreg (FuncState *fs, expdesc *e) {
  luaK_dischargevars(fs, e);
  if (e->k == VNONRELOC) {  
    if (!hasjumps(e))  
      return e->u.info;  
    if (e->u.info >= fs->nactvar) {  
      exp2reg(fs, e, e->u.info);  
      return e->u.info;
    }
  }
  luaK_exp2nextreg(fs, e);  
  return e->u.info;
}
void luaK_exp2anyregup (FuncState *fs, expdesc *e) {
  if (e->k != VUPVAL || hasjumps(e))
    luaK_exp2anyreg(fs, e);
}
void luaK_exp2val (FuncState *fs, expdesc *e) {
  if (hasjumps(e))
    luaK_exp2anyreg(fs, e);
  else
    luaK_dischargevars(fs, e);
}
int luaK_exp2RK (FuncState *fs, expdesc *e) {
  luaK_exp2val(fs, e);
  switch (e->k) {  
    case VTRUE: e->u.info = boolK(fs, 1); goto vk;
    case VFALSE: e->u.info = boolK(fs, 0); goto vk;
    case VNIL: e->u.info = nilK(fs); goto vk;
    case VKINT: e->u.info = luaK_intK(fs, e->u.ival); goto vk;
    case VKFLT: e->u.info = luaK_numberK(fs, e->u.nval); goto vk;
    case VK:
     vk:
      e->k = VK;
      if (e->u.info <= MAXINDEXRK)  
        return RKASK(e->u.info);
      else break;
    default: break;
  }
  return luaK_exp2anyreg(fs, e);
}
void luaK_storevar (FuncState *fs, expdesc *var, expdesc *ex) {
  switch (var->k) {
    case VLOCAL: {
      freeexp(fs, ex);
      exp2reg(fs, ex, var->u.info);  
      return;
    }
    case VUPVAL: {
      int e = luaK_exp2anyreg(fs, ex);
      luaK_codeABC(fs, OP_SETUPVAL, e, var->u.info, 0);
      break;
    }
    case VINDEXED: {
      OpCode op = (var->u.ind.vt == VLOCAL) ? OP_SETTABLE : OP_SETTABUP;
      int e = luaK_exp2RK(fs, ex);
      luaK_codeABC(fs, op, var->u.ind.t, var->u.ind.idx, e);
      break;
    }
    default: lua_assert(0);  
  }
  freeexp(fs, ex);
}
void luaK_self (FuncState *fs, expdesc *e, expdesc *key) {
  int ereg;
  luaK_exp2anyreg(fs, e);
  ereg = e->u.info;  
  freeexp(fs, e);
  e->u.info = fs->freereg;  
  e->k = VNONRELOC;  
  luaK_reserveregs(fs, 2);  
  luaK_codeABC(fs, OP_SELF, e->u.info, ereg, luaK_exp2RK(fs, key));
  freeexp(fs, key);
}
static void negatecondition (FuncState *fs, expdesc *e) {
  Instruction *pc = getjumpcontrol(fs, e->u.info);
  lua_assert(testTMode(GET_OPCODE(*pc)) && GET_OPCODE(*pc) != OP_TESTSET &&
                                           GET_OPCODE(*pc) != OP_TEST);
  SETARG_A(*pc, !(GETARG_A(*pc)));
}
static int jumponcond (FuncState *fs, expdesc *e, int cond) {
  if (e->k == VRELOCABLE) {
    Instruction ie = getinstruction(fs, e);
    if (GET_OPCODE(ie) == OP_NOT) {
      fs->pc--;  
      return condjump(fs, OP_TEST, GETARG_B(ie), 0, !cond);
    }
  }
  discharge2anyreg(fs, e);
  freeexp(fs, e);
  return condjump(fs, OP_TESTSET, NO_REG, e->u.info, cond);
}
void luaK_goiftrue (FuncState *fs, expdesc *e) {
  int pc;  
  luaK_dischargevars(fs, e);
  switch (e->k) {
    case VJMP: {  
      negatecondition(fs, e);  
      pc = e->u.info;  
      break;
    }
    case VK: case VKFLT: case VKINT: case VTRUE: {
      pc = NO_JUMP;  
      break;
    }
    default: {
      pc = jumponcond(fs, e, 0);  
      break;
    }
  }
  luaK_concat(fs, &e->f, pc);  
  luaK_patchtohere(fs, e->t);  
  e->t = NO_JUMP;
}
void luaK_goiffalse (FuncState *fs, expdesc *e) {
  int pc;  
  luaK_dischargevars(fs, e);
  switch (e->k) {
    case VJMP: {
      pc = e->u.info;  
      break;
    }
    case VNIL: case VFALSE: {
      pc = NO_JUMP;  
      break;
    }
    default: {
      pc = jumponcond(fs, e, 1);  
      break;
    }
  }
  luaK_concat(fs, &e->t, pc);  
  luaK_patchtohere(fs, e->f);  
  e->f = NO_JUMP;
}
static void codenot (FuncState *fs, expdesc *e) {
  luaK_dischargevars(fs, e);
  switch (e->k) {
    case VNIL: case VFALSE: {
      e->k = VTRUE;  
      break;
    }
    case VK: case VKFLT: case VKINT: case VTRUE: {
      e->k = VFALSE;  
      break;
    }
    case VJMP: {
      negatecondition(fs, e);
      break;
    }
    case VRELOCABLE:
    case VNONRELOC: {
      discharge2anyreg(fs, e);
      freeexp(fs, e);
      e->u.info = luaK_codeABC(fs, OP_NOT, 0, e->u.info, 0);
      e->k = VRELOCABLE;
      break;
    }
    default: lua_assert(0);  
  }
  { int temp = e->f; e->f = e->t; e->t = temp; }
  removevalues(fs, e->f);  
  removevalues(fs, e->t);
}
void luaK_indexed (FuncState *fs, expdesc *t, expdesc *k) {
  lua_assert(!hasjumps(t) && (vkisinreg(t->k) || t->k == VUPVAL));
  t->u.ind.t = t->u.info;  
  t->u.ind.idx = luaK_exp2RK(fs, k);  
  t->u.ind.vt = (t->k == VUPVAL) ? VUPVAL : VLOCAL;
  t->k = VINDEXED;
}
static int validop (int op, TValue *v1, TValue *v2) {
  switch (op) {
    case LUA_OPBAND: case LUA_OPBOR: case LUA_OPBXOR:
    case LUA_OPSHL: case LUA_OPSHR: case LUA_OPBNOT: {  
      lua_Integer i;
      return (tointeger(v1, &i) && tointeger(v2, &i));
    }
    case LUA_OPDIV: case LUA_OPIDIV: case LUA_OPMOD:  
      return (nvalue(v2) != 0);
    default: return 1;  
  }
}
static int constfolding (FuncState *fs, int op, expdesc *e1,
                                                const expdesc *e2) {
  TValue v1, v2, res;
  if (!tonumeral(e1, &v1) || !tonumeral(e2, &v2) || !validop(op, &v1, &v2))
    return 0;  
  luaO_arith(fs->ls->L, op, &v1, &v2, &res);  
  if (ttisinteger(&res)) {
    e1->k = VKINT;
    e1->u.ival = ivalue(&res);
  }
  else {  
    lua_Number n = fltvalue(&res);
    if (luai_numisnan(n) || n == 0)
      return 0;
    e1->k = VKFLT;
    e1->u.nval = n;
  }
  return 1;
}
static void codeunexpval (FuncState *fs, OpCode op, expdesc *e, int line) {
  int r = luaK_exp2anyreg(fs, e);  
  freeexp(fs, e);
  e->u.info = luaK_codeABC(fs, op, 0, r, 0);  
  e->k = VRELOCABLE;  
  luaK_fixline(fs, line);
}
static void codebinexpval (FuncState *fs, OpCode op,
                           expdesc *e1, expdesc *e2, int line) {
  int rk2 = luaK_exp2RK(fs, e2);  
  int rk1 = luaK_exp2RK(fs, e1);
  freeexps(fs, e1, e2);
  e1->u.info = luaK_codeABC(fs, op, 0, rk1, rk2);  
  e1->k = VRELOCABLE;  
  luaK_fixline(fs, line);
}
static void codecomp (FuncState *fs, BinOpr opr, expdesc *e1, expdesc *e2) {
  int rk1 = (e1->k == VK) ? RKASK(e1->u.info)
                          : check_exp(e1->k == VNONRELOC, e1->u.info);
  int rk2 = luaK_exp2RK(fs, e2);
  freeexps(fs, e1, e2);
  switch (opr) {
    case OPR_NE: {  
      e1->u.info = condjump(fs, OP_EQ, 0, rk1, rk2);
      break;
    }
    case OPR_GT: case OPR_GE: {
      OpCode op = cast(OpCode, (opr - OPR_NE) + OP_EQ);
      e1->u.info = condjump(fs, op, 1, rk2, rk1);  
      break;
    }
    default: {  
      OpCode op = cast(OpCode, (opr - OPR_EQ) + OP_EQ);
      e1->u.info = condjump(fs, op, 1, rk1, rk2);
      break;
    }
  }
  e1->k = VJMP;
}
void luaK_prefix (FuncState *fs, UnOpr op, expdesc *e, int line) {
  static const expdesc ef = {VKINT, {0}, NO_JUMP, NO_JUMP};
  switch (op) {
    case OPR_MINUS: case OPR_BNOT:  
      if (constfolding(fs, op + LUA_OPUNM, e, &ef))
        break;
    case OPR_LEN:
      codeunexpval(fs, cast(OpCode, op + OP_UNM), e, line);
      break;
    case OPR_NOT: codenot(fs, e); break;
    default: lua_assert(0);
  }
}
void luaK_infix (FuncState *fs, BinOpr op, expdesc *v) {
  switch (op) {
    case OPR_AND: {
      luaK_goiftrue(fs, v);  
      break;
    }
    case OPR_OR: {
      luaK_goiffalse(fs, v);  
      break;
    }
    case OPR_CONCAT: {
      luaK_exp2nextreg(fs, v);  
      break;
    }
    case OPR_ADD: case OPR_SUB:
    case OPR_MUL: case OPR_DIV: case OPR_IDIV:
    case OPR_MOD: case OPR_POW:
    case OPR_BAND: case OPR_BOR: case OPR_BXOR:
    case OPR_SHL: case OPR_SHR: {
      if (!tonumeral(v, NULL))
        luaK_exp2RK(fs, v);
      break;
    }
    default: {
      luaK_exp2RK(fs, v);
      break;
    }
  }
}
void luaK_posfix (FuncState *fs, BinOpr op,
                  expdesc *e1, expdesc *e2, int line) {
  switch (op) {
    case OPR_AND: {
      lua_assert(e1->t == NO_JUMP);  
      luaK_dischargevars(fs, e2);
      luaK_concat(fs, &e2->f, e1->f);
      *e1 = *e2;
      break;
    }
    case OPR_OR: {
      lua_assert(e1->f == NO_JUMP);  
      luaK_dischargevars(fs, e2);
      luaK_concat(fs, &e2->t, e1->t);
      *e1 = *e2;
      break;
    }
    case OPR_CONCAT: {
      luaK_exp2val(fs, e2);
      if (e2->k == VRELOCABLE &&
          GET_OPCODE(getinstruction(fs, e2)) == OP_CONCAT) {
        lua_assert(e1->u.info == GETARG_B(getinstruction(fs, e2))-1);
        freeexp(fs, e1);
        SETARG_B(getinstruction(fs, e2), e1->u.info);
        e1->k = VRELOCABLE; e1->u.info = e2->u.info;
      }
      else {
        luaK_exp2nextreg(fs, e2);  
        codebinexpval(fs, OP_CONCAT, e1, e2, line);
      }
      break;
    }
    case OPR_ADD: case OPR_SUB: case OPR_MUL: case OPR_DIV:
    case OPR_IDIV: case OPR_MOD: case OPR_POW:
    case OPR_BAND: case OPR_BOR: case OPR_BXOR:
    case OPR_SHL: case OPR_SHR: {
      if (!constfolding(fs, op + LUA_OPADD, e1, e2))
        codebinexpval(fs, cast(OpCode, op + OP_ADD), e1, e2, line);
      break;
    }
    case OPR_EQ: case OPR_LT: case OPR_LE:
    case OPR_NE: case OPR_GT: case OPR_GE: {
      codecomp(fs, op, e1, e2);
      break;
    }
    default: lua_assert(0);
  }
}
void luaK_fixline (FuncState *fs, int line) {
  fs->f->lineinfo[fs->pc - 1] = line;
}
void luaK_setlist (FuncState *fs, int base, int nelems, int tostore) {
  int c =  (nelems - 1)/LFIELDS_PER_FLUSH + 1;
  int b = (tostore == LUA_MULTRET) ? 0 : tostore;
  lua_assert(tostore != 0 && tostore <= LFIELDS_PER_FLUSH);
  if (c <= MAXARG_C)
    luaK_codeABC(fs, OP_SETLIST, base, b, c);
  else if (c <= MAXARG_Ax) {
    luaK_codeABC(fs, OP_SETLIST, base, b, 0);
    codeextraarg(fs, c);
  }
  else
    luaX_syntaxerror(fs->ls, "constructor too long");
  fs->freereg = base + 1;  
}
#define lctype_c
#define LUA_CORE
#if !LUA_USE_CTYPE	
#include <limits.h>
LUAI_DDEF const lu_byte luai_ctype_[UCHAR_MAX + 2] = {
  0x00,  
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x08,  0x08,  0x08,  0x08,  0x08,  0x00,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
  0x0c,  0x04,  0x04,  0x04,  0x04,  0x04,  0x04,  0x04,	
  0x04,  0x04,  0x04,  0x04,  0x04,  0x04,  0x04,  0x04,
  0x16,  0x16,  0x16,  0x16,  0x16,  0x16,  0x16,  0x16,	
  0x16,  0x16,  0x04,  0x04,  0x04,  0x04,  0x04,  0x04,
  0x04,  0x15,  0x15,  0x15,  0x15,  0x15,  0x15,  0x05,	
  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,
  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,	
  0x05,  0x05,  0x05,  0x04,  0x04,  0x04,  0x04,  0x05,
  0x04,  0x15,  0x15,  0x15,  0x15,  0x15,  0x15,  0x05,	
  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,
  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,  0x05,	
  0x05,  0x05,  0x05,  0x04,  0x04,  0x04,  0x04,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,	
  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,  0x00,
};
#endif			
#define ldebug_c
#define LUA_CORE
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#define noLuaClosure(f)		((f) == NULL || (f)->c.tt == LUA_TCCL)
#define ci_func(ci)		(clLvalue((ci)->func))
static const char *funcnamefromcode (lua_State *L, CallInfo *ci,
                                    const char **name);
static int currentpc (CallInfo *ci) {
  lua_assert(isLua(ci));
  return pcRel(ci->u.l.savedpc, ci_func(ci)->p);
}
static int currentline (CallInfo *ci) {
  return getfuncline(ci_func(ci)->p, currentpc(ci));
}
static void swapextra (lua_State *L) {
  if (L->status == LUA_YIELD) {
    CallInfo *ci = L->ci;  
    StkId temp = ci->func;  
    ci->func = restorestack(L, ci->extra);
    ci->extra = savestack(L, temp);
  }
}
LUA_API void lua_sethook (lua_State *L, lua_Hook func, int mask, int count) {
  if (func == NULL || mask == 0) {  
    mask = 0;
    func = NULL;
  }
  if (isLua(L->ci))
    L->oldpc = L->ci->u.l.savedpc;
  L->hook = func;
  L->basehookcount = count;
  resethookcount(L);
  L->hookmask = cast_byte(mask);
}
LUA_API lua_Hook lua_gethook (lua_State *L) {
  return L->hook;
}
LUA_API int lua_gethookmask (lua_State *L) {
  return L->hookmask;
}
LUA_API int lua_gethookcount (lua_State *L) {
  return L->basehookcount;
}
LUA_API int lua_getstack (lua_State *L, int level, lua_Debug *ar) {
  int status;
  CallInfo *ci;
  if (level < 0) return 0;  
  lua_lock(L);
  for (ci = L->ci; level > 0 && ci != &L->base_ci; ci = ci->previous)
    level--;
  if (level == 0 && ci != &L->base_ci) {  
    status = 1;
    ar->i_ci = ci;
  }
  else status = 0;  
  lua_unlock(L);
  return status;
}
static const char *upvalname (Proto *p, int uv) {
  TString *s = check_exp(uv < p->sizeupvalues, p->upvalues[uv].name);
  if (s == NULL) return "?";
  else return getstr(s);
}
static const char *findvararg (CallInfo *ci, int n, StkId *pos) {
  int nparams = clLvalue(ci->func)->p->numparams;
  int nvararg = cast_int(ci->u.l.base - ci->func) - nparams;
  if (n <= -nvararg)
    return NULL;  
  else {
    *pos = ci->func + nparams - n;
    return "(*vararg)";  
  }
}
static const char *findlocal (lua_State *L, CallInfo *ci, int n,
                              StkId *pos) {
  const char *name = NULL;
  StkId base;
  if (isLua(ci)) {
    if (n < 0)  
      return findvararg(ci, n, pos);
    else {
      base = ci->u.l.base;
      name = luaF_getlocalname(ci_func(ci)->p, n, currentpc(ci));
    }
  }
  else
    base = ci->func + 1;
  if (name == NULL) {  
    StkId limit = (ci == L->ci) ? L->top : ci->next->func;
    if (limit - base >= n && n > 0)  
      name = "(*temporary)";  
    else
      return NULL;  
  }
  *pos = base + (n - 1);
  return name;
}
LUA_API const char *lua_getlocal (lua_State *L, const lua_Debug *ar, int n) {
  const char *name;
  lua_lock(L);
  swapextra(L);
  if (ar == NULL) {  
    if (!isLfunction(L->top - 1))  
      name = NULL;
    else  
      name = luaF_getlocalname(clLvalue(L->top - 1)->p, n, 0);
  }
  else {  
    StkId pos = NULL;  
    name = findlocal(L, ar->i_ci, n, &pos);
    if (name) {
      setobj2s(L, L->top, pos);
      api_incr_top(L);
    }
  }
  swapextra(L);
  lua_unlock(L);
  return name;
}
LUA_API const char *lua_setlocal (lua_State *L, const lua_Debug *ar, int n) {
  StkId pos = NULL;  
  const char *name;
  lua_lock(L);
  swapextra(L);
  name = findlocal(L, ar->i_ci, n, &pos);
  if (name) {
    setobjs2s(L, pos, L->top - 1);
    L->top--;  
  }
  swapextra(L);
  lua_unlock(L);
  return name;
}
static void funcinfo (lua_Debug *ar, Closure *cl) {
  if (noLuaClosure(cl)) {
    ar->source = "=[C]";
    ar->linedefined = -1;
    ar->lastlinedefined = -1;
    ar->what = "C";
  }
  else {
    Proto *p = cl->l.p;
    ar->source = p->source ? getstr(p->source) : "=?";
    ar->linedefined = p->linedefined;
    ar->lastlinedefined = p->lastlinedefined;
    ar->what = (ar->linedefined == 0) ? "main" : "Lua";
  }
  luaO_chunkid(ar->short_src, ar->source, LUA_IDSIZE);
}
static void collectvalidlines (lua_State *L, Closure *f) {
  if (noLuaClosure(f)) {
    setnilvalue(L->top);
    api_incr_top(L);
  }
  else {
    int i;
    TValue v;
    int *lineinfo = f->l.p->lineinfo;
    Table *t = luaH_new(L);  
    sethvalue(L, L->top, t);  
    api_incr_top(L);
    setbvalue(&v, 1);  
    for (i = 0; i < f->l.p->sizelineinfo; i++)  
      luaH_setint(L, t, lineinfo[i], &v);  
  }
}
static const char *getfuncname (lua_State *L, CallInfo *ci, const char **name) {
  if (ci == NULL)  
    return NULL;  
  else if (ci->callstatus & CIST_FIN) {  
    *name = "__gc";
    return "metamethod";  
  }
  else if (!(ci->callstatus & CIST_TAIL) && isLua(ci->previous))
    return funcnamefromcode(L, ci->previous, name);
  else return NULL;  
}
static int auxgetinfo (lua_State *L, const char *what, lua_Debug *ar,
                       Closure *f, CallInfo *ci) {
  int status = 1;
  for (; *what; what++) {
    switch (*what) {
      case 'S': {
        funcinfo(ar, f);
        break;
      }
      case 'l': {
        ar->currentline = (ci && isLua(ci)) ? currentline(ci) : -1;
        break;
      }
      case 'u': {
        ar->nups = (f == NULL) ? 0 : f->c.nupvalues;
        if (noLuaClosure(f)) {
          ar->isvararg = 1;
          ar->nparams = 0;
        }
        else {
          ar->isvararg = f->l.p->is_vararg;
          ar->nparams = f->l.p->numparams;
        }
        break;
      }
      case 't': {
        ar->istailcall = (ci) ? ci->callstatus & CIST_TAIL : 0;
        break;
      }
      case 'n': {
        ar->namewhat = getfuncname(L, ci, &ar->name);
        if (ar->namewhat == NULL) {
          ar->namewhat = "";  
          ar->name = NULL;
        }
        break;
      }
      case 'L':
      case 'f':  
        break;
      default: status = 0;  
    }
  }
  return status;
}
LUA_API int lua_getinfo (lua_State *L, const char *what, lua_Debug *ar) {
  int status;
  Closure *cl;
  CallInfo *ci;
  StkId func;
  lua_lock(L);
  swapextra(L);
  if (*what == '>') {
    ci = NULL;
    func = L->top - 1;
    api_check(L, ttisfunction(func), "function expected");
    what++;  
    L->top--;  
  }
  else {
    ci = ar->i_ci;
    func = ci->func;
    lua_assert(ttisfunction(ci->func));
  }
  cl = ttisclosure(func) ? clvalue(func) : NULL;
  status = auxgetinfo(L, what, ar, cl, ci);
  if (strchr(what, 'f')) {
    setobjs2s(L, L->top, func);
    api_incr_top(L);
  }
  swapextra(L);  
  if (strchr(what, 'L'))
    collectvalidlines(L, cl);
  lua_unlock(L);
  return status;
}
static const char *getobjname (Proto *p, int lastpc, int reg,
                               const char **name);
static void kname (Proto *p, int pc, int c, const char **name) {
  if (ISK(c)) {  
    TValue *kvalue = &p->k[INDEXK(c)];
    if (ttisstring(kvalue)) {  
      *name = svalue(kvalue);  
      return;
    }
  }
  else {  
    const char *what = getobjname(p, pc, c, name); 
    if (what && *what == 'c') {  
      return;  
    }
  }
  *name = "?";  
}
static int filterpc (int pc, int jmptarget) {
  if (pc < jmptarget)  
    return -1;  
  else return pc;  
}
static int findsetreg (Proto *p, int lastpc, int reg) {
  int pc;
  int setreg = -1;  
  int jmptarget = 0;  
  for (pc = 0; pc < lastpc; pc++) {
    Instruction i = p->code[pc];
    OpCode op = GET_OPCODE(i);
    int a = GETARG_A(i);
    switch (op) {
      case OP_LOADNIL: {
        int b = GETARG_B(i);
        if (a <= reg && reg <= a + b)  
          setreg = filterpc(pc, jmptarget);
        break;
      }
      case OP_TFORCALL: {
        if (reg >= a + 2)  
          setreg = filterpc(pc, jmptarget);
        break;
      }
      case OP_CALL:
      case OP_TAILCALL: {
        if (reg >= a)  
          setreg = filterpc(pc, jmptarget);
        break;
      }
      case OP_JMP: {
        int b = GETARG_sBx(i);
        int dest = pc + 1 + b;
        if (pc < dest && dest <= lastpc) {
          if (dest > jmptarget)
            jmptarget = dest;  
        }
        break;
      }
      default:
        if (testAMode(op) && reg == a)  
          setreg = filterpc(pc, jmptarget);
        break;
    }
  }
  return setreg;
}
static const char *getobjname (Proto *p, int lastpc, int reg,
                               const char **name) {
  int pc;
  *name = luaF_getlocalname(p, reg + 1, lastpc);
  if (*name)  
    return "local";
  pc = findsetreg(p, lastpc, reg);
  if (pc != -1) {  
    Instruction i = p->code[pc];
    OpCode op = GET_OPCODE(i);
    switch (op) {
      case OP_MOVE: {
        int b = GETARG_B(i);  
        if (b < GETARG_A(i))
          return getobjname(p, pc, b, name);  
        break;
      }
      case OP_GETTABUP:
      case OP_GETTABLE: {
        int k = GETARG_C(i);  
        int t = GETARG_B(i);  
        const char *vn = (op == OP_GETTABLE)  
                         ? luaF_getlocalname(p, t + 1, pc)
                         : upvalname(p, t);
        kname(p, pc, k, name);
        return (vn && strcmp(vn, LUA_ENV) == 0) ? "global" : "field";
      }
      case OP_GETUPVAL: {
        *name = upvalname(p, GETARG_B(i));
        return "upvalue";
      }
      case OP_LOADK:
      case OP_LOADKX: {
        int b = (op == OP_LOADK) ? GETARG_Bx(i)
                                 : GETARG_Ax(p->code[pc + 1]);
        if (ttisstring(&p->k[b])) {
          *name = svalue(&p->k[b]);
          return "constant";
        }
        break;
      }
      case OP_SELF: {
        int k = GETARG_C(i);  
        kname(p, pc, k, name);
        return "method";
      }
      default: break;  
    }
  }
  return NULL;  
}
static const char *funcnamefromcode (lua_State *L, CallInfo *ci,
                                     const char **name) {
  TMS tm = (TMS)0;  
  Proto *p = ci_func(ci)->p;  
  int pc = currentpc(ci);  
  Instruction i = p->code[pc];  
  if (ci->callstatus & CIST_HOOKED) {  
    *name = "?";
    return "hook";
  }
  switch (GET_OPCODE(i)) {
    case OP_CALL:
    case OP_TAILCALL:
      return getobjname(p, pc, GETARG_A(i), name);  
    case OP_TFORCALL: {  
      *name = "for iterator";
       return "for iterator";
    }
    case OP_SELF: case OP_GETTABUP: case OP_GETTABLE:
      tm = TM_INDEX;
      break;
    case OP_SETTABUP: case OP_SETTABLE:
      tm = TM_NEWINDEX;
      break;
    case OP_ADD: case OP_SUB: case OP_MUL: case OP_MOD:
    case OP_POW: case OP_DIV: case OP_IDIV: case OP_BAND:
    case OP_BOR: case OP_BXOR: case OP_SHL: case OP_SHR: {
      int offset = cast_int(GET_OPCODE(i)) - cast_int(OP_ADD);  
      tm = cast(TMS, offset + cast_int(TM_ADD));  
      break;
    }
    case OP_UNM: tm = TM_UNM; break;
    case OP_BNOT: tm = TM_BNOT; break;
    case OP_LEN: tm = TM_LEN; break;
    case OP_CONCAT: tm = TM_CONCAT; break;
    case OP_EQ: tm = TM_EQ; break;
    case OP_LT: tm = TM_LT; break;
    case OP_LE: tm = TM_LE; break;
    default:
      return NULL;  
  }
  *name = getstr(G(L)->tmname[tm]);
  return "metamethod";
}
static int isinstack (CallInfo *ci, const TValue *o) {
  ptrdiff_t i = o - ci->u.l.base;
  return (0 <= i && i < (ci->top - ci->u.l.base) && ci->u.l.base + i == o);
}
static const char *getupvalname (CallInfo *ci, const TValue *o,
                                 const char **name) {
  LClosure *c = ci_func(ci);
  int i;
  for (i = 0; i < c->nupvalues; i++) {
    if (c->upvals[i]->v == o) {
      *name = upvalname(c->p, i);
      return "upvalue";
    }
  }
  return NULL;
}
static const char *varinfo (lua_State *L, const TValue *o) {
  const char *name = NULL;  
  CallInfo *ci = L->ci;
  const char *kind = NULL;
  if (isLua(ci)) {
    kind = getupvalname(ci, o, &name);  
    if (!kind && isinstack(ci, o))  
      kind = getobjname(ci_func(ci)->p, currentpc(ci),
                        cast_int(o - ci->u.l.base), &name);
  }
  return (kind) ? luaO_pushfstring(L, " (%s '%s')", kind, name) : "";
}
l_noret luaG_typeerror (lua_State *L, const TValue *o, const char *op) {
  const char *t = luaT_objtypename(L, o);
  luaG_runerror(L, "attempt to %s a %s value%s", op, t, varinfo(L, o));
}
l_noret luaG_concaterror (lua_State *L, const TValue *p1, const TValue *p2) {
  if (ttisstring(p1) || cvt2str(p1)) p1 = p2;
  luaG_typeerror(L, p1, "concatenate");
}
l_noret luaG_opinterror (lua_State *L, const TValue *p1,
                         const TValue *p2, const char *msg) {
  lua_Number temp;
  if (!tonumber(p1, &temp))  
    p2 = p1;  
  luaG_typeerror(L, p2, msg);
}
l_noret luaG_tointerror (lua_State *L, const TValue *p1, const TValue *p2) {
  lua_Integer temp;
  if (!tointeger(p1, &temp))
    p2 = p1;
  luaG_runerror(L, "number%s has no integer representation", varinfo(L, p2));
}
l_noret luaG_ordererror (lua_State *L, const TValue *p1, const TValue *p2) {
  const char *t1 = luaT_objtypename(L, p1);
  const char *t2 = luaT_objtypename(L, p2);
  if (strcmp(t1, t2) == 0)
    luaG_runerror(L, "attempt to compare two %s values", t1);
  else
    luaG_runerror(L, "attempt to compare %s with %s", t1, t2);
}
const char *luaG_addinfo (lua_State *L, const char *msg, TString *src,
                                        int line) {
  char buff[LUA_IDSIZE];
  if (src)
    luaO_chunkid(buff, getstr(src), LUA_IDSIZE);
  else {  
    buff[0] = '?'; buff[1] = '\0';
  }
  return luaO_pushfstring(L, "%s:%d: %s", buff, line, msg);
}
l_noret luaG_errormsg (lua_State *L) {
  if (L->errfunc != 0) {  
    StkId errfunc = restorestack(L, L->errfunc);
    setobjs2s(L, L->top, L->top - 1);  
    setobjs2s(L, L->top - 1, errfunc);  
    L->top++;  
    luaD_callnoyield(L, L->top - 2, 1);  
  }
  luaD_throw(L, LUA_ERRRUN);
}
l_noret luaG_runerror (lua_State *L, const char *fmt, ...) {
  CallInfo *ci = L->ci;
  const char *msg;
  va_list argp;
  luaC_checkGC(L);  
  va_start(argp, fmt);
  msg = luaO_pushvfstring(L, fmt, argp);  
  va_end(argp);
  if (isLua(ci))  
    luaG_addinfo(L, msg, ci_func(ci)->p->source, currentline(ci));
  luaG_errormsg(L);
}
void luaG_traceexec (lua_State *L) {
  CallInfo *ci = L->ci;
  lu_byte mask = L->hookmask;
  int counthook = (--L->hookcount == 0 && (mask & LUA_MASKCOUNT));
  if (counthook)
    resethookcount(L);  
  else if (!(mask & LUA_MASKLINE))
    return;  
  if (ci->callstatus & CIST_HOOKYIELD) {  
    ci->callstatus &= ~CIST_HOOKYIELD;  
    return;  
  }
  if (counthook)
    luaD_hook(L, LUA_HOOKCOUNT, -1);  
  if (mask & LUA_MASKLINE) {
    Proto *p = ci_func(ci)->p;
    int npc = pcRel(ci->u.l.savedpc, p);
    int newline = getfuncline(p, npc);
    if (npc == 0 ||  
        ci->u.l.savedpc <= L->oldpc ||  
        newline != getfuncline(p, pcRel(L->oldpc, p)))  
      luaD_hook(L, LUA_HOOKLINE, newline);  
  }
  L->oldpc = ci->u.l.savedpc;
  if (L->status == LUA_YIELD) {  
    if (counthook)
      L->hookcount = 1;  
    ci->u.l.savedpc--;  
    ci->callstatus |= CIST_HOOKYIELD;  
    ci->func = L->top - 1;  
    luaD_throw(L, LUA_YIELD);
  }
}
#define ldo_c
#define LUA_CORE
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>
#define errorstatus(s)	((s) > LUA_YIELD)
#if !defined(LUAI_THROW)				
#if defined(__cplusplus) && !defined(LUA_USE_LONGJMP)	
#define LUAI_THROW(L,c)		throw(c)
#define LUAI_TRY(L,c,a) \
	try { a } catch(...) { if ((c)->status == 0) (c)->status = -1; }
#define luai_jmpbuf		int  
#elif defined(LUA_USE_POSIX)				
#define LUAI_THROW(L,c)		_longjmp((c)->b, 1)
#define LUAI_TRY(L,c,a)		if (_setjmp((c)->b) == 0) { a }
#define luai_jmpbuf		jmp_buf
#else							
#define LUAI_THROW(L,c)		longjmp((c)->b, 1)
#define LUAI_TRY(L,c,a)		if (setjmp((c)->b) == 0) { a }
#define luai_jmpbuf		jmp_buf
#endif							
#endif							
struct lua_longjmp {
  struct lua_longjmp *previous;
  luai_jmpbuf b;
  volatile int status;  
};
void luaD_seterrorobj (lua_State *L, int errcode, StkId oldtop) {
  switch (errcode) {
    case LUA_ERRMEM: {  
      setsvalue2s(L, oldtop, G(L)->memerrmsg); 
      break;
    }
    case LUA_ERRERR: {
      setsvalue2s(L, oldtop, luaS_newliteral(L, "error in error handling"));
      break;
    }
    case CLOSEPROTECT: {
      setnilvalue(oldtop);  
      break;
    }
    default: {
      setobjs2s(L, oldtop, L->top - 1);  
      break;
    }
  }
  L->top = oldtop + 1;
}
l_noret luaD_throw (lua_State *L, int errcode) {
  if (L->errorJmp) {  
    L->errorJmp->status = errcode;  
    LUAI_THROW(L, L->errorJmp);  
  }
  else {  
    global_State *g = G(L);
    errcode = luaF_close(L, L->stack, errcode);  
    L->status = cast_byte(errcode);  
    if (g->mainthread->errorJmp) {  
      setobjs2s(L, g->mainthread->top++, L->top - 1);  
      luaD_throw(g->mainthread, errcode);  
    }
    else {  
      if (g->panic) {  
        luaD_seterrorobj(L, errcode, L->top);  
        if (L->ci->top < L->top)
          L->ci->top = L->top;  
        lua_unlock(L);
        g->panic(L);  
      }
      abort();
    }
  }
}
int luaD_rawrunprotected (lua_State *L, Pfunc f, void *ud) {
  unsigned short oldnCcalls = L->nCcalls;
  struct lua_longjmp lj;
  lj.status = LUA_OK;
  lj.previous = L->errorJmp;  
  L->errorJmp = &lj;
  LUAI_TRY(L, &lj,
    (*f)(L, ud);
  );
  L->errorJmp = lj.previous;  
  L->nCcalls = oldnCcalls;
  return lj.status;
}
static void correctstack (lua_State *L, TValue *oldstack) {
  CallInfo *ci;
  UpVal *up;
  L->top = (L->top - oldstack) + L->stack;
  for (up = L->openupval; up != NULL; up = up->u.open.next)
    up->v = (up->v - oldstack) + L->stack;
  for (ci = L->ci; ci != NULL; ci = ci->previous) {
    ci->top = (ci->top - oldstack) + L->stack;
    ci->func = (ci->func - oldstack) + L->stack;
    if (isLua(ci))
      ci->u.l.base = (ci->u.l.base - oldstack) + L->stack;
  }
}
#define ERRORSTACKSIZE	(LUAI_MAXSTACK + 200)
void luaD_reallocstack (lua_State *L, int newsize) {
  TValue *oldstack = L->stack;
  int lim = L->stacksize;
  lua_assert(newsize <= LUAI_MAXSTACK || newsize == ERRORSTACKSIZE);
  lua_assert(L->stack_last - L->stack == L->stacksize - EXTRA_STACK);
  luaM_reallocvector(L, L->stack, L->stacksize, newsize, TValue);
  for (; lim < newsize; lim++)
    setnilvalue(L->stack + lim); 
  L->stacksize = newsize;
  L->stack_last = L->stack + newsize - EXTRA_STACK;
  correctstack(L, oldstack);
}
void luaD_growstack (lua_State *L, int n) {
  int size = L->stacksize;
  if (size > LUAI_MAXSTACK)  
    luaD_throw(L, LUA_ERRERR);
  else {
    int needed = cast_int(L->top - L->stack) + n + EXTRA_STACK;
    int newsize = 2 * size;
    if (newsize > LUAI_MAXSTACK) newsize = LUAI_MAXSTACK;
    if (newsize < needed) newsize = needed;
    if (newsize > LUAI_MAXSTACK) {  
      luaD_reallocstack(L, ERRORSTACKSIZE);
      luaG_runerror(L, "stack overflow");
    }
    else
      luaD_reallocstack(L, newsize);
  }
}
static int stackinuse (lua_State *L) {
  CallInfo *ci;
  StkId lim = L->top;
  for (ci = L->ci; ci != NULL; ci = ci->previous) {
    if (lim < ci->top) lim = ci->top;
  }
  lua_assert(lim <= L->stack_last);
  return cast_int(lim - L->stack) + 1;  
}
void luaD_shrinkstack (lua_State *L) {
  int inuse = stackinuse(L);
  int goodsize = inuse + (inuse / 8) + 2*EXTRA_STACK;
  if (goodsize > LUAI_MAXSTACK)
    goodsize = LUAI_MAXSTACK;  
  if (L->stacksize > LUAI_MAXSTACK)  
    luaE_freeCI(L);  
  else
    luaE_shrinkCI(L);  
  if (inuse <= (LUAI_MAXSTACK - EXTRA_STACK) &&
      goodsize < L->stacksize)
    luaD_reallocstack(L, goodsize);
  else  
    condmovestack(L,{},{});  
}
void luaD_inctop (lua_State *L) {
  luaD_checkstack(L, 1);
  L->top++;
}
void luaD_hook (lua_State *L, int event, int line) {
  lua_Hook hook = L->hook;
  if (hook && L->allowhook) {  
    CallInfo *ci = L->ci;
    ptrdiff_t top = savestack(L, L->top);
    ptrdiff_t ci_top = savestack(L, ci->top);
    lua_Debug ar;
    ar.event = event;
    ar.currentline = line;
    ar.i_ci = ci;
    luaD_checkstack(L, LUA_MINSTACK);  
    ci->top = L->top + LUA_MINSTACK;
    lua_assert(ci->top <= L->stack_last);
    L->allowhook = 0;  
    ci->callstatus |= CIST_HOOKED;
    lua_unlock(L);
    (*hook)(L, &ar);
    lua_lock(L);
    lua_assert(!L->allowhook);
    L->allowhook = 1;
    ci->top = restorestack(L, ci_top);
    L->top = restorestack(L, top);
    ci->callstatus &= ~CIST_HOOKED;
  }
}
static void callhook (lua_State *L, CallInfo *ci) {
  int hook = LUA_HOOKCALL;
  ci->u.l.savedpc++;  
  if (isLua(ci->previous) &&
      GET_OPCODE(*(ci->previous->u.l.savedpc - 1)) == OP_TAILCALL) {
    ci->callstatus |= CIST_TAIL;
    hook = LUA_HOOKTAILCALL;
  }
  luaD_hook(L, hook, -1);
  ci->u.l.savedpc--;  
}
static StkId adjust_varargs (lua_State *L, Proto *p, int actual) {
  int i;
  int nfixargs = p->numparams;
  StkId base, fixed;
  fixed = L->top - actual;  
  base = L->top;  
  for (i = 0; i < nfixargs && i < actual; i++) {
    setobjs2s(L, L->top++, fixed + i);
    setnilvalue(fixed + i);  
  }
  for (; i < nfixargs; i++)
    setnilvalue(L->top++);  
  return base;
}
static void tryfuncTM (lua_State *L, StkId func) {
  const TValue *tm = luaT_gettmbyobj(L, func, TM_CALL);
  StkId p;
  if (!ttisfunction(tm))
    luaG_typeerror(L, func, "call");
  for (p = L->top; p > func; p--)
    setobjs2s(L, p, p-1);
  L->top++;  
  setobj2s(L, func, tm);  
}
static int moveresults (lua_State *L, const TValue *firstResult, StkId res,
                                      int nres, int wanted) {
  switch (wanted) {  
    case 0: break;  
    case 1: {  
      if (nres == 0)   
        firstResult = luaO_nilobject;  
      setobjs2s(L, res, firstResult);  
      break;
    }
    case LUA_MULTRET: {
      int i;
      for (i = 0; i < nres; i++)  
        setobjs2s(L, res + i, firstResult + i);
      L->top = res + nres;
      return 0;  
    }
    default: {
      int i;
      if (wanted <= nres) {  
        for (i = 0; i < wanted; i++)  
          setobjs2s(L, res + i, firstResult + i);
      }
      else {  
        for (i = 0; i < nres; i++)  
          setobjs2s(L, res + i, firstResult + i);
        for (; i < wanted; i++)  
          setnilvalue(res + i);
      }
      break;
    }
  }
  L->top = res + wanted;  
  return 1;
}
int luaD_poscall (lua_State *L, CallInfo *ci, StkId firstResult, int nres) {
  StkId res;
  int wanted = ci->nresults;
  if (L->hookmask & (LUA_MASKRET | LUA_MASKLINE)) {
    if (L->hookmask & LUA_MASKRET) {
      ptrdiff_t fr = savestack(L, firstResult);  
      luaD_hook(L, LUA_HOOKRET, -1);
      firstResult = restorestack(L, fr);
    }
    L->oldpc = ci->previous->u.l.savedpc;  
  }
  res = ci->func;  
  L->ci = ci->previous;  
  return moveresults(L, firstResult, res, nres, wanted);
}
#define next_ci(L) (L->ci = (L->ci->next ? L->ci->next : luaE_extendCI(L)))
#define checkstackp(L,n,p)  \
  luaD_checkstackaux(L, n, \
    ptrdiff_t t__ = savestack(L, p);   \
    luaC_checkGC(L),   \
    p = restorestack(L, t__))  
int luaD_precall (lua_State *L, StkId func, int nresults) {
  lua_CFunction f;
  CallInfo *ci;
  switch (ttype(func)) {
    case LUA_TCCL:  
      f = clCvalue(func)->f;
      goto Cfunc;
    case LUA_TLCF:  
      f = fvalue(func);
     Cfunc: {
      int n;  
      checkstackp(L, LUA_MINSTACK, func);  
      ci = next_ci(L);  
      ci->nresults = nresults;
      ci->func = func;
      ci->top = L->top + LUA_MINSTACK;
      lua_assert(ci->top <= L->stack_last);
      ci->callstatus = 0;
      if (L->hookmask & LUA_MASKCALL)
        luaD_hook(L, LUA_HOOKCALL, -1);
      lua_unlock(L);
      n = (*f)(L);  
      lua_lock(L);
      api_checknelems(L, n);
      luaD_poscall(L, ci, L->top - n, n);
      return 1;
    }
    case LUA_TLCL: {  
      StkId base;
      Proto *p = clLvalue(func)->p;
      int n = cast_int(L->top - func) - 1;  
      int fsize = p->maxstacksize;  
      checkstackp(L, fsize, func);
      if (p->is_vararg)
        base = adjust_varargs(L, p, n);
      else {  
        for (; n < p->numparams; n++)
          setnilvalue(L->top++);  
        base = func + 1;
      }
      ci = next_ci(L);  
      ci->nresults = nresults;
      ci->func = func;
      ci->u.l.base = base;
      L->top = ci->top = base + fsize;
      lua_assert(ci->top <= L->stack_last);
      ci->u.l.savedpc = p->code;  
      ci->callstatus = CIST_LUA;
      if (L->hookmask & LUA_MASKCALL)
        callhook(L, ci);
      return 0;
    }
    default: {  
      checkstackp(L, 1, func);  
      tryfuncTM(L, func);  
      return luaD_precall(L, func, nresults);  
    }
  }
}
static void stackerror (lua_State *L) {
  if (L->nCcalls == LUAI_MAXCCALLS)
    luaG_runerror(L, "C stack overflow");
  else if (L->nCcalls >= (LUAI_MAXCCALLS + (LUAI_MAXCCALLS>>3)))
    luaD_throw(L, LUA_ERRERR);  
}
void luaD_call (lua_State *L, StkId func, int nResults) {
  if (++L->nCcalls >= LUAI_MAXCCALLS)
    stackerror(L);
  if (!luaD_precall(L, func, nResults))  
    luaV_execute(L);  
  L->nCcalls--;
}
void luaD_callnoyield (lua_State *L, StkId func, int nResults) {
  L->nny++;
  luaD_call(L, func, nResults);
  L->nny--;
}
static void finishCcall (lua_State *L, int status) {
  CallInfo *ci = L->ci;
  int n;
  lua_assert(ci->u.c.k != NULL && L->nny == 0);
  lua_assert((ci->callstatus & CIST_YPCALL) || status == LUA_YIELD);
  if (ci->callstatus & CIST_YPCALL) {  
    ci->callstatus &= ~CIST_YPCALL;  
    L->errfunc = ci->u.c.old_errfunc;  
  }
  adjustresults(L, ci->nresults);
  lua_unlock(L);
  n = (*ci->u.c.k)(L, status, ci->u.c.ctx);  
  lua_lock(L);
  api_checknelems(L, n);
  luaD_poscall(L, ci, L->top - n, n);  
}
static void unroll (lua_State *L, void *ud) {
  if (ud != NULL)  
    finishCcall(L, *(int *)ud);  
  while (L->ci != &L->base_ci) {  
    if (!isLua(L->ci))  
      finishCcall(L, LUA_YIELD);  
    else {  
      luaV_finishOp(L);  
      luaV_execute(L);  
    }
  }
}
static CallInfo *findpcall (lua_State *L) {
  CallInfo *ci;
  for (ci = L->ci; ci != NULL; ci = ci->previous) {  
    if (ci->callstatus & CIST_YPCALL)
      return ci;
  }
  return NULL;  
}
static int recover (lua_State *L, int status) {
  StkId oldtop;
  CallInfo *ci = findpcall(L);
  if (ci == NULL) return 0;  
  oldtop = restorestack(L, ci->extra);
  luaF_close(L, oldtop, status);
  luaD_seterrorobj(L, status, oldtop);
  L->ci = ci;
  L->allowhook = getoah(ci->callstatus);  
  L->nny = 0;  
  luaD_shrinkstack(L);
  L->errfunc = ci->u.c.old_errfunc;
  return 1;  
}
static int resume_error (lua_State *L, const char *msg, int narg) {
  L->top -= narg;  
  setsvalue2s(L, L->top, luaS_new(L, msg));  
  api_incr_top(L);
  lua_unlock(L);
  return LUA_ERRRUN;
}
static void resume (lua_State *L, void *ud) {
  int n = *(cast(int*, ud));  
  StkId firstArg = L->top - n;  
  CallInfo *ci = L->ci;
  if (L->status == LUA_OK) {  
    if (!luaD_precall(L, firstArg - 1, LUA_MULTRET))  
      luaV_execute(L);  
  }
  else {  
    lua_assert(L->status == LUA_YIELD);
    L->status = LUA_OK;  
    ci->func = restorestack(L, ci->extra);
    if (isLua(ci))  
      luaV_execute(L);  
    else {  
      if (ci->u.c.k != NULL) {  
        lua_unlock(L);
        n = (*ci->u.c.k)(L, LUA_YIELD, ci->u.c.ctx); 
        lua_lock(L);
        api_checknelems(L, n);
        firstArg = L->top - n;  
      }
      luaD_poscall(L, ci, firstArg, n);  
    }
    unroll(L, NULL);  
  }
}
LUA_API int lua_resume (lua_State *L, lua_State *from, int nargs) {
  int status;
  unsigned short oldnny = L->nny;  
  lua_lock(L);
  if (L->status == LUA_OK) {  
    if (L->ci != &L->base_ci)  
      return resume_error(L, "cannot resume non-suspended coroutine", nargs);
  }
  else if (L->status != LUA_YIELD)
    return resume_error(L, "cannot resume dead coroutine", nargs);
  L->nCcalls = (from) ? from->nCcalls + 1 : 1;
  if (L->nCcalls >= LUAI_MAXCCALLS)
    return resume_error(L, "C stack overflow", nargs);
  luai_userstateresume(L, nargs);
  L->nny = 0;  
  api_checknelems(L, (L->status == LUA_OK) ? nargs + 1 : nargs);
  status = luaD_rawrunprotected(L, resume, &nargs);
  while (errorstatus(status) && recover(L, status)) {
    status = luaD_rawrunprotected(L, unroll, &status);
  }
  if (!errorstatus(status))
    lua_assert(status == L->status);  
  else {  
    L->status = cast_byte(status);  
    luaD_seterrorobj(L, status, L->top);  
    L->ci->top = L->top;
  }
  L->nny = oldnny;  
  L->nCcalls--;
  lua_assert(L->nCcalls == ((from) ? from->nCcalls : 0));
  lua_unlock(L);
  return status;
}
LUA_API int lua_isyieldable (lua_State *L) {
  return (L->nny == 0);
}
LUA_API int lua_yieldk (lua_State *L, int nresults, lua_KContext ctx,
                        lua_KFunction k) {
  CallInfo *ci = L->ci;
  luai_userstateyield(L, nresults);
  lua_lock(L);
  api_checknelems(L, nresults);
  if (L->nny > 0) {
    if (L != G(L)->mainthread)
      luaG_runerror(L, "attempt to yield across a C-call boundary");
    else
      luaG_runerror(L, "attempt to yield from outside a coroutine");
  }
  L->status = LUA_YIELD;
  ci->extra = savestack(L, ci->func);  
  if (isLua(ci)) {  
    api_check(L, k == NULL, "hooks cannot continue after yielding");
  }
  else {
    if ((ci->u.c.k = k) != NULL)  
      ci->u.c.ctx = ctx;  
    ci->func = L->top - nresults - 1;  
    luaD_throw(L, LUA_YIELD);
  }
  lua_assert(ci->callstatus & CIST_HOOKED);  
  lua_unlock(L);
  return 0;  
}
int luaD_pcall (lua_State *L, Pfunc func, void *u,
                ptrdiff_t old_top, ptrdiff_t ef) {
  int status;
  CallInfo *old_ci = L->ci;
  lu_byte old_allowhooks = L->allowhook;
  unsigned short old_nny = L->nny;
  ptrdiff_t old_errfunc = L->errfunc;
  L->errfunc = ef;
  status = luaD_rawrunprotected(L, func, u);
  if (status != LUA_OK) {  
    StkId oldtop = restorestack(L, old_top);
    L->ci = old_ci;
    L->allowhook = old_allowhooks;
    L->nny = old_nny;
    status = luaF_close(L, oldtop, status);  
    oldtop = restorestack(L, old_top);
    luaD_seterrorobj(L, status, oldtop);
    luaD_shrinkstack(L);
  }
  L->errfunc = old_errfunc;
  return status;
}
struct SParser {  
  ZIO *z;
  Mbuffer buff;  
  Dyndata dyd;  
  const char *mode;
  const char *name;
};
static void checkmode (lua_State *L, const char *mode, const char *x) {
  if (mode && strchr(mode, x[0]) == NULL) {
    luaO_pushfstring(L,
       "attempt to load a %s chunk (mode is '%s')", x, mode);
    luaD_throw(L, LUA_ERRSYNTAX);
  }
}
static void f_parser (lua_State *L, void *ud) {
  LClosure *cl;
  struct SParser *p = cast(struct SParser *, ud);
  int c = zgetc(p->z);  
  if (c == LUA_SIGNATURE[0]) {
    checkmode(L, p->mode, "binary");
    cl = luaU_undump(L, p->z, p->name);
  }
  else {
    checkmode(L, p->mode, "text");
    cl = luaY_parser(L, p->z, &p->buff, &p->dyd, p->name, c);
  }
  lua_assert(cl->nupvalues == cl->p->sizeupvalues);
  luaF_initupvals(L, cl);
}
int luaD_protectedparser (lua_State *L, ZIO *z, const char *name,
                                        const char *mode) {
  struct SParser p;
  int status;
  L->nny++;  
  p.z = z; p.name = name; p.mode = mode;
  p.dyd.actvar.arr = NULL; p.dyd.actvar.size = 0;
  p.dyd.gt.arr = NULL; p.dyd.gt.size = 0;
  p.dyd.label.arr = NULL; p.dyd.label.size = 0;
  luaZ_initbuffer(L, &p.buff);
  status = luaD_pcall(L, f_parser, &p, savestack(L, L->top), L->errfunc);
  luaZ_freebuffer(L, &p.buff);
  luaM_freearray(L, p.dyd.actvar.arr, p.dyd.actvar.size);
  luaM_freearray(L, p.dyd.gt.arr, p.dyd.gt.size);
  luaM_freearray(L, p.dyd.label.arr, p.dyd.label.size);
  L->nny--;
  return status;
}
#define ldump_c
#define LUA_CORE
#include <stddef.h>
typedef struct {
  lua_State *L;
  lua_Writer writer;
  void *data;
  int strip;
  int status;
} DumpState;
#define DumpVector(v,n,D)	DumpBlock(v,(n)*sizeof((v)[0]),D)
#define DumpLiteral(s,D)	DumpBlock(s, sizeof(s) - sizeof(char), D)
static void DumpBlock (const void *b, size_t size, DumpState *D) {
  if (D->status == 0 && size > 0) {
    lua_unlock(D->L);
    D->status = (*D->writer)(D->L, b, size, D->data);
    lua_lock(D->L);
  }
}
#define DumpVar(x,D)		DumpVector(&x,1,D)
static void DumpByte (int y, DumpState *D) {
  lu_byte x = (lu_byte)y;
  DumpVar(x, D);
}
static void DumpInt (int x, DumpState *D) {
  DumpVar(x, D);
}
static void DumpNumber (lua_Number x, DumpState *D) {
  DumpVar(x, D);
}
static void DumpInteger (lua_Integer x, DumpState *D) {
  DumpVar(x, D);
}
static void DumpString (const TString *s, DumpState *D) {
  if (s == NULL)
    DumpByte(0, D);
  else {
    size_t size = tsslen(s) + 1;  
    const char *str = getstr(s);
    if (size < 0xFF)
      DumpByte(cast_int(size), D);
    else {
      DumpByte(0xFF, D);
      DumpVar(size, D);
    }
    DumpVector(str, size - 1, D);  
  }
}
static void DumpCode (const Proto *f, DumpState *D) {
  DumpInt(f->sizecode, D);
  DumpVector(f->code, f->sizecode, D);
}
static void DumpFunction(const Proto *f, TString *psource, DumpState *D);
static void DumpConstants (const Proto *f, DumpState *D) {
  int i;
  int n = f->sizek;
  DumpInt(n, D);
  for (i = 0; i < n; i++) {
    const TValue *o = &f->k[i];
    DumpByte(ttype(o), D);
    switch (ttype(o)) {
    case LUA_TNIL:
      break;
    case LUA_TBOOLEAN:
      DumpByte(bvalue(o), D);
      break;
    case LUA_TNUMFLT:
      DumpNumber(fltvalue(o), D);
      break;
    case LUA_TNUMINT:
      DumpInteger(ivalue(o), D);
      break;
    case LUA_TSHRSTR:
    case LUA_TLNGSTR:
      DumpString(tsvalue(o), D);
      break;
    default:
      lua_assert(0);
    }
  }
}
static void DumpProtos (const Proto *f, DumpState *D) {
  int i;
  int n = f->sizep;
  DumpInt(n, D);
  for (i = 0; i < n; i++)
    DumpFunction(f->p[i], f->source, D);
}
static void DumpUpvalues (const Proto *f, DumpState *D) {
  int i, n = f->sizeupvalues;
  DumpInt(n, D);
  for (i = 0; i < n; i++) {
    DumpByte(f->upvalues[i].instack, D);
    DumpByte(f->upvalues[i].idx, D);
  }
}
static void DumpDebug (const Proto *f, DumpState *D) {
  int i, n;
  n = (D->strip) ? 0 : f->sizelineinfo;
  DumpInt(n, D);
  DumpVector(f->lineinfo, n, D);
  n = (D->strip) ? 0 : f->sizelocvars;
  DumpInt(n, D);
  for (i = 0; i < n; i++) {
    DumpString(f->locvars[i].varname, D);
    DumpInt(f->locvars[i].startpc, D);
    DumpInt(f->locvars[i].endpc, D);
  }
  n = (D->strip) ? 0 : f->sizeupvalues;
  DumpInt(n, D);
  for (i = 0; i < n; i++)
    DumpString(f->upvalues[i].name, D);
}
static void DumpFunction (const Proto *f, TString *psource, DumpState *D) {
  if (D->strip || f->source == psource)
    DumpString(NULL, D);  
  else
    DumpString(f->source, D);
  DumpInt(f->linedefined, D);
  DumpInt(f->lastlinedefined, D);
  DumpByte(f->numparams, D);
  DumpByte(f->is_vararg, D);
  DumpByte(f->maxstacksize, D);
  DumpCode(f, D);
  DumpConstants(f, D);
  DumpUpvalues(f, D);
  DumpProtos(f, D);
  DumpDebug(f, D);
}
static void DumpHeader (DumpState *D) {
  DumpLiteral(LUA_SIGNATURE, D);
  DumpByte(LUAC_VERSION, D);
  DumpByte(LUAC_FORMAT, D);
  DumpLiteral(LUAC_DATA, D);
  DumpByte(sizeof(int), D);
  DumpByte(sizeof(size_t), D);
  DumpByte(sizeof(Instruction), D);
  DumpByte(sizeof(lua_Integer), D);
  DumpByte(sizeof(lua_Number), D);
  DumpInteger(LUAC_INT, D);
  DumpNumber(LUAC_NUM, D);
}
int luaU_dump(lua_State *L, const Proto *f, lua_Writer w, void *data,
              int strip) {
  DumpState D;
  D.L = L;
  D.writer = w;
  D.data = data;
  D.strip = strip;
  D.status = 0;
  DumpHeader(&D);
  DumpByte(f->sizeupvalues, &D);
  DumpFunction(f, NULL, &D);
  return D.status;
}
#define lfunc_c
#define LUA_CORE
#include <stddef.h>
CClosure *luaF_newCclosure (lua_State *L, int n) {
  GCObject *o = luaC_newobj(L, LUA_TCCL, sizeCclosure(n));
  CClosure *c = gco2ccl(o);
  c->nupvalues = cast_byte(n);
  return c;
}
LClosure *luaF_newLclosure (lua_State *L, int n) {
  GCObject *o = luaC_newobj(L, LUA_TLCL, sizeLclosure(n));
  LClosure *c = gco2lcl(o);
  c->p = NULL;
  c->nupvalues = cast_byte(n);
  while (n--) c->upvals[n] = NULL;
  return c;
}
void luaF_initupvals (lua_State *L, LClosure *cl) {
  int i;
  for (i = 0; i < cl->nupvalues; i++) {
    UpVal *uv = luaM_new(L, UpVal);
    uv->refcount = 1;
    uv->v = &uv->u.value;  
    setnilvalue(uv->v);
    cl->upvals[i] = uv;
  }
}
UpVal *luaF_findupval (lua_State *L, StkId level) {
  UpVal **pp = &L->openupval;
  UpVal *p;
  UpVal *uv;
  lua_assert(isintwups(L) || L->openupval == NULL);
  while (*pp != NULL && (p = *pp)->v >= level) {
    lua_assert(upisopen(p));
    if (p->v == level && !p->flags)   {
      return p; 
    }
    pp = &p->u.open.next;
  }
  uv = luaM_new(L, UpVal);
  uv->refcount = 0;
  uv->flags = 0;
  uv->u.open.next = *pp;  
  uv->u.open.touched = 1;
  *pp = uv;
  uv->v = level;  
  if (!isintwups(L)) {  
    L->twups = G(L)->twups;  
    G(L)->twups = L;
  }
  return uv;
}
static void calldeferred(lua_State *L, void *ud) {
  UNUSED(ud);
  luaD_callnoyield(L, L->top - 2, 0);
}
static int preparetocall(lua_State *L, TValue *func, TValue *err) {
  StkId top = L->top;
  setobj2s(L, top, func);  
  if (err) {
    setobj2s(L, top + 1, err); 
  }
  else {
    setnilvalue(top + 1);
  }
  L->top = top + 2;  
  return 1;
}
static int calldeferredfunction(lua_State *L, StkId level, int status) {
  TValue *uv = level; 
  if (status == LUA_OK) {
    preparetocall(L, uv, NULL); 
    calldeferred(L, NULL);      
  }
  else { 
    ptrdiff_t oldtop;
    level++;                            
    oldtop = savestack(L, level + 1);   
    luaD_seterrorobj(L, status, level); 
    preparetocall(L, uv, level);
    int newstatus = luaD_pcall(L, calldeferred, NULL, oldtop, 0);
    if (newstatus != LUA_OK && status == CLOSEPROTECT) 
      status = newstatus;                    
    else {
      L->top = restorestack(L, oldtop);
    }
  }
  return status;
}
int luaF_close (lua_State *L, StkId level, int status) {
  UpVal *uv;
  while (L->openupval != NULL && (uv = L->openupval)->v >= level) {
    lua_assert(upisopen(uv));
    L->openupval = uv->u.open.next;  
    if (uv->refcount == 0) {        
      UpVal uv1 = *uv;              
      luaM_free(L, uv);             
      if (status != NOCLOSINGMETH && uv1.flags && ttisfunction(uv1.v)) {
        ptrdiff_t levelrel = savestack(L, level);
        status = calldeferredfunction(L, uv1.v, status);
        level = restorestack(L, levelrel);
      }
    }
    else {
      setobj(L, &uv->u.value, uv->v);  
      uv->v = &uv->u.value;  
      luaC_upvalbarrier(L, uv);
    }
  }
  return status;
}
Proto *luaF_newproto (lua_State *L) {
  GCObject *o = luaC_newobj(L, LUA_TPROTO, sizeof(Proto));
  Proto *f = gco2p(o);
  f->k = NULL;
  f->sizek = 0;
  f->p = NULL;
  f->sizep = 0;
  f->code = NULL;
  f->cache = NULL;
  f->sizecode = 0;
  f->lineinfo = NULL;
  f->sizelineinfo = 0;
  f->upvalues = NULL;
  f->sizeupvalues = 0;
  f->numparams = 0;
  f->is_vararg = 0;
  f->maxstacksize = 0;
  f->locvars = NULL;
  f->sizelocvars = 0;
  f->linedefined = 0;
  f->lastlinedefined = 0;
  f->source = NULL;
  return f;
}
void luaF_freeproto (lua_State *L, Proto *f) {
  luaM_freearray(L, f->code, f->sizecode);
  luaM_freearray(L, f->p, f->sizep);
  luaM_freearray(L, f->k, f->sizek);
  luaM_freearray(L, f->lineinfo, f->sizelineinfo);
  luaM_freearray(L, f->locvars, f->sizelocvars);
  luaM_freearray(L, f->upvalues, f->sizeupvalues);
  luaM_free(L, f);
}
const char *luaF_getlocalname (const Proto *f, int local_number, int pc) {
  int i;
  for (i = 0; i<f->sizelocvars && f->locvars[i].startpc <= pc; i++) {
    if (pc < f->locvars[i].endpc) {  
      local_number--;
      if (local_number == 0)
        return getstr(f->locvars[i].varname);
    }
  }
  return NULL;  
}
#define lgc_c
#define LUA_CORE
#include <string.h>
#define GCSinsideatomic		(GCSpause + 1)
#define GCSWEEPCOST	((sizeof(TString) + 4) / 4)
#define GCSWEEPMAX	(cast_int((GCSTEPSIZE / GCSWEEPCOST) / 4))
#define GCFINALIZECOST	GCSWEEPCOST
#define STEPMULADJ		200
#define PAUSEADJ		100
#define maskcolors	(~(bitmask(BLACKBIT) | WHITEBITS))
#define makewhite(g,x)	\
 (x->marked = cast_byte((x->marked & maskcolors) | luaC_white(g)))
#define white2gray(x)	resetbits(x->marked, WHITEBITS)
#define black2gray(x)	resetbit(x->marked, BLACKBIT)
#define valiswhite(x)   (iscollectable(x) && iswhite(gcvalue(x)))
#define checkdeadkey(n)	lua_assert(!ttisdeadkey(gkey(n)) || ttisnil(gval(n)))
#define checkconsistency(obj)  \
  lua_longassert(!iscollectable(obj) || righttt(obj))
#define markvalue(g,o) { checkconsistency(o); \
  if (valiswhite(o)) reallymarkobject(g,gcvalue(o)); }
#define markobject(g,t)	{ if (iswhite(t)) reallymarkobject(g, obj2gco(t)); }
#define markobjectN(g,t)	{ if (t) markobject(g,t); }
static void reallymarkobject (global_State *g, GCObject *o);
#define gnodelast(h)	gnode(h, cast(size_t, sizenode(h)))
#define linkgclist(o,p)	((o)->gclist = (p), (p) = obj2gco(o))
static void removeentry (Node *n) {
  lua_assert(ttisnil(gval(n)));
  if (valiswhite(gkey(n)))
    setdeadvalue(wgkey(n));  
}
static int iscleared (global_State *g, const TValue *o) {
  if (!iscollectable(o)) return 0;
  else if (ttisstring(o)) {
    markobject(g, tsvalue(o));  
    return 0;
  }
  else return iswhite(gcvalue(o));
}
void luaC_barrier_ (lua_State *L, GCObject *o, GCObject *v) {
  global_State *g = G(L);
  lua_assert(isblack(o) && iswhite(v) && !isdead(g, v) && !isdead(g, o));
  if (keepinvariant(g))  
    reallymarkobject(g, v);  
  else {  
    lua_assert(issweepphase(g));
    makewhite(g, o);  
  }
}
void luaC_barrierback_ (lua_State *L, Table *t) {
  global_State *g = G(L);
  lua_assert(isblack(t) && !isdead(g, t));
  black2gray(t);  
  linkgclist(t, g->grayagain);
}
void luaC_upvalbarrier_ (lua_State *L, UpVal *uv) {
  global_State *g = G(L);
  GCObject *o = gcvalue(uv->v);
  lua_assert(!upisopen(uv));  
  if (keepinvariant(g))
    markobject(g, o);
}
void luaC_fix (lua_State *L, GCObject *o) {
  global_State *g = G(L);
  lua_assert(g->allgc == o);  
  white2gray(o);  
  g->allgc = o->next;  
  o->next = g->fixedgc;  
  g->fixedgc = o;
}
GCObject *luaC_newobj (lua_State *L, int tt, size_t sz) {
  global_State *g = G(L);
  GCObject *o = cast(GCObject *, luaM_newobject(L, novariant(tt), sz));
  o->marked = luaC_white(g);
  o->tt = tt;
  o->next = g->allgc;
  g->allgc = o;
  return o;
}
static void reallymarkobject (global_State *g, GCObject *o) {
 reentry:
  white2gray(o);
  switch (o->tt) {
    case LUA_TSHRSTR: {
      gray2black(o);
      g->GCmemtrav += sizelstring(gco2ts(o)->shrlen);
      break;
    }
    case LUA_TLNGSTR: {
      gray2black(o);
      g->GCmemtrav += sizelstring(gco2ts(o)->u.lnglen);
      break;
    }
    case LUA_TUSERDATA: {
      TValue uvalue;
      markobjectN(g, gco2u(o)->metatable);  
      gray2black(o);
      g->GCmemtrav += sizeudata(gco2u(o));
      getuservalue(g->mainthread, gco2u(o), &uvalue);
      if (valiswhite(&uvalue)) {  
        o = gcvalue(&uvalue);
        goto reentry;
      }
      break;
    }
    case LUA_TLCL: {
      linkgclist(gco2lcl(o), g->gray);
      break;
    }
    case LUA_TCCL: {
      linkgclist(gco2ccl(o), g->gray);
      break;
    }
    case LUA_TTABLE: {
      linkgclist(gco2t(o), g->gray);
      break;
    }
    case LUA_TTHREAD: {
      linkgclist(gco2th(o), g->gray);
      break;
    }
    case LUA_TPROTO: {
      linkgclist(gco2p(o), g->gray);
      break;
    }
    default: lua_assert(0); break;
  }
}
static void markmt (global_State *g) {
  int i;
  for (i=0; i < LUA_NUMTAGS; i++)
    markobjectN(g, g->mt[i]);
}
static void markbeingfnz (global_State *g) {
  GCObject *o;
  for (o = g->tobefnz; o != NULL; o = o->next)
    markobject(g, o);
}
static void remarkupvals (global_State *g) {
  lua_State *thread;
  lua_State **p = &g->twups;
  while ((thread = *p) != NULL) {
    lua_assert(!isblack(thread));  
    if (isgray(thread) && thread->openupval != NULL)
      p = &thread->twups;  
    else {  
      UpVal *uv;
      *p = thread->twups;  
      thread->twups = thread;  
      for (uv = thread->openupval; uv != NULL; uv = uv->u.open.next) {
        if (uv->u.open.touched) {
          markvalue(g, uv->v);  
          uv->u.open.touched = 0;
        }
      }
    }
  }
}
static void restartcollection (global_State *g) {
  g->gray = g->grayagain = NULL;
  g->weak = g->allweak = g->ephemeron = NULL;
  markobject(g, g->mainthread);
  markvalue(g, &g->l_registry);
  markmt(g);
  markbeingfnz(g);  
}
static void traverseweakvalue (global_State *g, Table *h) {
  Node *n, *limit = gnodelast(h);
  int hasclears = (h->sizearray > 0);
  for (n = gnode(h, 0); n < limit; n++) {  
    checkdeadkey(n);
    if (ttisnil(gval(n)))  
      removeentry(n);  
    else {
      lua_assert(!ttisnil(gkey(n)));
      markvalue(g, gkey(n));  
      if (!hasclears && iscleared(g, gval(n)))  
        hasclears = 1;  
    }
  }
  if (g->gcstate == GCSpropagate)
    linkgclist(h, g->grayagain);  
  else if (hasclears)
    linkgclist(h, g->weak);  
}
static int traverseephemeron (global_State *g, Table *h) {
  int marked = 0;  
  int hasclears = 0;  
  int hasww = 0;  
  Node *n, *limit = gnodelast(h);
  unsigned int i;
  for (i = 0; i < h->sizearray; i++) {
    if (valiswhite(&h->array[i])) {
      marked = 1;
      reallymarkobject(g, gcvalue(&h->array[i]));
    }
  }
  for (n = gnode(h, 0); n < limit; n++) {
    checkdeadkey(n);
    if (ttisnil(gval(n)))  
      removeentry(n);  
    else if (iscleared(g, gkey(n))) {  
      hasclears = 1;  
      if (valiswhite(gval(n)))  
        hasww = 1;  
    }
    else if (valiswhite(gval(n))) {  
      marked = 1;
      reallymarkobject(g, gcvalue(gval(n)));  
    }
  }
  if (g->gcstate == GCSpropagate)
    linkgclist(h, g->grayagain);  
  else if (hasww)  
    linkgclist(h, g->ephemeron);  
  else if (hasclears)  
    linkgclist(h, g->allweak);  
  return marked;
}
static void traversestrongtable (global_State *g, Table *h) {
  Node *n, *limit = gnodelast(h);
  unsigned int i;
  for (i = 0; i < h->sizearray; i++)  
    markvalue(g, &h->array[i]);
  for (n = gnode(h, 0); n < limit; n++) {  
    checkdeadkey(n);
    if (ttisnil(gval(n)))  
      removeentry(n);  
    else {
      lua_assert(!ttisnil(gkey(n)));
      markvalue(g, gkey(n));  
      markvalue(g, gval(n));  
    }
  }
}
static lu_mem traversetable (global_State *g, Table *h) {
  const char *weakkey, *weakvalue;
  const TValue *mode = gfasttm(g, h->metatable, TM_MODE);
  markobjectN(g, h->metatable);
  if (mode && ttisstring(mode) &&  
      ((weakkey = strchr(svalue(mode), 'k')),
       (weakvalue = strchr(svalue(mode), 'v')),
       (weakkey || weakvalue))) {  
    black2gray(h);  
    if (!weakkey)  
      traverseweakvalue(g, h);
    else if (!weakvalue)  
      traverseephemeron(g, h);
    else  
      linkgclist(h, g->allweak);  
  }
  else  
    traversestrongtable(g, h);
  return sizeof(Table) + sizeof(TValue) * h->sizearray +
                         sizeof(Node) * cast(size_t, allocsizenode(h));
}
static int traverseproto (global_State *g, Proto *f) {
  int i;
  if (f->cache && iswhite(f->cache))
    f->cache = NULL;  
  markobjectN(g, f->source);
  for (i = 0; i < f->sizek; i++)  
    markvalue(g, &f->k[i]);
  for (i = 0; i < f->sizeupvalues; i++)  
    markobjectN(g, f->upvalues[i].name);
  for (i = 0; i < f->sizep; i++)  
    markobjectN(g, f->p[i]);
  for (i = 0; i < f->sizelocvars; i++)  
    markobjectN(g, f->locvars[i].varname);
  return sizeof(Proto) + sizeof(Instruction) * f->sizecode +
                         sizeof(Proto *) * f->sizep +
                         sizeof(TValue) * f->sizek +
                         sizeof(int) * f->sizelineinfo +
                         sizeof(LocVar) * f->sizelocvars +
                         sizeof(Upvaldesc) * f->sizeupvalues;
}
static lu_mem traverseCclosure (global_State *g, CClosure *cl) {
  int i;
  for (i = 0; i < cl->nupvalues; i++)  
    markvalue(g, &cl->upvalue[i]);
  return sizeCclosure(cl->nupvalues);
}
static lu_mem traverseLclosure (global_State *g, LClosure *cl) {
  int i;
  markobjectN(g, cl->p);  
  for (i = 0; i < cl->nupvalues; i++) {  
    UpVal *uv = cl->upvals[i];
    if (uv != NULL) {
      if (upisopen(uv) && g->gcstate != GCSinsideatomic)
        uv->u.open.touched = 1;  
      else
        markvalue(g, uv->v);
    }
  }
  return sizeLclosure(cl->nupvalues);
}
static lu_mem traversethread (global_State *g, lua_State *th) {
  StkId o = th->stack;
  if (o == NULL)
    return 1;  
  lua_assert(g->gcstate == GCSinsideatomic ||
             th->openupval == NULL || isintwups(th));
  for (; o < th->top; o++)  
    markvalue(g, o);
  if (g->gcstate == GCSinsideatomic) {  
    StkId lim = th->stack + th->stacksize;  
    for (; o < lim; o++)  
      setnilvalue(o);
    if (!isintwups(th) && th->openupval != NULL) {
      th->twups = g->twups;  
      g->twups = th;
    }
  }
  else if (g->gckind != KGC_EMERGENCY)
    luaD_shrinkstack(th); 
  return (sizeof(lua_State) + sizeof(TValue) * th->stacksize +
          sizeof(CallInfo) * th->nci);
}
static void propagatemark (global_State *g) {
  lu_mem size;
  GCObject *o = g->gray;
  lua_assert(isgray(o));
  gray2black(o);
  switch (o->tt) {
    case LUA_TTABLE: {
      Table *h = gco2t(o);
      g->gray = h->gclist;  
      size = traversetable(g, h);
      break;
    }
    case LUA_TLCL: {
      LClosure *cl = gco2lcl(o);
      g->gray = cl->gclist;  
      size = traverseLclosure(g, cl);
      break;
    }
    case LUA_TCCL: {
      CClosure *cl = gco2ccl(o);
      g->gray = cl->gclist;  
      size = traverseCclosure(g, cl);
      break;
    }
    case LUA_TTHREAD: {
      lua_State *th = gco2th(o);
      g->gray = th->gclist;  
      linkgclist(th, g->grayagain);  
      black2gray(o);
      size = traversethread(g, th);
      break;
    }
    case LUA_TPROTO: {
      Proto *p = gco2p(o);
      g->gray = p->gclist;  
      size = traverseproto(g, p);
      break;
    }
    default: lua_assert(0); return;
  }
  g->GCmemtrav += size;
}
static void propagateall (global_State *g) {
  while (g->gray) propagatemark(g);
}
static void convergeephemerons (global_State *g) {
  int changed;
  do {
    GCObject *w;
    GCObject *next = g->ephemeron;  
    g->ephemeron = NULL;  
    changed = 0;
    while ((w = next) != NULL) {
      next = gco2t(w)->gclist;
      if (traverseephemeron(g, gco2t(w))) {  
        propagateall(g);  
        changed = 1;  
      }
    }
  } while (changed);
}
static void clearkeys (global_State *g, GCObject *l, GCObject *f) {
  for (; l != f; l = gco2t(l)->gclist) {
    Table *h = gco2t(l);
    Node *n, *limit = gnodelast(h);
    for (n = gnode(h, 0); n < limit; n++) {
      if (!ttisnil(gval(n)) && (iscleared(g, gkey(n)))) {
        setnilvalue(gval(n));  
      }
      if (ttisnil(gval(n)))  
        removeentry(n);  
    }
  }
}
static void clearvalues (global_State *g, GCObject *l, GCObject *f) {
  for (; l != f; l = gco2t(l)->gclist) {
    Table *h = gco2t(l);
    Node *n, *limit = gnodelast(h);
    unsigned int i;
    for (i = 0; i < h->sizearray; i++) {
      TValue *o = &h->array[i];
      if (iscleared(g, o))  
        setnilvalue(o);  
    }
    for (n = gnode(h, 0); n < limit; n++) {
      if (!ttisnil(gval(n)) && iscleared(g, gval(n))) {
        setnilvalue(gval(n));  
        removeentry(n);  
      }
    }
  }
}
void luaC_upvdeccount (lua_State *L, UpVal *uv) {
  lua_assert(uv->refcount > 0);
  uv->refcount--;
  if (uv->refcount == 0 && !upisopen(uv))
    luaM_free(L, uv);
}
static void freeLclosure (lua_State *L, LClosure *cl) {
  int i;
  for (i = 0; i < cl->nupvalues; i++) {
    UpVal *uv = cl->upvals[i];
    if (uv)
      luaC_upvdeccount(L, uv);
  }
  luaM_freemem(L, cl, sizeLclosure(cl->nupvalues));
}
static void freeobj (lua_State *L, GCObject *o) {
  switch (o->tt) {
    case LUA_TPROTO: luaF_freeproto(L, gco2p(o)); break;
    case LUA_TLCL: {
      freeLclosure(L, gco2lcl(o));
      break;
    }
    case LUA_TCCL: {
      luaM_freemem(L, o, sizeCclosure(gco2ccl(o)->nupvalues));
      break;
    }
    case LUA_TTABLE: luaH_free(L, gco2t(o)); break;
    case LUA_TTHREAD: luaE_freethread(L, gco2th(o)); break;
    case LUA_TUSERDATA: luaM_freemem(L, o, sizeudata(gco2u(o))); break;
    case LUA_TSHRSTR:
      luaS_remove(L, gco2ts(o));  
      luaM_freemem(L, o, sizelstring(gco2ts(o)->shrlen));
      break;
    case LUA_TLNGSTR: {
      luaM_freemem(L, o, sizelstring(gco2ts(o)->u.lnglen));
      break;
    }
    default: lua_assert(0);
  }
}
#define sweepwholelist(L,p)	sweeplist(L,p,MAX_LUMEM)
static GCObject **sweeplist (lua_State *L, GCObject **p, lu_mem count);
static GCObject **sweeplist (lua_State *L, GCObject **p, lu_mem count) {
  global_State *g = G(L);
  int ow = otherwhite(g);
  int white = luaC_white(g);  
  while (*p != NULL && count-- > 0) {
    GCObject *curr = *p;
    int marked = curr->marked;
    if (isdeadm(ow, marked)) {  
      *p = curr->next;  
      freeobj(L, curr);  
    }
    else {  
      curr->marked = cast_byte((marked & maskcolors) | white);
      p = &curr->next;  
    }
  }
  return (*p == NULL) ? NULL : p;
}
static GCObject **sweeptolive (lua_State *L, GCObject **p) {
  GCObject **old = p;
  do {
    p = sweeplist(L, p, 1);
  } while (p == old);
  return p;
}
static void checkSizes (lua_State *L, global_State *g) {
  if (g->gckind != KGC_EMERGENCY) {
    l_mem olddebt = g->GCdebt;
    if (g->strt.nuse < g->strt.size / 4)  
      luaS_resize(L, g->strt.size / 2);  
    g->GCestimate += g->GCdebt - olddebt;  
  }
}
static GCObject *udata2finalize (global_State *g) {
  GCObject *o = g->tobefnz;  
  lua_assert(tofinalize(o));
  g->tobefnz = o->next;  
  o->next = g->allgc;  
  g->allgc = o;
  resetbit(o->marked, FINALIZEDBIT);  
  if (issweepphase(g))
    makewhite(g, o);  
  return o;
}
static void dothecall (lua_State *L, void *ud) {
  UNUSED(ud);
  luaD_callnoyield(L, L->top - 2, 0);
}
static void GCTM (lua_State *L, int propagateerrors) {
  global_State *g = G(L);
  const TValue *tm;
  TValue v;
  setgcovalue(L, &v, udata2finalize(g));
  tm = luaT_gettmbyobj(L, &v, TM_GC);
  if (tm != NULL && ttisfunction(tm)) {  
    int status;
    lu_byte oldah = L->allowhook;
    int running  = g->gcrunning;
    L->allowhook = 0;  
    g->gcrunning = 0;  
    setobj2s(L, L->top, tm);  
    setobj2s(L, L->top + 1, &v);  
    L->top += 2;  
    L->ci->callstatus |= CIST_FIN;  
    status = luaD_pcall(L, dothecall, NULL, savestack(L, L->top - 2), 0);
    L->ci->callstatus &= ~CIST_FIN;  
    L->allowhook = oldah;  
    g->gcrunning = running;  
    if (status != LUA_OK && propagateerrors) {  
      if (status == LUA_ERRRUN) {  
        const char *msg = (ttisstring(L->top - 1))
                            ? svalue(L->top - 1)
                            : "no message";
        luaO_pushfstring(L, "error in __gc metamethod (%s)", msg);
        status = LUA_ERRGCMM;  
      }
      luaD_throw(L, status);  
    }
  }
}
static int runafewfinalizers (lua_State *L) {
  global_State *g = G(L);
  unsigned int i;
  lua_assert(!g->tobefnz || g->gcfinnum > 0);
  for (i = 0; g->tobefnz && i < g->gcfinnum; i++)
    GCTM(L, 1);  
  g->gcfinnum = (!g->tobefnz) ? 0  
                    : g->gcfinnum * 2;  
  return i;
}
static void callallpendingfinalizers (lua_State *L) {
  global_State *g = G(L);
  while (g->tobefnz)
    GCTM(L, 0);
}
static GCObject **findlast (GCObject **p) {
  while (*p != NULL)
    p = &(*p)->next;
  return p;
}
static void separatetobefnz (global_State *g, int all) {
  GCObject *curr;
  GCObject **p = &g->finobj;
  GCObject **lastnext = findlast(&g->tobefnz);
  while ((curr = *p) != NULL) {  
    lua_assert(tofinalize(curr));
    if (!(iswhite(curr) || all))  
      p = &curr->next;  
    else {
      *p = curr->next;  
      curr->next = *lastnext;  
      *lastnext = curr;
      lastnext = &curr->next;
    }
  }
}
void luaC_checkfinalizer (lua_State *L, GCObject *o, Table *mt) {
  global_State *g = G(L);
  if (tofinalize(o) ||                 
      gfasttm(g, mt, TM_GC) == NULL)   
    return;  
  else {  
    GCObject **p;
    if (issweepphase(g)) {
      makewhite(g, o);  
      if (g->sweepgc == &o->next)  
        g->sweepgc = sweeptolive(L, g->sweepgc);  
    }
    for (p = &g->allgc; *p != o; p = &(*p)->next) {  }
    *p = o->next;  
    o->next = g->finobj;  
    g->finobj = o;
    l_setbit(o->marked, FINALIZEDBIT);  
  }
}
static void setpause (global_State *g) {
  l_mem threshold, debt;
  l_mem estimate = g->GCestimate / PAUSEADJ;  
  lua_assert(estimate > 0);
  threshold = (g->gcpause < MAX_LMEM / estimate)  
            ? estimate * g->gcpause  
            : MAX_LMEM;  
  debt = gettotalbytes(g) - threshold;
  luaE_setdebt(g, debt);
}
static void entersweep (lua_State *L) {
  global_State *g = G(L);
  g->gcstate = GCSswpallgc;
  lua_assert(g->sweepgc == NULL);
  g->sweepgc = sweeplist(L, &g->allgc, 1);
}
void luaC_freeallobjects (lua_State *L) {
  global_State *g = G(L);
  separatetobefnz(g, 1);  
  lua_assert(g->finobj == NULL);
  callallpendingfinalizers(L);
  lua_assert(g->tobefnz == NULL);
  g->currentwhite = WHITEBITS; 
  g->gckind = KGC_NORMAL;
  sweepwholelist(L, &g->finobj);
  sweepwholelist(L, &g->allgc);
  sweepwholelist(L, &g->fixedgc);  
  lua_assert(g->strt.nuse == 0);
}
static l_mem atomic (lua_State *L) {
  global_State *g = G(L);
  l_mem work;
  GCObject *origweak, *origall;
  GCObject *grayagain = g->grayagain;  
  lua_assert(g->ephemeron == NULL && g->weak == NULL);
  lua_assert(!iswhite(g->mainthread));
  g->gcstate = GCSinsideatomic;
  g->GCmemtrav = 0;  
  markobject(g, L);  
  markvalue(g, &g->l_registry);
  markmt(g);  
  remarkupvals(g);
  propagateall(g);  
  work = g->GCmemtrav;  
  g->gray = grayagain;
  propagateall(g);  
  g->GCmemtrav = 0;  
  convergeephemerons(g);
  clearvalues(g, g->weak, NULL);
  clearvalues(g, g->allweak, NULL);
  origweak = g->weak; origall = g->allweak;
  work += g->GCmemtrav;  
  separatetobefnz(g, 0);  
  g->gcfinnum = 1;  
  markbeingfnz(g);  
  propagateall(g);  
  g->GCmemtrav = 0;  
  convergeephemerons(g);
  clearkeys(g, g->ephemeron, NULL);  
  clearkeys(g, g->allweak, NULL);  
  clearvalues(g, g->weak, origweak);
  clearvalues(g, g->allweak, origall);
  luaS_clearcache(g);
  g->currentwhite = cast_byte(otherwhite(g));  
  work += g->GCmemtrav;  
  return work;  
}
static lu_mem sweepstep (lua_State *L, global_State *g,
                         int nextstate, GCObject **nextlist) {
  if (g->sweepgc) {
    l_mem olddebt = g->GCdebt;
    g->sweepgc = sweeplist(L, g->sweepgc, GCSWEEPMAX);
    g->GCestimate += g->GCdebt - olddebt;  
    if (g->sweepgc)  
      return (GCSWEEPMAX * GCSWEEPCOST);
  }
  g->gcstate = nextstate;
  g->sweepgc = nextlist;
  return 0;
}
static lu_mem singlestep (lua_State *L) {
  global_State *g = G(L);
  switch (g->gcstate) {
    case GCSpause: {
      g->GCmemtrav = g->strt.size * sizeof(GCObject*);
      restartcollection(g);
      g->gcstate = GCSpropagate;
      return g->GCmemtrav;
    }
    case GCSpropagate: {
      g->GCmemtrav = 0;
      lua_assert(g->gray);
      propagatemark(g);
       if (g->gray == NULL)  
        g->gcstate = GCSatomic;  
      return g->GCmemtrav;  
    }
    case GCSatomic: {
      lu_mem work;
      propagateall(g);  
      work = atomic(L);  
      entersweep(L);
      g->GCestimate = gettotalbytes(g);  ;
      return work;
    }
    case GCSswpallgc: {  
      return sweepstep(L, g, GCSswpfinobj, &g->finobj);
    }
    case GCSswpfinobj: {  
      return sweepstep(L, g, GCSswptobefnz, &g->tobefnz);
    }
    case GCSswptobefnz: {  
      return sweepstep(L, g, GCSswpend, NULL);
    }
    case GCSswpend: {  
      makewhite(g, g->mainthread);  
      checkSizes(L, g);
      g->gcstate = GCScallfin;
      return 0;
    }
    case GCScallfin: {  
      if (g->tobefnz && g->gckind != KGC_EMERGENCY) {
        int n = runafewfinalizers(L);
        return (n * GCFINALIZECOST);
      }
      else {  
        g->gcstate = GCSpause;  
        return 0;
      }
    }
    default: lua_assert(0); return 0;
  }
}
void luaC_runtilstate (lua_State *L, int statesmask) {
  global_State *g = G(L);
  while (!testbit(statesmask, g->gcstate))
    singlestep(L);
}
static l_mem getdebt (global_State *g) {
  l_mem debt = g->GCdebt;
  int stepmul = g->gcstepmul;
  if (debt <= 0) return 0;  
  else {
    debt = (debt / STEPMULADJ) + 1;
    debt = (debt < MAX_LMEM / stepmul) ? debt * stepmul : MAX_LMEM;
    return debt;
  }
}
void luaC_step (lua_State *L) {
  global_State *g = G(L);
  l_mem debt = getdebt(g);  
  if (!g->gcrunning) {  
    luaE_setdebt(g, -GCSTEPSIZE * 10);  
    return;
  }
  do {  
    lu_mem work = singlestep(L);  
    debt -= work;
  } while (debt > -GCSTEPSIZE && g->gcstate != GCSpause);
  if (g->gcstate == GCSpause)
    setpause(g);  
  else {
    debt = (debt / g->gcstepmul) * STEPMULADJ;  
    luaE_setdebt(g, debt);
    runafewfinalizers(L);
  }
}
void luaC_fullgc (lua_State *L, int isemergency) {
  global_State *g = G(L);
  lua_assert(g->gckind == KGC_NORMAL);
  if (isemergency) g->gckind = KGC_EMERGENCY;  
  if (keepinvariant(g)) {  
    entersweep(L); 
  }
  luaC_runtilstate(L, bitmask(GCSpause));
  luaC_runtilstate(L, ~bitmask(GCSpause));  
  luaC_runtilstate(L, bitmask(GCScallfin));  
  lua_assert(g->GCestimate == gettotalbytes(g));
  luaC_runtilstate(L, bitmask(GCSpause));  
  g->gckind = KGC_NORMAL;
  setpause(g);
}
#define llex_c
#define LUA_CORE
#include <locale.h>
#include <string.h>
#define next(ls) (ls->current = zgetc(ls->z))
#define currIsNewline(ls)	(ls->current == '\n' || ls->current == '\r')
static const char *const luaX_tokens [] = {
    "and", "break", "do", "else", "elseif",
    "end", "false", "for", "function", "goto", "if",
    "in", "local", "defer", "nil", "not", "or", "repeat",
    "return", "then", "true", "until", "while",
    "//", "..", "...", "==", ">=", "<=", "~=",
    "<<", ">>", "::", "<eof>",
    "<number>", "<integer>", "<name>", "<string>"
};
#define save_and_next(ls) (save(ls, ls->current), next(ls))
static l_noret lexerror (LexState *ls, const char *msg, int token);
static void save (LexState *ls, int c) {
  Mbuffer *b = ls->buff;
  if (luaZ_bufflen(b) + 1 > luaZ_sizebuffer(b)) {
    size_t newsize;
    if (luaZ_sizebuffer(b) >= MAX_SIZE/2)
      lexerror(ls, "lexical element too long", 0);
    newsize = luaZ_sizebuffer(b) * 2;
    luaZ_resizebuffer(ls->L, b, newsize);
  }
  b->buffer[luaZ_bufflen(b)++] = cast(char, c);
}
void luaX_init (lua_State *L) {
  int i;
  TString *e = luaS_newliteral(L, LUA_ENV);  
  luaC_fix(L, obj2gco(e));  
  for (i=0; i<NUM_RESERVED; i++) {
    TString *ts = luaS_new(L, luaX_tokens[i]);
    luaC_fix(L, obj2gco(ts));  
    ts->extra = cast_byte(i+1);  
  }
}
const char *luaX_token2str (LexState *ls, int token) {
  if (token < FIRST_RESERVED) {  
    lua_assert(token == cast_uchar(token));
    return luaO_pushfstring(ls->L, "'%c'", token);
  }
  else {
    const char *s = luaX_tokens[token - FIRST_RESERVED];
    if (token < TK_EOS)  
      return luaO_pushfstring(ls->L, "'%s'", s);
    else  
      return s;
  }
}
static const char *txtToken (LexState *ls, int token) {
  switch (token) {
    case TK_NAME: case TK_STRING:
    case TK_FLT: case TK_INT:
      save(ls, '\0');
      return luaO_pushfstring(ls->L, "'%s'", luaZ_buffer(ls->buff));
    default:
      return luaX_token2str(ls, token);
  }
}
static l_noret lexerror (LexState *ls, const char *msg, int token) {
  msg = luaG_addinfo(ls->L, msg, ls->source, ls->linenumber);
  if (token)
    luaO_pushfstring(ls->L, "%s near %s", msg, txtToken(ls, token));
  luaD_throw(ls->L, LUA_ERRSYNTAX);
}
l_noret luaX_syntaxerror (LexState *ls, const char *msg) {
  lexerror(ls, msg, ls->t.token);
}
TString *luaX_newstring (LexState *ls, const char *str, size_t l) {
  lua_State *L = ls->L;
  TValue *o;  
  TString *ts = luaS_newlstr(L, str, l);  
  setsvalue2s(L, L->top++, ts);  
  o = luaH_set(L, ls->h, L->top - 1);
  if (ttisnil(o)) {  
    setbvalue(o, 1);  
    luaC_checkGC(L);
  }
  else {  
    ts = tsvalue(keyfromval(o));  
  }
  L->top--;  
  return ts;
}
static void inclinenumber (LexState *ls) {
  int old = ls->current;
  lua_assert(currIsNewline(ls));
  next(ls);  
  if (currIsNewline(ls) && ls->current != old)
    next(ls);  
  if (++ls->linenumber >= MAX_INT)
    lexerror(ls, "chunk has too many lines", 0);
}
void luaX_setinput (lua_State *L, LexState *ls, ZIO *z, TString *source,
                    int firstchar) {
  ls->t.token = 0;
  ls->L = L;
  ls->current = firstchar;
  ls->lookahead.token = TK_EOS;  
  ls->z = z;
  ls->fs = NULL;
  ls->linenumber = 1;
  ls->lastline = 1;
  ls->source = source;
  ls->envn = luaS_newliteral(L, LUA_ENV);  
  luaZ_resizebuffer(ls->L, ls->buff, LUA_MINBUFFER);  
}
static int check_next1 (LexState *ls, int c) {
  if (ls->current == c) {
    next(ls);
    return 1;
  }
  else return 0;
}
static int check_next2 (LexState *ls, const char *set) {
  lua_assert(set[2] == '\0');
  if (ls->current == set[0] || ls->current == set[1]) {
    save_and_next(ls);
    return 1;
  }
  else return 0;
}
static int read_numeral (LexState *ls, SemInfo *seminfo) {
  TValue obj;
  const char *expo = "Ee";
  int first = ls->current;
  lua_assert(lisdigit(ls->current));
  save_and_next(ls);
  if (first == '0' && check_next2(ls, "xX"))  
    expo = "Pp";
  for (;;) {
    if (check_next2(ls, expo))  
      check_next2(ls, "-+");  
    if (lisxdigit(ls->current))
      save_and_next(ls);
    else if (ls->current == '.')
      save_and_next(ls);
    else break;
  }
  save(ls, '\0');
  if (luaO_str2num(luaZ_buffer(ls->buff), &obj) == 0)  
    lexerror(ls, "malformed number", TK_FLT);
  if (ttisinteger(&obj)) {
    seminfo->i = ivalue(&obj);
    return TK_INT;
  }
  else {
    lua_assert(ttisfloat(&obj));
    seminfo->r = fltvalue(&obj);
    return TK_FLT;
  }
}
static size_t skip_sep (LexState *ls) {
  size_t count = 0;
  int s = ls->current;
  lua_assert(s == '[' || s == ']');
  save_and_next(ls);
  while (ls->current == '=') {
    save_and_next(ls);
    count++;
  }
  return (ls->current == s) ? count + 2
         : (count == 0) ? 1
         : 0;
}
static void read_long_string (LexState *ls, SemInfo *seminfo, size_t sep) {
  int line = ls->linenumber;  
  save_and_next(ls);  
  if (currIsNewline(ls))  
    inclinenumber(ls);  
  for (;;) {
    switch (ls->current) {
      case EOZ: {  
        const char *what = (seminfo ? "string" : "comment");
        const char *msg = luaO_pushfstring(ls->L,
                     "unfinished long %s (starting at line %d)", what, line);
        lexerror(ls, msg, TK_EOS);
        break;  
      }
      case ']': {
        if (skip_sep(ls) == sep) {
          save_and_next(ls);  
          goto endloop;
        }
        break;
      }
      case '\n': case '\r': {
        save(ls, '\n');
        inclinenumber(ls);
        if (!seminfo) luaZ_resetbuffer(ls->buff);  
        break;
      }
      default: {
        if (seminfo) save_and_next(ls);
        else next(ls);
      }
    }
  } endloop:
  if (seminfo)
    seminfo->ts = luaX_newstring(ls, luaZ_buffer(ls->buff) + sep,
                                     luaZ_bufflen(ls->buff) - 2 * sep);
}
static void esccheck (LexState *ls, int c, const char *msg) {
  if (!c) {
    if (ls->current != EOZ)
      save_and_next(ls);  
    lexerror(ls, msg, TK_STRING);
  }
}
static int gethexa (LexState *ls) {
  save_and_next(ls);
  esccheck (ls, lisxdigit(ls->current), "hexadecimal digit expected");
  return luaO_hexavalue(ls->current);
}
static int readhexaesc (LexState *ls) {
  int r = gethexa(ls);
  r = (r << 4) + gethexa(ls);
  luaZ_buffremove(ls->buff, 2);  
  return r;
}
static unsigned long readutf8esc (LexState *ls) {
  unsigned long r;
  int i = 4;  
  save_and_next(ls);  
  esccheck(ls, ls->current == '{', "missing '{'");
  r = gethexa(ls);  
  while ((save_and_next(ls), lisxdigit(ls->current))) {
    i++;
    r = (r << 4) + luaO_hexavalue(ls->current);
    esccheck(ls, r <= 0x10FFFF, "UTF-8 value too large");
  }
  esccheck(ls, ls->current == '}', "missing '}'");
  next(ls);  
  luaZ_buffremove(ls->buff, i);  
  return r;
}
static void utf8esc (LexState *ls) {
  char buff[UTF8BUFFSZ];
  int n = luaO_utf8esc(buff, readutf8esc(ls));
  for (; n > 0; n--)  
    save(ls, buff[UTF8BUFFSZ - n]);
}
static int readdecesc (LexState *ls) {
  int i;
  int r = 0;  
  for (i = 0; i < 3 && lisdigit(ls->current); i++) {  
    r = 10*r + ls->current - '0';
    save_and_next(ls);
  }
  esccheck(ls, r <= UCHAR_MAX, "decimal escape too large");
  luaZ_buffremove(ls->buff, i);  
  return r;
}
static void read_string (LexState *ls, int del, SemInfo *seminfo) {
  save_and_next(ls);  
  while (ls->current != del) {
    switch (ls->current) {
      case EOZ:
        lexerror(ls, "unfinished string", TK_EOS);
        break;  
      case '\n':
      case '\r':
        lexerror(ls, "unfinished string", TK_STRING);
        break;  
      case '\\': {  
        int c;  
        save_and_next(ls);  
        switch (ls->current) {
          case 'a': c = '\a'; goto read_save;
          case 'b': c = '\b'; goto read_save;
          case 'f': c = '\f'; goto read_save;
          case 'n': c = '\n'; goto read_save;
          case 'r': c = '\r'; goto read_save;
          case 't': c = '\t'; goto read_save;
          case 'v': c = '\v'; goto read_save;
          case 'x': c = readhexaesc(ls); goto read_save;
          case 'u': utf8esc(ls);  goto no_save;
          case '\n': case '\r':
            inclinenumber(ls); c = '\n'; goto only_save;
          case '\\': case '\"': case '\'':
            c = ls->current; goto read_save;
          case EOZ: goto no_save;  
          case 'z': {  
            luaZ_buffremove(ls->buff, 1);  
            next(ls);  
            while (lisspace(ls->current)) {
              if (currIsNewline(ls)) inclinenumber(ls);
              else next(ls);
            }
            goto no_save;
          }
          default: {
            esccheck(ls, lisdigit(ls->current), "invalid escape sequence");
            c = readdecesc(ls);  
            goto only_save;
          }
        }
       read_save:
         next(ls);
       only_save:
         luaZ_buffremove(ls->buff, 1);  
         save(ls, c);
       no_save: break;
      }
      default:
        save_and_next(ls);
    }
  }
  save_and_next(ls);  
  seminfo->ts = luaX_newstring(ls, luaZ_buffer(ls->buff) + 1,
                                   luaZ_bufflen(ls->buff) - 2);
}
static int llex (LexState *ls, SemInfo *seminfo) {
  luaZ_resetbuffer(ls->buff);
  for (;;) {
    switch (ls->current) {
      case '\n': case '\r': {  
        inclinenumber(ls);
        break;
      }
      case ' ': case '\f': case '\t': case '\v': {  
        next(ls);
        break;
      }
      case '-': {  
        next(ls);
        if (ls->current != '-') return '-';
        next(ls);
        if (ls->current == '[') {  
          size_t sep = skip_sep(ls);
          luaZ_resetbuffer(ls->buff);  
          if (sep >= 2) {
            read_long_string(ls, NULL, sep);  
            luaZ_resetbuffer(ls->buff);  
            break;
          }
        }
        while (!currIsNewline(ls) && ls->current != EOZ)
          next(ls);  
        break;
      }
      case '[': {  
        size_t sep = skip_sep(ls);
        if (sep >= 2) {
          read_long_string(ls, seminfo, sep);
          return TK_STRING;
        }
        else if (sep == 0)  
          lexerror(ls, "invalid long string delimiter", TK_STRING);
        return '[';
      }
      case '=': {
        next(ls);
        if (check_next1(ls, '=')) return TK_EQ;
        else return '=';
      }
      case '<': {
        next(ls);
        if (check_next1(ls, '=')) return TK_LE;
        else if (check_next1(ls, '<')) return TK_SHL;
        else return '<';
      }
      case '>': {
        next(ls);
        if (check_next1(ls, '=')) return TK_GE;
        else if (check_next1(ls, '>')) return TK_SHR;
        else return '>';
      }
      case '/': {
        next(ls);
        if (check_next1(ls, '/')) return TK_IDIV;
        else return '/';
      }
      case '~': {
        next(ls);
        if (check_next1(ls, '=')) return TK_NE;
        else return '~';
      }
      case ':': {
        next(ls);
        if (check_next1(ls, ':')) return TK_DBCOLON;
        else return ':';
      }
      case '"': case '\'': {  
        read_string(ls, ls->current, seminfo);
        return TK_STRING;
      }
      case '.': {  
        save_and_next(ls);
        if (check_next1(ls, '.')) {
          if (check_next1(ls, '.'))
            return TK_DOTS;   
          else return TK_CONCAT;   
        }
        else if (!lisdigit(ls->current)) return '.';
        else return read_numeral(ls, seminfo);
      }
      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9': {
        return read_numeral(ls, seminfo);
      }
      case EOZ: {
        return TK_EOS;
      }
      default: {
        if (lislalpha(ls->current)) {  
          TString *ts;
          do {
            save_and_next(ls);
          } while (lislalnum(ls->current));
          ts = luaX_newstring(ls, luaZ_buffer(ls->buff),
                                  luaZ_bufflen(ls->buff));
          seminfo->ts = ts;
          if (isreserved(ts))  
            return ts->extra - 1 + FIRST_RESERVED;
          else {
            return TK_NAME;
          }
        }
        else {  
          int c = ls->current;
          next(ls);
          return c;
        }
      }
    }
  }
}
void luaX_next (LexState *ls) {
  ls->lastline = ls->linenumber;
  if (ls->lookahead.token != TK_EOS) {  
    ls->t = ls->lookahead;  
    ls->lookahead.token = TK_EOS;  
  }
  else
    ls->t.token = llex(ls, &ls->t.seminfo);  
}
int luaX_lookahead (LexState *ls) {
  lua_assert(ls->lookahead.token == TK_EOS);
  ls->lookahead.token = llex(ls, &ls->lookahead.seminfo);
  return ls->lookahead.token;
}
#define lmem_c
#define LUA_CORE
#include <stddef.h>
#define MINSIZEARRAY	4
void *luaM_growaux_ (lua_State *L, void *block, int *size, size_t size_elems,
                     int limit, const char *what) {
  void *newblock;
  int newsize;
  if (*size >= limit/2) {  
    if (*size >= limit)  
      luaG_runerror(L, "too many %s (limit is %d)", what, limit);
    newsize = limit;  
  }
  else {
    newsize = (*size)*2;
    if (newsize < MINSIZEARRAY)
      newsize = MINSIZEARRAY;  
  }
  newblock = luaM_reallocv(L, block, *size, newsize, size_elems);
  *size = newsize;  
  return newblock;
}
l_noret luaM_toobig (lua_State *L) {
  luaG_runerror(L, "memory allocation error: block too big");
}
void *luaM_realloc_ (lua_State *L, void *block, size_t osize, size_t nsize) {
  void *newblock;
  global_State *g = G(L);
  size_t realosize = (block) ? osize : 0;
  lua_assert((realosize == 0) == (block == NULL));
#if defined(HARDMEMTESTS)
  if (nsize > realosize && g->gcrunning)
    luaC_fullgc(L, 1);  
#endif
  newblock = (*g->frealloc)(g->ud, block, osize, nsize);
  if (newblock == NULL && nsize > 0) {
    lua_assert(nsize > realosize);  
    if (g->version) {  
      luaC_fullgc(L, 1);  
      newblock = (*g->frealloc)(g->ud, block, osize, nsize);  
    }
    if (newblock == NULL)
      luaD_throw(L, LUA_ERRMEM);
  }
  lua_assert((nsize == 0) == (newblock == NULL));
  g->GCdebt = (g->GCdebt + nsize) - realosize;
  return newblock;
}
#define lobject_c
#define LUA_CORE
#include <locale.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
LUAI_DDEF const TValue luaO_nilobject_ = {NILCONSTANT};
int luaO_int2fb (unsigned int x) {
  int e = 0;  
  if (x < 8) return x;
  while (x >= (8 << 4)) {  
    x = (x + 0xf) >> 4;  
    e += 4;
  }
  while (x >= (8 << 1)) {  
    x = (x + 1) >> 1;  
    e++;
  }
  return ((e+1) << 3) | (cast_int(x) - 8);
}
int luaO_fb2int (int x) {
  return (x < 8) ? x : ((x & 7) + 8) << ((x >> 3) - 1);
}
int luaO_ceillog2 (unsigned int x) {
  static const lu_byte log_2[256] = {  
    0,1,2,2,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
    6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
    8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
    8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
    8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
    8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8
  };
  int l = 0;
  x--;
  while (x >= 256) { l += 8; x >>= 8; }
  return l + log_2[x];
}
static lua_Integer intarith (lua_State *L, int op, lua_Integer v1,
                                                   lua_Integer v2) {
  switch (op) {
    case LUA_OPADD: return intop(+, v1, v2);
    case LUA_OPSUB:return intop(-, v1, v2);
    case LUA_OPMUL:return intop(*, v1, v2);
    case LUA_OPMOD: return luaV_mod(L, v1, v2);
    case LUA_OPIDIV: return luaV_div(L, v1, v2);
    case LUA_OPBAND: return intop(&, v1, v2);
    case LUA_OPBOR: return intop(|, v1, v2);
    case LUA_OPBXOR: return intop(^, v1, v2);
    case LUA_OPSHL: return luaV_shiftl(v1, v2);
    case LUA_OPSHR: return luaV_shiftl(v1, -v2);
    case LUA_OPUNM: return intop(-, 0, v1);
    case LUA_OPBNOT: return intop(^, ~l_castS2U(0), v1);
    default: lua_assert(0); return 0;
  }
}
static lua_Number numarith (lua_State *L, int op, lua_Number v1,
                                                  lua_Number v2) {
  switch (op) {
    case LUA_OPADD: return luai_numadd(L, v1, v2);
    case LUA_OPSUB: return luai_numsub(L, v1, v2);
    case LUA_OPMUL: return luai_nummul(L, v1, v2);
    case LUA_OPDIV: return luai_numdiv(L, v1, v2);
    case LUA_OPPOW: return luai_numpow(L, v1, v2);
    case LUA_OPIDIV: return luai_numidiv(L, v1, v2);
    case LUA_OPUNM: return luai_numunm(L, v1);
    case LUA_OPMOD: {
      lua_Number m;
      luai_nummod(L, v1, v2, m);
      return m;
    }
    default: lua_assert(0); return 0;
  }
}
void luaO_arith (lua_State *L, int op, const TValue *p1, const TValue *p2,
                 TValue *res) {
  switch (op) {
    case LUA_OPBAND: case LUA_OPBOR: case LUA_OPBXOR:
    case LUA_OPSHL: case LUA_OPSHR:
    case LUA_OPBNOT: {  
      lua_Integer i1; lua_Integer i2;
      if (tointeger(p1, &i1) && tointeger(p2, &i2)) {
        setivalue(res, intarith(L, op, i1, i2));
        return;
      }
      else break;  
    }
    case LUA_OPDIV: case LUA_OPPOW: {  
      lua_Number n1; lua_Number n2;
      if (tonumber(p1, &n1) && tonumber(p2, &n2)) {
        setfltvalue(res, numarith(L, op, n1, n2));
        return;
      }
      else break;  
    }
    default: {  
      lua_Number n1; lua_Number n2;
      if (ttisinteger(p1) && ttisinteger(p2)) {
        setivalue(res, intarith(L, op, ivalue(p1), ivalue(p2)));
        return;
      }
      else if (tonumber(p1, &n1) && tonumber(p2, &n2)) {
        setfltvalue(res, numarith(L, op, n1, n2));
        return;
      }
      else break;  
    }
  }
  lua_assert(L != NULL);  
  luaT_trybinTM(L, p1, p2, res, cast(TMS, (op - LUA_OPADD) + TM_ADD));
}
int luaO_hexavalue (int c) {
  if (lisdigit(c)) return c - '0';
  else return (ltolower(c) - 'a') + 10;
}
static int isneg (const char **s) {
  if (**s == '-') { (*s)++; return 1; }
  else if (**s == '+') (*s)++;
  return 0;
}
#if !defined(lua_strx2number)
#define MAXSIGDIG	30
static lua_Number lua_strx2number (const char *s, char **endptr) {
  int dot = lua_getlocaledecpoint();
  lua_Number r = 0.0;  
  int sigdig = 0;  
  int nosigdig = 0;  
  int e = 0;  
  int neg;  
  int hasdot = 0;  
  *endptr = cast(char *, s);  
  while (lisspace(cast_uchar(*s))) s++;  
  neg = isneg(&s);  
  if (!(*s == '0' && (*(s + 1) == 'x' || *(s + 1) == 'X')))  
    return 0.0;  
  for (s += 2; ; s++) {  
    if (*s == dot) {
      if (hasdot) break;  
      else hasdot = 1;
    }
    else if (lisxdigit(cast_uchar(*s))) {
      if (sigdig == 0 && *s == '0')  
        nosigdig++;
      else if (++sigdig <= MAXSIGDIG)  
          r = (r * cast_num(16.0)) + luaO_hexavalue(*s);
      else e++; 
      if (hasdot) e--;  
    }
    else break;  
  }
  if (nosigdig + sigdig == 0)  
    return 0.0;  
  *endptr = cast(char *, s);  
  e *= 4;  
  if (*s == 'p' || *s == 'P') {  
    int exp1 = 0;  
    int neg1;  
    s++;  
    neg1 = isneg(&s);  
    if (!lisdigit(cast_uchar(*s)))
      return 0.0;  
    while (lisdigit(cast_uchar(*s)))  
      exp1 = exp1 * 10 + *(s++) - '0';
    if (neg1) exp1 = -exp1;
    e += exp1;
    *endptr = cast(char *, s);  
  }
  if (neg) r = -r;
  return l_mathop(ldexp)(r, e);
}
#endif
#if !defined (L_MAXLENNUM)
#define L_MAXLENNUM	200
#endif
static const char *l_str2dloc (const char *s, lua_Number *result, int mode) {
  char *endptr;
  *result = (mode == 'x') ? lua_strx2number(s, &endptr)  
                          : lua_str2number(s, &endptr);
  if (endptr == s) return NULL;  
  while (lisspace(cast_uchar(*endptr))) endptr++;  
  return (*endptr == '\0') ? endptr : NULL;  
}
static const char *l_str2d (const char *s, lua_Number *result) {
  const char *endptr;
  const char *pmode = strpbrk(s, ".xXnN");
  int mode = pmode ? ltolower(cast_uchar(*pmode)) : 0;
  if (mode == 'n')  
    return NULL;
  endptr = l_str2dloc(s, result, mode);  
  if (endptr == NULL) {  
    char buff[L_MAXLENNUM + 1];
    const char *pdot = strchr(s, '.');
    if (strlen(s) > L_MAXLENNUM || pdot == NULL)
      return NULL;  
    strcpy(buff, s);  
    buff[pdot - s] = lua_getlocaledecpoint();  
    endptr = l_str2dloc(buff, result, mode);  
    if (endptr != NULL)
      endptr = s + (endptr - buff);  
  }
  return endptr;
}
#define MAXBY10		cast(lua_Unsigned, LUA_MAXINTEGER / 10)
#define MAXLASTD	cast_int(LUA_MAXINTEGER % 10)
static const char *l_str2int (const char *s, lua_Integer *result) {
  lua_Unsigned a = 0;
  int empty = 1;
  int neg;
  while (lisspace(cast_uchar(*s))) s++;  
  neg = isneg(&s);
  if (s[0] == '0' &&
      (s[1] == 'x' || s[1] == 'X')) {  
    s += 2;  
    for (; lisxdigit(cast_uchar(*s)); s++) {
      a = a * 16 + luaO_hexavalue(*s);
      empty = 0;
    }
  }
  else {  
    for (; lisdigit(cast_uchar(*s)); s++) {
      int d = *s - '0';
      if (a >= MAXBY10 && (a > MAXBY10 || d > MAXLASTD + neg))  
        return NULL;  
      a = a * 10 + d;
      empty = 0;
    }
  }
  while (lisspace(cast_uchar(*s))) s++;  
  if (empty || *s != '\0') return NULL;  
  else {
    *result = l_castU2S((neg) ? 0u - a : a);
    return s;
  }
}
size_t luaO_str2num (const char *s, TValue *o) {
  lua_Integer i; lua_Number n;
  const char *e;
  if ((e = l_str2int(s, &i)) != NULL) {  
    setivalue(o, i);
  }
  else if ((e = l_str2d(s, &n)) != NULL) {  
    setfltvalue(o, n);
  }
  else
    return 0;  
  return (e - s) + 1;  
}
int luaO_utf8esc (char *buff, unsigned long x) {
  int n = 1;  
  lua_assert(x <= 0x10FFFF);
  if (x < 0x80)  
    buff[UTF8BUFFSZ - 1] = cast(char, x);
  else {  
    unsigned int mfb = 0x3f;  
    do {  
      buff[UTF8BUFFSZ - (n++)] = cast(char, 0x80 | (x & 0x3f));
      x >>= 6;  
      mfb >>= 1;  
    } while (x > mfb);  
    buff[UTF8BUFFSZ - n] = cast(char, (~mfb << 1) | x);  
  }
  return n;
}
#define MAXNUMBER2STR	50
void luaO_tostring (lua_State *L, StkId obj) {
  char buff[MAXNUMBER2STR];
  size_t len;
  lua_assert(ttisnumber(obj));
  if (ttisinteger(obj))
    len = lua_integer2str(buff, sizeof(buff), ivalue(obj));
  else {
    len = lua_number2str(buff, sizeof(buff), fltvalue(obj));
#if !defined(LUA_COMPAT_FLOATSTRING)
    if (buff[strspn(buff, "-0123456789")] == '\0') {  
      buff[len++] = lua_getlocaledecpoint();
      buff[len++] = '0';  
    }
#endif
  }
  setsvalue2s(L, obj, luaS_newlstr(L, buff, len));
}
static void pushstr (lua_State *L, const char *str, size_t l) {
  setsvalue2s(L, L->top, luaS_newlstr(L, str, l));
  luaD_inctop(L);
}
const char *luaO_pushvfstring (lua_State *L, const char *fmt, va_list argp) {
  int n = 0;
  for (;;) {
    const char *e = strchr(fmt, '%');
    if (e == NULL) break;
    pushstr(L, fmt, e - fmt);
    switch (*(e+1)) {
      case 's': {  
        const char *s = va_arg(argp, char *);
        if (s == NULL) s = "(null)";
        pushstr(L, s, strlen(s));
        break;
      }
      case 'c': {  
        char buff = cast(char, va_arg(argp, int));
        if (lisprint(cast_uchar(buff)))
          pushstr(L, &buff, 1);
        else  
          luaO_pushfstring(L, "<\\%d>", cast_uchar(buff));
        break;
      }
      case 'd': {  
        setivalue(L->top, va_arg(argp, int));
        goto top2str;
      }
      case 'I': {  
        setivalue(L->top, cast(lua_Integer, va_arg(argp, l_uacInt)));
        goto top2str;
      }
      case 'f': {  
        setfltvalue(L->top, cast_num(va_arg(argp, l_uacNumber)));
      top2str:  
        luaD_inctop(L);
        luaO_tostring(L, L->top - 1);
        break;
      }
      case 'p': {  
        char buff[4*sizeof(void *) + 8]; 
        void *p = va_arg(argp, void *);
        int l = lua_pointer2str(buff, sizeof(buff), p);
        pushstr(L, buff, l);
        break;
      }
      case 'U': {  
        char buff[UTF8BUFFSZ];
        int l = luaO_utf8esc(buff, cast(long, va_arg(argp, long)));
        pushstr(L, buff + UTF8BUFFSZ - l, l);
        break;
      }
      case '%': {
        pushstr(L, "%", 1);
        break;
      }
      default: {
        luaG_runerror(L, "invalid option '%%%c' to 'lua_pushfstring'",
                         *(e + 1));
      }
    }
    n += 2;
    fmt = e+2;
  }
  luaD_checkstack(L, 1);
  pushstr(L, fmt, strlen(fmt));
  if (n > 0) luaV_concat(L, n + 1);
  return svalue(L->top - 1);
}
const char *luaO_pushfstring (lua_State *L, const char *fmt, ...) {
  const char *msg;
  va_list argp;
  va_start(argp, fmt);
  msg = luaO_pushvfstring(L, fmt, argp);
  va_end(argp);
  return msg;
}
#define LL(x)	(sizeof(x)/sizeof(char) - 1)
#define RETS	"..."
#define PRE	"[string \""
#define POS	"\"]"
#define addstr(a,b,l)	( memcpy(a,b,(l) * sizeof(char)), a += (l) )
void luaO_chunkid (char *out, const char *source, size_t bufflen) {
  size_t l = strlen(source);
  if (*source == '=') {  
    if (l <= bufflen)  
      memcpy(out, source + 1, l * sizeof(char));
    else {  
      addstr(out, source + 1, bufflen - 1);
      *out = '\0';
    }
  }
  else if (*source == '@') {  
    if (l <= bufflen)  
      memcpy(out, source + 1, l * sizeof(char));
    else {  
      addstr(out, RETS, LL(RETS));
      bufflen -= LL(RETS);
      memcpy(out, source + 1 + l - bufflen, bufflen * sizeof(char));
    }
  }
  else {  
    const char *nl = strchr(source, '\n');  
    addstr(out, PRE, LL(PRE));  
    bufflen -= LL(PRE RETS POS) + 1;  
    if (l < bufflen && nl == NULL) {  
      addstr(out, source, l);  
    }
    else {
      if (nl != NULL) l = nl - source;  
      if (l > bufflen) l = bufflen;
      addstr(out, source, l);
      addstr(out, RETS, LL(RETS));
    }
    memcpy(out, POS, (LL(POS) + 1) * sizeof(char));
  }
}
#define lopcodes_c
#define LUA_CORE
#include <stddef.h>
LUAI_DDEF const char *const luaP_opnames[NUM_OPCODES+1] = {
  "MOVE",
  "LOADK",
  "LOADKX",
  "LOADBOOL",
  "LOADNIL",
  "GETUPVAL",
  "GETTABUP",
  "GETTABLE",
  "SETTABUP",
  "SETUPVAL",
  "SETTABLE",
  "NEWTABLE",
  "SELF",
  "ADD",
  "SUB",
  "MUL",
  "MOD",
  "POW",
  "DIV",
  "IDIV",
  "BAND",
  "BOR",
  "BXOR",
  "SHL",
  "SHR",
  "UNM",
  "BNOT",
  "NOT",
  "LEN",
  "CONCAT",
  "JMP",
  "EQ",
  "LT",
  "LE",
  "TEST",
  "TESTSET",
  "CALL",
  "TAILCALL",
  "RETURN",
  "FORLOOP",
  "FORPREP",
  "TFORCALL",
  "TFORLOOP",
  "SETLIST",
  "CLOSURE",
  "VARARG",
  "EXTRAARG",
  "DEFER",
  NULL
};
#define opmode(t,a,b,c,m) (((t)<<7) | ((a)<<6) | ((b)<<4) | ((c)<<2) | (m))
LUAI_DDEF const lu_byte luaP_opmodes[NUM_OPCODES] = {
  opmode(0, 1, OpArgR, OpArgN, iABC)		
 ,opmode(0, 1, OpArgK, OpArgN, iABx)		
 ,opmode(0, 1, OpArgN, OpArgN, iABx)		
 ,opmode(0, 1, OpArgU, OpArgU, iABC)		
 ,opmode(0, 1, OpArgU, OpArgN, iABC)		
 ,opmode(0, 1, OpArgU, OpArgN, iABC)		
 ,opmode(0, 1, OpArgU, OpArgK, iABC)		
 ,opmode(0, 1, OpArgR, OpArgK, iABC)		
 ,opmode(0, 0, OpArgK, OpArgK, iABC)		
 ,opmode(0, 0, OpArgU, OpArgN, iABC)		
 ,opmode(0, 0, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgU, OpArgU, iABC)		
 ,opmode(0, 1, OpArgR, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgK, OpArgK, iABC)		
 ,opmode(0, 1, OpArgR, OpArgN, iABC)		
 ,opmode(0, 1, OpArgR, OpArgN, iABC)		
 ,opmode(0, 1, OpArgR, OpArgN, iABC)		
 ,opmode(0, 1, OpArgR, OpArgN, iABC)		
 ,opmode(0, 1, OpArgR, OpArgR, iABC)		
 ,opmode(0, 0, OpArgR, OpArgN, iAsBx)		
 ,opmode(1, 0, OpArgK, OpArgK, iABC)		
 ,opmode(1, 0, OpArgK, OpArgK, iABC)		
 ,opmode(1, 0, OpArgK, OpArgK, iABC)		
 ,opmode(1, 0, OpArgN, OpArgU, iABC)		
 ,opmode(1, 1, OpArgR, OpArgU, iABC)		
 ,opmode(0, 1, OpArgU, OpArgU, iABC)		
 ,opmode(0, 1, OpArgU, OpArgU, iABC)		
 ,opmode(0, 0, OpArgU, OpArgN, iABC)		
 ,opmode(0, 1, OpArgR, OpArgN, iAsBx)		
 ,opmode(0, 1, OpArgR, OpArgN, iAsBx)		
 ,opmode(0, 0, OpArgN, OpArgU, iABC)		
 ,opmode(0, 1, OpArgR, OpArgN, iAsBx)		
 ,opmode(0, 0, OpArgU, OpArgU, iABC)		
 ,opmode(0, 1, OpArgU, OpArgN, iABx)		
 ,opmode(0, 1, OpArgU, OpArgN, iABC)		
 ,opmode(0, 0, OpArgU, OpArgU, iAx)		  
 ,opmode(0, 1, OpArgN, OpArgN, iABC)		
};
#define lparser_c
#define LUA_CORE
#include <string.h>
#define MAXVARS		200
#define hasmultret(k)		((k) == VCALL || (k) == VVARARG)
#define eqstr(a,b)	((a) == (b))
typedef struct BlockCnt {
  struct BlockCnt *previous;  
  int firstlabel;  
  int firstgoto;  
  lu_byte nactvar;  
  lu_byte upval;  
  lu_byte isloop;  
  lu_byte insidetbc;  
} BlockCnt;
static void statement (LexState *ls);
static void expr (LexState *ls, expdesc *v);
static l_noret semerror (LexState *ls, const char *msg) {
  ls->t.token = 0;  
  luaX_syntaxerror(ls, msg);
}
static l_noret error_expected (LexState *ls, int token) {
  luaX_syntaxerror(ls,
      luaO_pushfstring(ls->L, "%s expected", luaX_token2str(ls, token)));
}
static l_noret errorlimit (FuncState *fs, int limit, const char *what) {
  lua_State *L = fs->ls->L;
  const char *msg;
  int line = fs->f->linedefined;
  const char *where = (line == 0)
                      ? "main function"
                      : luaO_pushfstring(L, "function at line %d", line);
  msg = luaO_pushfstring(L, "too many %s (limit is %d) in %s",
                             what, limit, where);
  luaX_syntaxerror(fs->ls, msg);
}
static void checklimit (FuncState *fs, int v, int l, const char *what) {
  if (v > l) errorlimit(fs, l, what);
}
static int testnext (LexState *ls, int c) {
  if (ls->t.token == c) {
    luaX_next(ls);
    return 1;
  }
  else return 0;
}
static void check (LexState *ls, int c) {
  if (ls->t.token != c)
    error_expected(ls, c);
}
static void checknext (LexState *ls, int c) {
  check(ls, c);
  luaX_next(ls);
}
#define check_condition(ls,c,msg)	{ if (!(c)) luaX_syntaxerror(ls, msg); }
static void check_match (LexState *ls, int what, int who, int where) {
  if (!testnext(ls, what)) {
    if (where == ls->linenumber)
      error_expected(ls, what);
    else {
      luaX_syntaxerror(ls, luaO_pushfstring(ls->L,
             "%s expected (to close %s at line %d)",
              luaX_token2str(ls, what), luaX_token2str(ls, who), where));
    }
  }
}
static TString *str_checkname (LexState *ls) {
  TString *ts;
  check(ls, TK_NAME);
  ts = ls->t.seminfo.ts;
  luaX_next(ls);
  return ts;
}
static void init_exp (expdesc *e, expkind k, int i) {
  e->f = e->t = NO_JUMP;
  e->k = k;
  e->u.info = i;
}
static void codestring (LexState *ls, expdesc *e, TString *s) {
  init_exp(e, VK, luaK_stringK(ls->fs, s));
}
static void checkname (LexState *ls, expdesc *e) {
  codestring(ls, e, str_checkname(ls));
}
static int registerlocalvar (LexState *ls, TString *varname) {
  FuncState *fs = ls->fs;
  Proto *f = fs->f;
  int oldsize = f->sizelocvars;
  luaM_growvector(ls->L, f->locvars, fs->nlocvars, f->sizelocvars,
                  LocVar, SHRT_MAX, "local variables");
  while (oldsize < f->sizelocvars)
    f->locvars[oldsize++].varname = NULL;
  f->locvars[fs->nlocvars].varname = varname;
  luaC_objbarrier(ls->L, f, varname);
  return fs->nlocvars++;
}
static void new_localvar (LexState *ls, TString *name) {
  FuncState *fs = ls->fs;
  Dyndata *dyd = ls->dyd;
  int reg = registerlocalvar(ls, name);
  checklimit(fs, dyd->actvar.n + 1 - fs->firstlocal,
                  MAXVARS, "local variables");
  luaM_growvector(ls->L, dyd->actvar.arr, dyd->actvar.n + 1,
                  dyd->actvar.size, Vardesc, MAX_INT, "local variables");
  dyd->actvar.arr[dyd->actvar.n++].idx = cast(short, reg);
}
static void new_localvarliteral_ (LexState *ls, const char *name, size_t sz) {
  new_localvar(ls, luaX_newstring(ls, name, sz));
}
#define new_localvarliteral(ls,v) \
	new_localvarliteral_(ls, "" v, (sizeof(v)/sizeof(char))-1)
static LocVar *getlocvar (FuncState *fs, int i) {
  int idx = fs->ls->dyd->actvar.arr[fs->firstlocal + i].idx;
  lua_assert(idx < fs->nlocvars);
  return &fs->f->locvars[idx];
}
static void adjustlocalvars (LexState *ls, int nvars) {
  FuncState *fs = ls->fs;
  fs->nactvar = cast_byte(fs->nactvar + nvars);
  for (; nvars; nvars--) {
    getlocvar(fs, fs->nactvar - nvars)->startpc = fs->pc;
  }
}
static void removevars (FuncState *fs, int tolevel) {
  fs->ls->dyd->actvar.n -= (fs->nactvar - tolevel);
  while (fs->nactvar > tolevel)
    getlocvar(fs, --fs->nactvar)->endpc = fs->pc;
}
static int searchupvalue (FuncState *fs, TString *name) {
  int i;
  Upvaldesc *up = fs->f->upvalues;
  for (i = 0; i < fs->nups; i++) {
    if (eqstr(up[i].name, name)) return i;
  }
  return -1;  
}
static int newupvalue (FuncState *fs, TString *name, expdesc *v) {
  Proto *f = fs->f;
  int oldsize = f->sizeupvalues;
  checklimit(fs, fs->nups + 1, MAXUPVAL, "upvalues");
  luaM_growvector(fs->ls->L, f->upvalues, fs->nups, f->sizeupvalues,
                  Upvaldesc, MAXUPVAL, "upvalues");
  while (oldsize < f->sizeupvalues)
    f->upvalues[oldsize++].name = NULL;
  f->upvalues[fs->nups].instack = (v->k == VLOCAL);
  f->upvalues[fs->nups].idx = cast_byte(v->u.info);
  f->upvalues[fs->nups].name = name;
  luaC_objbarrier(fs->ls->L, f, name);
  return fs->nups++;
}
static int searchvar (FuncState *fs, TString *n) {
  int i;
  for (i = cast_int(fs->nactvar) - 1; i >= 0; i--) {
    if (eqstr(n, getlocvar(fs, i)->varname))
      return i;
  }
  return -1;  
}
static void markupval (FuncState *fs, int level) {
  BlockCnt *bl = fs->bl;
  while (bl->nactvar > level)
    bl = bl->previous;
  bl->upval = 1;
}
static void singlevaraux (FuncState *fs, TString *n, expdesc *var, int base) {
  if (fs == NULL)  
    init_exp(var, VVOID, 0);  
  else {
    int v = searchvar(fs, n);  
    if (v >= 0) {  
      init_exp(var, VLOCAL, v);  
      if (!base)
        markupval(fs, v);  
    }
    else {  
      int idx = searchupvalue(fs, n);  
      if (idx < 0) {  
        singlevaraux(fs->prev, n, var, 0);  
        if (var->k == VVOID)  
          return;  
        idx  = newupvalue(fs, n, var);  
      }
      init_exp(var, VUPVAL, idx);  
    }
  }
}
static void singlevar (LexState *ls, expdesc *var) {
  TString *varname = str_checkname(ls);
  FuncState *fs = ls->fs;
  singlevaraux(fs, varname, var, 1);
  if (var->k == VVOID) {  
    expdesc key;
    singlevaraux(fs, ls->envn, var, 1);  
    lua_assert(var->k != VVOID);  
    codestring(ls, &key, varname);  
    luaK_indexed(fs, var, &key);  
  }
}
static void adjust_assign (LexState *ls, int nvars, int nexps, expdesc *e) {
  FuncState *fs = ls->fs;
  int extra = nvars - nexps;
  if (hasmultret(e->k)) {
    extra++;  
    if (extra < 0) extra = 0;
    luaK_setreturns(fs, e, extra);  
    if (extra > 1) luaK_reserveregs(fs, extra-1);
  }
  else {
    if (e->k != VVOID) luaK_exp2nextreg(fs, e);  
    if (extra > 0) {
      int reg = fs->freereg;
      luaK_reserveregs(fs, extra);
      luaK_nil(fs, reg, extra);
    }
  }
  if (nexps > nvars)
    ls->fs->freereg -= nexps - nvars;  
}
static void enterlevel (LexState *ls) {
  lua_State *L = ls->L;
  ++L->nCcalls;
  checklimit(ls->fs, L->nCcalls, LUAI_MAXCCALLS, "C levels");
}
#define leavelevel(ls)	((ls)->L->nCcalls--)
static void closegoto (LexState *ls, int g, Labeldesc *label) {
  int i;
  FuncState *fs = ls->fs;
  Labellist *gl = &ls->dyd->gt;
  Labeldesc *gt = &gl->arr[g];
  lua_assert(eqstr(gt->name, label->name));
  if (gt->nactvar < label->nactvar) {
    TString *vname = getlocvar(fs, gt->nactvar)->varname;
    const char *msg = luaO_pushfstring(ls->L,
      "<goto %s> at line %d jumps into the scope of local '%s'",
      getstr(gt->name), gt->line, getstr(vname));
    semerror(ls, msg);
  }
  luaK_patchlist(fs, gt->pc, label->pc);
  for (i = g; i < gl->n - 1; i++)
    gl->arr[i] = gl->arr[i + 1];
  gl->n--;
}
static int findlabel (LexState *ls, int g) {
  int i;
  BlockCnt *bl = ls->fs->bl;
  Dyndata *dyd = ls->dyd;
  Labeldesc *gt = &dyd->gt.arr[g];
  for (i = bl->firstlabel; i < dyd->label.n; i++) {
    Labeldesc *lb = &dyd->label.arr[i];
    if (eqstr(lb->name, gt->name)) {  
      if (gt->nactvar > lb->nactvar &&
          (bl->upval || dyd->label.n > bl->firstlabel))
        luaK_patchclose(ls->fs, gt->pc, lb->nactvar);
      closegoto(ls, g, lb);  
      return 1;
    }
  }
  return 0;  
}
static int newlabelentry (LexState *ls, Labellist *l, TString *name,
                          int line, int pc) {
  int n = l->n;
  luaM_growvector(ls->L, l->arr, n, l->size,
                  Labeldesc, SHRT_MAX, "labels/gotos");
  l->arr[n].name = name;
  l->arr[n].line = line;
  l->arr[n].nactvar = ls->fs->nactvar;
  l->arr[n].pc = pc;
  l->n = n + 1;
  return n;
}
static void findgotos (LexState *ls, Labeldesc *lb) {
  Labellist *gl = &ls->dyd->gt;
  int i = ls->fs->bl->firstgoto;
  while (i < gl->n) {
    if (eqstr(gl->arr[i].name, lb->name))
      closegoto(ls, i, lb);
    else
      i++;
  }
}
static void movegotosout (FuncState *fs, BlockCnt *bl) {
  int i = bl->firstgoto;
  Labellist *gl = &fs->ls->dyd->gt;
  while (i < gl->n) {
    Labeldesc *gt = &gl->arr[i];
    if (gt->nactvar > bl->nactvar) {
      if (bl->upval)
        luaK_patchclose(fs, gt->pc, bl->nactvar);
      gt->nactvar = bl->nactvar;
    }
    if (!findlabel(fs->ls, i))
      i++;  
  }
}
static void enterblock (FuncState *fs, BlockCnt *bl, lu_byte isloop) {
  bl->isloop = isloop;
  bl->nactvar = fs->nactvar;
  bl->firstlabel = fs->ls->dyd->label.n;
  bl->firstgoto = fs->ls->dyd->gt.n;
  bl->upval = 0;
  bl->insidetbc = (fs->bl != NULL && fs->bl->insidetbc);
  bl->previous = fs->bl;
  fs->bl = bl;
  lua_assert(fs->freereg == fs->nactvar);
}
static void breaklabel (LexState *ls) {
  TString *n = luaS_new(ls->L, "break");
  int l = newlabelentry(ls, &ls->dyd->label, n, 0, ls->fs->pc);
  findgotos(ls, &ls->dyd->label.arr[l]);
}
static l_noret undefgoto (LexState *ls, Labeldesc *gt) {
  const char *msg = isreserved(gt->name)
                    ? "<%s> at line %d not inside a loop"
                    : "no visible label '%s' for <goto> at line %d";
  msg = luaO_pushfstring(ls->L, msg, getstr(gt->name), gt->line);
  semerror(ls, msg);
}
static void leaveblock (FuncState *fs) {
  BlockCnt *bl = fs->bl;
  LexState *ls = fs->ls;
  if (bl->previous && bl->upval) {
    int j = luaK_jump(fs);
    luaK_patchclose(fs, j, bl->nactvar);
    luaK_patchtohere(fs, j);
  }
  if (bl->isloop)
    breaklabel(ls);  
  fs->bl = bl->previous;
  removevars(fs, bl->nactvar);
  lua_assert(bl->nactvar == fs->nactvar);
  fs->freereg = fs->nactvar;  
  ls->dyd->label.n = bl->firstlabel;  
  if (bl->previous)  
    movegotosout(fs, bl);  
  else if (bl->firstgoto < ls->dyd->gt.n)  
    undefgoto(ls, &ls->dyd->gt.arr[bl->firstgoto]);  
}
static Proto *addprototype (LexState *ls) {
  Proto *clp;
  lua_State *L = ls->L;
  FuncState *fs = ls->fs;
  Proto *f = fs->f;  
  if (fs->np >= f->sizep) {
    int oldsize = f->sizep;
    luaM_growvector(L, f->p, fs->np, f->sizep, Proto *, MAXARG_Bx, "functions");
    while (oldsize < f->sizep)
      f->p[oldsize++] = NULL;
  }
  f->p[fs->np++] = clp = luaF_newproto(L);
  luaC_objbarrier(L, f, clp);
  return clp;
}
static void codeclosure (LexState *ls, expdesc *v, int deferred) {
  FuncState *fs = ls->fs->prev;
  int pc = -1;
  if (deferred) {
    pc = luaK_codeABC(fs, OP_DEFER, 0, 0, 0);
  }
  init_exp(v, VRELOCABLE, luaK_codeABx(fs, OP_CLOSURE, 0, fs->np - 1));
  luaK_exp2nextreg(fs, v);  
  if (deferred) {
    SETARG_A(fs->f->code[pc], v->u.info);
  }
}
static void open_func (LexState *ls, FuncState *fs, BlockCnt *bl) {
  Proto *f;
  fs->prev = ls->fs;  
  fs->ls = ls;
  ls->fs = fs;
  fs->pc = 0;
  fs->lasttarget = 0;
  fs->jpc = NO_JUMP;
  fs->freereg = 0;
  fs->nk = 0;
  fs->np = 0;
  fs->nups = 0;
  fs->nlocvars = 0;
  fs->nactvar = 0;
  fs->firstlocal = ls->dyd->actvar.n;
  fs->bl = NULL;
  f = fs->f;
  f->source = ls->source;
  luaC_objbarrier(ls->L, f, f->source);
  f->maxstacksize = 2;  
  enterblock(fs, bl, 0);
}
static void close_func (LexState *ls) {
  lua_State *L = ls->L;
  FuncState *fs = ls->fs;
  Proto *f = fs->f;
  luaK_ret(fs, 0, 0);  
  leaveblock(fs);
  luaM_reallocvector(L, f->code, f->sizecode, fs->pc, Instruction);
  f->sizecode = fs->pc;
  luaM_reallocvector(L, f->lineinfo, f->sizelineinfo, fs->pc, int);
  f->sizelineinfo = fs->pc;
  luaM_reallocvector(L, f->k, f->sizek, fs->nk, TValue);
  f->sizek = fs->nk;
  luaM_reallocvector(L, f->p, f->sizep, fs->np, Proto *);
  f->sizep = fs->np;
  luaM_reallocvector(L, f->locvars, f->sizelocvars, fs->nlocvars, LocVar);
  f->sizelocvars = fs->nlocvars;
  luaM_reallocvector(L, f->upvalues, f->sizeupvalues, fs->nups, Upvaldesc);
  f->sizeupvalues = fs->nups;
  lua_assert(fs->bl == NULL);
  ls->fs = fs->prev;
  luaC_checkGC(L);
}
static int block_follow (LexState *ls, int withuntil) {
  switch (ls->t.token) {
    case TK_ELSE: case TK_ELSEIF:
    case TK_END: case TK_EOS:
      return 1;
    case TK_UNTIL: return withuntil;
    default: return 0;
  }
}
static void statlist (LexState *ls) {
  while (!block_follow(ls, 1)) {
    if (ls->t.token == TK_RETURN) {
      statement(ls);
      return;  
    }
    statement(ls);
  }
}
static void fieldsel (LexState *ls, expdesc *v) {
  FuncState *fs = ls->fs;
  expdesc key;
  luaK_exp2anyregup(fs, v);
  luaX_next(ls);  
  checkname(ls, &key);
  luaK_indexed(fs, v, &key);
}
static void yindex (LexState *ls, expdesc *v) {
  luaX_next(ls);  
  expr(ls, v);
  luaK_exp2val(ls->fs, v);
  checknext(ls, ']');
}
struct ConsControl {
  expdesc v;  
  expdesc *t;  
  int nh;  
  int na;  
  int tostore;  
};
static void recfield (LexState *ls, struct ConsControl *cc) {
  FuncState *fs = ls->fs;
  int reg = ls->fs->freereg;
  expdesc key, val;
  int rkkey;
  if (ls->t.token == TK_NAME) {
    checklimit(fs, cc->nh, MAX_INT, "items in a constructor");
    checkname(ls, &key);
  }
  else  
    yindex(ls, &key);
  cc->nh++;
  checknext(ls, '=');
  rkkey = luaK_exp2RK(fs, &key);
  expr(ls, &val);
  luaK_codeABC(fs, OP_SETTABLE, cc->t->u.info, rkkey, luaK_exp2RK(fs, &val));
  fs->freereg = reg;  
}
static void closelistfield (FuncState *fs, struct ConsControl *cc) {
  if (cc->v.k == VVOID) return;  
  luaK_exp2nextreg(fs, &cc->v);
  cc->v.k = VVOID;
  if (cc->tostore == LFIELDS_PER_FLUSH) {
    luaK_setlist(fs, cc->t->u.info, cc->na, cc->tostore);  
    cc->tostore = 0;  
  }
}
static void lastlistfield (FuncState *fs, struct ConsControl *cc) {
  if (cc->tostore == 0) return;
  if (hasmultret(cc->v.k)) {
    luaK_setmultret(fs, &cc->v);
    luaK_setlist(fs, cc->t->u.info, cc->na, LUA_MULTRET);
    cc->na--;  
  }
  else {
    if (cc->v.k != VVOID)
      luaK_exp2nextreg(fs, &cc->v);
    luaK_setlist(fs, cc->t->u.info, cc->na, cc->tostore);
  }
}
static void listfield (LexState *ls, struct ConsControl *cc) {
  expr(ls, &cc->v);
  checklimit(ls->fs, cc->na, MAX_INT, "items in a constructor");
  cc->na++;
  cc->tostore++;
}
static void field (LexState *ls, struct ConsControl *cc) {
  switch(ls->t.token) {
    case TK_NAME: {  
      if (luaX_lookahead(ls) != '=')  
        listfield(ls, cc);
      else
        recfield(ls, cc);
      break;
    }
    case '[': {
      recfield(ls, cc);
      break;
    }
    default: {
      listfield(ls, cc);
      break;
    }
  }
}
static void constructor (LexState *ls, expdesc *t) {
  FuncState *fs = ls->fs;
  int line = ls->linenumber;
  int pc = luaK_codeABC(fs, OP_NEWTABLE, 0, 0, 0);
  struct ConsControl cc;
  cc.na = cc.nh = cc.tostore = 0;
  cc.t = t;
  init_exp(t, VRELOCABLE, pc);
  init_exp(&cc.v, VVOID, 0);  
  luaK_exp2nextreg(ls->fs, t);  
  checknext(ls, '{');
  do {
    lua_assert(cc.v.k == VVOID || cc.tostore > 0);
    if (ls->t.token == '}') break;
    closelistfield(fs, &cc);
    field(ls, &cc);
  } while (testnext(ls, ',') || testnext(ls, ';'));
  check_match(ls, '}', '{', line);
  lastlistfield(fs, &cc);
  SETARG_B(fs->f->code[pc], luaO_int2fb(cc.na)); 
  SETARG_C(fs->f->code[pc], luaO_int2fb(cc.nh));  
}
static void parlist (LexState *ls) {
  FuncState *fs = ls->fs;
  Proto *f = fs->f;
  int nparams = 0;
  f->is_vararg = 0;
  if (ls->t.token != ')') {  
    do {
      switch (ls->t.token) {
        case TK_NAME: {  
          new_localvar(ls, str_checkname(ls));
          nparams++;
          break;
        }
        case TK_DOTS: {  
          luaX_next(ls);
          f->is_vararg = 1;  
          break;
        }
        default: luaX_syntaxerror(ls, "<name> or '...' expected");
      }
    } while (!f->is_vararg && testnext(ls, ','));
  }
  adjustlocalvars(ls, nparams);
  f->numparams = cast_byte(fs->nactvar);
  luaK_reserveregs(fs, fs->nactvar);  
}
static void body (LexState *ls, expdesc *e, int ismethod, int line, int deferred) {
  FuncState new_fs;
  BlockCnt bl;
  new_fs.f = addprototype(ls);
  new_fs.f->linedefined = line;
  open_func(ls, &new_fs, &bl);
  if (!deferred) {
    checknext(ls, '(');
    if (ismethod) {
      new_localvarliteral(ls, "self");  
      adjustlocalvars(ls, 1);
    }
    parlist(ls);
    checknext(ls, ')');
  }
  statlist(ls);
  new_fs.f->lastlinedefined = ls->linenumber;
  check_match(ls, TK_END, TK_FUNCTION, line);
  codeclosure(ls, e, deferred);
  close_func(ls);
}
static int explist (LexState *ls, expdesc *v) {
  int n = 1;  
  expr(ls, v);
  while (testnext(ls, ',')) {
    luaK_exp2nextreg(ls->fs, v);
    expr(ls, v);
    n++;
  }
  return n;
}
static void funcargs (LexState *ls, expdesc *f, int line) {
  FuncState *fs = ls->fs;
  expdesc args;
  int base, nparams;
  switch (ls->t.token) {
    case '(': {  
      luaX_next(ls);
      if (ls->t.token == ')')  
        args.k = VVOID;
      else {
        explist(ls, &args);
        luaK_setmultret(fs, &args);
      }
      check_match(ls, ')', '(', line);
      break;
    }
    case '{': {  
      constructor(ls, &args);
      break;
    }
    case TK_STRING: {  
      codestring(ls, &args, ls->t.seminfo.ts);
      luaX_next(ls);  
      break;
    }
    default: {
      luaX_syntaxerror(ls, "function arguments expected");
    }
  }
  lua_assert(f->k == VNONRELOC);
  base = f->u.info;  
  if (hasmultret(args.k))
    nparams = LUA_MULTRET;  
  else {
    if (args.k != VVOID)
      luaK_exp2nextreg(fs, &args);  
    nparams = fs->freereg - (base+1);
  }
  init_exp(f, VCALL, luaK_codeABC(fs, OP_CALL, base, nparams+1, 2));
  luaK_fixline(fs, line);
  fs->freereg = base+1;  
}
static void primaryexp (LexState *ls, expdesc *v) {
  switch (ls->t.token) {
    case '(': {
      int line = ls->linenumber;
      luaX_next(ls);
      expr(ls, v);
      check_match(ls, ')', '(', line);
      luaK_dischargevars(ls->fs, v);
      return;
    }
    case TK_NAME: {
      singlevar(ls, v);
      return;
    }
    default: {
      luaX_syntaxerror(ls, "unexpected symbol");
    }
  }
}
static void suffixedexp (LexState *ls, expdesc *v) {
  FuncState *fs = ls->fs;
  int line = ls->linenumber;
  primaryexp(ls, v);
  for (;;) {
    switch (ls->t.token) {
      case '.': {  
        fieldsel(ls, v);
        break;
      }
      case '[': {  
        expdesc key;
        luaK_exp2anyregup(fs, v);
        yindex(ls, &key);
        luaK_indexed(fs, v, &key);
        break;
      }
      case ':': {  
        expdesc key;
        luaX_next(ls);
        checkname(ls, &key);
        luaK_self(fs, v, &key);
        funcargs(ls, v, line);
        break;
      }
      case '(': case TK_STRING: case '{': {  
        luaK_exp2nextreg(fs, v);
        funcargs(ls, v, line);
        break;
      }
      default: return;
    }
  }
}
static void simpleexp (LexState *ls, expdesc *v) {
  switch (ls->t.token) {
    case TK_FLT: {
      init_exp(v, VKFLT, 0);
      v->u.nval = ls->t.seminfo.r;
      break;
    }
    case TK_INT: {
      init_exp(v, VKINT, 0);
      v->u.ival = ls->t.seminfo.i;
      break;
    }
    case TK_STRING: {
      codestring(ls, v, ls->t.seminfo.ts);
      break;
    }
    case TK_NIL: {
      init_exp(v, VNIL, 0);
      break;
    }
    case TK_TRUE: {
      init_exp(v, VTRUE, 0);
      break;
    }
    case TK_FALSE: {
      init_exp(v, VFALSE, 0);
      break;
    }
    case TK_DOTS: {  
      FuncState *fs = ls->fs;
      check_condition(ls, fs->f->is_vararg,
                      "cannot use '...' outside a vararg function");
      init_exp(v, VVARARG, luaK_codeABC(fs, OP_VARARG, 0, 1, 0));
      break;
    }
    case '{': {  
      constructor(ls, v);
      return;
    }
    case TK_FUNCTION: {
      luaX_next(ls);
      body(ls, v, 0, ls->linenumber, 0);
      return;
    }
    default: {
      suffixedexp(ls, v);
      return;
    }
  }
  luaX_next(ls);
}
static UnOpr getunopr (int op) {
  switch (op) {
    case TK_NOT: return OPR_NOT;
    case '-': return OPR_MINUS;
    case '~': return OPR_BNOT;
    case '#': return OPR_LEN;
    default: return OPR_NOUNOPR;
  }
}
static BinOpr getbinopr (int op) {
  switch (op) {
    case '+': return OPR_ADD;
    case '-': return OPR_SUB;
    case '*': return OPR_MUL;
    case '%': return OPR_MOD;
    case '^': return OPR_POW;
    case '/': return OPR_DIV;
    case TK_IDIV: return OPR_IDIV;
    case '&': return OPR_BAND;
    case '|': return OPR_BOR;
    case '~': return OPR_BXOR;
    case TK_SHL: return OPR_SHL;
    case TK_SHR: return OPR_SHR;
    case TK_CONCAT: return OPR_CONCAT;
    case TK_NE: return OPR_NE;
    case TK_EQ: return OPR_EQ;
    case '<': return OPR_LT;
    case TK_LE: return OPR_LE;
    case '>': return OPR_GT;
    case TK_GE: return OPR_GE;
    case TK_AND: return OPR_AND;
    case TK_OR: return OPR_OR;
    default: return OPR_NOBINOPR;
  }
}
static const struct {
  lu_byte left;  
  lu_byte right; 
} priority[] = {  
   {10, 10}, {10, 10},           
   {11, 11}, {11, 11},           
   {14, 13},                  
   {11, 11}, {11, 11},           
   {6, 6}, {4, 4}, {5, 5},   
   {7, 7}, {7, 7},           
   {9, 8},                   
   {3, 3}, {3, 3}, {3, 3},   
   {3, 3}, {3, 3}, {3, 3},   
   {2, 2}, {1, 1}            
};
#define UNARY_PRIORITY	12  
static BinOpr subexpr (LexState *ls, expdesc *v, int limit) {
  BinOpr op;
  UnOpr uop;
  enterlevel(ls);
  uop = getunopr(ls->t.token);
  if (uop != OPR_NOUNOPR) {
    int line = ls->linenumber;
    luaX_next(ls);
    subexpr(ls, v, UNARY_PRIORITY);
    luaK_prefix(ls->fs, uop, v, line);
  }
  else simpleexp(ls, v);
  op = getbinopr(ls->t.token);
  while (op != OPR_NOBINOPR && priority[op].left > limit) {
    expdesc v2;
    BinOpr nextop;
    int line = ls->linenumber;
    luaX_next(ls);
    luaK_infix(ls->fs, op, v);
    nextop = subexpr(ls, &v2, priority[op].right);
    luaK_posfix(ls->fs, op, v, &v2, line);
    op = nextop;
  }
  leavelevel(ls);
  return op;  
}
static void expr (LexState *ls, expdesc *v) {
  subexpr(ls, v, 0);
}
static void block (LexState *ls) {
  FuncState *fs = ls->fs;
  BlockCnt bl;
  enterblock(fs, &bl, 0);
  statlist(ls);
  leaveblock(fs);
}
struct LHS_assign {
  struct LHS_assign *prev;
  expdesc v;  
};
static void check_conflict (LexState *ls, struct LHS_assign *lh, expdesc *v) {
  FuncState *fs = ls->fs;
  int extra = fs->freereg;  
  int conflict = 0;
  for (; lh; lh = lh->prev) {  
    if (lh->v.k == VINDEXED) {  
      if (lh->v.u.ind.vt == v->k && lh->v.u.ind.t == v->u.info) {
        conflict = 1;
        lh->v.u.ind.vt = VLOCAL;
        lh->v.u.ind.t = extra;  
      }
      if (v->k == VLOCAL && lh->v.u.ind.idx == v->u.info) {
        conflict = 1;
        lh->v.u.ind.idx = extra;  
      }
    }
  }
  if (conflict) {
    OpCode op = (v->k == VLOCAL) ? OP_MOVE : OP_GETUPVAL;
    luaK_codeABC(fs, op, extra, v->u.info, 0);
    luaK_reserveregs(fs, 1);
  }
}
static void assignment (LexState *ls, struct LHS_assign *lh, int nvars) {
  expdesc e;
  check_condition(ls, vkisvar(lh->v.k), "syntax error");
  if (testnext(ls, ',')) {  
    struct LHS_assign nv;
    nv.prev = lh;
    suffixedexp(ls, &nv.v);
    if (nv.v.k != VINDEXED)
      check_conflict(ls, lh, &nv.v);
    checklimit(ls->fs, nvars + ls->L->nCcalls, LUAI_MAXCCALLS,
                    "C levels");
    assignment(ls, &nv, nvars+1);
  }
  else {  
    int nexps;
    checknext(ls, '=');
    nexps = explist(ls, &e);
    if (nexps != nvars)
      adjust_assign(ls, nvars, nexps, &e);
    else {
      luaK_setoneret(ls->fs, &e);  
      luaK_storevar(ls->fs, &lh->v, &e);
      return;  
    }
  }
  init_exp(&e, VNONRELOC, ls->fs->freereg-1);  
  luaK_storevar(ls->fs, &lh->v, &e);
}
static int cond (LexState *ls) {
  expdesc v;
  expr(ls, &v);  
  if (v.k == VNIL) v.k = VFALSE;  
  luaK_goiftrue(ls->fs, &v);
  return v.f;
}
static void gotostat (LexState *ls, int pc) {
  int line = ls->linenumber;
  TString *label;
  int g;
  if (testnext(ls, TK_GOTO))
    label = str_checkname(ls);
  else {
    luaX_next(ls);  
    label = luaS_new(ls->L, "break");
  }
  g = newlabelentry(ls, &ls->dyd->gt, label, line, pc);
  findlabel(ls, g);  
}
static void checkrepeated (FuncState *fs, Labellist *ll, TString *label) {
  int i;
  for (i = fs->bl->firstlabel; i < ll->n; i++) {
    if (eqstr(label, ll->arr[i].name)) {
      const char *msg = luaO_pushfstring(fs->ls->L,
                          "label '%s' already defined on line %d",
                          getstr(label), ll->arr[i].line);
      semerror(fs->ls, msg);
    }
  }
}
static void skipnoopstat (LexState *ls) {
  while (ls->t.token == ';' || ls->t.token == TK_DBCOLON)
    statement(ls);
}
static void labelstat (LexState *ls, TString *label, int line) {
  FuncState *fs = ls->fs;
  Labellist *ll = &ls->dyd->label;
  int l;  
  checkrepeated(fs, ll, label);  
  checknext(ls, TK_DBCOLON);  
  l = newlabelentry(ls, ll, label, line, luaK_getlabel(fs));
  skipnoopstat(ls);  
  if (block_follow(ls, 0)) {  
    ll->arr[l].nactvar = fs->bl->nactvar;
  }
  findgotos(ls, &ll->arr[l]);
}
static void whilestat (LexState *ls, int line) {
  FuncState *fs = ls->fs;
  int whileinit;
  int condexit;
  BlockCnt bl;
  luaX_next(ls);  
  whileinit = luaK_getlabel(fs);
  condexit = cond(ls);
  enterblock(fs, &bl, 1);
  checknext(ls, TK_DO);
  block(ls);
  luaK_jumpto(fs, whileinit);
  check_match(ls, TK_END, TK_WHILE, line);
  leaveblock(fs);
  luaK_patchtohere(fs, condexit);  
}
static void repeatstat (LexState *ls, int line) {
  int condexit;
  FuncState *fs = ls->fs;
  int repeat_init = luaK_getlabel(fs);
  BlockCnt bl1, bl2;
  enterblock(fs, &bl1, 1);  
  enterblock(fs, &bl2, 0);  
  luaX_next(ls);  
  statlist(ls);
  check_match(ls, TK_UNTIL, TK_REPEAT, line);
  condexit = cond(ls);  
  if (bl2.upval)  
    luaK_patchclose(fs, condexit, bl2.nactvar);
  leaveblock(fs);  
  luaK_patchlist(fs, condexit, repeat_init);  
  leaveblock(fs);  
}
static int exp1 (LexState *ls) {
  expdesc e;
  int reg;
  expr(ls, &e);
  luaK_exp2nextreg(ls->fs, &e);
  lua_assert(e.k == VNONRELOC);
  reg = e.u.info;
  return reg;
}
static void forbody (LexState *ls, int base, int line, int nvars, int isnum) {
  BlockCnt bl;
  FuncState *fs = ls->fs;
  int prep, endfor;
  adjustlocalvars(ls, 3);  
  checknext(ls, TK_DO);
  prep = isnum ? luaK_codeAsBx(fs, OP_FORPREP, base, NO_JUMP) : luaK_jump(fs);
  enterblock(fs, &bl, 0);  
  adjustlocalvars(ls, nvars);
  luaK_reserveregs(fs, nvars);
  block(ls);
  leaveblock(fs);  
  luaK_patchtohere(fs, prep);
  if (isnum)  
    endfor = luaK_codeAsBx(fs, OP_FORLOOP, base, NO_JUMP);
  else {  
    luaK_codeABC(fs, OP_TFORCALL, base, 0, nvars);
    luaK_fixline(fs, line);
    endfor = luaK_codeAsBx(fs, OP_TFORLOOP, base + 2, NO_JUMP);
  }
  luaK_patchlist(fs, endfor, prep + 1);
  luaK_fixline(fs, line);
}
static void fornum (LexState *ls, TString *varname, int line) {
  FuncState *fs = ls->fs;
  int base = fs->freereg;
  new_localvarliteral(ls, "(for index)");
  new_localvarliteral(ls, "(for limit)");
  new_localvarliteral(ls, "(for step)");
  new_localvar(ls, varname);
  checknext(ls, '=');
  exp1(ls);  
  checknext(ls, ',');
  exp1(ls);  
  if (testnext(ls, ','))
    exp1(ls);  
  else {  
    luaK_codek(fs, fs->freereg, luaK_intK(fs, 1));
    luaK_reserveregs(fs, 1);
  }
  forbody(ls, base, line, 1, 1);
}
static void forlist (LexState *ls, TString *indexname) {
  FuncState *fs = ls->fs;
  expdesc e;
  int nvars = 4;  
  int line;
  int base = fs->freereg;
  new_localvarliteral(ls, "(for generator)");
  new_localvarliteral(ls, "(for state)");
  new_localvarliteral(ls, "(for control)");
  new_localvar(ls, indexname);
  while (testnext(ls, ',')) {
    new_localvar(ls, str_checkname(ls));
    nvars++;
  }
  checknext(ls, TK_IN);
  line = ls->linenumber;
  adjust_assign(ls, 3, explist(ls, &e), &e);
  luaK_checkstack(fs, 3);  
  forbody(ls, base, line, nvars - 3, 0);
}
static void forstat (LexState *ls, int line) {
  FuncState *fs = ls->fs;
  TString *varname;
  BlockCnt bl;
  enterblock(fs, &bl, 1);  
  luaX_next(ls);  
  varname = str_checkname(ls);  
  switch (ls->t.token) {
    case '=': fornum(ls, varname, line); break;
    case ',': case TK_IN: forlist(ls, varname); break;
    default: luaX_syntaxerror(ls, "'=' or 'in' expected");
  }
  check_match(ls, TK_END, TK_FOR, line);
  leaveblock(fs);  
}
static void test_then_block (LexState *ls, int *escapelist) {
  BlockCnt bl;
  FuncState *fs = ls->fs;
  expdesc v;
  int jf;  
  luaX_next(ls);  
  expr(ls, &v);  
  checknext(ls, TK_THEN);
  if (ls->t.token == TK_GOTO || ls->t.token == TK_BREAK) {
    luaK_goiffalse(ls->fs, &v);  
    enterblock(fs, &bl, 0);  
    gotostat(ls, v.t);  
    while (testnext(ls, ';')) {}  
    if (block_follow(ls, 0)) {  
      leaveblock(fs);
      return;  
    }
    else  
      jf = luaK_jump(fs);
  }
  else {  
    luaK_goiftrue(ls->fs, &v);  
    enterblock(fs, &bl, 0);
    jf = v.f;
  }
  statlist(ls);  
  leaveblock(fs);
  if (ls->t.token == TK_ELSE ||
      ls->t.token == TK_ELSEIF)  
    luaK_concat(fs, escapelist, luaK_jump(fs));  
  luaK_patchtohere(fs, jf);
}
static void ifstat (LexState *ls, int line) {
  FuncState *fs = ls->fs;
  int escapelist = NO_JUMP;  
  test_then_block(ls, &escapelist);  
  while (ls->t.token == TK_ELSEIF)
    test_then_block(ls, &escapelist);  
  if (testnext(ls, TK_ELSE))
    block(ls);  
  check_match(ls, TK_END, TK_IF, line);
  luaK_patchtohere(fs, escapelist);  
}
static void localfunc (LexState *ls, int defer) {
  expdesc b;
  FuncState *fs = ls->fs;
  if (defer) {
    static const char funcname[] = "(deferred function)";
    new_localvar(ls, luaX_newstring(ls, funcname, sizeof funcname-1));  
    markupval(fs, fs->nactvar);
    fs->bl->insidetbc = 1;  
  } else {
    new_localvar(ls, str_checkname(ls));  
  }
  adjustlocalvars(ls, 1);  
  body(ls, &b, 0, ls->linenumber, defer);  
  getlocvar(fs, b.u.info)->startpc = fs->pc;
}
static void localstat (LexState *ls) {
  int nvars = 0;
  int nexps;
  expdesc e;
  do {
    new_localvar(ls, str_checkname(ls));
    nvars++;
  } while (testnext(ls, ','));
  if (testnext(ls, '='))
    nexps = explist(ls, &e);
  else {
    e.k = VVOID;
    nexps = 0;
  }
  adjust_assign(ls, nvars, nexps, &e);
  adjustlocalvars(ls, nvars);
}
static int funcname (LexState *ls, expdesc *v) {
  int ismethod = 0;
  singlevar(ls, v);
  while (ls->t.token == '.')
    fieldsel(ls, v);
  if (ls->t.token == ':') {
    ismethod = 1;
    fieldsel(ls, v);
  }
  return ismethod;
}
static void funcstat (LexState *ls, int line) {
  int ismethod;
  expdesc v, b;
  luaX_next(ls);  
  ismethod = funcname(ls, &v);
  body(ls, &b, ismethod, line, 0);
  luaK_storevar(ls->fs, &v, &b);
  luaK_fixline(ls->fs, line);  
}
static void exprstat (LexState *ls) {
  FuncState *fs = ls->fs;
  struct LHS_assign v;
  suffixedexp(ls, &v.v);
  if (ls->t.token == '=' || ls->t.token == ',') { 
    v.prev = NULL;
    assignment(ls, &v, 1);
  }
  else {  
    check_condition(ls, v.v.k == VCALL, "syntax error");
    SETARG_C(getinstruction(fs, &v.v), 1);  
  }
}
static void retstat (LexState *ls) {
  FuncState *fs = ls->fs;
  expdesc e;
  int first, nret;  
  if (block_follow(ls, 1) || ls->t.token == ';')
    first = nret = 0;  
  else {
    nret = explist(ls, &e);  
    if (hasmultret(e.k)) {
      luaK_setmultret(fs, &e);
      if (e.k == VCALL && nret == 1 && !fs->bl->insidetbc) {  
        SET_OPCODE(getinstruction(fs,&e), OP_TAILCALL);
        lua_assert(GETARG_A(getinstruction(fs,&e)) == fs->nactvar);
      }
      first = fs->nactvar;
      nret = LUA_MULTRET;  
    }
    else {
      if (nret == 1)  
        first = luaK_exp2anyreg(fs, &e);
      else {
        luaK_exp2nextreg(fs, &e);  
        first = fs->nactvar;  
        lua_assert(nret == fs->freereg - first);
      }
    }
  }
  luaK_ret(fs, first, nret);
  testnext(ls, ';');  
}
static void statement (LexState *ls) {
  int line = ls->linenumber;  
  enterlevel(ls);
  switch (ls->t.token) {
    case ';': {  
      luaX_next(ls);  
      break;
    }
    case TK_IF: {  
      ifstat(ls, line);
      break;
    }
    case TK_WHILE: {  
      whilestat(ls, line);
      break;
    }
    case TK_DO: {  
      luaX_next(ls);  
      block(ls);
      check_match(ls, TK_END, TK_DO, line);
      break;
    }
    case TK_FOR: {  
      forstat(ls, line);
      break;
    }
    case TK_REPEAT: {  
      repeatstat(ls, line);
      break;
    }
    case TK_FUNCTION: {  
      funcstat(ls, line);
      break;
    }
    case TK_LOCAL: {  
      luaX_next(ls);  
      if (testnext(ls, TK_FUNCTION))  
        localfunc(ls, 0);
      else
        localstat(ls);
      break;
    }
    case TK_DEFER: {  
      luaX_next(ls);  
      localfunc(ls, 1);
      break;
    }
    case TK_DBCOLON: {  
      luaX_next(ls);  
      labelstat(ls, str_checkname(ls), line);
      break;
    }
    case TK_RETURN: {  
      luaX_next(ls);  
      retstat(ls);
      break;
    }
    case TK_BREAK:   
    case TK_GOTO: {  
      gotostat(ls, luaK_jump(ls->fs));
      break;
    }
    default: {  
      exprstat(ls);
      break;
    }
  }
  lua_assert(ls->fs->f->maxstacksize >= ls->fs->freereg &&
             ls->fs->freereg >= ls->fs->nactvar);
  ls->fs->freereg = ls->fs->nactvar;  
  leavelevel(ls);
}
static void mainfunc (LexState *ls, FuncState *fs) {
  BlockCnt bl;
  expdesc v;
  open_func(ls, fs, &bl);
  fs->f->is_vararg = 1;  
  init_exp(&v, VLOCAL, 0);  
  newupvalue(fs, ls->envn, &v);  
  luaC_objbarrier(ls->L, fs->f, ls->envn);
  luaX_next(ls);  
  statlist(ls);  
  check(ls, TK_EOS);
  close_func(ls);
}
LClosure *luaY_parser (lua_State *L, ZIO *z, Mbuffer *buff,
                       Dyndata *dyd, const char *name, int firstchar) {
  LexState lexstate;
  FuncState funcstate;
  LClosure *cl = luaF_newLclosure(L, 1);  
  setclLvalue(L, L->top, cl);  
  luaD_inctop(L);
  lexstate.h = luaH_new(L);  
  sethvalue(L, L->top, lexstate.h);  
  luaD_inctop(L);
  funcstate.f = cl->p = luaF_newproto(L);
  luaC_objbarrier(L, cl, cl->p);
  funcstate.f->source = luaS_new(L, name);  
  lua_assert(iswhite(funcstate.f));  
  lexstate.buff = buff;
  lexstate.dyd = dyd;
  dyd->actvar.n = dyd->gt.n = dyd->label.n = 0;
  luaX_setinput(L, &lexstate, z, funcstate.f->source, firstchar);
  mainfunc(&lexstate, &funcstate);
  lua_assert(!funcstate.prev && funcstate.nups == 1 && !lexstate.fs);
  lua_assert(dyd->actvar.n == 0 && dyd->gt.n == 0 && dyd->label.n == 0);
  L->top--;  
  return cl;  
}
#define lstate_c
#define LUA_CORE
#include <stddef.h>
#include <string.h>
#if !defined(LUAI_GCPAUSE)
#define LUAI_GCPAUSE	200  
#endif
#if !defined(LUAI_GCMUL)
#define LUAI_GCMUL	200 
#endif
#if !defined(luai_makeseed)
#include <time.h>
#define luai_makeseed()		cast(unsigned int, time(NULL))
#endif
typedef struct LX {
  lu_byte extra_[LUA_EXTRASPACE];
  lua_State l;
} LX;
typedef struct LG {
  LX l;
  global_State g;
} LG;
#define fromstate(L)	(cast(LX *, cast(lu_byte *, (L)) - offsetof(LX, l)))
#define addbuff(b,p,e) \
  { size_t t = cast(size_t, e); \
    memcpy(b + p, &t, sizeof(t)); p += sizeof(t); }
static unsigned int makeseed (lua_State *L) {
  char buff[4 * sizeof(size_t)];
  unsigned int h = luai_makeseed();
  int p = 0;
  addbuff(buff, p, L);  
  addbuff(buff, p, &h);  
  addbuff(buff, p, luaO_nilobject);  
  addbuff(buff, p, &lua_newstate);  
  lua_assert(p == sizeof(buff));
  return luaS_hash(buff, p, h);
}
void luaE_setdebt (global_State *g, l_mem debt) {
  l_mem tb = gettotalbytes(g);
  lua_assert(tb > 0);
  if (debt < tb - MAX_LMEM)
    debt = tb - MAX_LMEM;  
  g->totalbytes = tb - debt;
  g->GCdebt = debt;
}
CallInfo *luaE_extendCI (lua_State *L) {
  CallInfo *ci = luaM_new(L, CallInfo);
  lua_assert(L->ci->next == NULL);
  L->ci->next = ci;
  ci->previous = L->ci;
  ci->next = NULL;
  L->nci++;
  return ci;
}
void luaE_freeCI (lua_State *L) {
  CallInfo *ci = L->ci;
  CallInfo *next = ci->next;
  ci->next = NULL;
  while ((ci = next) != NULL) {
    next = ci->next;
    luaM_free(L, ci);
    L->nci--;
  }
}
void luaE_shrinkCI (lua_State *L) {
  CallInfo *ci = L->ci;
  CallInfo *next2;  
  while (ci->next != NULL && (next2 = ci->next->next) != NULL) {
    luaM_free(L, ci->next);  
    L->nci--;
    ci->next = next2;  
    next2->previous = ci;
    ci = next2;  
  }
}
static void stack_init (lua_State *L1, lua_State *L) {
  int i; CallInfo *ci;
  L1->stack = luaM_newvector(L, BASIC_STACK_SIZE, TValue);
  L1->stacksize = BASIC_STACK_SIZE;
  for (i = 0; i < BASIC_STACK_SIZE; i++)
    setnilvalue(L1->stack + i);  
  L1->top = L1->stack;
  L1->stack_last = L1->stack + L1->stacksize - EXTRA_STACK;
  ci = &L1->base_ci;
  ci->next = ci->previous = NULL;
  ci->callstatus = 0;
  ci->func = L1->top;
  setnilvalue(L1->top++);  
  ci->top = L1->top + LUA_MINSTACK;
  L1->ci = ci;
}
static void freestack (lua_State *L) {
  if (L->stack == NULL)
    return;  
  L->ci = &L->base_ci;  
  luaE_freeCI(L);
  lua_assert(L->nci == 0);
  luaM_freearray(L, L->stack, L->stacksize);  
}
static void init_registry (lua_State *L, global_State *g) {
  TValue temp;
  Table *registry = luaH_new(L);
  sethvalue(L, &g->l_registry, registry);
  luaH_resize(L, registry, LUA_RIDX_LAST, 0);
  setthvalue(L, &temp, L);  
  luaH_setint(L, registry, LUA_RIDX_MAINTHREAD, &temp);
  sethvalue(L, &temp, luaH_new(L));  
  luaH_setint(L, registry, LUA_RIDX_GLOBALS, &temp);
}
static void f_luaopen (lua_State *L, void *ud) {
  global_State *g = G(L);
  UNUSED(ud);
  stack_init(L, L);  
  init_registry(L, g);
  luaS_init(L);
  luaT_init(L);
  luaX_init(L);
  g->gcrunning = 1;  
  g->version = lua_version(NULL);
  luai_userstateopen(L);
}
static void preinit_thread (lua_State *L, global_State *g) {
  G(L) = g;
  L->stack = NULL;
  L->ci = NULL;
  L->nci = 0;
  L->stacksize = 0;
  L->twups = L;  
  L->errorJmp = NULL;
  L->nCcalls = 0;
  L->hook = NULL;
  L->hookmask = 0;
  L->basehookcount = 0;
  L->allowhook = 1;
  resethookcount(L);
  L->openupval = NULL;
  L->nny = 1;
  L->status = LUA_OK;
  L->errfunc = 0;
}
static void close_state (lua_State *L) {
  global_State *g = G(L);
  luaF_close(L, L->stack, CLOSEPROTECT);  
  luaC_freeallobjects(L);  
  if (g->version)  
    luai_userstateclose(L);
  luaM_freearray(L, G(L)->strt.hash, G(L)->strt.size);
  freestack(L);
  lua_assert(gettotalbytes(g) == sizeof(LG));
  (*g->frealloc)(g->ud, fromstate(L), sizeof(LG), 0);  
}
LUA_API lua_State *lua_newthread (lua_State *L) {
  global_State *g = G(L);
  lua_State *L1;
  lua_lock(L);
  luaC_checkGC(L);
  L1 = &cast(LX *, luaM_newobject(L, LUA_TTHREAD, sizeof(LX)))->l;
  L1->marked = luaC_white(g);
  L1->tt = LUA_TTHREAD;
  L1->next = g->allgc;
  g->allgc = obj2gco(L1);
  setthvalue(L, L->top, L1);
  api_incr_top(L);
  preinit_thread(L1, g);
  L1->hookmask = L->hookmask;
  L1->basehookcount = L->basehookcount;
  L1->hook = L->hook;
  resethookcount(L1);
  memcpy(lua_getextraspace(L1), lua_getextraspace(g->mainthread),
         LUA_EXTRASPACE);
  luai_userstatethread(L, L1);
  stack_init(L1, L);  
  lua_unlock(L);
  return L1;
}
void luaE_freethread (lua_State *L, lua_State *L1) {
  LX *l = fromstate(L1);
  luaF_close(L1, L1->stack, NOCLOSINGMETH);  
  lua_assert(L1->openupval == NULL);
  luai_userstatefree(L, L1);
  freestack(L1);
  luaM_free(L, l);
}
int lua_resetthread (lua_State *L) {
  CallInfo *ci;
  int status;
  lua_lock(L);
  L->ci = ci = &L->base_ci;  
  setnilvalue(L->stack);  
  ci->func = L->stack;
  ci->callstatus = 0;
  status = luaF_close(L, L->stack, CLOSEPROTECT);
  if (status != CLOSEPROTECT)  
    luaD_seterrorobj(L, status, L->stack + 1);
  else {
    status = LUA_OK;
    L->top = L->stack + 1;
  }
  ci->top = L->top + LUA_MINSTACK;
  L->status = status;
  lua_unlock(L);
  return status;
}
LUA_API lua_State *lua_newstate (lua_Alloc f, void *ud) {
  int i;
  lua_State *L;
  global_State *g;
  LG *l = cast(LG *, (*f)(ud, NULL, LUA_TTHREAD, sizeof(LG)));
  if (l == NULL) return NULL;
  L = &l->l.l;
  g = &l->g;
  L->next = NULL;
  L->tt = LUA_TTHREAD;
  g->currentwhite = bitmask(WHITE0BIT);
  L->marked = luaC_white(g);
  preinit_thread(L, g);
  g->frealloc = f;
  g->ud = ud;
  g->mainthread = L;
  g->seed = makeseed(L);
  g->gcrunning = 0;  
  g->GCestimate = 0;
  g->strt.size = g->strt.nuse = 0;
  g->strt.hash = NULL;
  setnilvalue(&g->l_registry);
  g->panic = NULL;
  g->version = NULL;
  g->gcstate = GCSpause;
  g->gckind = KGC_NORMAL;
  g->allgc = g->finobj = g->tobefnz = g->fixedgc = NULL;
  g->sweepgc = NULL;
  g->gray = g->grayagain = NULL;
  g->weak = g->ephemeron = g->allweak = NULL;
  g->twups = NULL;
  g->totalbytes = sizeof(LG);
  g->GCdebt = 0;
  g->gcfinnum = 0;
  g->gcpause = LUAI_GCPAUSE;
  g->gcstepmul = LUAI_GCMUL;
  for (i=0; i < LUA_NUMTAGS; i++) g->mt[i] = NULL;
  if (luaD_rawrunprotected(L, f_luaopen, NULL) != LUA_OK) {
    close_state(L);
    L = NULL;
  }
  return L;
}
LUA_API void lua_close (lua_State *L) {
  L = G(L)->mainthread;  
  lua_lock(L);
  close_state(L);
}
#define lstring_c
#define LUA_CORE
#include <string.h>
#define MEMERRMSG       "not enough memory"
#if !defined(LUAI_HASHLIMIT)
#define LUAI_HASHLIMIT		5
#endif
int luaS_eqlngstr (TString *a, TString *b) {
  size_t len = a->u.lnglen;
  lua_assert(a->tt == LUA_TLNGSTR && b->tt == LUA_TLNGSTR);
  return (a == b) ||  
    ((len == b->u.lnglen) &&  
     (memcmp(getstr(a), getstr(b), len) == 0));  
}
unsigned int luaS_hash (const char *str, size_t l, unsigned int seed) {
  unsigned int h = seed ^ cast(unsigned int, l);
  size_t step = (l >> LUAI_HASHLIMIT) + 1;
  for (; l >= step; l -= step)
    h ^= ((h<<5) + (h>>2) + cast_byte(str[l - 1]));
  return h;
}
unsigned int luaS_hashlongstr (TString *ts) {
  lua_assert(ts->tt == LUA_TLNGSTR);
  if (ts->extra == 0) {  
    ts->hash = luaS_hash(getstr(ts), ts->u.lnglen, ts->hash);
    ts->extra = 1;  
  }
  return ts->hash;
}
void luaS_resize (lua_State *L, int newsize) {
  int i;
  stringtable *tb = &G(L)->strt;
  if (newsize > tb->size) {  
    luaM_reallocvector(L, tb->hash, tb->size, newsize, TString *);
    for (i = tb->size; i < newsize; i++)
      tb->hash[i] = NULL;
  }
  for (i = 0; i < tb->size; i++) {  
    TString *p = tb->hash[i];
    tb->hash[i] = NULL;
    while (p) {  
      TString *hnext = p->u.hnext;  
      unsigned int h = lmod(p->hash, newsize);  
      p->u.hnext = tb->hash[h];  
      tb->hash[h] = p;
      p = hnext;
    }
  }
  if (newsize < tb->size) {  
    lua_assert(tb->hash[newsize] == NULL && tb->hash[tb->size - 1] == NULL);
    luaM_reallocvector(L, tb->hash, tb->size, newsize, TString *);
  }
  tb->size = newsize;
}
void luaS_clearcache (global_State *g) {
  int i, j;
  for (i = 0; i < STRCACHE_N; i++)
    for (j = 0; j < STRCACHE_M; j++) {
    if (iswhite(g->strcache[i][j]))  
      g->strcache[i][j] = g->memerrmsg;  
    }
}
void luaS_init (lua_State *L) {
  global_State *g = G(L);
  int i, j;
  luaS_resize(L, MINSTRTABSIZE);  
  g->memerrmsg = luaS_newliteral(L, MEMERRMSG);
  luaC_fix(L, obj2gco(g->memerrmsg));  
  for (i = 0; i < STRCACHE_N; i++)  
    for (j = 0; j < STRCACHE_M; j++)
      g->strcache[i][j] = g->memerrmsg;
}
static TString *createstrobj (lua_State *L, size_t l, int tag, unsigned int h) {
  TString *ts;
  GCObject *o;
  size_t totalsize;  
  totalsize = sizelstring(l);
  o = luaC_newobj(L, tag, totalsize);
  ts = gco2ts(o);
  ts->hash = h;
  ts->extra = 0;
  getstr(ts)[l] = '\0';  
  return ts;
}
TString *luaS_createlngstrobj (lua_State *L, size_t l) {
  TString *ts = createstrobj(L, l, LUA_TLNGSTR, G(L)->seed);
  ts->u.lnglen = l;
  return ts;
}
void luaS_remove (lua_State *L, TString *ts) {
  stringtable *tb = &G(L)->strt;
  TString **p = &tb->hash[lmod(ts->hash, tb->size)];
  while (*p != ts)  
    p = &(*p)->u.hnext;
  *p = (*p)->u.hnext;  
  tb->nuse--;
}
static TString *internshrstr (lua_State *L, const char *str, size_t l) {
  TString *ts;
  global_State *g = G(L);
  unsigned int h = luaS_hash(str, l, g->seed);
  TString **list = &g->strt.hash[lmod(h, g->strt.size)];
  lua_assert(str != NULL);  
  for (ts = *list; ts != NULL; ts = ts->u.hnext) {
    if (l == ts->shrlen &&
        (memcmp(str, getstr(ts), l * sizeof(char)) == 0)) {
      if (isdead(g, ts))  
        changewhite(ts);  
      return ts;
    }
  }
  if (g->strt.nuse >= g->strt.size && g->strt.size <= MAX_INT/2) {
    luaS_resize(L, g->strt.size * 2);
    list = &g->strt.hash[lmod(h, g->strt.size)];  
  }
  ts = createstrobj(L, l, LUA_TSHRSTR, h);
  memcpy(getstr(ts), str, l * sizeof(char));
  ts->shrlen = cast_byte(l);
  ts->u.hnext = *list;
  *list = ts;
  g->strt.nuse++;
  return ts;
}
TString *luaS_newlstr (lua_State *L, const char *str, size_t l) {
  if (l <= LUAI_MAXSHORTLEN)  
    return internshrstr(L, str, l);
  else {
    TString *ts;
    if (l >= (MAX_SIZE - sizeof(TString))/sizeof(char))
      luaM_toobig(L);
    ts = luaS_createlngstrobj(L, l);
    memcpy(getstr(ts), str, l * sizeof(char));
    return ts;
  }
}
TString *luaS_new (lua_State *L, const char *str) {
  unsigned int i = point2uint(str) % STRCACHE_N;  
  int j;
  TString **p = G(L)->strcache[i];
  for (j = 0; j < STRCACHE_M; j++) {
    if (strcmp(str, getstr(p[j])) == 0)  
      return p[j];  
  }
  for (j = STRCACHE_M - 1; j > 0; j--)
    p[j] = p[j - 1];  
  p[0] = luaS_newlstr(L, str, strlen(str));
  return p[0];
}
Udata *luaS_newudata (lua_State *L, size_t s) {
  Udata *u;
  GCObject *o;
  if (s > MAX_SIZE - sizeof(Udata))
    luaM_toobig(L);
  o = luaC_newobj(L, LUA_TUSERDATA, sizeludata(s));
  u = gco2u(o);
  u->len = s;
  u->metatable = NULL;
  setuservalue(L, u, luaO_nilobject);
  return u;
}
#define ltable_c
#define LUA_CORE
#include <math.h>
#include <limits.h>
#define MAXABITS	cast_int(sizeof(int) * CHAR_BIT - 1)
#define MAXASIZE	(1u << MAXABITS)
#define MAXHBITS	(MAXABITS - 1)
#define hashpow2(t,n)		(gnode(t, lmod((n), sizenode(t))))
#define hashstr(t,str)		hashpow2(t, (str)->hash)
#define hashboolean(t,p)	hashpow2(t, p)
#define hashint(t,i)		hashpow2(t, i)
#define hashmod(t,n)	(gnode(t, ((n) % ((sizenode(t)-1)|1))))
#define hashpointer(t,p)	hashmod(t, point2uint(p))
#define dummynode		(&dummynode_)
static const Node dummynode_ = {
  {NILCONSTANT},  
  {{NILCONSTANT, 0}}  
};
#if !defined(l_hashfloat)
static int l_hashfloat (lua_Number n) {
  int i;
  lua_Integer ni;
  n = l_mathop(frexp)(n, &i) * -cast_num(INT_MIN);
  if (!lua_numbertointeger(n, &ni)) {  
    lua_assert(luai_numisnan(n) || l_mathop(fabs)(n) == cast_num(HUGE_VAL));
    return 0;
  }
  else {  
    unsigned int u = cast(unsigned int, i) + cast(unsigned int, ni);
    return cast_int(u <= cast(unsigned int, INT_MAX) ? u : ~u);
  }
}
#endif
static Node *mainposition (const Table *t, const TValue *key) {
  switch (ttype(key)) {
    case LUA_TNUMINT:
      return hashint(t, ivalue(key));
    case LUA_TNUMFLT:
      return hashmod(t, l_hashfloat(fltvalue(key)));
    case LUA_TSHRSTR:
      return hashstr(t, tsvalue(key));
    case LUA_TLNGSTR:
      return hashpow2(t, luaS_hashlongstr(tsvalue(key)));
    case LUA_TBOOLEAN:
      return hashboolean(t, bvalue(key));
    case LUA_TLIGHTUSERDATA:
      return hashpointer(t, pvalue(key));
    case LUA_TLCF:
      return hashpointer(t, fvalue(key));
    default:
      lua_assert(!ttisdeadkey(key));
      return hashpointer(t, gcvalue(key));
  }
}
static unsigned int arrayindex (const TValue *key) {
  if (ttisinteger(key)) {
    lua_Integer k = ivalue(key);
    if (0 < k && (lua_Unsigned)k <= MAXASIZE)
      return cast(unsigned int, k);  
  }
  return 0;  
}
static unsigned int findindex (lua_State *L, Table *t, StkId key) {
  unsigned int i;
  if (ttisnil(key)) return 0;  
  i = arrayindex(key);
  if (i != 0 && i <= t->sizearray)  
    return i;  
  else {
    int nx;
    Node *n = mainposition(t, key);
    for (;;) {  
      if (luaV_rawequalobj(gkey(n), key) ||
            (ttisdeadkey(gkey(n)) && iscollectable(key) &&
             deadvalue(gkey(n)) == gcvalue(key))) {
        i = cast_int(n - gnode(t, 0));  
        return (i + 1) + t->sizearray;
      }
      nx = gnext(n);
      if (nx == 0)
        luaG_runerror(L, "invalid key to 'next'");  
      else n += nx;
    }
  }
}
int luaH_next (lua_State *L, Table *t, StkId key) {
  unsigned int i = findindex(L, t, key);  
  for (; i < t->sizearray; i++) {  
    if (!ttisnil(&t->array[i])) {  
      setivalue(key, i + 1);
      setobj2s(L, key+1, &t->array[i]);
      return 1;
    }
  }
  for (i -= t->sizearray; cast_int(i) < sizenode(t); i++) {  
    if (!ttisnil(gval(gnode(t, i)))) {  
      setobj2s(L, key, gkey(gnode(t, i)));
      setobj2s(L, key+1, gval(gnode(t, i)));
      return 1;
    }
  }
  return 0;  
}
static unsigned int computesizes (unsigned int nums[], unsigned int *pna) {
  int i;
  unsigned int twotoi;  
  unsigned int a = 0;  
  unsigned int na = 0;  
  unsigned int optimal = 0;  
  for (i = 0, twotoi = 1;
       twotoi > 0 && *pna > twotoi / 2;
       i++, twotoi *= 2) {
    if (nums[i] > 0) {
      a += nums[i];
      if (a > twotoi/2) {  
        optimal = twotoi;  
        na = a;  
      }
    }
  }
  lua_assert((optimal == 0 || optimal / 2 < na) && na <= optimal);
  *pna = na;
  return optimal;
}
static int countint (const TValue *key, unsigned int *nums) {
  unsigned int k = arrayindex(key);
  if (k != 0) {  
    nums[luaO_ceillog2(k)]++;  
    return 1;
  }
  else
    return 0;
}
static unsigned int numusearray (const Table *t, unsigned int *nums) {
  int lg;
  unsigned int ttlg;  
  unsigned int ause = 0;  
  unsigned int i = 1;  
  for (lg = 0, ttlg = 1; lg <= MAXABITS; lg++, ttlg *= 2) {
    unsigned int lc = 0;  
    unsigned int lim = ttlg;
    if (lim > t->sizearray) {
      lim = t->sizearray;  
      if (i > lim)
        break;  
    }
    for (; i <= lim; i++) {
      if (!ttisnil(&t->array[i-1]))
        lc++;
    }
    nums[lg] += lc;
    ause += lc;
  }
  return ause;
}
static int numusehash (const Table *t, unsigned int *nums, unsigned int *pna) {
  int totaluse = 0;  
  int ause = 0;  
  int i = sizenode(t);
  while (i--) {
    Node *n = &t->node[i];
    if (!ttisnil(gval(n))) {
      ause += countint(gkey(n), nums);
      totaluse++;
    }
  }
  *pna += ause;
  return totaluse;
}
static void setarrayvector (lua_State *L, Table *t, unsigned int size) {
  unsigned int i;
  luaM_reallocvector(L, t->array, t->sizearray, size, TValue);
  for (i=t->sizearray; i<size; i++)
     setnilvalue(&t->array[i]);
  t->sizearray = size;
}
static void setnodevector (lua_State *L, Table *t, unsigned int size) {
  if (size == 0) {  
    t->node = cast(Node *, dummynode);  
    t->lsizenode = 0;
    t->lastfree = NULL;  
  }
  else {
    int i;
    int lsize = luaO_ceillog2(size);
    if (lsize > MAXHBITS)
      luaG_runerror(L, "table overflow");
    size = twoto(lsize);
    t->node = luaM_newvector(L, size, Node);
    for (i = 0; i < (int)size; i++) {
      Node *n = gnode(t, i);
      gnext(n) = 0;
      setnilvalue(wgkey(n));
      setnilvalue(gval(n));
    }
    t->lsizenode = cast_byte(lsize);
    t->lastfree = gnode(t, size);  
  }
}
typedef struct {
  Table *t;
  unsigned int nhsize;
} AuxsetnodeT;
static void auxsetnode (lua_State *L, void *ud) {
  AuxsetnodeT *asn = cast(AuxsetnodeT *, ud);
  setnodevector(L, asn->t, asn->nhsize);
}
void luaH_resize (lua_State *L, Table *t, unsigned int nasize,
                                          unsigned int nhsize) {
  unsigned int i;
  int j;
  AuxsetnodeT asn;
  unsigned int oldasize = t->sizearray;
  int oldhsize = allocsizenode(t);
  Node *nold = t->node;  
  if (nasize > oldasize)  
    setarrayvector(L, t, nasize);
  asn.t = t; asn.nhsize = nhsize;
  if (luaD_rawrunprotected(L, auxsetnode, &asn) != LUA_OK) {  
    setarrayvector(L, t, oldasize);  
    luaD_throw(L, LUA_ERRMEM);  
  }
  if (nasize < oldasize) {  
    t->sizearray = nasize;
    for (i=nasize; i<oldasize; i++) {
      if (!ttisnil(&t->array[i]))
        luaH_setint(L, t, i + 1, &t->array[i]);
    }
    luaM_reallocvector(L, t->array, oldasize, nasize, TValue);
  }
  for (j = oldhsize - 1; j >= 0; j--) {
    Node *old = nold + j;
    if (!ttisnil(gval(old))) {
      setobjt2t(L, luaH_set(L, t, gkey(old)), gval(old));
    }
  }
  if (oldhsize > 0)  
    luaM_freearray(L, nold, cast(size_t, oldhsize)); 
}
void luaH_resizearray (lua_State *L, Table *t, unsigned int nasize) {
  int nsize = allocsizenode(t);
  luaH_resize(L, t, nasize, nsize);
}
static void rehash (lua_State *L, Table *t, const TValue *ek) {
  unsigned int asize;  
  unsigned int na;  
  unsigned int nums[MAXABITS + 1];
  int i;
  int totaluse;
  for (i = 0; i <= MAXABITS; i++) nums[i] = 0;  
  na = numusearray(t, nums);  
  totaluse = na;  
  totaluse += numusehash(t, nums, &na);  
  na += countint(ek, nums);
  totaluse++;
  asize = computesizes(nums, &na);
  luaH_resize(L, t, asize, totaluse - na);
}
Table *luaH_new (lua_State *L) {
  GCObject *o = luaC_newobj(L, LUA_TTABLE, sizeof(Table));
  Table *t = gco2t(o);
  t->metatable = NULL;
  t->flags = cast_byte(~0);
  t->array = NULL;
  t->sizearray = 0;
  setnodevector(L, t, 0);
  return t;
}
void luaH_free (lua_State *L, Table *t) {
  if (!isdummy(t))
    luaM_freearray(L, t->node, cast(size_t, sizenode(t)));
  luaM_freearray(L, t->array, t->sizearray);
  luaM_free(L, t);
}
static Node *getfreepos (Table *t) {
  if (!isdummy(t)) {
    while (t->lastfree > t->node) {
      t->lastfree--;
      if (ttisnil(gkey(t->lastfree)))
        return t->lastfree;
    }
  }
  return NULL;  
}
TValue *luaH_newkey (lua_State *L, Table *t, const TValue *key) {
  Node *mp;
  TValue aux;
  if (ttisnil(key)) luaG_runerror(L, "table index is nil");
  else if (ttisfloat(key)) {
    lua_Integer k;
    if (luaV_tointeger(key, &k, 0)) {  
      setivalue(&aux, k);
      key = &aux;  
    }
    else if (luai_numisnan(fltvalue(key)))
      luaG_runerror(L, "table index is NaN");
  }
  mp = mainposition(t, key);
  if (!ttisnil(gval(mp)) || isdummy(t)) {  
    Node *othern;
    Node *f = getfreepos(t);  
    if (f == NULL) {  
      rehash(L, t, key);  
      return luaH_set(L, t, key);  
    }
    lua_assert(!isdummy(t));
    othern = mainposition(t, gkey(mp));
    if (othern != mp) {  
      while (othern + gnext(othern) != mp)  
        othern += gnext(othern);
      gnext(othern) = cast_int(f - othern);  
      *f = *mp;  
      if (gnext(mp) != 0) {
        gnext(f) += cast_int(mp - f);  
        gnext(mp) = 0;  
      }
      setnilvalue(gval(mp));
    }
    else {  
      if (gnext(mp) != 0)
        gnext(f) = cast_int((mp + gnext(mp)) - f);  
      else lua_assert(gnext(f) == 0);
      gnext(mp) = cast_int(f - mp);
      mp = f;
    }
  }
  setnodekey(L, &mp->i_key, key);
  luaC_barrierback(L, t, key);
  lua_assert(ttisnil(gval(mp)));
  return gval(mp);
}
const TValue *luaH_getint (Table *t, lua_Integer key) {
  if (l_castS2U(key) - 1 < t->sizearray)
    return &t->array[key - 1];
  else {
    Node *n = hashint(t, key);
    for (;;) {  
      if (ttisinteger(gkey(n)) && ivalue(gkey(n)) == key)
        return gval(n);  
      else {
        int nx = gnext(n);
        if (nx == 0) break;
        n += nx;
      }
    }
    return luaO_nilobject;
  }
}
const TValue *luaH_getshortstr (Table *t, TString *key) {
  Node *n = hashstr(t, key);
  lua_assert(key->tt == LUA_TSHRSTR);
  for (;;) {  
    const TValue *k = gkey(n);
    if (ttisshrstring(k) && eqshrstr(tsvalue(k), key))
      return gval(n);  
    else {
      int nx = gnext(n);
      if (nx == 0)
        return luaO_nilobject;  
      n += nx;
    }
  }
}
static const TValue *getgeneric (Table *t, const TValue *key) {
  Node *n = mainposition(t, key);
  for (;;) {  
    if (luaV_rawequalobj(gkey(n), key))
      return gval(n);  
    else {
      int nx = gnext(n);
      if (nx == 0)
        return luaO_nilobject;  
      n += nx;
    }
  }
}
const TValue *luaH_getstr (Table *t, TString *key) {
  if (key->tt == LUA_TSHRSTR)
    return luaH_getshortstr(t, key);
  else {  
    TValue ko;
    setsvalue(cast(lua_State *, NULL), &ko, key);
    return getgeneric(t, &ko);
  }
}
const TValue *luaH_get (Table *t, const TValue *key) {
  switch (ttype(key)) {
    case LUA_TSHRSTR: return luaH_getshortstr(t, tsvalue(key));
    case LUA_TNUMINT: return luaH_getint(t, ivalue(key));
    case LUA_TNIL: return luaO_nilobject;
    case LUA_TNUMFLT: {
      lua_Integer k;
      if (luaV_tointeger(key, &k, 0)) 
        return luaH_getint(t, k);  
    }  
    default:
      return getgeneric(t, key);
  }
}
TValue *luaH_set (lua_State *L, Table *t, const TValue *key) {
  const TValue *p = luaH_get(t, key);
  if (p != luaO_nilobject)
    return cast(TValue *, p);
  else return luaH_newkey(L, t, key);
}
void luaH_setint (lua_State *L, Table *t, lua_Integer key, TValue *value) {
  const TValue *p = luaH_getint(t, key);
  TValue *cell;
  if (p != luaO_nilobject)
    cell = cast(TValue *, p);
  else {
    TValue k;
    setivalue(&k, key);
    cell = luaH_newkey(L, t, &k);
  }
  setobj2t(L, cell, value);
}
static lua_Unsigned unbound_search (Table *t, lua_Unsigned j) {
  lua_Unsigned i = j;  
  j++;
  while (!ttisnil(luaH_getint(t, j))) {
    i = j;
    if (j > l_castS2U(LUA_MAXINTEGER) / 2) {  
      i = 1;
      while (!ttisnil(luaH_getint(t, i))) i++;
      return i - 1;
    }
    j *= 2;
  }
  while (j - i > 1) {
    lua_Unsigned m = (i+j)/2;
    if (ttisnil(luaH_getint(t, m))) j = m;
    else i = m;
  }
  return i;
}
lua_Unsigned luaH_getn (Table *t) {
  unsigned int j = t->sizearray;
  if (j > 0 && ttisnil(&t->array[j - 1])) {
    unsigned int i = 0;
    while (j - i > 1) {
      unsigned int m = (i+j)/2;
      if (ttisnil(&t->array[m - 1])) j = m;
      else i = m;
    }
    return i;
  }
  else if (isdummy(t))  
    return j;  
  else return unbound_search(t, j);
}
#if defined(LUA_DEBUG)
Node *luaH_mainposition (const Table *t, const TValue *key) {
  return mainposition(t, key);
}
int luaH_isdummy (const Table *t) { return isdummy(t); }
#endif
#define ltm_c
#define LUA_CORE
#include <string.h>
static const char udatatypename[] = "userdata";
LUAI_DDEF const char *const luaT_typenames_[LUA_TOTALTAGS] = {
  "no value",
  "nil", "boolean", udatatypename, "number",
  "string", "table", "function", udatatypename, "thread",
  "proto" 
};
void luaT_init (lua_State *L) {
  static const char *const luaT_eventname[] = {  
    "__index", "__newindex",
    "__gc", "__mode", "__len", "__eq",
    "__add", "__sub", "__mul", "__mod", "__pow",
    "__div", "__idiv",
    "__band", "__bor", "__bxor", "__shl", "__shr",
    "__unm", "__bnot", "__lt", "__le",
    "__concat", "__call"
  };
  int i;
  for (i=0; i<TM_N; i++) {
    G(L)->tmname[i] = luaS_new(L, luaT_eventname[i]);
    luaC_fix(L, obj2gco(G(L)->tmname[i]));  
  }
}
const TValue *luaT_gettm (Table *events, TMS event, TString *ename) {
  const TValue *tm = luaH_getshortstr(events, ename);
  lua_assert(event <= TM_EQ);
  if (ttisnil(tm)) {  
    events->flags |= cast_byte(1u<<event);  
    return NULL;
  }
  else return tm;
}
const TValue *luaT_gettmbyobj (lua_State *L, const TValue *o, TMS event) {
  Table *mt;
  switch (ttnov(o)) {
    case LUA_TTABLE:
      mt = hvalue(o)->metatable;
      break;
    case LUA_TUSERDATA:
      mt = uvalue(o)->metatable;
      break;
    default:
      mt = G(L)->mt[ttnov(o)];
  }
  return (mt ? luaH_getshortstr(mt, G(L)->tmname[event]) : luaO_nilobject);
}
const char *luaT_objtypename (lua_State *L, const TValue *o) {
  Table *mt;
  if ((ttistable(o) && (mt = hvalue(o)->metatable) != NULL) ||
      (ttisfulluserdata(o) && (mt = uvalue(o)->metatable) != NULL)) {
    const TValue *name = luaH_getshortstr(mt, luaS_new(L, "__name"));
    if (ttisstring(name))  
      return getstr(tsvalue(name));  
  }
  return ttypename(ttnov(o));  
}
void luaT_callTM (lua_State *L, const TValue *f, const TValue *p1,
                  const TValue *p2, TValue *p3, int hasres) {
  ptrdiff_t result = savestack(L, p3);
  StkId func = L->top;
  setobj2s(L, func, f);  
  setobj2s(L, func + 1, p1);  
  setobj2s(L, func + 2, p2);  
  L->top += 3;
  if (!hasres)  
    setobj2s(L, L->top++, p3);  
  if (isLua(L->ci))
    luaD_call(L, func, hasres);
  else
    luaD_callnoyield(L, func, hasres);
  if (hasres) {  
    p3 = restorestack(L, result);
    setobjs2s(L, p3, --L->top);
  }
}
int luaT_callbinTM (lua_State *L, const TValue *p1, const TValue *p2,
                    StkId res, TMS event) {
  const TValue *tm = luaT_gettmbyobj(L, p1, event);  
  if (ttisnil(tm))
    tm = luaT_gettmbyobj(L, p2, event);  
  if (ttisnil(tm)) return 0;
  luaT_callTM(L, tm, p1, p2, res, 1);
  return 1;
}
void luaT_trybinTM (lua_State *L, const TValue *p1, const TValue *p2,
                    StkId res, TMS event) {
  if (!luaT_callbinTM(L, p1, p2, res, event)) {
    switch (event) {
      case TM_CONCAT:
        luaG_concaterror(L, p1, p2);
      case TM_BAND: case TM_BOR: case TM_BXOR:
      case TM_SHL: case TM_SHR: case TM_BNOT: {
        lua_Number dummy;
        if (tonumber(p1, &dummy) && tonumber(p2, &dummy))
          luaG_tointerror(L, p1, p2);
        else
          luaG_opinterror(L, p1, p2, "perform bitwise operation on");
      }
      default:
        luaG_opinterror(L, p1, p2, "perform arithmetic on");
    }
  }
}
int luaT_callorderTM (lua_State *L, const TValue *p1, const TValue *p2,
                      TMS event) {
  if (!luaT_callbinTM(L, p1, p2, L->top, event))
    return -1;  
  else
    return !l_isfalse(L->top);
}
#define lundump_c
#define LUA_CORE
#include <string.h>
#if !defined(luai_verifycode)
#define luai_verifycode(L,b,f)  
#endif
typedef struct {
  lua_State *L;
  ZIO *Z;
  const char *name;
} LoadState;
static l_noret error(LoadState *S, const char *why) {
  luaO_pushfstring(S->L, "%s: %s precompiled chunk", S->name, why);
  luaD_throw(S->L, LUA_ERRSYNTAX);
}
#define LoadVector(S,b,n)	LoadBlock(S,b,(n)*sizeof((b)[0]))
static void LoadBlock (LoadState *S, void *b, size_t size) {
  if (luaZ_read(S->Z, b, size) != 0)
    error(S, "truncated");
}
#define LoadVar(S,x)		LoadVector(S,&x,1)
static lu_byte LoadByte (LoadState *S) {
  lu_byte x;
  LoadVar(S, x);
  return x;
}
static int LoadInt (LoadState *S) {
  int x;
  LoadVar(S, x);
  return x;
}
static lua_Number LoadNumber (LoadState *S) {
  lua_Number x;
  LoadVar(S, x);
  return x;
}
static lua_Integer LoadInteger (LoadState *S) {
  lua_Integer x;
  LoadVar(S, x);
  return x;
}
static TString *LoadString (LoadState *S, Proto *p) {
  lua_State *L = S->L;
  size_t size = LoadByte(S);
  TString *ts;
  if (size == 0xFF)
    LoadVar(S, size);
  if (size == 0)
    return NULL;
  else if (--size <= LUAI_MAXSHORTLEN) {  
    char buff[LUAI_MAXSHORTLEN];
    LoadVector(S, buff, size);
    ts = luaS_newlstr(L, buff, size);
  }
  else {  
    ts = luaS_createlngstrobj(L, size);
    setsvalue2s(L, L->top, ts);  
    luaD_inctop(L);
    LoadVector(S, getstr(ts), size);  
    L->top--;  
  }
  luaC_objbarrier(L, p, ts);
  return ts;
}
static void LoadCode (LoadState *S, Proto *f) {
  int n = LoadInt(S);
  f->code = luaM_newvector(S->L, n, Instruction);
  f->sizecode = n;
  LoadVector(S, f->code, n);
}
static void LoadFunction(LoadState *S, Proto *f, TString *psource);
static void LoadConstants (LoadState *S, Proto *f) {
  int i;
  int n = LoadInt(S);
  f->k = luaM_newvector(S->L, n, TValue);
  f->sizek = n;
  for (i = 0; i < n; i++)
    setnilvalue(&f->k[i]);
  for (i = 0; i < n; i++) {
    TValue *o = &f->k[i];
    int t = LoadByte(S);
    switch (t) {
    case LUA_TNIL:
      setnilvalue(o);
      break;
    case LUA_TBOOLEAN:
      setbvalue(o, LoadByte(S));
      break;
    case LUA_TNUMFLT:
      setfltvalue(o, LoadNumber(S));
      break;
    case LUA_TNUMINT:
      setivalue(o, LoadInteger(S));
      break;
    case LUA_TSHRSTR:
    case LUA_TLNGSTR:
      setsvalue2n(S->L, o, LoadString(S, f));
      break;
    default:
      lua_assert(0);
    }
  }
}
static void LoadProtos (LoadState *S, Proto *f) {
  int i;
  int n = LoadInt(S);
  f->p = luaM_newvector(S->L, n, Proto *);
  f->sizep = n;
  for (i = 0; i < n; i++)
    f->p[i] = NULL;
  for (i = 0; i < n; i++) {
    f->p[i] = luaF_newproto(S->L);
    luaC_objbarrier(S->L, f, f->p[i]);
    LoadFunction(S, f->p[i], f->source);
  }
}
static void LoadUpvalues (LoadState *S, Proto *f) {
  int i, n;
  n = LoadInt(S);
  f->upvalues = luaM_newvector(S->L, n, Upvaldesc);
  f->sizeupvalues = n;
  for (i = 0; i < n; i++)
    f->upvalues[i].name = NULL;
  for (i = 0; i < n; i++) {
    f->upvalues[i].instack = LoadByte(S);
    f->upvalues[i].idx = LoadByte(S);
  }
}
static void LoadDebug (LoadState *S, Proto *f) {
  int i, n;
  n = LoadInt(S);
  f->lineinfo = luaM_newvector(S->L, n, int);
  f->sizelineinfo = n;
  LoadVector(S, f->lineinfo, n);
  n = LoadInt(S);
  f->locvars = luaM_newvector(S->L, n, LocVar);
  f->sizelocvars = n;
  for (i = 0; i < n; i++)
    f->locvars[i].varname = NULL;
  for (i = 0; i < n; i++) {
    f->locvars[i].varname = LoadString(S, f);
    f->locvars[i].startpc = LoadInt(S);
    f->locvars[i].endpc = LoadInt(S);
  }
  n = LoadInt(S);
  for (i = 0; i < n; i++)
    f->upvalues[i].name = LoadString(S, f);
}
static void LoadFunction (LoadState *S, Proto *f, TString *psource) {
  f->source = LoadString(S, f);
  if (f->source == NULL)  
    f->source = psource;  
  f->linedefined = LoadInt(S);
  f->lastlinedefined = LoadInt(S);
  f->numparams = LoadByte(S);
  f->is_vararg = LoadByte(S);
  f->maxstacksize = LoadByte(S);
  LoadCode(S, f);
  LoadConstants(S, f);
  LoadUpvalues(S, f);
  LoadProtos(S, f);
  LoadDebug(S, f);
}
static void checkliteral (LoadState *S, const char *s, const char *msg) {
  char buff[sizeof(LUA_SIGNATURE) + sizeof(LUAC_DATA)]; 
  size_t len = strlen(s);
  LoadVector(S, buff, len);
  if (memcmp(s, buff, len) != 0)
    error(S, msg);
}
static void fchecksize (LoadState *S, size_t size, const char *tname) {
  if (LoadByte(S) != size)
    error(S, luaO_pushfstring(S->L, "%s size mismatch in", tname));
}
#define checksize(S,t)	fchecksize(S,sizeof(t),#t)
static void checkHeader (LoadState *S) {
  checkliteral(S, LUA_SIGNATURE + 1, "not a");  
  if (LoadByte(S) != LUAC_VERSION)
    error(S, "version mismatch in");
  if (LoadByte(S) != LUAC_FORMAT)
    error(S, "format mismatch in");
  checkliteral(S, LUAC_DATA, "corrupted");
  checksize(S, int);
  checksize(S, size_t);
  checksize(S, Instruction);
  checksize(S, lua_Integer);
  checksize(S, lua_Number);
  if (LoadInteger(S) != LUAC_INT)
    error(S, "endianness mismatch in");
  if (LoadNumber(S) != LUAC_NUM)
    error(S, "float format mismatch in");
}
LClosure *luaU_undump(lua_State *L, ZIO *Z, const char *name) {
  LoadState S;
  LClosure *cl;
  if (*name == '@' || *name == '=')
    S.name = name + 1;
  else if (*name == LUA_SIGNATURE[0])
    S.name = "binary string";
  else
    S.name = name;
  S.L = L;
  S.Z = Z;
  checkHeader(&S);
  cl = luaF_newLclosure(L, LoadByte(&S));
  setclLvalue(L, L->top, cl);
  luaD_inctop(L);
  cl->p = luaF_newproto(L);
  luaC_objbarrier(L, cl, cl->p);
  LoadFunction(&S, cl->p, NULL);
  lua_assert(cl->nupvalues == cl->p->sizeupvalues);
  luai_verifycode(L, buff, cl->p);
  return cl;
}
#define lvm_c
#define LUA_CORE
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXTAGLOOP	2000
#if !defined(l_intfitsf)
#define NBM		(l_mathlim(MANT_DIG))
#if ((((LUA_MAXINTEGER >> (NBM / 4)) >> (NBM / 4)) >> (NBM / 4)) \
	>> (NBM - (3 * (NBM / 4))))  >  0
#define l_intfitsf(i)  \
  (-((lua_Integer)1 << NBM) <= (i) && (i) <= ((lua_Integer)1 << NBM))
#endif
#endif
int luaV_tonumber_ (const TValue *obj, lua_Number *n) {
  TValue v;
  if (ttisinteger(obj)) {
    *n = cast_num(ivalue(obj));
    return 1;
  }
  else if (cvt2num(obj) &&  
            luaO_str2num(svalue(obj), &v) == vslen(obj) + 1) {
    *n = nvalue(&v);  
    return 1;
  }
  else
    return 0;  
}
int luaV_tointeger (const TValue *obj, lua_Integer *p, int mode) {
  TValue v;
 again:
  if (ttisfloat(obj)) {
    lua_Number n = fltvalue(obj);
    lua_Number f = l_floor(n);
    if (n != f) {  
      if (mode == 0) return 0;  
      else if (mode > 1)  
        f += 1;  
    }
    return lua_numbertointeger(f, p);
  }
  else if (ttisinteger(obj)) {
    *p = ivalue(obj);
    return 1;
  }
  else if (cvt2num(obj) &&
            luaO_str2num(svalue(obj), &v) == vslen(obj) + 1) {
    obj = &v;
    goto again;  
  }
  return 0;  
}
static int forlimit (const TValue *obj, lua_Integer *p, lua_Integer step,
                     int *stopnow) {
  *stopnow = 0;  
  if (!luaV_tointeger(obj, p, (step < 0 ? 2 : 1))) {  
    lua_Number n;  
    if (!tonumber(obj, &n)) 
      return 0;  
    if (luai_numlt(0, n)) {  
      *p = LUA_MAXINTEGER;
      if (step < 0) *stopnow = 1;
    }
    else {  
      *p = LUA_MININTEGER;
      if (step >= 0) *stopnow = 1;
    }
  }
  return 1;
}
void luaV_finishget (lua_State *L, const TValue *t, TValue *key, StkId val,
                      const TValue *slot) {
  int loop;  
  const TValue *tm;  
  for (loop = 0; loop < MAXTAGLOOP; loop++) {
    if (slot == NULL) {  
      lua_assert(!ttistable(t));
      tm = luaT_gettmbyobj(L, t, TM_INDEX);
      if (ttisnil(tm))
        luaG_typeerror(L, t, "index");  
    }
    else {  
      lua_assert(ttisnil(slot));
      tm = fasttm(L, hvalue(t)->metatable, TM_INDEX);  
      if (tm == NULL) {  
        setnilvalue(val);  
        return;
      }
    }
    if (ttisfunction(tm)) {  
      luaT_callTM(L, tm, t, key, val, 1);  
      return;
    }
    t = tm;  
    if (luaV_fastget(L,t,key,slot,luaH_get)) {  
      setobj2s(L, val, slot);  
      return;
    }
  }
  luaG_runerror(L, "'__index' chain too long; possible loop");
}
void luaV_finishset (lua_State *L, const TValue *t, TValue *key,
                     StkId val, const TValue *slot) {
  int loop;  
  for (loop = 0; loop < MAXTAGLOOP; loop++) {
    const TValue *tm;  
    if (slot != NULL) {  
      Table *h = hvalue(t);  
      lua_assert(ttisnil(slot));  
      tm = fasttm(L, h->metatable, TM_NEWINDEX);  
      if (tm == NULL) {  
        if (slot == luaO_nilobject)  
          slot = luaH_newkey(L, h, key);  
        setobj2t(L, cast(TValue *, slot), val);  
        invalidateTMcache(h);
        luaC_barrierback(L, h, val);
        return;
      }
    }
    else {  
      if (ttisnil(tm = luaT_gettmbyobj(L, t, TM_NEWINDEX)))
        luaG_typeerror(L, t, "index");
    }
    if (ttisfunction(tm)) {
      luaT_callTM(L, tm, t, key, val, 0);
      return;
    }
    t = tm;  
    if (luaV_fastset(L, t, key, slot, luaH_get, val))
      return;  
  }
  luaG_runerror(L, "'__newindex' chain too long; possible loop");
}
static int l_strcmp (const TString *ls, const TString *rs) {
  const char *l = getstr(ls);
  size_t ll = tsslen(ls);
  const char *r = getstr(rs);
  size_t lr = tsslen(rs);
  for (;;) {  
    int temp = strcoll(l, r);
    if (temp != 0)  
      return temp;  
    else {  
      size_t len = strlen(l);  
      if (len == lr)  
        return (len == ll) ? 0 : 1;  
      else if (len == ll)  
        return -1;  
      len++;
      l += len; ll -= len; r += len; lr -= len;
    }
  }
}
static int LTintfloat (lua_Integer i, lua_Number f) {
#if defined(l_intfitsf)
  if (!l_intfitsf(i)) {
    if (f >= -cast_num(LUA_MININTEGER))  
      return 1;  
    else if (f > cast_num(LUA_MININTEGER))  
      return (i < cast(lua_Integer, f));  
    else  
      return 0;
  }
#endif
  return luai_numlt(cast_num(i), f);  
}
static int LEintfloat (lua_Integer i, lua_Number f) {
#if defined(l_intfitsf)
  if (!l_intfitsf(i)) {
    if (f >= -cast_num(LUA_MININTEGER))  
      return 1;  
    else if (f >= cast_num(LUA_MININTEGER))  
      return (i <= cast(lua_Integer, f));  
    else  
      return 0;
  }
#endif
  return luai_numle(cast_num(i), f);  
}
static int LTnum (const TValue *l, const TValue *r) {
  if (ttisinteger(l)) {
    lua_Integer li = ivalue(l);
    if (ttisinteger(r))
      return li < ivalue(r);  
    else  
      return LTintfloat(li, fltvalue(r));  
  }
  else {
    lua_Number lf = fltvalue(l);  
    if (ttisfloat(r))
      return luai_numlt(lf, fltvalue(r));  
    else if (luai_numisnan(lf))  
      return 0;  
    else  
      return !LEintfloat(ivalue(r), lf);  
  }
}
static int LEnum (const TValue *l, const TValue *r) {
  if (ttisinteger(l)) {
    lua_Integer li = ivalue(l);
    if (ttisinteger(r))
      return li <= ivalue(r);  
    else  
      return LEintfloat(li, fltvalue(r));  
  }
  else {
    lua_Number lf = fltvalue(l);  
    if (ttisfloat(r))
      return luai_numle(lf, fltvalue(r));  
    else if (luai_numisnan(lf))  
      return 0;  
    else  
      return !LTintfloat(ivalue(r), lf);  
  }
}
int luaV_lessthan (lua_State *L, const TValue *l, const TValue *r) {
  int res;
  if (ttisnumber(l) && ttisnumber(r))  
    return LTnum(l, r);
  else if (ttisstring(l) && ttisstring(r))  
    return l_strcmp(tsvalue(l), tsvalue(r)) < 0;
  else if ((res = luaT_callorderTM(L, l, r, TM_LT)) < 0)  
    luaG_ordererror(L, l, r);  
  return res;
}
int luaV_lessequal (lua_State *L, const TValue *l, const TValue *r) {
  int res;
  if (ttisnumber(l) && ttisnumber(r))  
    return LEnum(l, r);
  else if (ttisstring(l) && ttisstring(r))  
    return l_strcmp(tsvalue(l), tsvalue(r)) <= 0;
  else if ((res = luaT_callorderTM(L, l, r, TM_LE)) >= 0)  
    return res;
  else {  
    L->ci->callstatus |= CIST_LEQ;  
    res = luaT_callorderTM(L, r, l, TM_LT);
    L->ci->callstatus ^= CIST_LEQ;  
    if (res < 0)
      luaG_ordererror(L, l, r);
    return !res;  
  }
}
int luaV_equalobj (lua_State *L, const TValue *t1, const TValue *t2) {
  const TValue *tm;
  if (ttype(t1) != ttype(t2)) {  
    if (ttnov(t1) != ttnov(t2) || ttnov(t1) != LUA_TNUMBER)
      return 0;  
    else {  
      lua_Integer i1, i2;  
      return (tointeger(t1, &i1) && tointeger(t2, &i2) && i1 == i2);
    }
  }
  switch (ttype(t1)) {
    case LUA_TNIL: return 1;
    case LUA_TNUMINT: return (ivalue(t1) == ivalue(t2));
    case LUA_TNUMFLT: return luai_numeq(fltvalue(t1), fltvalue(t2));
    case LUA_TBOOLEAN: return bvalue(t1) == bvalue(t2);  
    case LUA_TLIGHTUSERDATA: return pvalue(t1) == pvalue(t2);
    case LUA_TLCF: return fvalue(t1) == fvalue(t2);
    case LUA_TSHRSTR: return eqshrstr(tsvalue(t1), tsvalue(t2));
    case LUA_TLNGSTR: return luaS_eqlngstr(tsvalue(t1), tsvalue(t2));
    case LUA_TUSERDATA: {
      if (uvalue(t1) == uvalue(t2)) return 1;
      else if (L == NULL) return 0;
      tm = fasttm(L, uvalue(t1)->metatable, TM_EQ);
      if (tm == NULL)
        tm = fasttm(L, uvalue(t2)->metatable, TM_EQ);
      break;  
    }
    case LUA_TTABLE: {
      if (hvalue(t1) == hvalue(t2)) return 1;
      else if (L == NULL) return 0;
      tm = fasttm(L, hvalue(t1)->metatable, TM_EQ);
      if (tm == NULL)
        tm = fasttm(L, hvalue(t2)->metatable, TM_EQ);
      break;  
    }
    default:
      return gcvalue(t1) == gcvalue(t2);
  }
  if (tm == NULL)  
    return 0;  
  luaT_callTM(L, tm, t1, t2, L->top, 1);  
  return !l_isfalse(L->top);
}
#define tostring(L,o)  \
	(ttisstring(o) || (cvt2str(o) && (luaO_tostring(L, o), 1)))
#define isemptystr(o)	(ttisshrstring(o) && tsvalue(o)->shrlen == 0)
static void copy2buff (StkId top, int n, char *buff) {
  size_t tl = 0;  
  do {
    size_t l = vslen(top - n);  
    memcpy(buff + tl, svalue(top - n), l * sizeof(char));
    tl += l;
  } while (--n > 0);
}
void luaV_concat (lua_State *L, int total) {
  lua_assert(total >= 2);
  do {
    StkId top = L->top;
    int n = 2;  
    if (!(ttisstring(top-2) || cvt2str(top-2)) || !tostring(L, top-1))
      luaT_trybinTM(L, top-2, top-1, top-2, TM_CONCAT);
    else if (isemptystr(top - 1))  
      cast_void(tostring(L, top - 2));  
    else if (isemptystr(top - 2)) {  
      setobjs2s(L, top - 2, top - 1);  
    }
    else {
      size_t tl = vslen(top - 1);
      TString *ts;
      for (n = 1; n < total && tostring(L, top - n - 1); n++) {
        size_t l = vslen(top - n - 1);
        if (l >= (MAX_SIZE/sizeof(char)) - tl)
          luaG_runerror(L, "string length overflow");
        tl += l;
      }
      if (tl <= LUAI_MAXSHORTLEN) {  
        char buff[LUAI_MAXSHORTLEN];
        copy2buff(top, n, buff);  
        ts = luaS_newlstr(L, buff, tl);
      }
      else {  
        ts = luaS_createlngstrobj(L, tl);
        copy2buff(top, n, getstr(ts));
      }
      setsvalue2s(L, top - n, ts);  
    }
    total -= n-1;  
    L->top -= n-1;  
  } while (total > 1);  
}
void luaV_objlen (lua_State *L, StkId ra, const TValue *rb) {
  const TValue *tm;
  switch (ttype(rb)) {
    case LUA_TTABLE: {
      Table *h = hvalue(rb);
      tm = fasttm(L, h->metatable, TM_LEN);
      if (tm) break;  
      setivalue(ra, luaH_getn(h));  
      return;
    }
    case LUA_TSHRSTR: {
      setivalue(ra, tsvalue(rb)->shrlen);
      return;
    }
    case LUA_TLNGSTR: {
      setivalue(ra, tsvalue(rb)->u.lnglen);
      return;
    }
    default: {  
      tm = luaT_gettmbyobj(L, rb, TM_LEN);
      if (ttisnil(tm))  
        luaG_typeerror(L, rb, "get length of");
      break;
    }
  }
  luaT_callTM(L, tm, rb, rb, ra, 1);
}
lua_Integer luaV_div (lua_State *L, lua_Integer m, lua_Integer n) {
  if (l_castS2U(n) + 1u <= 1u) {  
    if (n == 0)
      luaG_runerror(L, "attempt to divide by zero");
    return intop(-, 0, m);   
  }
  else {
    lua_Integer q = m / n;  
    if ((m ^ n) < 0 && m % n != 0)  
      q -= 1;  
    return q;
  }
}
lua_Integer luaV_mod (lua_State *L, lua_Integer m, lua_Integer n) {
  if (l_castS2U(n) + 1u <= 1u) {  
    if (n == 0)
      luaG_runerror(L, "attempt to perform 'n%%0'");
    return 0;   
  }
  else {
    lua_Integer r = m % n;
    if (r != 0 && (m ^ n) < 0)  
      r += n;  
    return r;
  }
}
#define NBITS	cast_int(sizeof(lua_Integer) * CHAR_BIT)
lua_Integer luaV_shiftl (lua_Integer x, lua_Integer y) {
  if (y < 0) {  
    if (y <= -NBITS) return 0;
    else return intop(>>, x, -y);
  }
  else {  
    if (y >= NBITS) return 0;
    else return intop(<<, x, y);
  }
}
static LClosure *getcached (Proto *p, UpVal **encup, StkId base) {
  LClosure *c = p->cache;
  if (c != NULL) {  
    int nup = p->sizeupvalues;
    Upvaldesc *uv = p->upvalues;
    int i;
    for (i = 0; i < nup; i++) {  
      TValue *v = uv[i].instack ? base + uv[i].idx : encup[uv[i].idx]->v;
      if (c->upvals[i]->v != v)
        return NULL;  
    }
  }
  return c;  
}
static void pushclosure (lua_State *L, Proto *p, UpVal **encup, StkId base,
                         StkId ra) {
  int nup = p->sizeupvalues;
  Upvaldesc *uv = p->upvalues;
  int i;
  LClosure *ncl = luaF_newLclosure(L, nup);
  ncl->p = p;
  setclLvalue(L, ra, ncl);  
  for (i = 0; i < nup; i++) {  
    if (uv[i].instack)  
      ncl->upvals[i] = luaF_findupval(L, base + uv[i].idx);
    else  
      ncl->upvals[i] = encup[uv[i].idx];
    ncl->upvals[i]->refcount++;
  }
  if (!isblack(p))  
    p->cache = ncl;  
}
void luaV_finishOp (lua_State *L) {
  CallInfo *ci = L->ci;
  StkId base = ci->u.l.base;
  Instruction inst = *(ci->u.l.savedpc - 1);  
  OpCode op = GET_OPCODE(inst);
  switch (op) {  
    case OP_ADD: case OP_SUB: case OP_MUL: case OP_DIV: case OP_IDIV:
    case OP_BAND: case OP_BOR: case OP_BXOR: case OP_SHL: case OP_SHR:
    case OP_MOD: case OP_POW:
    case OP_UNM: case OP_BNOT: case OP_LEN:
    case OP_GETTABUP: case OP_GETTABLE: case OP_SELF: {
      setobjs2s(L, base + GETARG_A(inst), --L->top);
      break;
    }
    case OP_LE: case OP_LT: case OP_EQ: {
      int res = !l_isfalse(L->top - 1);
      L->top--;
      if (ci->callstatus & CIST_LEQ) {  
        lua_assert(op == OP_LE);
        ci->callstatus ^= CIST_LEQ;  
        res = !res;  
      }
      lua_assert(GET_OPCODE(*ci->u.l.savedpc) == OP_JMP);
      if (res != GETARG_A(inst))  
        ci->u.l.savedpc++;  
      break;
    }
    case OP_CONCAT: {
      StkId top = L->top - 1;  
      int b = GETARG_B(inst);      
      int total = cast_int(top - 1 - (base + b));  
      setobj2s(L, top - 2, top);  
      if (total > 1) {  
        L->top = top - 1;  
        luaV_concat(L, total);  
      }
      setobj2s(L, ci->u.l.base + GETARG_A(inst), L->top - 1);
      L->top = ci->top;  
      break;
    }
    case OP_TFORCALL: {
      lua_assert(GET_OPCODE(*ci->u.l.savedpc) == OP_TFORLOOP);
      L->top = ci->top;  
      break;
    }
    case OP_CALL: {
      if (GETARG_C(inst) - 1 >= 0)  
        L->top = ci->top;  
      break;
    }
    case OP_TAILCALL: case OP_SETTABUP: case OP_SETTABLE:
      break;
    default: lua_assert(0);
  }
}
#define RA(i)	(base+GETARG_A(i))
#define RB(i)	check_exp(getBMode(GET_OPCODE(i)) == OpArgR, base+GETARG_B(i))
#define RC(i)	check_exp(getCMode(GET_OPCODE(i)) == OpArgR, base+GETARG_C(i))
#define RKB(i)	check_exp(getBMode(GET_OPCODE(i)) == OpArgK, \
	ISK(GETARG_B(i)) ? k+INDEXK(GETARG_B(i)) : base+GETARG_B(i))
#define RKC(i)	check_exp(getCMode(GET_OPCODE(i)) == OpArgK, \
	ISK(GETARG_C(i)) ? k+INDEXK(GETARG_C(i)) : base+GETARG_C(i))
#define dojump(ci,i,e) \
  { int a = GETARG_A(i); \
    if (a != 0) Protect(luaF_close(L, ci->u.l.base + a - 1, LUA_OK)); \
    ci->u.l.savedpc += GETARG_sBx(i) + e; }
#define donextjump(ci)	{ i = *ci->u.l.savedpc; dojump(ci, i, 1); }
#define Protect(x)	{ {x;}; base = ci->u.l.base; }
#define checkGC(L,c)  \
	{ luaC_condGC(L, L->top = (c),   \
                         Protect(L->top = ci->top));   \
           luai_threadyield(L); }
#define vmfetch()	{ \
  i = *(ci->u.l.savedpc++); \
  if (L->hookmask & (LUA_MASKLINE | LUA_MASKCOUNT)) \
    Protect(luaG_traceexec(L)); \
  ra = RA(i);  \
  lua_assert(base == ci->u.l.base); \
  lua_assert(base <= L->top && L->top < L->stack + L->stacksize); \
}
#define vmdispatch(o)	switch(o)
#define vmcase(l)	case l:
#define vmbreak		break
#define gettableProtected(L,t,k,v)  { const TValue *slot; \
  if (luaV_fastget(L,t,k,slot,luaH_get)) { setobj2s(L, v, slot); } \
  else Protect(luaV_finishget(L,t,k,v,slot)); }
#define settableProtected(L,t,k,v) { const TValue *slot; \
  if (!luaV_fastset(L,t,k,slot,luaH_get,v)) \
    Protect(luaV_finishset(L,t,k,v,slot)); }
void luaV_execute (lua_State *L) {
  CallInfo *ci = L->ci;
  LClosure *cl;
  TValue *k;
  StkId base;
  ci->callstatus |= CIST_FRESH;  
 newframe:  
  lua_assert(ci == L->ci);
  cl = clLvalue(ci->func);  
  k = cl->p->k;  
  base = ci->u.l.base;  
  for (;;) {
    Instruction i;
    StkId ra;
    vmfetch();
    vmdispatch (GET_OPCODE(i)) {
      vmcase(OP_MOVE) {
        setobjs2s(L, ra, RB(i));
        vmbreak;
      }
      vmcase(OP_LOADK) {
        TValue *rb = k + GETARG_Bx(i);
        setobj2s(L, ra, rb);
        vmbreak;
      }
      vmcase(OP_LOADKX) {
        TValue *rb;
        lua_assert(GET_OPCODE(*ci->u.l.savedpc) == OP_EXTRAARG);
        rb = k + GETARG_Ax(*ci->u.l.savedpc++);
        setobj2s(L, ra, rb);
        vmbreak;
      }
      vmcase(OP_LOADBOOL) {
        setbvalue(ra, GETARG_B(i));
        if (GETARG_C(i)) ci->u.l.savedpc++;  
        vmbreak;
      }
      vmcase(OP_LOADNIL) {
        int b = GETARG_B(i);
        do {
          setnilvalue(ra++);
        } while (b--);
        vmbreak;
      }
      vmcase(OP_GETUPVAL) {
        int b = GETARG_B(i);
        setobj2s(L, ra, cl->upvals[b]->v);
        vmbreak;
      }
      vmcase(OP_GETTABUP) {
        TValue *upval = cl->upvals[GETARG_B(i)]->v;
        TValue *rc = RKC(i);
        gettableProtected(L, upval, rc, ra);
        vmbreak;
      }
      vmcase(OP_GETTABLE) {
        StkId rb = RB(i);
        TValue *rc = RKC(i);
        gettableProtected(L, rb, rc, ra);
        vmbreak;
      }
      vmcase(OP_SETTABUP) {
        TValue *upval = cl->upvals[GETARG_A(i)]->v;
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        settableProtected(L, upval, rb, rc);
        vmbreak;
      }
      vmcase(OP_SETUPVAL) {
        UpVal *uv = cl->upvals[GETARG_B(i)];
        setobj(L, uv->v, ra);
        luaC_upvalbarrier(L, uv);
        vmbreak;
      }
      vmcase(OP_SETTABLE) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        settableProtected(L, ra, rb, rc);
        vmbreak;
      }
      vmcase(OP_NEWTABLE) {
        int b = GETARG_B(i);
        int c = GETARG_C(i);
        Table *t = luaH_new(L);
        sethvalue(L, ra, t);
        if (b != 0 || c != 0)
          luaH_resize(L, t, luaO_fb2int(b), luaO_fb2int(c));
        checkGC(L, ra + 1);
        vmbreak;
      }
      vmcase(OP_SELF) {
        const TValue *aux;
        StkId rb = RB(i);
        TValue *rc = RKC(i);
        TString *key = tsvalue(rc);  
        setobjs2s(L, ra + 1, rb);
        if (luaV_fastget(L, rb, key, aux, luaH_getstr)) {
          setobj2s(L, ra, aux);
        }
        else Protect(luaV_finishget(L, rb, rc, ra, aux));
        vmbreak;
      }
      vmcase(OP_ADD) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Number nb; lua_Number nc;
        if (ttisinteger(rb) && ttisinteger(rc)) {
          lua_Integer ib = ivalue(rb); lua_Integer ic = ivalue(rc);
          setivalue(ra, intop(+, ib, ic));
        }
        else if (tonumber(rb, &nb) && tonumber(rc, &nc)) {
          setfltvalue(ra, luai_numadd(L, nb, nc));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_ADD)); }
        vmbreak;
      }
      vmcase(OP_SUB) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Number nb; lua_Number nc;
        if (ttisinteger(rb) && ttisinteger(rc)) {
          lua_Integer ib = ivalue(rb); lua_Integer ic = ivalue(rc);
          setivalue(ra, intop(-, ib, ic));
        }
        else if (tonumber(rb, &nb) && tonumber(rc, &nc)) {
          setfltvalue(ra, luai_numsub(L, nb, nc));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_SUB)); }
        vmbreak;
      }
      vmcase(OP_MUL) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Number nb; lua_Number nc;
        if (ttisinteger(rb) && ttisinteger(rc)) {
          lua_Integer ib = ivalue(rb); lua_Integer ic = ivalue(rc);
          setivalue(ra, intop(*, ib, ic));
        }
        else if (tonumber(rb, &nb) && tonumber(rc, &nc)) {
          setfltvalue(ra, luai_nummul(L, nb, nc));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_MUL)); }
        vmbreak;
      }
      vmcase(OP_DIV) {  
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Number nb; lua_Number nc;
        if (tonumber(rb, &nb) && tonumber(rc, &nc)) {
          setfltvalue(ra, luai_numdiv(L, nb, nc));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_DIV)); }
        vmbreak;
      }
      vmcase(OP_BAND) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Integer ib; lua_Integer ic;
        if (tointeger(rb, &ib) && tointeger(rc, &ic)) {
          setivalue(ra, intop(&, ib, ic));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_BAND)); }
        vmbreak;
      }
      vmcase(OP_BOR) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Integer ib; lua_Integer ic;
        if (tointeger(rb, &ib) && tointeger(rc, &ic)) {
          setivalue(ra, intop(|, ib, ic));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_BOR)); }
        vmbreak;
      }
      vmcase(OP_BXOR) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Integer ib; lua_Integer ic;
        if (tointeger(rb, &ib) && tointeger(rc, &ic)) {
          setivalue(ra, intop(^, ib, ic));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_BXOR)); }
        vmbreak;
      }
      vmcase(OP_SHL) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Integer ib; lua_Integer ic;
        if (tointeger(rb, &ib) && tointeger(rc, &ic)) {
          setivalue(ra, luaV_shiftl(ib, ic));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_SHL)); }
        vmbreak;
      }
      vmcase(OP_SHR) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Integer ib; lua_Integer ic;
        if (tointeger(rb, &ib) && tointeger(rc, &ic)) {
          setivalue(ra, luaV_shiftl(ib, -ic));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_SHR)); }
        vmbreak;
      }
      vmcase(OP_MOD) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Number nb; lua_Number nc;
        if (ttisinteger(rb) && ttisinteger(rc)) {
          lua_Integer ib = ivalue(rb); lua_Integer ic = ivalue(rc);
          setivalue(ra, luaV_mod(L, ib, ic));
        }
        else if (tonumber(rb, &nb) && tonumber(rc, &nc)) {
          lua_Number m;
          luai_nummod(L, nb, nc, m);
          setfltvalue(ra, m);
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_MOD)); }
        vmbreak;
      }
      vmcase(OP_IDIV) {  
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Number nb; lua_Number nc;
        if (ttisinteger(rb) && ttisinteger(rc)) {
          lua_Integer ib = ivalue(rb); lua_Integer ic = ivalue(rc);
          setivalue(ra, luaV_div(L, ib, ic));
        }
        else if (tonumber(rb, &nb) && tonumber(rc, &nc)) {
          setfltvalue(ra, luai_numidiv(L, nb, nc));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_IDIV)); }
        vmbreak;
      }
      vmcase(OP_POW) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        lua_Number nb; lua_Number nc;
        if (tonumber(rb, &nb) && tonumber(rc, &nc)) {
          setfltvalue(ra, luai_numpow(L, nb, nc));
        }
        else { Protect(luaT_trybinTM(L, rb, rc, ra, TM_POW)); }
        vmbreak;
      }
      vmcase(OP_UNM) {
        TValue *rb = RB(i);
        lua_Number nb;
        if (ttisinteger(rb)) {
          lua_Integer ib = ivalue(rb);
          setivalue(ra, intop(-, 0, ib));
        }
        else if (tonumber(rb, &nb)) {
          setfltvalue(ra, luai_numunm(L, nb));
        }
        else {
          Protect(luaT_trybinTM(L, rb, rb, ra, TM_UNM));
        }
        vmbreak;
      }
      vmcase(OP_BNOT) {
        TValue *rb = RB(i);
        lua_Integer ib;
        if (tointeger(rb, &ib)) {
          setivalue(ra, intop(^, ~l_castS2U(0), ib));
        }
        else {
          Protect(luaT_trybinTM(L, rb, rb, ra, TM_BNOT));
        }
        vmbreak;
      }
      vmcase(OP_NOT) {
        TValue *rb = RB(i);
        int res = l_isfalse(rb);  
        setbvalue(ra, res);
        vmbreak;
      }
      vmcase(OP_LEN) {
        Protect(luaV_objlen(L, ra, RB(i)));
        vmbreak;
      }
      vmcase(OP_CONCAT) {
        int b = GETARG_B(i);
        int c = GETARG_C(i);
        StkId rb;
        L->top = base + c + 1;  
        Protect(luaV_concat(L, c - b + 1));
        ra = RA(i);  
        rb = base + b;
        setobjs2s(L, ra, rb);
        checkGC(L, (ra >= rb ? ra + 1 : rb));
        L->top = ci->top;  
        vmbreak;
      }
      vmcase(OP_JMP) {
        dojump(ci, i, 0);
        vmbreak;
      }
      vmcase(OP_EQ) {
        TValue *rb = RKB(i);
        TValue *rc = RKC(i);
        Protect(
          if (luaV_equalobj(L, rb, rc) != GETARG_A(i))
            ci->u.l.savedpc++;
          else
            donextjump(ci);
        )
        vmbreak;
      }
      vmcase(OP_LT) {
        Protect(
          if (luaV_lessthan(L, RKB(i), RKC(i)) != GETARG_A(i))
            ci->u.l.savedpc++;
          else
            donextjump(ci);
        )
        vmbreak;
      }
      vmcase(OP_LE) {
        Protect(
          if (luaV_lessequal(L, RKB(i), RKC(i)) != GETARG_A(i))
            ci->u.l.savedpc++;
          else
            donextjump(ci);
        )
        vmbreak;
      }
      vmcase(OP_TEST) {
        if (GETARG_C(i) ? l_isfalse(ra) : !l_isfalse(ra))
            ci->u.l.savedpc++;
          else
          donextjump(ci);
        vmbreak;
      }
      vmcase(OP_TESTSET) {
        TValue *rb = RB(i);
        if (GETARG_C(i) ? l_isfalse(rb) : !l_isfalse(rb))
          ci->u.l.savedpc++;
        else {
          setobjs2s(L, ra, rb);
          donextjump(ci);
        }
        vmbreak;
      }
      vmcase(OP_CALL) {
        int b = GETARG_B(i);
        int nresults = GETARG_C(i) - 1;
        if (b != 0) L->top = ra+b;  
        if (luaD_precall(L, ra, nresults)) {  
          if (nresults >= 0)
            L->top = ci->top;  
          Protect((void)0);  
        }
        else {  
          ci = L->ci;
          goto newframe;  
        }
        vmbreak;
      }
      vmcase(OP_TAILCALL) {
        int b = GETARG_B(i);
        if (b != 0) L->top = ra+b;  
        lua_assert(GETARG_C(i) - 1 == LUA_MULTRET);
        if (luaD_precall(L, ra, LUA_MULTRET)) {  
          Protect((void)0);  
        }
        else {
          CallInfo *nci = L->ci;  
          CallInfo *oci = nci->previous;  
          StkId nfunc = nci->func;  
          StkId ofunc = oci->func;  
          StkId lim = nci->u.l.base + getproto(nfunc)->numparams;
          int aux;
          if (cl->p->sizep > 0) Protect(luaF_close(L, oci->u.l.base, NOCLOSINGMETH));
          for (aux = 0; nfunc + aux < lim; aux++)
            setobjs2s(L, ofunc + aux, nfunc + aux);
          oci->u.l.base = ofunc + (nci->u.l.base - nfunc);  
          oci->top = L->top = ofunc + (L->top - nfunc);  
          oci->u.l.savedpc = nci->u.l.savedpc;
          oci->callstatus |= CIST_TAIL;  
          ci = L->ci = oci;  
          lua_assert(L->top == oci->u.l.base + getproto(ofunc)->maxstacksize);
          goto newframe;  
        }
        vmbreak;
      }
      vmcase(OP_RETURN) {
        int b = GETARG_B(i);
        if (cl->p->sizep > 0) {
          Protect(luaF_close(L, base, LUA_OK));
          ra = RA(i);
        }
        b = luaD_poscall(L, ci, ra, (b != 0 ? b - 1 : cast_int(L->top - ra)));
        if (ci->callstatus & CIST_FRESH)  
          return;  
        else {  
          ci = L->ci;
          if (b) L->top = ci->top;
          lua_assert(isLua(ci));
          lua_assert(GET_OPCODE(*((ci)->u.l.savedpc - 1)) == OP_CALL);
          goto newframe;  
        }
      }
      vmcase(OP_FORLOOP) {
        if (ttisinteger(ra)) {  
          lua_Integer step = ivalue(ra + 2);
          lua_Integer idx = intop(+, ivalue(ra), step); 
          lua_Integer limit = ivalue(ra + 1);
          if ((0 < step) ? (idx <= limit) : (limit <= idx)) {
            ci->u.l.savedpc += GETARG_sBx(i);  
            chgivalue(ra, idx);  
            setivalue(ra + 3, idx);  
          }
        }
        else {  
          lua_Number step = fltvalue(ra + 2);
          lua_Number idx = luai_numadd(L, fltvalue(ra), step); 
          lua_Number limit = fltvalue(ra + 1);
          if (luai_numlt(0, step) ? luai_numle(idx, limit)
                                  : luai_numle(limit, idx)) {
            ci->u.l.savedpc += GETARG_sBx(i);  
            chgfltvalue(ra, idx);  
            setfltvalue(ra + 3, idx);  
          }
        }
        vmbreak;
      }
      vmcase(OP_FORPREP) {
        TValue *init = ra;
        TValue *plimit = ra + 1;
        TValue *pstep = ra + 2;
        lua_Integer ilimit;
        int stopnow;
        if (ttisinteger(init) && ttisinteger(pstep) &&
            forlimit(plimit, &ilimit, ivalue(pstep), &stopnow)) {
          lua_Integer initv = (stopnow ? 0 : ivalue(init));
          setivalue(plimit, ilimit);
          setivalue(init, intop(-, initv, ivalue(pstep)));
        }
        else {  
          lua_Number ninit; lua_Number nlimit; lua_Number nstep;
          if (!tonumber(plimit, &nlimit))
            luaG_runerror(L, "'for' limit must be a number");
          setfltvalue(plimit, nlimit);
          if (!tonumber(pstep, &nstep))
            luaG_runerror(L, "'for' step must be a number");
          setfltvalue(pstep, nstep);
          if (!tonumber(init, &ninit))
            luaG_runerror(L, "'for' initial value must be a number");
          setfltvalue(init, luai_numsub(L, ninit, nstep));
        }
        ci->u.l.savedpc += GETARG_sBx(i);
        vmbreak;
      }
      vmcase(OP_TFORCALL) {
        StkId cb = ra + 3;  
        setobjs2s(L, cb+2, ra+2);
        setobjs2s(L, cb+1, ra+1);
        setobjs2s(L, cb, ra);
        L->top = cb + 3;  
        Protect(luaD_call(L, cb, GETARG_C(i)));
        L->top = ci->top;
        i = *(ci->u.l.savedpc++);  
        ra = RA(i);
        lua_assert(GET_OPCODE(i) == OP_TFORLOOP);
        goto l_tforloop;
      }
      vmcase(OP_TFORLOOP) {
        l_tforloop:
        if (!ttisnil(ra + 1)) {  
          setobjs2s(L, ra, ra + 1);  
           ci->u.l.savedpc += GETARG_sBx(i);  
        }
        vmbreak;
      }
      vmcase(OP_SETLIST) {
        int n = GETARG_B(i);
        int c = GETARG_C(i);
        unsigned int last;
        Table *h;
        if (n == 0) n = cast_int(L->top - ra) - 1;
        if (c == 0) {
          lua_assert(GET_OPCODE(*ci->u.l.savedpc) == OP_EXTRAARG);
          c = GETARG_Ax(*ci->u.l.savedpc++);
        }
        h = hvalue(ra);
        last = ((c-1)*LFIELDS_PER_FLUSH) + n;
        if (last > h->sizearray)  
          luaH_resizearray(L, h, last);  
        for (; n > 0; n--) {
          TValue *val = ra+n;
          luaH_setint(L, h, last--, val);
          luaC_barrierback(L, h, val);
        }
        L->top = ci->top;  
        vmbreak;
      }
      vmcase(OP_CLOSURE) {
        Proto *p = cl->p->p[GETARG_Bx(i)];
        LClosure *ncl = getcached(p, cl->upvals, base);  
        if (ncl == NULL)  
          pushclosure(L, p, cl->upvals, base, ra);  
        else
          setclLvalue(L, ra, ncl);  
        checkGC(L, ra + 1);
        vmbreak;
      }
      vmcase(OP_VARARG) {
        int b = GETARG_B(i) - 1;  
        int j;
        int n = cast_int(base - ci->func) - cl->p->numparams - 1;
        if (n < 0)  
          n = 0;  
        if (b < 0) {  
          b = n;  
          Protect(luaD_checkstack(L, n));
          ra = RA(i);  
          L->top = ra + n;
        }
        for (j = 0; j < b && j < n; j++)
          setobjs2s(L, ra + j, base - n + j);
        for (; j < b; j++)  
          setnilvalue(ra + j);
        vmbreak;
      }
      vmcase(OP_EXTRAARG) {
        lua_assert(0);
        vmbreak;
      }
      vmcase(OP_DEFER) {
        UpVal *up = luaF_findupval(L, ra); 
        up->flags = 1;  
        setnilvalue(ra);  
        vmbreak;
      }
    }
  }
}
#define lzio_c
#define LUA_CORE
#include <string.h>
int luaZ_fill (ZIO *z) {
  size_t size;
  lua_State *L = z->L;
  const char *buff;
  lua_unlock(L);
  buff = z->reader(L, z->data, &size);
  lua_lock(L);
  if (buff == NULL || size == 0)
    return EOZ;
  z->n = size - 1;  
  z->p = buff;
  return cast_uchar(*(z->p++));
}
void luaZ_init (lua_State *L, ZIO *z, lua_Reader reader, void *data) {
  z->L = L;
  z->reader = reader;
  z->data = data;
  z->n = 0;
  z->p = NULL;
}
size_t luaZ_read (ZIO *z, void *b, size_t n) {
  while (n) {
    size_t m;
    if (z->n == 0) {  
      if (luaZ_fill(z) == EOZ)  
        return n;  
      else {
        z->n++;  
        z->p--;
      }
    }
    m = (n <= z->n) ? n : z->n;  
    memcpy(b, z->p, m);
    z->n -= m;
    z->p += m;
    b = (char *)b + m;
    n -= m;
  }
  return 0;
}
#define lauxlib_c
#define LUA_LIB
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEVELS1	10	
#define LEVELS2	11	
static int findfield (lua_State *L, int objidx, int level) {
  if (level == 0 || !lua_istable(L, -1))
    return 0;  
  lua_pushnil(L);  
  while (lua_next(L, -2)) {  
    if (lua_type(L, -2) == LUA_TSTRING) {  
      if (lua_rawequal(L, objidx, -1)) {  
        lua_pop(L, 1);  
        return 1;
      }
      else if (findfield(L, objidx, level - 1)) {  
        lua_remove(L, -2);  
        lua_pushliteral(L, ".");
        lua_insert(L, -2);  
        lua_concat(L, 3);
        return 1;
      }
    }
    lua_pop(L, 1);  
  }
  return 0;  
}
static int pushglobalfuncname (lua_State *L, lua_Debug *ar) {
  int top = lua_gettop(L);
  lua_getinfo(L, "f", ar);  
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_LOADED_TABLE);
  if (findfield(L, top + 1, 2)) {
    const char *name = lua_tostring(L, -1);
    if (strncmp(name, "_G.", 3) == 0) {  
      lua_pushstring(L, name + 3);  
      lua_remove(L, -2);  
    }
    lua_copy(L, -1, top + 1);  
    lua_pop(L, 2);  
    return 1;
  }
  else {
    lua_settop(L, top);  
    return 0;
  }
}
static void pushfuncname (lua_State *L, lua_Debug *ar) {
  if (pushglobalfuncname(L, ar)) {  
    lua_pushfstring(L, "function '%s'", lua_tostring(L, -1));
    lua_remove(L, -2);  
  }
  else if (*ar->namewhat != '\0')  
    lua_pushfstring(L, "%s '%s'", ar->namewhat, ar->name);  
  else if (*ar->what == 'm')  
      lua_pushliteral(L, "main chunk");
  else if (*ar->what != 'C')  
    lua_pushfstring(L, "function <%s:%d>", ar->short_src, ar->linedefined);
  else  
    lua_pushliteral(L, "?");
}
static int lastlevel (lua_State *L) {
  lua_Debug ar;
  int li = 1, le = 1;
  while (lua_getstack(L, le, &ar)) { li = le; le *= 2; }
  while (li < le) {
    int m = (li + le)/2;
    if (lua_getstack(L, m, &ar)) li = m + 1;
    else le = m;
  }
  return le - 1;
}
LUALIB_API void luaL_traceback (lua_State *L, lua_State *L1,
                                const char *msg, int level) {
  lua_Debug ar;
  int top = lua_gettop(L);
  int last = lastlevel(L1);
  int n1 = (last - level > LEVELS1 + LEVELS2) ? LEVELS1 : -1;
  if (msg)
    lua_pushfstring(L, "%s\n", msg);
  luaL_checkstack(L, 10, NULL);
  lua_pushliteral(L, "stack traceback:");
  while (lua_getstack(L1, level++, &ar)) {
    if (n1-- == 0) {  
      lua_pushliteral(L, "\n\t...");  
      level = last - LEVELS2 + 1;  
    }
    else {
      lua_getinfo(L1, "Slnt", &ar);
      lua_pushfstring(L, "\n\t%s:", ar.short_src);
      if (ar.currentline > 0)
        lua_pushfstring(L, "%d:", ar.currentline);
      lua_pushliteral(L, " in ");
      pushfuncname(L, &ar);
      if (ar.istailcall)
        lua_pushliteral(L, "\n\t(...tail calls...)");
      lua_concat(L, lua_gettop(L) - top);
    }
  }
  lua_concat(L, lua_gettop(L) - top);
}
LUALIB_API int luaL_argerror (lua_State *L, int arg, const char *extramsg) {
  lua_Debug ar;
  if (!lua_getstack(L, 0, &ar))  
    return luaL_error(L, "bad argument #%d (%s)", arg, extramsg);
  lua_getinfo(L, "n", &ar);
  if (strcmp(ar.namewhat, "method") == 0) {
    arg--;  
    if (arg == 0)  
      return luaL_error(L, "calling '%s' on bad self (%s)",
                           ar.name, extramsg);
  }
  if (ar.name == NULL)
    ar.name = (pushglobalfuncname(L, &ar)) ? lua_tostring(L, -1) : "?";
  return luaL_error(L, "bad argument #%d to '%s' (%s)",
                        arg, ar.name, extramsg);
}
static int typeerror (lua_State *L, int arg, const char *tname) {
  const char *msg;
  const char *typearg;  
  if (luaL_getmetafield(L, arg, "__name") == LUA_TSTRING)
    typearg = lua_tostring(L, -1);  
  else if (lua_type(L, arg) == LUA_TLIGHTUSERDATA)
    typearg = "light userdata";  
  else
    typearg = luaL_typename(L, arg);  
  msg = lua_pushfstring(L, "%s expected, got %s", tname, typearg);
  return luaL_argerror(L, arg, msg);
}
static void tag_error (lua_State *L, int arg, int tag) {
  typeerror(L, arg, lua_typename(L, tag));
}
LUALIB_API void luaL_where (lua_State *L, int level) {
  lua_Debug ar;
  if (lua_getstack(L, level, &ar)) {  
    lua_getinfo(L, "Sl", &ar);  
    if (ar.currentline > 0) {  
      lua_pushfstring(L, "%s:%d: ", ar.short_src, ar.currentline);
      return;
    }
  }
  lua_pushfstring(L, "");  
}
LUALIB_API int luaL_error (lua_State *L, const char *fmt, ...) {
  va_list argp;
  va_start(argp, fmt);
  luaL_where(L, 1);
  lua_pushvfstring(L, fmt, argp);
  va_end(argp);
  lua_concat(L, 2);
  return lua_error(L);
}
LUALIB_API int luaL_fileresult (lua_State *L, int stat, const char *fname) {
  int en = errno;  
  if (stat) {
    lua_pushboolean(L, 1);
    return 1;
  }
  else {
    lua_pushnil(L);
    if (fname)
      lua_pushfstring(L, "%s: %s", fname, strerror(en));
    else
      lua_pushstring(L, strerror(en));
    lua_pushinteger(L, en);
    return 3;
  }
}
#if !defined(l_inspectstat)	
#if defined(LUA_USE_POSIX)
#include <sys/wait.h>
#define l_inspectstat(stat,what)  \
   if (WIFEXITED(stat)) { stat = WEXITSTATUS(stat); } \
   else if (WIFSIGNALED(stat)) { stat = WTERMSIG(stat); what = "signal"; }
#else
#define l_inspectstat(stat,what)  
#endif
#endif				
LUALIB_API int luaL_execresult (lua_State *L, int stat) {
  const char *what = "exit";  
  if (stat == -1)  
    return luaL_fileresult(L, 0, NULL);
  else {
    l_inspectstat(stat, what);  
    if (*what == 'e' && stat == 0)  
      lua_pushboolean(L, 1);
    else
      lua_pushnil(L);
    lua_pushstring(L, what);
    lua_pushinteger(L, stat);
    return 3;  
  }
}
LUALIB_API int luaL_newmetatable (lua_State *L, const char *tname) {
  if (luaL_getmetatable(L, tname) != LUA_TNIL)  
    return 0;  
  lua_pop(L, 1);
  lua_createtable(L, 0, 2);  
  lua_pushstring(L, tname);
  lua_setfield(L, -2, "__name");  
  lua_pushvalue(L, -1);
  lua_setfield(L, LUA_REGISTRYINDEX, tname);  
  return 1;
}
LUALIB_API void luaL_setmetatable (lua_State *L, const char *tname) {
  luaL_getmetatable(L, tname);
  lua_setmetatable(L, -2);
}
LUALIB_API void *luaL_testudata (lua_State *L, int ud, const char *tname) {
  void *p = lua_touserdata(L, ud);
  if (p != NULL) {  
    if (lua_getmetatable(L, ud)) {  
      luaL_getmetatable(L, tname);  
      if (!lua_rawequal(L, -1, -2))  
        p = NULL;  
      lua_pop(L, 2);  
      return p;
    }
  }
  return NULL;  
}
LUALIB_API void *luaL_checkudata (lua_State *L, int ud, const char *tname) {
  void *p = luaL_testudata(L, ud, tname);
  if (p == NULL) typeerror(L, ud, tname);
  return p;
}
LUALIB_API int luaL_checkoption (lua_State *L, int arg, const char *def,
                                 const char *const lst[]) {
  const char *name = (def) ? luaL_optstring(L, arg, def) :
                             luaL_checkstring(L, arg);
  int i;
  for (i=0; lst[i]; i++)
    if (strcmp(lst[i], name) == 0)
      return i;
  return luaL_argerror(L, arg,
                       lua_pushfstring(L, "invalid option '%s'", name));
}
LUALIB_API void luaL_checkstack (lua_State *L, int space, const char *msg) {
  if (!lua_checkstack(L, space)) {
    if (msg)
      luaL_error(L, "stack overflow (%s)", msg);
    else
      luaL_error(L, "stack overflow");
  }
}
LUALIB_API void luaL_checktype (lua_State *L, int arg, int t) {
  if (lua_type(L, arg) != t)
    tag_error(L, arg, t);
}
LUALIB_API void luaL_checkany (lua_State *L, int arg) {
  if (lua_type(L, arg) == LUA_TNONE)
    luaL_argerror(L, arg, "value expected");
}
LUALIB_API const char *luaL_checklstring (lua_State *L, int arg, size_t *len) {
  const char *s = lua_tolstring(L, arg, len);
  if (!s) tag_error(L, arg, LUA_TSTRING);
  return s;
}
LUALIB_API const char *luaL_optlstring (lua_State *L, int arg,
                                        const char *def, size_t *len) {
  if (lua_isnoneornil(L, arg)) {
    if (len)
      *len = (def ? strlen(def) : 0);
    return def;
  }
  else return luaL_checklstring(L, arg, len);
}
LUALIB_API lua_Number luaL_checknumber (lua_State *L, int arg) {
  int isnum;
  lua_Number d = lua_tonumberx(L, arg, &isnum);
  if (!isnum)
    tag_error(L, arg, LUA_TNUMBER);
  return d;
}
LUALIB_API lua_Number luaL_optnumber (lua_State *L, int arg, lua_Number def) {
  return luaL_opt(L, luaL_checknumber, arg, def);
}
static void interror (lua_State *L, int arg) {
  if (lua_isnumber(L, arg))
    luaL_argerror(L, arg, "number has no integer representation");
  else
    tag_error(L, arg, LUA_TNUMBER);
}
LUALIB_API lua_Integer luaL_checkinteger (lua_State *L, int arg) {
  int isnum;
  lua_Integer d = lua_tointegerx(L, arg, &isnum);
  if (!isnum) {
    interror(L, arg);
  }
  return d;
}
LUALIB_API lua_Integer luaL_optinteger (lua_State *L, int arg,
                                                      lua_Integer def) {
  return luaL_opt(L, luaL_checkinteger, arg, def);
}
typedef struct UBox {
  void *box;
  size_t bsize;
} UBox;
static void *resizebox (lua_State *L, int idx, size_t newsize) {
  void *ud;
  lua_Alloc allocf = lua_getallocf(L, &ud);
  UBox *box = (UBox *)lua_touserdata(L, idx);
  void *temp = allocf(ud, box->box, box->bsize, newsize);
  if (temp == NULL && newsize > 0) {  
    resizebox(L, idx, 0);  
    luaL_error(L, "not enough memory for buffer allocation");
  }
  box->box = temp;
  box->bsize = newsize;
  return temp;
}
static int boxgc (lua_State *L) {
  resizebox(L, 1, 0);
  return 0;
}
static void *newbox (lua_State *L, size_t newsize) {
  UBox *box = (UBox *)lua_newuserdata(L, sizeof(UBox));
  box->box = NULL;
  box->bsize = 0;
  if (luaL_newmetatable(L, "LUABOX")) {  
    lua_pushcfunction(L, boxgc);
    lua_setfield(L, -2, "__gc");  
  }
  lua_setmetatable(L, -2);
  return resizebox(L, -1, newsize);
}
#define buffonstack(B)	((B)->b != (B)->initb)
LUALIB_API char *luaL_prepbuffsize (luaL_Buffer *B, size_t sz) {
  lua_State *L = B->L;
  if (B->size - B->n < sz) {  
    char *newbuff;
    size_t newsize = B->size * 2;  
    if (newsize - B->n < sz)  
      newsize = B->n + sz;
    if (newsize < B->n || newsize - B->n < sz)
      luaL_error(L, "buffer too large");
    if (buffonstack(B))
      newbuff = (char *)resizebox(L, -1, newsize);
    else {  
      newbuff = (char *)newbox(L, newsize);
      memcpy(newbuff, B->b, B->n * sizeof(char));  
    }
    B->b = newbuff;
    B->size = newsize;
  }
  return &B->b[B->n];
}
LUALIB_API void luaL_addlstring (luaL_Buffer *B, const char *s, size_t l) {
  if (l > 0) {  
    char *b = luaL_prepbuffsize(B, l);
    memcpy(b, s, l * sizeof(char));
    luaL_addsize(B, l);
  }
}
LUALIB_API void luaL_addstring (luaL_Buffer *B, const char *s) {
  luaL_addlstring(B, s, strlen(s));
}
LUALIB_API void luaL_pushresult (luaL_Buffer *B) {
  lua_State *L = B->L;
  lua_pushlstring(L, B->b, B->n);
  if (buffonstack(B)) {
    resizebox(L, -2, 0);  
    lua_remove(L, -2);  
  }
}
LUALIB_API void luaL_pushresultsize (luaL_Buffer *B, size_t sz) {
  luaL_addsize(B, sz);
  luaL_pushresult(B);
}
LUALIB_API void luaL_addvalue (luaL_Buffer *B) {
  lua_State *L = B->L;
  size_t l;
  const char *s = lua_tolstring(L, -1, &l);
  if (buffonstack(B))
    lua_insert(L, -2);  
  luaL_addlstring(B, s, l);
  lua_remove(L, (buffonstack(B)) ? -2 : -1);  
}
LUALIB_API void luaL_buffinit (lua_State *L, luaL_Buffer *B) {
  B->L = L;
  B->b = B->initb;
  B->n = 0;
  B->size = LUAL_BUFFERSIZE;
}
LUALIB_API char *luaL_buffinitsize (lua_State *L, luaL_Buffer *B, size_t sz) {
  luaL_buffinit(L, B);
  return luaL_prepbuffsize(B, sz);
}
#define freelist	0
LUALIB_API int luaL_ref (lua_State *L, int t) {
  int ref;
  if (lua_isnil(L, -1)) {
    lua_pop(L, 1);  
    return LUA_REFNIL;  
  }
  t = lua_absindex(L, t);
  lua_rawgeti(L, t, freelist);  
  ref = (int)lua_tointeger(L, -1);  
  lua_pop(L, 1);  
  if (ref != 0) {  
    lua_rawgeti(L, t, ref);  
    lua_rawseti(L, t, freelist);  
  }
  else  
    ref = (int)lua_rawlen(L, t) + 1;  
  lua_rawseti(L, t, ref);
  return ref;
}
LUALIB_API void luaL_unref (lua_State *L, int t, int ref) {
  if (ref >= 0) {
    t = lua_absindex(L, t);
    lua_rawgeti(L, t, freelist);
    lua_rawseti(L, t, ref);  
    lua_pushinteger(L, ref);
    lua_rawseti(L, t, freelist);  
  }
}
typedef struct LoadF {
  int n;  
  FILE *f;  
  char buff[BUFSIZ];  
} LoadF;
static const char *getF (lua_State *L, void *ud, size_t *size) {
  LoadF *lf = (LoadF *)ud;
  (void)L;  
  if (lf->n > 0) {  
    *size = lf->n;  
    lf->n = 0;  
  }
  else {  
    if (feof(lf->f)) return NULL;
    *size = fread(lf->buff, 1, sizeof(lf->buff), lf->f);  
  }
  return lf->buff;
}
static int errfile (lua_State *L, const char *what, int fnameindex) {
  const char *serr = strerror(errno);
  const char *filename = lua_tostring(L, fnameindex) + 1;
  lua_pushfstring(L, "cannot %s %s: %s", what, filename, serr);
  lua_remove(L, fnameindex);
  return LUA_ERRFILE;
}
static int skipBOM (LoadF *lf) {
  const char *p = "\xEF\xBB\xBF";  
  int c;
  lf->n = 0;
  do {
    c = getc(lf->f);
    if (c == EOF || c != *(const unsigned char *)p++) return c;
    lf->buff[lf->n++] = c;  
  } while (*p != '\0');
  lf->n = 0;  
  return getc(lf->f);  
}
static int skipcomment (LoadF *lf, int *cp) {
  int c = *cp = skipBOM(lf);
  if (c == '#') {  
    do {  
      c = getc(lf->f);
    } while (c != EOF && c != '\n');
    *cp = getc(lf->f);  
    return 1;  
  }
  else return 0;  
}
LUALIB_API int luaL_loadfilex (lua_State *L, const char *filename,
                                             const char *mode) {
  LoadF lf;
  int status, readstatus;
  int c;
  int fnameindex = lua_gettop(L) + 1;  
  if (filename == NULL) {
    lua_pushliteral(L, "=stdin");
    lf.f = stdin;
  }
  else {
    lua_pushfstring(L, "@%s", filename);
    lf.f = fopen(filename, "r");
    if (lf.f == NULL) return errfile(L, "open", fnameindex);
  }
  if (skipcomment(&lf, &c))  
    lf.buff[lf.n++] = '\n';  
  if (c == LUA_SIGNATURE[0] && filename) {  
    lf.f = freopen(filename, "rb", lf.f);  
    if (lf.f == NULL) return errfile(L, "reopen", fnameindex);
    skipcomment(&lf, &c);  
  }
  if (c != EOF)
    lf.buff[lf.n++] = c;  
  status = lua_load(L, getF, &lf, lua_tostring(L, -1), mode);
  readstatus = ferror(lf.f);
  if (filename) fclose(lf.f);  
  if (readstatus) {
    lua_settop(L, fnameindex);  
    return errfile(L, "read", fnameindex);
  }
  lua_remove(L, fnameindex);
  return status;
}
typedef struct LoadS {
  const char *s;
  size_t size;
} LoadS;
static const char *getS (lua_State *L, void *ud, size_t *size) {
  LoadS *ls = (LoadS *)ud;
  (void)L;  
  if (ls->size == 0) return NULL;
  *size = ls->size;
  ls->size = 0;
  return ls->s;
}
LUALIB_API int luaL_loadbufferx (lua_State *L, const char *buff, size_t size,
                                 const char *name, const char *mode) {
  LoadS ls;
  ls.s = buff;
  ls.size = size;
  return lua_load(L, getS, &ls, name, mode);
}
LUALIB_API int luaL_loadstring (lua_State *L, const char *s) {
  return luaL_loadbuffer(L, s, strlen(s), s);
}
LUALIB_API int luaL_getmetafield (lua_State *L, int obj, const char *event) {
  if (!lua_getmetatable(L, obj))  
    return LUA_TNIL;
  else {
    int tt;
    lua_pushstring(L, event);
    tt = lua_rawget(L, -2);
    if (tt == LUA_TNIL)  
      lua_pop(L, 2);  
    else
      lua_remove(L, -2);  
    return tt;  
  }
}
LUALIB_API int luaL_callmeta (lua_State *L, int obj, const char *event) {
  obj = lua_absindex(L, obj);
  if (luaL_getmetafield(L, obj, event) == LUA_TNIL)  
    return 0;
  lua_pushvalue(L, obj);
  lua_call(L, 1, 1);
  return 1;
}
LUALIB_API lua_Integer luaL_len (lua_State *L, int idx) {
  lua_Integer l;
  int isnum;
  lua_len(L, idx);
  l = lua_tointegerx(L, -1, &isnum);
  if (!isnum)
    luaL_error(L, "object length is not an integer");
  lua_pop(L, 1);  
  return l;
}
LUALIB_API const char *luaL_tolstring (lua_State *L, int idx, size_t *len) {
  if (luaL_callmeta(L, idx, "__tostring")) {  
    if (!lua_isstring(L, -1))
      luaL_error(L, "'__tostring' must return a string");
  }
  else {
    switch (lua_type(L, idx)) {
      case LUA_TNUMBER: {
        if (lua_isinteger(L, idx))
          lua_pushfstring(L, "%I", (LUAI_UACINT)lua_tointeger(L, idx));
        else
          lua_pushfstring(L, "%f", (LUAI_UACNUMBER)lua_tonumber(L, idx));
        break;
      }
      case LUA_TSTRING:
        lua_pushvalue(L, idx);
        break;
      case LUA_TBOOLEAN:
        lua_pushstring(L, (lua_toboolean(L, idx) ? "true" : "false"));
        break;
      case LUA_TNIL:
        lua_pushliteral(L, "nil");
        break;
      default: {
        int tt = luaL_getmetafield(L, idx, "__name");  
        const char *kind = (tt == LUA_TSTRING) ? lua_tostring(L, -1) :
                                                 luaL_typename(L, idx);
        lua_pushfstring(L, "%s: %p", kind, lua_topointer(L, idx));
        if (tt != LUA_TNIL)
          lua_remove(L, -2);  
        break;
      }
    }
  }
  return lua_tolstring(L, -1, len);
}
#if defined(LUA_COMPAT_MODULE)
static const char *luaL_findtable (lua_State *L, int idx,
                                   const char *fname, int szhint) {
  const char *e;
  if (idx) lua_pushvalue(L, idx);
  do {
    e = strchr(fname, '.');
    if (e == NULL) e = fname + strlen(fname);
    lua_pushlstring(L, fname, e - fname);
    if (lua_rawget(L, -2) == LUA_TNIL) {  
      lua_pop(L, 1);  
      lua_createtable(L, 0, (*e == '.' ? 1 : szhint)); 
      lua_pushlstring(L, fname, e - fname);
      lua_pushvalue(L, -2);
      lua_settable(L, -4);  
    }
    else if (!lua_istable(L, -1)) {  
      lua_pop(L, 2);  
      return fname;  
    }
    lua_remove(L, -2);  
    fname = e + 1;
  } while (*e == '.');
  return NULL;
}
static int libsize (const luaL_Reg *l) {
  int size = 0;
  for (; l && l->name; l++) size++;
  return size;
}
LUALIB_API void luaL_pushmodule (lua_State *L, const char *modname,
                                 int sizehint) {
  luaL_findtable(L, LUA_REGISTRYINDEX, LUA_LOADED_TABLE, 1);
  if (lua_getfield(L, -1, modname) != LUA_TTABLE) {  
    lua_pop(L, 1);  
    lua_pushglobaltable(L);
    if (luaL_findtable(L, 0, modname, sizehint) != NULL)
      luaL_error(L, "name conflict for module '%s'", modname);
    lua_pushvalue(L, -1);
    lua_setfield(L, -3, modname);  
  }
  lua_remove(L, -2);  
}
LUALIB_API void luaL_openlib (lua_State *L, const char *libname,
                               const luaL_Reg *l, int nup) {
  luaL_checkversion(L);
  if (libname) {
    luaL_pushmodule(L, libname, libsize(l));  
    lua_insert(L, -(nup + 1));  
  }
  if (l)
    luaL_setfuncs(L, l, nup);
  else
    lua_pop(L, nup);  
}
#endif
LUALIB_API void luaL_setfuncs (lua_State *L, const luaL_Reg *l, int nup) {
  luaL_checkstack(L, nup, "too many upvalues");
  for (; l->name != NULL; l++) {  
    int i;
    for (i = 0; i < nup; i++)  
      lua_pushvalue(L, -nup);
    lua_pushcclosure(L, l->func, nup);  
    lua_setfield(L, -(nup + 2), l->name);
  }
  lua_pop(L, nup);  
}
LUALIB_API int luaL_getsubtable (lua_State *L, int idx, const char *fname) {
  if (lua_getfield(L, idx, fname) == LUA_TTABLE)
    return 1;  
  else {
    lua_pop(L, 1);  
    idx = lua_absindex(L, idx);
    lua_newtable(L);
    lua_pushvalue(L, -1);  
    lua_setfield(L, idx, fname);  
    return 0;  
  }
}
LUALIB_API void luaL_requiref (lua_State *L, const char *modname,
                               lua_CFunction openf, int glb) {
  luaL_getsubtable(L, LUA_REGISTRYINDEX, LUA_LOADED_TABLE);
  lua_getfield(L, -1, modname);  
  if (!lua_toboolean(L, -1)) {  
    lua_pop(L, 1);  
    lua_pushcfunction(L, openf);
    lua_pushstring(L, modname);  
    lua_call(L, 1, 1);  
    lua_pushvalue(L, -1);  
    lua_setfield(L, -3, modname);  
  }
  lua_remove(L, -2);  
  if (glb) {
    lua_pushvalue(L, -1);  
    lua_setglobal(L, modname);  
  }
}
LUALIB_API const char *luaL_gsub (lua_State *L, const char *s, const char *p,
                                                               const char *r) {
  const char *wild;
  size_t l = strlen(p);
  luaL_Buffer b;
  luaL_buffinit(L, &b);
  while ((wild = strstr(s, p)) != NULL) {
    luaL_addlstring(&b, s, wild - s);  
    luaL_addstring(&b, r);  
    s = wild + l;  
  }
  luaL_addstring(&b, s);  
  luaL_pushresult(&b);
  return lua_tostring(L, -1);
}
static void *l_alloc (void *ud, void *ptr, size_t osize, size_t nsize) {
  (void)ud; (void)osize;  
  if (nsize == 0) {
    free(ptr);
    return NULL;
  }
  else {  
    void *newptr = realloc(ptr, nsize);
    if (newptr == NULL && ptr != NULL && nsize <= osize)
      return ptr;  
    else  
     return newptr;  
  }
}
static int panic (lua_State *L) {
  lua_writestringerror("PANIC: unprotected error in call to Lua API (%s)\n",
                        lua_tostring(L, -1));
  return 0;  
}
LUALIB_API lua_State *luaL_newstate (void) {
  lua_State *L = lua_newstate(l_alloc, NULL);
  if (L) lua_atpanic(L, &panic);
  return L;
}
LUALIB_API void luaL_checkversion_ (lua_State *L, lua_Number ver, size_t sz) {
  const lua_Number *v = lua_version(L);
  if (sz != LUAL_NUMSIZES)  
    luaL_error(L, "core and library have incompatible numeric types");
  if (v != lua_version(NULL))
    luaL_error(L, "multiple Lua VMs detected");
  else if (*v != ver)
    luaL_error(L, "version mismatch: app. needs %f, Lua core provides %f",
                  (LUAI_UACNUMBER)ver, (LUAI_UACNUMBER)*v);
}
#define lbaselib_c
#define LUA_LIB
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
static int luaB_print (lua_State *L) {
  int n = lua_gettop(L);  
  int i;
  lua_getglobal(L, "tostring");
  for (i=1; i<=n; i++) {
    const char *s;
    size_t l;
    lua_pushvalue(L, -1);  
    lua_pushvalue(L, i);   
    lua_call(L, 1, 1);
    s = lua_tolstring(L, -1, &l);  
    if (s == NULL)
      return luaL_error(L, "'tostring' must return a string to 'print'");
    if (i>1) lua_writestring("\t", 1);
    lua_writestring(s, l);
    lua_pop(L, 1);  
  }
  lua_writeline();
  return 0;
}
#define SPACECHARS	" \f\n\r\t\v"
static const char *b_str2int (const char *s, int base, lua_Integer *pn) {
  lua_Unsigned n = 0;
  int neg = 0;
  s += strspn(s, SPACECHARS);  
  if (*s == '-') { s++; neg = 1; }  
  else if (*s == '+') s++;
  if (!isalnum((unsigned char)*s))  
    return NULL;
  do {
    int digit = (isdigit((unsigned char)*s)) ? *s - '0'
                   : (toupper((unsigned char)*s) - 'A') + 10;
    if (digit >= base) return NULL;  
    n = n * base + digit;
    s++;
  } while (isalnum((unsigned char)*s));
  s += strspn(s, SPACECHARS);  
  *pn = (lua_Integer)((neg) ? (0u - n) : n);
  return s;
}
static int luaB_tonumber (lua_State *L) {
  if (lua_isnoneornil(L, 2)) {  
    luaL_checkany(L, 1);
    if (lua_type(L, 1) == LUA_TNUMBER) {  
      lua_settop(L, 1);  
      return 1;
    }
    else {
      size_t l;
      const char *s = lua_tolstring(L, 1, &l);
      if (s != NULL && lua_stringtonumber(L, s) == l + 1)
        return 1;  
    }
  }
  else {
    size_t l;
    const char *s;
    lua_Integer n = 0;  
    lua_Integer base = luaL_checkinteger(L, 2);
    luaL_checktype(L, 1, LUA_TSTRING);  
    s = lua_tolstring(L, 1, &l);
    luaL_argcheck(L, 2 <= base && base <= 36, 2, "base out of range");
    if (b_str2int(s, (int)base, &n) == s + l) {
      lua_pushinteger(L, n);
      return 1;
    }  
  }  
  lua_pushnil(L);  
  return 1;
}
static int luaB_error (lua_State *L) {
  int level = (int)luaL_optinteger(L, 2, 1);
  lua_settop(L, 1);
  if (lua_type(L, 1) == LUA_TSTRING && level > 0) {
    luaL_where(L, level);   
    lua_pushvalue(L, 1);
    lua_concat(L, 2);
  }
  return lua_error(L);
}
static int luaB_getmetatable (lua_State *L) {
  luaL_checkany(L, 1);
  if (!lua_getmetatable(L, 1)) {
    lua_pushnil(L);
    return 1;  
  }
  luaL_getmetafield(L, 1, "__metatable");
  return 1;  
}
static int luaB_setmetatable (lua_State *L) {
  int t = lua_type(L, 2);
  luaL_checktype(L, 1, LUA_TTABLE);
  luaL_argcheck(L, t == LUA_TNIL || t == LUA_TTABLE, 2,
                    "nil or table expected");
  if (luaL_getmetafield(L, 1, "__metatable") != LUA_TNIL)
    return luaL_error(L, "cannot change a protected metatable");
  lua_settop(L, 2);
  lua_setmetatable(L, 1);
  return 1;
}
static int luaB_rawequal (lua_State *L) {
  luaL_checkany(L, 1);
  luaL_checkany(L, 2);
  lua_pushboolean(L, lua_rawequal(L, 1, 2));
  return 1;
}
static int luaB_rawlen (lua_State *L) {
  int t = lua_type(L, 1);
  luaL_argcheck(L, t == LUA_TTABLE || t == LUA_TSTRING, 1,
                   "table or string expected");
  lua_pushinteger(L, lua_rawlen(L, 1));
  return 1;
}
static int luaB_rawget (lua_State *L) {
  luaL_checktype(L, 1, LUA_TTABLE);
  luaL_checkany(L, 2);
  lua_settop(L, 2);
  lua_rawget(L, 1);
  return 1;
}
static int luaB_rawset (lua_State *L) {
  luaL_checktype(L, 1, LUA_TTABLE);
  luaL_checkany(L, 2);
  luaL_checkany(L, 3);
  lua_settop(L, 3);
  lua_rawset(L, 1);
  return 1;
}
static int luaB_collectgarbage (lua_State *L) {
  static const char *const opts[] = {"stop", "restart", "collect",
    "count", "step", "setpause", "setstepmul",
    "isrunning", NULL};
  static const int optsnum[] = {LUA_GCSTOP, LUA_GCRESTART, LUA_GCCOLLECT,
    LUA_GCCOUNT, LUA_GCSTEP, LUA_GCSETPAUSE, LUA_GCSETSTEPMUL,
    LUA_GCISRUNNING};
  int o = optsnum[luaL_checkoption(L, 1, "collect", opts)];
  int ex = (int)luaL_optinteger(L, 2, 0);
  int res = lua_gc(L, o, ex);
  switch (o) {
    case LUA_GCCOUNT: {
      int b = lua_gc(L, LUA_GCCOUNTB, 0);
      lua_pushnumber(L, (lua_Number)res + ((lua_Number)b/1024));
      return 1;
    }
    case LUA_GCSTEP: case LUA_GCISRUNNING: {
      lua_pushboolean(L, res);
      return 1;
    }
    default: {
      lua_pushinteger(L, res);
      return 1;
    }
  }
}
static int luaB_type (lua_State *L) {
  int t = lua_type(L, 1);
  luaL_argcheck(L, t != LUA_TNONE, 1, "value expected");
  lua_pushstring(L, lua_typename(L, t));
  return 1;
}
static int pairsmeta (lua_State *L, const char *method, int iszero,
                      lua_CFunction iter) {
  luaL_checkany(L, 1);
  if (luaL_getmetafield(L, 1, method) == LUA_TNIL) {  
    lua_pushcfunction(L, iter);  
    lua_pushvalue(L, 1);  
    if (iszero) lua_pushinteger(L, 0);  
    else lua_pushnil(L);
  }
  else {
    lua_pushvalue(L, 1);  
    lua_call(L, 1, 3);  
  }
  return 3;
}
static int luaB_next (lua_State *L) {
  luaL_checktype(L, 1, LUA_TTABLE);
  lua_settop(L, 2);  
  if (lua_next(L, 1))
    return 2;
  else {
    lua_pushnil(L);
    return 1;
  }
}
static int luaB_pairs (lua_State *L) {
  return pairsmeta(L, "__pairs", 0, luaB_next);
}
static int ipairsaux (lua_State *L) {
  lua_Integer i = luaL_checkinteger(L, 2) + 1;
  lua_pushinteger(L, i);
  return (lua_geti(L, 1, i) == LUA_TNIL) ? 1 : 2;
}
static int luaB_ipairs (lua_State *L) {
#if defined(LUA_COMPAT_IPAIRS)
  return pairsmeta(L, "__ipairs", 1, ipairsaux);
#else
  luaL_checkany(L, 1);
  lua_pushcfunction(L, ipairsaux);  
  lua_pushvalue(L, 1);  
  lua_pushinteger(L, 0);  
  return 3;
#endif
}
static int load_aux (lua_State *L, int status, int envidx) {
  if (status == LUA_OK) {
    if (envidx != 0) {  
      lua_pushvalue(L, envidx);  
      if (!lua_setupvalue(L, -2, 1))  
        lua_pop(L, 1);  
    }
    return 1;
  }
  else {  
    lua_pushnil(L);
    lua_insert(L, -2);  
    return 2;  
  }
}
static int luaB_loadfile (lua_State *L) {
  const char *fname = luaL_optstring(L, 1, NULL);
  const char *mode = luaL_optstring(L, 2, NULL);
  int env = (!lua_isnone(L, 3) ? 3 : 0);  
  int status = luaL_loadfilex(L, fname, mode);
  return load_aux(L, status, env);
}
#define RESERVEDSLOT	5
static const char *generic_reader (lua_State *L, void *ud, size_t *size) {
  (void)(ud);  
  luaL_checkstack(L, 2, "too many nested functions");
  lua_pushvalue(L, 1);  
  lua_call(L, 0, 1);  
  if (lua_isnil(L, -1)) {
    lua_pop(L, 1);  
    *size = 0;
    return NULL;
  }
  else if (!lua_isstring(L, -1))
    luaL_error(L, "reader function must return a string");
  lua_replace(L, RESERVEDSLOT);  
  return lua_tolstring(L, RESERVEDSLOT, size);
}
static int luaB_load (lua_State *L) {
  int status;
  size_t l;
  const char *s = lua_tolstring(L, 1, &l);
  const char *mode = luaL_optstring(L, 3, "bt");
  int env = (!lua_isnone(L, 4) ? 4 : 0);  
  if (s != NULL) {  
    const char *chunkname = luaL_optstring(L, 2, s);
    status = luaL_loadbufferx(L, s, l, chunkname, mode);
  }
  else {  
    const char *chunkname = luaL_optstring(L, 2, "=(load)");
    luaL_checktype(L, 1, LUA_TFUNCTION);
    lua_settop(L, RESERVEDSLOT);  
    status = lua_load(L, generic_reader, NULL, chunkname, mode);
  }
  return load_aux(L, status, env);
}
static int dofilecont (lua_State *L, int d1, lua_KContext d2) {
  (void)d1;  (void)d2;  
  return lua_gettop(L) - 1;
}
static int luaB_dofile (lua_State *L) {
  const char *fname = luaL_optstring(L, 1, NULL);
  lua_settop(L, 1);
  if (luaL_loadfile(L, fname) != LUA_OK)
    return lua_error(L);
  lua_callk(L, 0, LUA_MULTRET, 0, dofilecont);
  return dofilecont(L, 0, 0);
}
static int luaB_assert (lua_State *L) {
  if (lua_toboolean(L, 1))  
    return lua_gettop(L);  
  else {  
    luaL_checkany(L, 1);  
    lua_remove(L, 1);  
    lua_pushliteral(L, "assertion failed!");  
    lua_settop(L, 1);  
    return luaB_error(L);  
  }
}
static int luaB_select (lua_State *L) {
  int n = lua_gettop(L);
  if (lua_type(L, 1) == LUA_TSTRING && *lua_tostring(L, 1) == '#') {
    lua_pushinteger(L, n-1);
    return 1;
  }
  else {
    lua_Integer i = luaL_checkinteger(L, 1);
    if (i < 0) i = n + i;
    else if (i > n) i = n;
    luaL_argcheck(L, 1 <= i, 1, "index out of range");
    return n - (int)i;
  }
}
static int finishpcall (lua_State *L, int status, lua_KContext extra) {
  if (status != LUA_OK && status != LUA_YIELD) {  
    lua_pushboolean(L, 0);  
    lua_pushvalue(L, -2);  
    return 2;  
  }
  else
    return lua_gettop(L) - (int)extra;  
}
static int luaB_pcall (lua_State *L) {
  int status;
  luaL_checkany(L, 1);
  lua_pushboolean(L, 1);  
  lua_insert(L, 1);  
  status = lua_pcallk(L, lua_gettop(L) - 2, LUA_MULTRET, 0, 0, finishpcall);
  return finishpcall(L, status, 0);
}
static int luaB_xpcall (lua_State *L) {
  int status;
  int n = lua_gettop(L);
  luaL_checktype(L, 2, LUA_TFUNCTION);  
  lua_pushboolean(L, 1);  
  lua_pushvalue(L, 1);  
  lua_rotate(L, 3, 2);  
  status = lua_pcallk(L, n - 2, LUA_MULTRET, 2, 2, finishpcall);
  return finishpcall(L, status, 2);
}
static int luaB_tostring (lua_State *L) {
  luaL_checkany(L, 1);
  luaL_tolstring(L, 1, NULL);
  return 1;
}
static const luaL_Reg base_funcs[] = {
  {"assert", luaB_assert},
  {"collectgarbage", luaB_collectgarbage},
  {"dofile", luaB_dofile},
  {"error", luaB_error},
  {"getmetatable", luaB_getmetatable},
  {"ipairs", luaB_ipairs},
  {"loadfile", luaB_loadfile},
  {"load", luaB_load},
#if defined(LUA_COMPAT_LOADSTRING)
  {"loadstring", luaB_load},
#endif
  {"next", luaB_next},
  {"pairs", luaB_pairs},
  {"pcall", luaB_pcall},
  {"print", luaB_print},
  {"rawequal", luaB_rawequal},
  {"rawlen", luaB_rawlen},
  {"rawget", luaB_rawget},
  {"rawset", luaB_rawset},
  {"select", luaB_select},
  {"setmetatable", luaB_setmetatable},
  {"tonumber", luaB_tonumber},
  {"tostring", luaB_tostring},
  {"type", luaB_type},
  {"xpcall", luaB_xpcall},
  {"_G", NULL},
  {"_VERSION", NULL},
  {NULL, NULL}
};
LUAMOD_API int luaopen_base (lua_State *L) {
  lua_pushglobaltable(L);
  luaL_setfuncs(L, base_funcs, 0);
  lua_pushvalue(L, -1);
  lua_setfield(L, -2, "_G");
  lua_pushliteral(L, LUA_VERSION);
  lua_setfield(L, -2, "_VERSION");
  return 1;
}
#define lcorolib_c
#define LUA_LIB
#include <stdlib.h>
static lua_State *getco (lua_State *L) {
  lua_State *co = lua_tothread(L, 1);
  luaL_argcheck(L, co, 1, "thread expected");
  return co;
}
static int auxresume (lua_State *L, lua_State *co, int narg) {
  int status;
  if (!lua_checkstack(co, narg)) {
    lua_pushliteral(L, "too many arguments to resume");
    return -1;  
  }
  if (lua_status(co) == LUA_OK && lua_gettop(co) == 0) {
    lua_pushliteral(L, "cannot resume dead coroutine");
    return -1;  
  }
  lua_xmove(L, co, narg);
  status = lua_resume(co, L, narg);
  if (status == LUA_OK || status == LUA_YIELD) {
    int nres = lua_gettop(co);
    if (!lua_checkstack(L, nres + 1)) {
      lua_pop(co, nres);  
      lua_pushliteral(L, "too many results to resume");
      return -1;  
    }
    lua_xmove(co, L, nres);  
    return nres;
  }
  else {
    lua_xmove(co, L, 1);  
    return -1;  
  }
}
static int luaB_coresume (lua_State *L) {
  lua_State *co = getco(L);
  int r;
  r = auxresume(L, co, lua_gettop(L) - 1);
  if (r < 0) {
    lua_pushboolean(L, 0);
    lua_insert(L, -2);
    return 2;  
  }
  else {
    lua_pushboolean(L, 1);
    lua_insert(L, -(r + 1));
    return r + 1;  
  }
}
static int luaB_auxwrap (lua_State *L) {
  lua_State *co = lua_tothread(L, lua_upvalueindex(1));
  int r = auxresume(L, co, lua_gettop(L));
  if (r < 0) {
    int stat = lua_status(co);
    if (stat != LUA_OK && stat != LUA_YIELD)
      lua_resetthread(co);  
    if (lua_type(L, -1) == LUA_TSTRING) {  
      luaL_where(L, 1);  
      lua_insert(L, -2);
      lua_concat(L, 2);
    }
    return lua_error(L);  
  }
  return r;
}
static int luaB_cocreate (lua_State *L) {
  lua_State *NL;
  luaL_checktype(L, 1, LUA_TFUNCTION);
  NL = lua_newthread(L);
  lua_pushvalue(L, 1);  
  lua_xmove(L, NL, 1);  
  return 1;
}
static int luaB_cowrap (lua_State *L) {
  luaB_cocreate(L);
  lua_pushcclosure(L, luaB_auxwrap, 1);
  return 1;
}
static int luaB_yield (lua_State *L) {
  return lua_yield(L, lua_gettop(L));
}
static int luaB_costatus (lua_State *L) {
  lua_State *co = getco(L);
  if (L == co) lua_pushliteral(L, "running");
  else {
    switch (lua_status(co)) {
      case LUA_YIELD:
        lua_pushliteral(L, "suspended");
        break;
      case LUA_OK: {
        lua_Debug ar;
        if (lua_getstack(co, 0, &ar) > 0)  
          lua_pushliteral(L, "normal");  
        else if (lua_gettop(co) == 0)
            lua_pushliteral(L, "dead");
        else
          lua_pushliteral(L, "suspended");  
        break;
      }
      default:  
        lua_pushliteral(L, "dead");
        break;
    }
  }
  return 1;
}
static int luaB_yieldable (lua_State *L) {
  lua_pushboolean(L, lua_isyieldable(L));
  return 1;
}
static int luaB_corunning (lua_State *L) {
  int ismain = lua_pushthread(L);
  lua_pushboolean(L, ismain);
  return 2;
}
static const luaL_Reg co_funcs[] = {
  {"create", luaB_cocreate},
  {"resume", luaB_coresume},
  {"running", luaB_corunning},
  {"status", luaB_costatus},
  {"wrap", luaB_cowrap},
  {"yield", luaB_yield},
  {"isyieldable", luaB_yieldable},
  {NULL, NULL}
};
LUAMOD_API int luaopen_coroutine (lua_State *L) {
  luaL_newlib(L, co_funcs);
  return 1;
}
#define ldblib_c
#define LUA_LIB
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
static const int HOOKKEY = 0;
static void checkstack (lua_State *L, lua_State *L1, int n) {
  if (L != L1 && !lua_checkstack(L1, n))
    luaL_error(L, "stack overflow");
}
static int db_getregistry (lua_State *L) {
  lua_pushvalue(L, LUA_REGISTRYINDEX);
  return 1;
}
static int db_getmetatable (lua_State *L) {
  luaL_checkany(L, 1);
  if (!lua_getmetatable(L, 1)) {
    lua_pushnil(L);  
  }
  return 1;
}
static int db_setmetatable (lua_State *L) {
  int t = lua_type(L, 2);
  luaL_argcheck(L, t == LUA_TNIL || t == LUA_TTABLE, 2,
                    "nil or table expected");
  lua_settop(L, 2);
  lua_setmetatable(L, 1);
  return 1;  
}
static int db_getuservalue (lua_State *L) {
  if (lua_type(L, 1) != LUA_TUSERDATA)
    lua_pushnil(L);
  else
    lua_getuservalue(L, 1);
  return 1;
}
static int db_setuservalue (lua_State *L) {
  luaL_checktype(L, 1, LUA_TUSERDATA);
  luaL_checkany(L, 2);
  lua_settop(L, 2);
  lua_setuservalue(L, 1);
  return 1;
}
static lua_State *getthread (lua_State *L, int *arg) {
  if (lua_isthread(L, 1)) {
    *arg = 1;
    return lua_tothread(L, 1);
  }
  else {
    *arg = 0;
    return L;  
  }
}
static void settabss (lua_State *L, const char *k, const char *v) {
  lua_pushstring(L, v);
  lua_setfield(L, -2, k);
}
static void settabsi (lua_State *L, const char *k, int v) {
  lua_pushinteger(L, v);
  lua_setfield(L, -2, k);
}
static void settabsb (lua_State *L, const char *k, int v) {
  lua_pushboolean(L, v);
  lua_setfield(L, -2, k);
}
static void treatstackoption (lua_State *L, lua_State *L1, const char *fname) {
  if (L == L1)
    lua_rotate(L, -2, 1);  
  else
    lua_xmove(L1, L, 1);  
  lua_setfield(L, -2, fname);  
}
static int db_getinfo (lua_State *L) {
  lua_Debug ar;
  int arg;
  lua_State *L1 = getthread(L, &arg);
  const char *options = luaL_optstring(L, arg+2, "flnStu");
  checkstack(L, L1, 3);
  if (lua_isfunction(L, arg + 1)) {  
    options = lua_pushfstring(L, ">%s", options);  
    lua_pushvalue(L, arg + 1);  
    lua_xmove(L, L1, 1);
  }
  else {  
    if (!lua_getstack(L1, (int)luaL_checkinteger(L, arg + 1), &ar)) {
      lua_pushnil(L);  
      return 1;
    }
  }
  if (!lua_getinfo(L1, options, &ar))
    return luaL_argerror(L, arg+2, "invalid option");
  lua_newtable(L);  
  if (strchr(options, 'S')) {
    settabss(L, "source", ar.source);
    settabss(L, "short_src", ar.short_src);
    settabsi(L, "linedefined", ar.linedefined);
    settabsi(L, "lastlinedefined", ar.lastlinedefined);
    settabss(L, "what", ar.what);
  }
  if (strchr(options, 'l'))
    settabsi(L, "currentline", ar.currentline);
  if (strchr(options, 'u')) {
    settabsi(L, "nups", ar.nups);
    settabsi(L, "nparams", ar.nparams);
    settabsb(L, "isvararg", ar.isvararg);
  }
  if (strchr(options, 'n')) {
    settabss(L, "name", ar.name);
    settabss(L, "namewhat", ar.namewhat);
  }
  if (strchr(options, 't'))
    settabsb(L, "istailcall", ar.istailcall);
  if (strchr(options, 'L'))
    treatstackoption(L, L1, "activelines");
  if (strchr(options, 'f'))
    treatstackoption(L, L1, "func");
  return 1;  
}
static int db_getlocal (lua_State *L) {
  int arg;
  lua_State *L1 = getthread(L, &arg);
  lua_Debug ar;
  const char *name;
  int nvar = (int)luaL_checkinteger(L, arg + 2);  
  if (lua_isfunction(L, arg + 1)) {  
    lua_pushvalue(L, arg + 1);  
    lua_pushstring(L, lua_getlocal(L, NULL, nvar));  
    return 1;  
  }
  else {  
    int level = (int)luaL_checkinteger(L, arg + 1);
    if (!lua_getstack(L1, level, &ar))  
      return luaL_argerror(L, arg+1, "level out of range");
    checkstack(L, L1, 1);
    name = lua_getlocal(L1, &ar, nvar);
    if (name) {
      lua_xmove(L1, L, 1);  
      lua_pushstring(L, name);  
      lua_rotate(L, -2, 1);  
      return 2;
    }
    else {
      lua_pushnil(L);  
      return 1;
    }
  }
}
static int db_setlocal (lua_State *L) {
  int arg;
  const char *name;
  lua_State *L1 = getthread(L, &arg);
  lua_Debug ar;
  int level = (int)luaL_checkinteger(L, arg + 1);
  int nvar = (int)luaL_checkinteger(L, arg + 2);
  if (!lua_getstack(L1, level, &ar))  
    return luaL_argerror(L, arg+1, "level out of range");
  luaL_checkany(L, arg+3);
  lua_settop(L, arg+3);
  checkstack(L, L1, 1);
  lua_xmove(L, L1, 1);
  name = lua_setlocal(L1, &ar, nvar);
  if (name == NULL)
    lua_pop(L1, 1);  
  lua_pushstring(L, name);
  return 1;
}
static int auxupvalue (lua_State *L, int get) {
  const char *name;
  int n = (int)luaL_checkinteger(L, 2);  
  luaL_checktype(L, 1, LUA_TFUNCTION);  
  name = get ? lua_getupvalue(L, 1, n) : lua_setupvalue(L, 1, n);
  if (name == NULL) return 0;
  lua_pushstring(L, name);
  lua_insert(L, -(get+1));  
  return get + 1;
}
static int db_getupvalue (lua_State *L) {
  return auxupvalue(L, 1);
}
static int db_setupvalue (lua_State *L) {
  luaL_checkany(L, 3);
  return auxupvalue(L, 0);
}
static int checkupval (lua_State *L, int argf, int argnup) {
  int nup = (int)luaL_checkinteger(L, argnup);  
  luaL_checktype(L, argf, LUA_TFUNCTION);  
  luaL_argcheck(L, (lua_getupvalue(L, argf, nup) != NULL), argnup,
                   "invalid upvalue index");
  return nup;
}
static int db_upvalueid (lua_State *L) {
  int n = checkupval(L, 1, 2);
  lua_pushlightuserdata(L, lua_upvalueid(L, 1, n));
  return 1;
}
static int db_upvaluejoin (lua_State *L) {
  int n1 = checkupval(L, 1, 2);
  int n2 = checkupval(L, 3, 4);
  luaL_argcheck(L, !lua_iscfunction(L, 1), 1, "Lua function expected");
  luaL_argcheck(L, !lua_iscfunction(L, 3), 3, "Lua function expected");
  lua_upvaluejoin(L, 1, n1, 3, n2);
  return 0;
}
static void hookf (lua_State *L, lua_Debug *ar) {
  static const char *const hooknames[] =
    {"call", "return", "line", "count", "tail call"};
  lua_rawgetp(L, LUA_REGISTRYINDEX, &HOOKKEY);
  lua_pushthread(L);
  if (lua_rawget(L, -2) == LUA_TFUNCTION) {  
    lua_pushstring(L, hooknames[(int)ar->event]);  
    if (ar->currentline >= 0)
      lua_pushinteger(L, ar->currentline);  
    else lua_pushnil(L);
    lua_assert(lua_getinfo(L, "lS", ar));
    lua_call(L, 2, 0);  
  }
}
static int makemask (const char *smask, int count) {
  int mask = 0;
  if (strchr(smask, 'c')) mask |= LUA_MASKCALL;
  if (strchr(smask, 'r')) mask |= LUA_MASKRET;
  if (strchr(smask, 'l')) mask |= LUA_MASKLINE;
  if (count > 0) mask |= LUA_MASKCOUNT;
  return mask;
}
static char *unmakemask (int mask, char *smask) {
  int i = 0;
  if (mask & LUA_MASKCALL) smask[i++] = 'c';
  if (mask & LUA_MASKRET) smask[i++] = 'r';
  if (mask & LUA_MASKLINE) smask[i++] = 'l';
  smask[i] = '\0';
  return smask;
}
static int db_sethook (lua_State *L) {
  int arg, mask, count;
  lua_Hook func;
  lua_State *L1 = getthread(L, &arg);
  if (lua_isnoneornil(L, arg+1)) {  
    lua_settop(L, arg+1);
    func = NULL; mask = 0; count = 0;  
  }
  else {
    const char *smask = luaL_checkstring(L, arg+2);
    luaL_checktype(L, arg+1, LUA_TFUNCTION);
    count = (int)luaL_optinteger(L, arg + 3, 0);
    func = hookf; mask = makemask(smask, count);
  }
  if (lua_rawgetp(L, LUA_REGISTRYINDEX, &HOOKKEY) == LUA_TNIL) {
    lua_createtable(L, 0, 2);  
    lua_pushvalue(L, -1);
    lua_rawsetp(L, LUA_REGISTRYINDEX, &HOOKKEY);  
    lua_pushstring(L, "k");
    lua_setfield(L, -2, "__mode");  
    lua_pushvalue(L, -1);
    lua_setmetatable(L, -2);  
  }
  checkstack(L, L1, 1);
  lua_pushthread(L1); lua_xmove(L1, L, 1);  
  lua_pushvalue(L, arg + 1);  
  lua_rawset(L, -3);  
  lua_sethook(L1, func, mask, count);
  return 0;
}
static int db_gethook (lua_State *L) {
  int arg;
  lua_State *L1 = getthread(L, &arg);
  char buff[5];
  int mask = lua_gethookmask(L1);
  lua_Hook hook = lua_gethook(L1);
  if (hook == NULL)  
    lua_pushnil(L);
  else if (hook != hookf)  
    lua_pushliteral(L, "external hook");
  else {  
    lua_rawgetp(L, LUA_REGISTRYINDEX, &HOOKKEY);
    checkstack(L, L1, 1);
    lua_pushthread(L1); lua_xmove(L1, L, 1);
    lua_rawget(L, -2);   
    lua_remove(L, -2);  
  }
  lua_pushstring(L, unmakemask(mask, buff));  
  lua_pushinteger(L, lua_gethookcount(L1));  
  return 3;
}
static int db_debug (lua_State *L) {
  for (;;) {
    char buffer[250];
    lua_writestringerror("%s", "lua_debug> ");
    if (fgets(buffer, sizeof(buffer), stdin) == 0 ||
        strcmp(buffer, "cont\n") == 0)
      return 0;
    if (luaL_loadbuffer(L, buffer, strlen(buffer), "=(debug command)") ||
        lua_pcall(L, 0, 0, 0))
      lua_writestringerror("%s\n", lua_tostring(L, -1));
    lua_settop(L, 0);  
  }
}
static int db_traceback (lua_State *L) {
  int arg;
  lua_State *L1 = getthread(L, &arg);
  const char *msg = lua_tostring(L, arg + 1);
  if (msg == NULL && !lua_isnoneornil(L, arg + 1))  
    lua_pushvalue(L, arg + 1);  
  else {
    int level = (int)luaL_optinteger(L, arg + 2, (L == L1) ? 1 : 0);
    luaL_traceback(L, L1, msg, level);
  }
  return 1;
}
static const luaL_Reg dblib[] = {
  {"debug", db_debug},
  {"getuservalue", db_getuservalue},
  {"gethook", db_gethook},
  {"getinfo", db_getinfo},
  {"getlocal", db_getlocal},
  {"getregistry", db_getregistry},
  {"getmetatable", db_getmetatable},
  {"getupvalue", db_getupvalue},
  {"upvaluejoin", db_upvaluejoin},
  {"upvalueid", db_upvalueid},
  {"setuservalue", db_setuservalue},
  {"sethook", db_sethook},
  {"setlocal", db_setlocal},
  {"setmetatable", db_setmetatable},
  {"setupvalue", db_setupvalue},
  {"traceback", db_traceback},
  {NULL, NULL}
};
LUAMOD_API int luaopen_debug (lua_State *L) {
  luaL_newlib(L, dblib);
  return 1;
}
#define liolib_c
#define LUA_LIB
#include <ctype.h>
#include <errno.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if !defined(l_checkmode)
#if !defined(L_MODEEXT)
#define L_MODEEXT	"b"
#endif
static int l_checkmode (const char *mode) {
  return (*mode != '\0' && strchr("rwa", *(mode++)) != NULL &&
         (*mode != '+' || (++mode, 1)) &&  
         (strspn(mode, L_MODEEXT) == strlen(mode)));  
}
#endif
#if !defined(l_popen)		
#if defined(LUA_USE_POSIX)	
#define l_popen(L,c,m)		(fflush(NULL), popen(c,m))
#define l_pclose(L,file)	(pclose(file))
#elif defined(LUA_USE_WINDOWS)	
#define l_popen(L,c,m)		(_popen(c,m))
#define l_pclose(L,file)	(_pclose(file))
#else				
#define l_popen(L,c,m)  \
	  ((void)((void)c, m), \
	  luaL_error(L, "'popen' not supported"), \
	  (FILE*)0)
#define l_pclose(L,file)		((void)L, (void)file, -1)
#endif				
#endif				
#if !defined(l_getc)		
#if defined(LUA_USE_POSIX)
#define l_getc(f)		getc_unlocked(f)
#define l_lockfile(f)		flockfile(f)
#define l_unlockfile(f)		funlockfile(f)
#else
#define l_getc(f)		getc(f)
#define l_lockfile(f)		((void)0)
#define l_unlockfile(f)		((void)0)
#endif
#endif				
#if !defined(l_fseek)		
#if defined(LUA_USE_POSIX)	
#include <sys/types.h>
#define l_fseek(f,o,w)		fseeko(f,o,w)
#define l_ftell(f)		ftello(f)
#define l_seeknum		off_t
#elif defined(LUA_USE_WINDOWS) && !defined(_CRTIMP_TYPEINFO) \
   && defined(_MSC_VER) && (_MSC_VER >= 1400)	
#define l_fseek(f,o,w)		_fseeki64(f,o,w)
#define l_ftell(f)		_ftelli64(f)
#define l_seeknum		__int64
#else				
#define l_fseek(f,o,w)		fseek(f,o,w)
#define l_ftell(f)		ftell(f)
#define l_seeknum		long
#endif				
#endif				
#define IO_PREFIX	"_IO_"
#define IOPREF_LEN	(sizeof(IO_PREFIX)/sizeof(char) - 1)
#define IO_INPUT	(IO_PREFIX "input")
#define IO_OUTPUT	(IO_PREFIX "output")
typedef luaL_Stream LStream;
#define tolstream(L)	((LStream *)luaL_checkudata(L, 1, LUA_FILEHANDLE))
#define isclosed(p)	((p)->closef == NULL)
static int io_type (lua_State *L) {
  LStream *p;
  luaL_checkany(L, 1);
  p = (LStream *)luaL_testudata(L, 1, LUA_FILEHANDLE);
  if (p == NULL)
    lua_pushnil(L);  
  else if (isclosed(p))
    lua_pushliteral(L, "closed file");
  else
    lua_pushliteral(L, "file");
  return 1;
}
static int f_tostring (lua_State *L) {
  LStream *p = tolstream(L);
  if (isclosed(p))
    lua_pushliteral(L, "file (closed)");
  else
    lua_pushfstring(L, "file (%p)", p->f);
  return 1;
}
static FILE *tofile (lua_State *L) {
  LStream *p = tolstream(L);
  if (isclosed(p))
    luaL_error(L, "attempt to use a closed file");
  lua_assert(p->f);
  return p->f;
}
static LStream *newprefile (lua_State *L) {
  LStream *p = (LStream *)lua_newuserdata(L, sizeof(LStream));
  p->closef = NULL;  
  luaL_setmetatable(L, LUA_FILEHANDLE);
  return p;
}
static int aux_close (lua_State *L) {
  LStream *p = tolstream(L);
  volatile lua_CFunction cf = p->closef;
  p->closef = NULL;  
  return (*cf)(L);  
}
static int f_close (lua_State *L) {
  tofile(L);  
  return aux_close(L);
}
static int io_close (lua_State *L) {
  if (lua_isnone(L, 1))  
    lua_getfield(L, LUA_REGISTRYINDEX, IO_OUTPUT);  
  return f_close(L);
}
static int f_gc (lua_State *L) {
  LStream *p = tolstream(L);
  if (!isclosed(p) && p->f != NULL)
    aux_close(L);  
  return 0;
}
static int io_fclose (lua_State *L) {
  LStream *p = tolstream(L);
  int res = fclose(p->f);
  return luaL_fileresult(L, (res == 0), NULL);
}
static LStream *newfile (lua_State *L) {
  LStream *p = newprefile(L);
  p->f = NULL;
  p->closef = &io_fclose;
  return p;
}
static void opencheck (lua_State *L, const char *fname, const char *mode) {
  LStream *p = newfile(L);
  p->f = fopen(fname, mode);
  if (p->f == NULL)
    luaL_error(L, "cannot open file '%s' (%s)", fname, strerror(errno));
}
static int io_open (lua_State *L) {
  const char *filename = luaL_checkstring(L, 1);
  const char *mode = luaL_optstring(L, 2, "r");
  LStream *p = newfile(L);
  const char *md = mode;  
  luaL_argcheck(L, l_checkmode(md), 2, "invalid mode");
  p->f = fopen(filename, mode);
  return (p->f == NULL) ? luaL_fileresult(L, 0, filename) : 1;
}
static int io_pclose (lua_State *L) {
  LStream *p = tolstream(L);
  return luaL_execresult(L, l_pclose(L, p->f));
}
static int io_popen (lua_State *L) {
  const char *filename = luaL_checkstring(L, 1);
  const char *mode = luaL_optstring(L, 2, "r");
  LStream *p = newprefile(L);
  luaL_argcheck(L, ((mode[0] == 'r' || mode[0] == 'w') && mode[1] == '\0'),
                   2, "invalid mode");
  p->f = l_popen(L, filename, mode);
  p->closef = &io_pclose;
  return (p->f == NULL) ? luaL_fileresult(L, 0, filename) : 1;
}
static int io_tmpfile (lua_State *L) {
  LStream *p = newfile(L);
  p->f = tmpfile();
  return (p->f == NULL) ? luaL_fileresult(L, 0, NULL) : 1;
}
static FILE *getiofile (lua_State *L, const char *findex) {
  LStream *p;
  lua_getfield(L, LUA_REGISTRYINDEX, findex);
  p = (LStream *)lua_touserdata(L, -1);
  if (isclosed(p))
    luaL_error(L, "standard %s file is closed", findex + IOPREF_LEN);
  return p->f;
}
static int g_iofile (lua_State *L, const char *f, const char *mode) {
  if (!lua_isnoneornil(L, 1)) {
    const char *filename = lua_tostring(L, 1);
    if (filename)
      opencheck(L, filename, mode);
    else {
      tofile(L);  
      lua_pushvalue(L, 1);
    }
    lua_setfield(L, LUA_REGISTRYINDEX, f);
  }
  lua_getfield(L, LUA_REGISTRYINDEX, f);
  return 1;
}
static int io_input (lua_State *L) {
  return g_iofile(L, IO_INPUT, "r");
}
static int io_output (lua_State *L) {
  return g_iofile(L, IO_OUTPUT, "w");
}
static int io_readline (lua_State *L);
#define MAXARGLINE	250
static void aux_lines (lua_State *L, int toclose) {
  int n = lua_gettop(L) - 1;  
  luaL_argcheck(L, n <= MAXARGLINE, MAXARGLINE + 2, "too many arguments");
  lua_pushinteger(L, n);  
  lua_pushboolean(L, toclose);  
  lua_rotate(L, 2, 2);  
  lua_pushcclosure(L, io_readline, 3 + n);
}
static int f_lines (lua_State *L) {
  tofile(L);  
  aux_lines(L, 0);
  return 1;
}
static int io_lines (lua_State *L) {
  int toclose;
  if (lua_isnone(L, 1)) lua_pushnil(L);  
  if (lua_isnil(L, 1)) {  
    lua_getfield(L, LUA_REGISTRYINDEX, IO_INPUT);  
    lua_replace(L, 1);  
    tofile(L);  
    toclose = 0;  
  }
  else {  
    const char *filename = luaL_checkstring(L, 1);
    opencheck(L, filename, "r");
    lua_replace(L, 1);  
    toclose = 1;  
  }
  aux_lines(L, toclose);
  return 1;
}
#if !defined (L_MAXLENNUM)
#define L_MAXLENNUM     200
#endif
typedef struct {
  FILE *f;  
  int c;  
  int n;  
  char buff[L_MAXLENNUM + 1];  
} RN;
static int nextc (RN *rn) {
  if (rn->n >= L_MAXLENNUM) {  
    rn->buff[0] = '\0';  
    return 0;  
  }
  else {
    rn->buff[rn->n++] = rn->c;  
    rn->c = l_getc(rn->f);  
    return 1;
  }
}
static int test2 (RN *rn, const char *set) {
  if (rn->c == set[0] || rn->c == set[1])
    return nextc(rn);
  else return 0;
}
static int readdigits (RN *rn, int hex) {
  int count = 0;
  while ((hex ? isxdigit(rn->c) : isdigit(rn->c)) && nextc(rn))
    count++;
  return count;
}
static int read_number (lua_State *L, FILE *f) {
  RN rn;
  int count = 0;
  int hex = 0;
  char decp[2];
  rn.f = f; rn.n = 0;
  decp[0] = lua_getlocaledecpoint();  
  decp[1] = '.';  
  l_lockfile(rn.f);
  do { rn.c = l_getc(rn.f); } while (isspace(rn.c));  
  test2(&rn, "-+");  
  if (test2(&rn, "00")) {
    if (test2(&rn, "xX")) hex = 1;  
    else count = 1;  
  }
  count += readdigits(&rn, hex);  
  if (test2(&rn, decp))  
    count += readdigits(&rn, hex);  
  if (count > 0 && test2(&rn, (hex ? "pP" : "eE"))) {  
    test2(&rn, "-+");  
    readdigits(&rn, 0);  
  }
  ungetc(rn.c, rn.f);  
  l_unlockfile(rn.f);
  rn.buff[rn.n] = '\0';  
  if (lua_stringtonumber(L, rn.buff))  
    return 1;  
  else {  
   lua_pushnil(L);  
   return 0;  
  }
}
static int test_eof (lua_State *L, FILE *f) {
  int c = getc(f);
  ungetc(c, f);  
  lua_pushliteral(L, "");
  return (c != EOF);
}
static int read_line (lua_State *L, FILE *f, int chop) {
  luaL_Buffer b;
  int c = '\0';
  luaL_buffinit(L, &b);
  while (c != EOF && c != '\n') {  
    char *buff = luaL_prepbuffer(&b);  
    int i = 0;
    l_lockfile(f);  
    while (i < LUAL_BUFFERSIZE && (c = l_getc(f)) != EOF && c != '\n')
      buff[i++] = c;
    l_unlockfile(f);
    luaL_addsize(&b, i);
  }
  if (!chop && c == '\n')  
    luaL_addchar(&b, c);  
  luaL_pushresult(&b);  
  return (c == '\n' || lua_rawlen(L, -1) > 0);
}
static void read_all (lua_State *L, FILE *f) {
  size_t nr;
  luaL_Buffer b;
  luaL_buffinit(L, &b);
  do {  
    char *p = luaL_prepbuffer(&b);
    nr = fread(p, sizeof(char), LUAL_BUFFERSIZE, f);
    luaL_addsize(&b, nr);
  } while (nr == LUAL_BUFFERSIZE);
  luaL_pushresult(&b);  
}
static int read_chars (lua_State *L, FILE *f, size_t n) {
  size_t nr;  
  char *p;
  luaL_Buffer b;
  luaL_buffinit(L, &b);
  p = luaL_prepbuffsize(&b, n);  
  nr = fread(p, sizeof(char), n, f);  
  luaL_addsize(&b, nr);
  luaL_pushresult(&b);  
  return (nr > 0);  
}
static int g_read (lua_State *L, FILE *f, int first) {
  int nargs = lua_gettop(L) - 1;
  int success;
  int n;
  clearerr(f);
  if (nargs == 0) {  
    success = read_line(L, f, 1);
    n = first+1;  
  }
  else {  
    luaL_checkstack(L, nargs+LUA_MINSTACK, "too many arguments");
    success = 1;
    for (n = first; nargs-- && success; n++) {
      if (lua_type(L, n) == LUA_TNUMBER) {
        size_t l = (size_t)luaL_checkinteger(L, n);
        success = (l == 0) ? test_eof(L, f) : read_chars(L, f, l);
      }
      else {
        const char *p = luaL_checkstring(L, n);
        if (*p == '*') p++;  
        switch (*p) {
          case 'n':  
            success = read_number(L, f);
            break;
          case 'l':  
            success = read_line(L, f, 1);
            break;
          case 'L':  
            success = read_line(L, f, 0);
            break;
          case 'a':  
            read_all(L, f);  
            success = 1; 
            break;
          default:
            return luaL_argerror(L, n, "invalid format");
        }
      }
    }
  }
  if (ferror(f))
    return luaL_fileresult(L, 0, NULL);
  if (!success) {
    lua_pop(L, 1);  
    lua_pushnil(L);  
  }
  return n - first;
}
static int io_read (lua_State *L) {
  return g_read(L, getiofile(L, IO_INPUT), 1);
}
static int f_read (lua_State *L) {
  return g_read(L, tofile(L), 2);
}
static int io_readline (lua_State *L) {
  LStream *p = (LStream *)lua_touserdata(L, lua_upvalueindex(1));
  int i;
  int n = (int)lua_tointeger(L, lua_upvalueindex(2));
  if (isclosed(p))  
    return luaL_error(L, "file is already closed");
  lua_settop(L , 1);
  luaL_checkstack(L, n, "too many arguments");
  for (i = 1; i <= n; i++)  
    lua_pushvalue(L, lua_upvalueindex(3 + i));
  n = g_read(L, p->f, 2);  
  lua_assert(n > 0);  
  if (lua_toboolean(L, -n))  
    return n;  
  else {  
    if (n > 1) {  
      return luaL_error(L, "%s", lua_tostring(L, -n + 1));
    }
    if (lua_toboolean(L, lua_upvalueindex(3))) {  
      lua_settop(L, 0);
      lua_pushvalue(L, lua_upvalueindex(1));
      aux_close(L);  
    }
    return 0;
  }
}
static int g_write (lua_State *L, FILE *f, int arg) {
  int nargs = lua_gettop(L) - arg;
  int status = 1;
  for (; nargs--; arg++) {
    if (lua_type(L, arg) == LUA_TNUMBER) {
      int len = lua_isinteger(L, arg)
                ? fprintf(f, LUA_INTEGER_FMT,
                             (LUAI_UACINT)lua_tointeger(L, arg))
                : fprintf(f, LUA_NUMBER_FMT,
                             (LUAI_UACNUMBER)lua_tonumber(L, arg));
      status = status && (len > 0);
    }
    else {
      size_t l;
      const char *s = luaL_checklstring(L, arg, &l);
      status = status && (fwrite(s, sizeof(char), l, f) == l);
    }
  }
  if (status) return 1;  
  else return luaL_fileresult(L, status, NULL);
}
static int io_write (lua_State *L) {
  return g_write(L, getiofile(L, IO_OUTPUT), 1);
}
static int f_write (lua_State *L) {
  FILE *f = tofile(L);
  lua_pushvalue(L, 1);  
  return g_write(L, f, 2);
}
static int f_seek (lua_State *L) {
  static const int mode[] = {SEEK_SET, SEEK_CUR, SEEK_END};
  static const char *const modenames[] = {"set", "cur", "end", NULL};
  FILE *f = tofile(L);
  int op = luaL_checkoption(L, 2, "cur", modenames);
  lua_Integer p3 = luaL_optinteger(L, 3, 0);
  l_seeknum offset = (l_seeknum)p3;
  luaL_argcheck(L, (lua_Integer)offset == p3, 3,
                  "not an integer in proper range");
  op = l_fseek(f, offset, mode[op]);
  if (op)
    return luaL_fileresult(L, 0, NULL);  
  else {
    lua_pushinteger(L, (lua_Integer)l_ftell(f));
    return 1;
  }
}
static int f_setvbuf (lua_State *L) {
  static const int mode[] = {_IONBF, _IOFBF, _IOLBF};
  static const char *const modenames[] = {"no", "full", "line", NULL};
  FILE *f = tofile(L);
  int op = luaL_checkoption(L, 2, NULL, modenames);
  lua_Integer sz = luaL_optinteger(L, 3, LUAL_BUFFERSIZE);
  int res = setvbuf(f, NULL, mode[op], (size_t)sz);
  return luaL_fileresult(L, res == 0, NULL);
}
static int io_flush (lua_State *L) {
  return luaL_fileresult(L, fflush(getiofile(L, IO_OUTPUT)) == 0, NULL);
}
static int f_flush (lua_State *L) {
  return luaL_fileresult(L, fflush(tofile(L)) == 0, NULL);
}
static const luaL_Reg iolib[] = {
  {"close", io_close},
  {"flush", io_flush},
  {"input", io_input},
  {"lines", io_lines},
  {"open", io_open},
  {"output", io_output},
  {"popen", io_popen},
  {"read", io_read},
  {"tmpfile", io_tmpfile},
  {"type", io_type},
  {"write", io_write},
  {NULL, NULL}
};
static const luaL_Reg flib[] = {
  {"close", f_close},
  {"flush", f_flush},
  {"lines", f_lines},
  {"read", f_read},
  {"seek", f_seek},
  {"setvbuf", f_setvbuf},
  {"write", f_write},
  {"__gc", f_gc},
  {"__tostring", f_tostring},
  {NULL, NULL}
};
static void createmeta (lua_State *L) {
  luaL_newmetatable(L, LUA_FILEHANDLE);  
  lua_pushvalue(L, -1);  
  lua_setfield(L, -2, "__index");  
  luaL_setfuncs(L, flib, 0);  
  lua_pop(L, 1);  
}
static int io_noclose (lua_State *L) {
  LStream *p = tolstream(L);
  p->closef = &io_noclose;  
  lua_pushnil(L);
  lua_pushliteral(L, "cannot close standard file");
  return 2;
}
static void createstdfile (lua_State *L, FILE *f, const char *k,
                           const char *fname) {
  LStream *p = newprefile(L);
  p->f = f;
  p->closef = &io_noclose;
  if (k != NULL) {
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, k);  
  }
  lua_setfield(L, -2, fname);  
}
LUAMOD_API int luaopen_io (lua_State *L) {
  luaL_newlib(L, iolib);  
  createmeta(L);
  createstdfile(L, stdin, IO_INPUT, "stdin");
  createstdfile(L, stdout, IO_OUTPUT, "stdout");
  createstdfile(L, stderr, NULL, "stderr");
  return 1;
}
#define lmathlib_c
#define LUA_LIB
#include <stdlib.h>
#include <math.h>
#undef PI
#define PI	(l_mathop(3.141592653589793238462643383279502884))
#if !defined(l_rand)		
#if defined(LUA_USE_POSIX)
#define l_rand()	random()
#define l_srand(x)	srandom(x)
#define L_RANDMAX	2147483647	
#else
#define l_rand()	rand()
#define l_srand(x)	srand(x)
#define L_RANDMAX	RAND_MAX
#endif
#endif				
static int math_abs (lua_State *L) {
  if (lua_isinteger(L, 1)) {
    lua_Integer n = lua_tointeger(L, 1);
    if (n < 0) n = (lua_Integer)(0u - (lua_Unsigned)n);
    lua_pushinteger(L, n);
  }
  else
    lua_pushnumber(L, l_mathop(fabs)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_sin (lua_State *L) {
  lua_pushnumber(L, l_mathop(sin)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_cos (lua_State *L) {
  lua_pushnumber(L, l_mathop(cos)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_tan (lua_State *L) {
  lua_pushnumber(L, l_mathop(tan)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_asin (lua_State *L) {
  lua_pushnumber(L, l_mathop(asin)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_acos (lua_State *L) {
  lua_pushnumber(L, l_mathop(acos)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_atan (lua_State *L) {
  lua_Number y = luaL_checknumber(L, 1);
  lua_Number x = luaL_optnumber(L, 2, 1);
  lua_pushnumber(L, l_mathop(atan2)(y, x));
  return 1;
}
static int math_toint (lua_State *L) {
  int valid;
  lua_Integer n = lua_tointegerx(L, 1, &valid);
  if (valid)
    lua_pushinteger(L, n);
  else {
    luaL_checkany(L, 1);
    lua_pushnil(L);  
  }
  return 1;
}
static void pushnumint (lua_State *L, lua_Number d) {
  lua_Integer n;
  if (lua_numbertointeger(d, &n))  
    lua_pushinteger(L, n);  
  else
    lua_pushnumber(L, d);  
}
static int math_floor (lua_State *L) {
  if (lua_isinteger(L, 1))
    lua_settop(L, 1);  
  else {
    lua_Number d = l_mathop(floor)(luaL_checknumber(L, 1));
    pushnumint(L, d);
  }
  return 1;
}
static int math_ceil (lua_State *L) {
  if (lua_isinteger(L, 1))
    lua_settop(L, 1);  
  else {
    lua_Number d = l_mathop(ceil)(luaL_checknumber(L, 1));
    pushnumint(L, d);
  }
  return 1;
}
static int math_fmod (lua_State *L) {
  if (lua_isinteger(L, 1) && lua_isinteger(L, 2)) {
    lua_Integer d = lua_tointeger(L, 2);
    if ((lua_Unsigned)d + 1u <= 1u) {  
      luaL_argcheck(L, d != 0, 2, "zero");
      lua_pushinteger(L, 0);  
    }
    else
      lua_pushinteger(L, lua_tointeger(L, 1) % d);
  }
  else
    lua_pushnumber(L, l_mathop(fmod)(luaL_checknumber(L, 1),
                                     luaL_checknumber(L, 2)));
  return 1;
}
static int math_modf (lua_State *L) {
  if (lua_isinteger(L ,1)) {
    lua_settop(L, 1);  
    lua_pushnumber(L, 0);  
  }
  else {
    lua_Number n = luaL_checknumber(L, 1);
    lua_Number ip = (n < 0) ? l_mathop(ceil)(n) : l_mathop(floor)(n);
    pushnumint(L, ip);
    lua_pushnumber(L, (n == ip) ? l_mathop(0.0) : (n - ip));
  }
  return 2;
}
static int math_sqrt (lua_State *L) {
  lua_pushnumber(L, l_mathop(sqrt)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_ult (lua_State *L) {
  lua_Integer a = luaL_checkinteger(L, 1);
  lua_Integer b = luaL_checkinteger(L, 2);
  lua_pushboolean(L, (lua_Unsigned)a < (lua_Unsigned)b);
  return 1;
}
static int math_log (lua_State *L) {
  lua_Number x = luaL_checknumber(L, 1);
  lua_Number res;
  if (lua_isnoneornil(L, 2))
    res = l_mathop(log)(x);
  else {
    lua_Number base = luaL_checknumber(L, 2);
#if !defined(LUA_USE_C89)
    if (base == l_mathop(2.0))
      res = l_mathop(log2)(x); else
#endif
    if (base == l_mathop(10.0))
      res = l_mathop(log10)(x);
    else
      res = l_mathop(log)(x)/l_mathop(log)(base);
  }
  lua_pushnumber(L, res);
  return 1;
}
static int math_exp (lua_State *L) {
  lua_pushnumber(L, l_mathop(exp)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_deg (lua_State *L) {
  lua_pushnumber(L, luaL_checknumber(L, 1) * (l_mathop(180.0) / PI));
  return 1;
}
static int math_rad (lua_State *L) {
  lua_pushnumber(L, luaL_checknumber(L, 1) * (PI / l_mathop(180.0)));
  return 1;
}
static int math_min (lua_State *L) {
  int n = lua_gettop(L);  
  int imin = 1;  
  int i;
  luaL_argcheck(L, n >= 1, 1, "value expected");
  for (i = 2; i <= n; i++) {
    if (lua_compare(L, i, imin, LUA_OPLT))
      imin = i;
  }
  lua_pushvalue(L, imin);
  return 1;
}
static int math_max (lua_State *L) {
  int n = lua_gettop(L);  
  int imax = 1;  
  int i;
  luaL_argcheck(L, n >= 1, 1, "value expected");
  for (i = 2; i <= n; i++) {
    if (lua_compare(L, imax, i, LUA_OPLT))
      imax = i;
  }
  lua_pushvalue(L, imax);
  return 1;
}
static int math_random (lua_State *L) {
  lua_Integer low, up;
  double r = (double)l_rand() * (1.0 / ((double)L_RANDMAX + 1.0));
  switch (lua_gettop(L)) {  
    case 0: {  
      lua_pushnumber(L, (lua_Number)r);  
      return 1;
    }
    case 1: {  
      low = 1;
      up = luaL_checkinteger(L, 1);
      break;
    }
    case 2: {  
      low = luaL_checkinteger(L, 1);
      up = luaL_checkinteger(L, 2);
      break;
    }
    default: return luaL_error(L, "wrong number of arguments");
  }
  luaL_argcheck(L, low <= up, 1, "interval is empty");
  luaL_argcheck(L, low >= 0 || up <= LUA_MAXINTEGER + low, 1,
                   "interval too large");
  r *= (double)(up - low) + 1.0;
  lua_pushinteger(L, (lua_Integer)r + low);
  return 1;
}
static int math_randomseed (lua_State *L) {
  l_srand((unsigned int)(lua_Integer)luaL_checknumber(L, 1));
  (void)l_rand(); 
  return 0;
}
static int math_type (lua_State *L) {
  if (lua_type(L, 1) == LUA_TNUMBER) {
      if (lua_isinteger(L, 1))
        lua_pushliteral(L, "integer");
      else
        lua_pushliteral(L, "float");
  }
  else {
    luaL_checkany(L, 1);
    lua_pushnil(L);
  }
  return 1;
}
#if defined(LUA_COMPAT_MATHLIB)
static int math_cosh (lua_State *L) {
  lua_pushnumber(L, l_mathop(cosh)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_sinh (lua_State *L) {
  lua_pushnumber(L, l_mathop(sinh)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_tanh (lua_State *L) {
  lua_pushnumber(L, l_mathop(tanh)(luaL_checknumber(L, 1)));
  return 1;
}
static int math_pow (lua_State *L) {
  lua_Number x = luaL_checknumber(L, 1);
  lua_Number y = luaL_checknumber(L, 2);
  lua_pushnumber(L, l_mathop(pow)(x, y));
  return 1;
}
static int math_frexp (lua_State *L) {
  int e;
  lua_pushnumber(L, l_mathop(frexp)(luaL_checknumber(L, 1), &e));
  lua_pushinteger(L, e);
  return 2;
}
static int math_ldexp (lua_State *L) {
  lua_Number x = luaL_checknumber(L, 1);
  int ep = (int)luaL_checkinteger(L, 2);
  lua_pushnumber(L, l_mathop(ldexp)(x, ep));
  return 1;
}
static int math_log10 (lua_State *L) {
  lua_pushnumber(L, l_mathop(log10)(luaL_checknumber(L, 1)));
  return 1;
}
#endif
static const luaL_Reg mathlib[] = {
  {"abs",   math_abs},
  {"acos",  math_acos},
  {"asin",  math_asin},
  {"atan",  math_atan},
  {"ceil",  math_ceil},
  {"cos",   math_cos},
  {"deg",   math_deg},
  {"exp",   math_exp},
  {"tointeger", math_toint},
  {"floor", math_floor},
  {"fmod",   math_fmod},
  {"ult",   math_ult},
  {"log",   math_log},
  {"max",   math_max},
  {"min",   math_min},
  {"modf",   math_modf},
  {"rad",   math_rad},
  {"random",     math_random},
  {"randomseed", math_randomseed},
  {"sin",   math_sin},
  {"sqrt",  math_sqrt},
  {"tan",   math_tan},
  {"type", math_type},
#if defined(LUA_COMPAT_MATHLIB)
  {"atan2", math_atan},
  {"cosh",   math_cosh},
  {"sinh",   math_sinh},
  {"tanh",   math_tanh},
  {"pow",   math_pow},
  {"frexp", math_frexp},
  {"ldexp", math_ldexp},
  {"log10", math_log10},
#endif
  {"pi", NULL},
  {"huge", NULL},
  {"maxinteger", NULL},
  {"mininteger", NULL},
  {NULL, NULL}
};
LUAMOD_API int luaopen_math (lua_State *L) {
  luaL_newlib(L, mathlib);
  lua_pushnumber(L, PI);
  lua_setfield(L, -2, "pi");
  lua_pushnumber(L, (lua_Number)HUGE_VAL);
  lua_setfield(L, -2, "huge");
  lua_pushinteger(L, LUA_MAXINTEGER);
  lua_setfield(L, -2, "maxinteger");
  lua_pushinteger(L, LUA_MININTEGER);
  lua_setfield(L, -2, "mininteger");
  return 1;
}
#define loadlib_c
#define LUA_LIB
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if !defined (LUA_IGMARK)
#define LUA_IGMARK		"-"
#endif
#if !defined(LUA_CSUBSEP)
#define LUA_CSUBSEP		LUA_DIRSEP
#endif
#if !defined(LUA_LSUBSEP)
#define LUA_LSUBSEP		LUA_DIRSEP
#endif
#define LUA_POF		"luaopen_"
#define LUA_OFSEP	"_"
static const int CLIBS = 0;
#define LIB_FAIL	"open"
#define setprogdir(L)           ((void)0)
static void lsys_unloadlib (void *lib);
static void *lsys_load (lua_State *L, const char *path, int seeglb);
static lua_CFunction lsys_sym (lua_State *L, void *lib, const char *sym);
#if defined(LUA_USE_DLOPEN)	
#include <dlfcn.h>
#if defined(__GNUC__)
#define cast_func(p) (__extension__ (lua_CFunction)(p))
#else
#define cast_func(p) ((lua_CFunction)(p))
#endif
static void lsys_unloadlib (void *lib) {
  dlclose(lib);
}
static void *lsys_load (lua_State *L, const char *path, int seeglb) {
  void *lib = dlopen(path, RTLD_NOW | (seeglb ? RTLD_GLOBAL : RTLD_LOCAL));
  if (lib == NULL) lua_pushstring(L, dlerror());
  return lib;
}
static lua_CFunction lsys_sym (lua_State *L, void *lib, const char *sym) {
  lua_CFunction f = cast_func(dlsym(lib, sym));
  if (f == NULL) lua_pushstring(L, dlerror());
  return f;
}
#elif defined(LUA_DL_DLL)	
#include <windows.h>
#if !defined(LUA_LLE_FLAGS)
#define LUA_LLE_FLAGS	0
#endif
#undef setprogdir
static void setprogdir (lua_State *L) {
  char buff[MAX_PATH + 1];
  char *lb;
  DWORD nsize = sizeof(buff)/sizeof(char);
  DWORD n = GetModuleFileNameA(NULL, buff, nsize);  
  if (n == 0 || n == nsize || (lb = strrchr(buff, '\\')) == NULL)
    luaL_error(L, "unable to get ModuleFileName");
  else {
    *lb = '\0';  
    luaL_gsub(L, lua_tostring(L, -1), LUA_EXEC_DIR, buff);
    lua_remove(L, -2);  
  }
}
static void pusherror (lua_State *L) {
  int error = GetLastError();
  char buffer[128];
  if (FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM,
      NULL, error, 0, buffer, sizeof(buffer)/sizeof(char), NULL))
    lua_pushstring(L, buffer);
  else
    lua_pushfstring(L, "system error %d\n", error);
}
static void lsys_unloadlib (void *lib) {
  FreeLibrary((HMODULE)lib);
}
static void *lsys_load (lua_State *L, const char *path, int seeglb) {
  HMODULE lib = LoadLibraryExA(path, NULL, LUA_LLE_FLAGS);
  (void)(seeglb);  
  if (lib == NULL) pusherror(L);
  return lib;
}
static lua_CFunction lsys_sym (lua_State *L, void *lib, const char *sym) {
  lua_CFunction f = (lua_CFunction)GetProcAddress((HMODULE)lib, sym);
  if (f == NULL) pusherror(L);
  return f;
}
#else				
#undef LIB_FAIL
#define LIB_FAIL	"absent"
#define DLMSG	"dynamic libraries not enabled; check your Lua installation"
static void lsys_unloadlib (void *lib) {
  (void)(lib);  
}
static void *lsys_load (lua_State *L, const char *path, int seeglb) {
  (void)(path); (void)(seeglb);  
  lua_pushliteral(L, DLMSG);
  return NULL;
}
static lua_CFunction lsys_sym (lua_State *L, void *lib, const char *sym) {
  (void)(lib); (void)(sym);  
  lua_pushliteral(L, DLMSG);
  return NULL;
}
#endif				
#if !defined(LUA_PATH_VAR)
#define LUA_PATH_VAR    "LUA_PATH"
#endif
#if !defined(LUA_CPATH_VAR)
#define LUA_CPATH_VAR   "LUA_CPATH"
#endif
#define AUXMARK         "\1"	
static int noenv (lua_State *L) {
  int b;
  lua_getfield(L, LUA_REGISTRYINDEX, "LUA_NOENV");
  b = lua_toboolean(L, -1);
  lua_pop(L, 1);  
  return b;
}
static void setpath (lua_State *L, const char *fieldname,
                                   const char *envname,
                                   const char *dft) {
  const char *nver = lua_pushfstring(L, "%s%s", envname, LUA_VERSUFFIX);
  const char *path = getenv(nver);  
  if (path == NULL)  
    path = getenv(envname);  
  if (path == NULL || noenv(L))  
    lua_pushstring(L, dft);  
  else {
    path = luaL_gsub(L, path, LUA_PATH_SEP LUA_PATH_SEP,
                              LUA_PATH_SEP AUXMARK LUA_PATH_SEP);
    luaL_gsub(L, path, AUXMARK, dft);
    lua_remove(L, -2); 
  }
  setprogdir(L);
  lua_setfield(L, -3, fieldname);  
  lua_pop(L, 1);  
}
static void *checkclib (lua_State *L, const char *path) {
  void *plib;
  lua_rawgetp(L, LUA_REGISTRYINDEX, &CLIBS);
  lua_getfield(L, -1, path);
  plib = lua_touserdata(L, -1);  
  lua_pop(L, 2);  
  return plib;
}
static void addtoclib (lua_State *L, const char *path, void *plib) {
  lua_rawgetp(L, LUA_REGISTRYINDEX, &CLIBS);
  lua_pushlightuserdata(L, plib);
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, path);  
  lua_rawseti(L, -2, luaL_len(L, -2) + 1);  
  lua_pop(L, 1);  
}
static int gctm (lua_State *L) {
  lua_Integer n = luaL_len(L, 1);
  for (; n >= 1; n--) {  
    lua_rawgeti(L, 1, n);  
    lsys_unloadlib(lua_touserdata(L, -1));
    lua_pop(L, 1);  
  }
  return 0;
}
#define ERRLIB		1
#define ERRFUNC		2
static int lookforfunc (lua_State *L, const char *path, const char *sym) {
  void *reg = checkclib(L, path);  
  if (reg == NULL) {  
    reg = lsys_load(L, path, *sym == '*');  
    if (reg == NULL) return ERRLIB;  
    addtoclib(L, path, reg);
  }
  if (*sym == '*') {  
    lua_pushboolean(L, 1);  
    return 0;  
  }
  else {
    lua_CFunction f = lsys_sym(L, reg, sym);
    if (f == NULL)
      return ERRFUNC;  
    lua_pushcfunction(L, f);  
    return 0;  
  }
}
static int ll_loadlib (lua_State *L) {
  const char *path = luaL_checkstring(L, 1);
  const char *init = luaL_checkstring(L, 2);
  int stat = lookforfunc(L, path, init);
  if (stat == 0)  
    return 1;  
  else {  
    lua_pushnil(L);
    lua_insert(L, -2);
    lua_pushstring(L, (stat == ERRLIB) ?  LIB_FAIL : "init");
    return 3;  
  }
}
static int readable (const char *filename) {
  FILE *f = fopen(filename, "r");  
  if (f == NULL) return 0;  
  fclose(f);
  return 1;
}
static const char *pushnexttemplate (lua_State *L, const char *path) {
  const char *l;
  while (*path == *LUA_PATH_SEP) path++;  
  if (*path == '\0') return NULL;  
  l = strchr(path, *LUA_PATH_SEP);  
  if (l == NULL) l = path + strlen(path);
  lua_pushlstring(L, path, l - path);  
  return l;
}
static const char *searchpath (lua_State *L, const char *name,
                                             const char *path,
                                             const char *sep,
                                             const char *dirsep) {
  luaL_Buffer msg;  
  luaL_buffinit(L, &msg);
  if (*sep != '\0')  
    name = luaL_gsub(L, name, sep, dirsep);  
  while ((path = pushnexttemplate(L, path)) != NULL) {
    const char *filename = luaL_gsub(L, lua_tostring(L, -1),
                                     LUA_PATH_MARK, name);
    lua_remove(L, -2);  
    if (readable(filename))  
      return filename;  
    lua_pushfstring(L, "\n\tno file '%s'", filename);
    lua_remove(L, -2);  
    luaL_addvalue(&msg);  
  }
  luaL_pushresult(&msg);  
  return NULL;  
}
static int ll_searchpath (lua_State *L) {
  const char *f = searchpath(L, luaL_checkstring(L, 1),
                                luaL_checkstring(L, 2),
                                luaL_optstring(L, 3, "."),
                                luaL_optstring(L, 4, LUA_DIRSEP));
  if (f != NULL) return 1;
  else {  
    lua_pushnil(L);
    lua_insert(L, -2);
    return 2;  
  }
}
static const char *findfile (lua_State *L, const char *name,
                                           const char *pname,
                                           const char *dirsep) {
  const char *path;
  lua_getfield(L, lua_upvalueindex(1), pname);
  path = lua_tostring(L, -1);
  if (path == NULL)
    luaL_error(L, "'package.%s' must be a string", pname);
  return searchpath(L, name, path, ".", dirsep);
}
static int checkload (lua_State *L, int stat, const char *filename) {
  if (stat) {  
    lua_pushstring(L, filename);  
    return 2;  
  }
  else
    return luaL_error(L, "error loading module '%s' from file '%s':\n\t%s",
                          lua_tostring(L, 1), filename, lua_tostring(L, -1));
}
static int searcher_Lua (lua_State *L) {
  const char *filename;
  const char *name = luaL_checkstring(L, 1);
  filename = findfile(L, name, "path", LUA_LSUBSEP);
  if (filename == NULL) return 1;  
  return checkload(L, (luaL_loadfile(L, filename) == LUA_OK), filename);
}
static int loadfunc (lua_State *L, const char *filename, const char *modname) {
  const char *openfunc;
  const char *mark;
  modname = luaL_gsub(L, modname, ".", LUA_OFSEP);
  mark = strchr(modname, *LUA_IGMARK);
  if (mark) {
    int stat;
    openfunc = lua_pushlstring(L, modname, mark - modname);
    openfunc = lua_pushfstring(L, LUA_POF"%s", openfunc);
    stat = lookforfunc(L, filename, openfunc);
    if (stat != ERRFUNC) return stat;
    modname = mark + 1;  
  }
  openfunc = lua_pushfstring(L, LUA_POF"%s", modname);
  return lookforfunc(L, filename, openfunc);
}
static int searcher_C (lua_State *L) {
  const char *name = luaL_checkstring(L, 1);
  const char *filename = findfile(L, name, "cpath", LUA_CSUBSEP);
  if (filename == NULL) return 1;  
  return checkload(L, (loadfunc(L, filename, name) == 0), filename);
}
static int searcher_Croot (lua_State *L) {
  const char *filename;
  const char *name = luaL_checkstring(L, 1);
  const char *p = strchr(name, '.');
  int stat;
  if (p == NULL) return 0;  
  lua_pushlstring(L, name, p - name);
  filename = findfile(L, lua_tostring(L, -1), "cpath", LUA_CSUBSEP);
  if (filename == NULL) return 1;  
  if ((stat = loadfunc(L, filename, name)) != 0) {
    if (stat != ERRFUNC)
      return checkload(L, 0, filename);  
    else {  
      lua_pushfstring(L, "\n\tno module '%s' in file '%s'", name, filename);
      return 1;
    }
  }
  lua_pushstring(L, filename);  
  return 2;
}
static int searcher_preload (lua_State *L) {
  const char *name = luaL_checkstring(L, 1);
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_PRELOAD_TABLE);
  if (lua_getfield(L, -1, name) == LUA_TNIL)  
    lua_pushfstring(L, "\n\tno field package.preload['%s']", name);
  return 1;
}
static void findloader (lua_State *L, const char *name) {
  int i;
  luaL_Buffer msg;  
  luaL_buffinit(L, &msg);
  if (lua_getfield(L, lua_upvalueindex(1), "searchers") != LUA_TTABLE)
    luaL_error(L, "'package.searchers' must be a table");
  for (i = 1; ; i++) {
    if (lua_rawgeti(L, 3, i) == LUA_TNIL) {  
      lua_pop(L, 1);  
      luaL_pushresult(&msg);  
      luaL_error(L, "module '%s' not found:%s", name, lua_tostring(L, -1));
    }
    lua_pushstring(L, name);
    lua_call(L, 1, 2);  
    if (lua_isfunction(L, -2))  
      return;  
    else if (lua_isstring(L, -2)) {  
      lua_pop(L, 1);  
      luaL_addvalue(&msg);  
    }
    else
      lua_pop(L, 2);  
  }
}
static int ll_require (lua_State *L) {
  const char *name = luaL_checkstring(L, 1);
  lua_settop(L, 1);  
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_LOADED_TABLE);
  lua_getfield(L, 2, name);  
  if (lua_toboolean(L, -1))  
    return 1;  
  lua_pop(L, 1);  
  findloader(L, name);
  lua_pushstring(L, name);  
  lua_insert(L, -2);  
  lua_call(L, 2, 1);  
  if (!lua_isnil(L, -1))  
    lua_setfield(L, 2, name);  
  if (lua_getfield(L, 2, name) == LUA_TNIL) {   
    lua_pushboolean(L, 1);  
    lua_pushvalue(L, -1);  
    lua_setfield(L, 2, name);  
  }
  return 1;
}
#if defined(LUA_COMPAT_MODULE)
static void set_env (lua_State *L) {
  lua_Debug ar;
  if (lua_getstack(L, 1, &ar) == 0 ||
      lua_getinfo(L, "f", &ar) == 0 ||  
      lua_iscfunction(L, -1))
    luaL_error(L, "'module' not called from a Lua function");
  lua_pushvalue(L, -2);  
  lua_setupvalue(L, -2, 1);
  lua_pop(L, 1);  
}
static void dooptions (lua_State *L, int n) {
  int i;
  for (i = 2; i <= n; i++) {
    if (lua_isfunction(L, i)) {  
      lua_pushvalue(L, i);  
      lua_pushvalue(L, -2);  
      lua_call(L, 1, 0);
    }
  }
}
static void modinit (lua_State *L, const char *modname) {
  const char *dot;
  lua_pushvalue(L, -1);
  lua_setfield(L, -2, "_M");  
  lua_pushstring(L, modname);
  lua_setfield(L, -2, "_NAME");
  dot = strrchr(modname, '.');  
  if (dot == NULL) dot = modname;
  else dot++;
  lua_pushlstring(L, modname, dot - modname);
  lua_setfield(L, -2, "_PACKAGE");
}
static int ll_module (lua_State *L) {
  const char *modname = luaL_checkstring(L, 1);
  int lastarg = lua_gettop(L);  
  luaL_pushmodule(L, modname, 1);  
  if (lua_getfield(L, -1, "_NAME") != LUA_TNIL)
    lua_pop(L, 1);  
  else {  
    lua_pop(L, 1);
    modinit(L, modname);
  }
  lua_pushvalue(L, -1);
  set_env(L);
  dooptions(L, lastarg);
  return 1;
}
static int ll_seeall (lua_State *L) {
  luaL_checktype(L, 1, LUA_TTABLE);
  if (!lua_getmetatable(L, 1)) {
    lua_createtable(L, 0, 1); 
    lua_pushvalue(L, -1);
    lua_setmetatable(L, 1);
  }
  lua_pushglobaltable(L);
  lua_setfield(L, -2, "__index");  
  return 0;
}
#endif
static const luaL_Reg pk_funcs[] = {
  {"loadlib", ll_loadlib},
  {"searchpath", ll_searchpath},
#if defined(LUA_COMPAT_MODULE)
  {"seeall", ll_seeall},
#endif
  {"preload", NULL},
  {"cpath", NULL},
  {"path", NULL},
  {"searchers", NULL},
  {"loaded", NULL},
  {NULL, NULL}
};
static const luaL_Reg ll_funcs[] = {
#if defined(LUA_COMPAT_MODULE)
  {"module", ll_module},
#endif
  {"require", ll_require},
  {NULL, NULL}
};
static void createsearcherstable (lua_State *L) {
  static const lua_CFunction searchers[] =
    {searcher_preload, searcher_Lua, searcher_C, searcher_Croot, NULL};
  int i;
  lua_createtable(L, sizeof(searchers)/sizeof(searchers[0]) - 1, 0);
  for (i=0; searchers[i] != NULL; i++) {
    lua_pushvalue(L, -2);  
    lua_pushcclosure(L, searchers[i], 1);
    lua_rawseti(L, -2, i+1);
  }
#if defined(LUA_COMPAT_LOADERS)
  lua_pushvalue(L, -1);  
  lua_setfield(L, -3, "loaders");  
#endif
  lua_setfield(L, -2, "searchers");  
}
static void createclibstable (lua_State *L) {
  lua_newtable(L);  
  lua_createtable(L, 0, 1);  
  lua_pushcfunction(L, gctm);
  lua_setfield(L, -2, "__gc");  
  lua_setmetatable(L, -2);
  lua_rawsetp(L, LUA_REGISTRYINDEX, &CLIBS);  
}
LUAMOD_API int luaopen_package (lua_State *L) {
  createclibstable(L);
  luaL_newlib(L, pk_funcs);  
  createsearcherstable(L);
  setpath(L, "path", LUA_PATH_VAR, LUA_PATH_DEFAULT);
  setpath(L, "cpath", LUA_CPATH_VAR, LUA_CPATH_DEFAULT);
  lua_pushliteral(L, LUA_DIRSEP "\n" LUA_PATH_SEP "\n" LUA_PATH_MARK "\n"
                     LUA_EXEC_DIR "\n" LUA_IGMARK "\n");
  lua_setfield(L, -2, "config");
  luaL_getsubtable(L, LUA_REGISTRYINDEX, LUA_LOADED_TABLE);
  lua_setfield(L, -2, "loaded");
  luaL_getsubtable(L, LUA_REGISTRYINDEX, LUA_PRELOAD_TABLE);
  lua_setfield(L, -2, "preload");
  lua_pushglobaltable(L);
  lua_pushvalue(L, -2);  
  luaL_setfuncs(L, ll_funcs, 1);  
  lua_pop(L, 1);  
  return 1;  
}
#define loslib_c
#define LUA_LIB
#include <errno.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#if !defined(LUA_STRFTIMEOPTIONS)	
#define L_STRFTIMEC89		"aAbBcdHIjmMpSUwWxXyYZ%"
#define L_STRFTIMEC99 "aAbBcCdDeFgGhHIjmMnprRStTuUVwWxXyYzZ%" \
    "||" "EcECExEXEyEY" "OdOeOHOIOmOMOSOuOUOVOwOWOy"  
#define L_STRFTIMEWIN "aAbBcdHIjmMpSUwWxXyYzZ%" \
    "||" "#c#x#d#H#I#j#m#M#S#U#w#W#y#Y"  
#if defined(LUA_USE_WINDOWS)
#define LUA_STRFTIMEOPTIONS	L_STRFTIMEWIN
#elif defined(LUA_USE_C89)
#define LUA_STRFTIMEOPTIONS	L_STRFTIMEC89
#else  
#define LUA_STRFTIMEOPTIONS	L_STRFTIMEC99
#endif
#endif					
#if !defined(l_time_t)		
#define l_timet			lua_Integer
#define l_pushtime(L,t)		lua_pushinteger(L,(lua_Integer)(t))
static time_t l_checktime (lua_State *L, int arg) {
  lua_Integer t = luaL_checkinteger(L, arg);
  luaL_argcheck(L, (time_t)t == t, arg, "time out-of-bounds");
  return (time_t)t;
}
#endif				
#if !defined(l_gmtime)		
#if defined(LUA_USE_POSIX)	
#define l_gmtime(t,r)		gmtime_r(t,r)
#define l_localtime(t,r)	localtime_r(t,r)
#else				
#define l_gmtime(t,r)		((void)(r)->tm_sec, gmtime(t))
#define l_localtime(t,r)  	((void)(r)->tm_sec, localtime(t))
#endif				
#endif				
#if !defined(lua_tmpnam)	
#if defined(LUA_USE_POSIX)	
#include <unistd.h>
#define LUA_TMPNAMBUFSIZE	32
#if !defined(LUA_TMPNAMTEMPLATE)
#define LUA_TMPNAMTEMPLATE	"/tmp/lua_XXXXXX"
#endif
#define lua_tmpnam(b,e) { \
        strcpy(b, LUA_TMPNAMTEMPLATE); \
        e = mkstemp(b); \
        if (e != -1) close(e); \
        e = (e == -1); }
#else				
#define LUA_TMPNAMBUFSIZE	L_tmpnam
#define lua_tmpnam(b,e)		{ e = (tmpnam(b) == NULL); }
#endif				
#endif				
static int os_execute (lua_State *L) {
  const char *cmd = luaL_optstring(L, 1, NULL);
  int stat = system(cmd);
  if (cmd != NULL)
    return luaL_execresult(L, stat);
  else {
    lua_pushboolean(L, stat);  
    return 1;
  }
}
static int os_remove (lua_State *L) {
  const char *filename = luaL_checkstring(L, 1);
  return luaL_fileresult(L, remove(filename) == 0, filename);
}
static int os_rename (lua_State *L) {
  const char *fromname = luaL_checkstring(L, 1);
  const char *toname = luaL_checkstring(L, 2);
  return luaL_fileresult(L, rename(fromname, toname) == 0, NULL);
}
static int os_tmpname (lua_State *L) {
  char buff[LUA_TMPNAMBUFSIZE];
  int err;
  lua_tmpnam(buff, err);
  if (err)
    return luaL_error(L, "unable to generate a unique filename");
  lua_pushstring(L, buff);
  return 1;
}
static int os_getenv (lua_State *L) {
  lua_pushstring(L, getenv(luaL_checkstring(L, 1)));  
  return 1;
}
static int os_clock (lua_State *L) {
  lua_pushnumber(L, ((lua_Number)clock())/(lua_Number)CLOCKS_PER_SEC);
  return 1;
}
static void setfield (lua_State *L, const char *key, int value) {
  lua_pushinteger(L, value);
  lua_setfield(L, -2, key);
}
static void setboolfield (lua_State *L, const char *key, int value) {
  if (value < 0)  
    return;  
  lua_pushboolean(L, value);
  lua_setfield(L, -2, key);
}
static void setallfields (lua_State *L, struct tm *stm) {
  setfield(L, "sec", stm->tm_sec);
  setfield(L, "min", stm->tm_min);
  setfield(L, "hour", stm->tm_hour);
  setfield(L, "day", stm->tm_mday);
  setfield(L, "month", stm->tm_mon + 1);
  setfield(L, "year", stm->tm_year + 1900);
  setfield(L, "wday", stm->tm_wday + 1);
  setfield(L, "yday", stm->tm_yday + 1);
  setboolfield(L, "isdst", stm->tm_isdst);
}
static int getboolfield (lua_State *L, const char *key) {
  int res;
  res = (lua_getfield(L, -1, key) == LUA_TNIL) ? -1 : lua_toboolean(L, -1);
  lua_pop(L, 1);
  return res;
}
#if !defined(L_MAXDATEFIELD)
#define L_MAXDATEFIELD	(INT_MAX / 2)
#endif
static int getfield (lua_State *L, const char *key, int d, int delta) {
  int isnum;
  int t = lua_getfield(L, -1, key);  
  lua_Integer res = lua_tointegerx(L, -1, &isnum);
  if (!isnum) {  
    if (t != LUA_TNIL)  
      return luaL_error(L, "field '%s' is not an integer", key);
    else if (d < 0)  
      return luaL_error(L, "field '%s' missing in date table", key);
    res = d;
  }
  else {
    if (!(-L_MAXDATEFIELD <= res && res <= L_MAXDATEFIELD))
      return luaL_error(L, "field '%s' is out-of-bound", key);
    res -= delta;
  }
  lua_pop(L, 1);
  return (int)res;
}
static const char *checkoption (lua_State *L, const char *conv,
                                ptrdiff_t convlen, char *buff) {
  const char *option = LUA_STRFTIMEOPTIONS;
  int oplen = 1;  
  for (; *option != '\0' && oplen <= convlen; option += oplen) {
    if (*option == '|')  
      oplen++;  
    else if (memcmp(conv, option, oplen) == 0) {  
      memcpy(buff, conv, oplen);  
      buff[oplen] = '\0';
      return conv + oplen;  
    }
  }
  luaL_argerror(L, 1,
    lua_pushfstring(L, "invalid conversion specifier '%%%s'", conv));
  return conv;  
}
#define SIZETIMEFMT	250
static int os_date (lua_State *L) {
  size_t slen;
  const char *s = luaL_optlstring(L, 1, "%c", &slen);
  time_t t = luaL_opt(L, l_checktime, 2, time(NULL));
  const char *se = s + slen;  
  struct tm tmr, *stm;
  if (*s == '!') {  
    stm = l_gmtime(&t, &tmr);
    s++;  
  }
  else
    stm = l_localtime(&t, &tmr);
  if (stm == NULL)  
    return luaL_error(L,
                 "time result cannot be represented in this installation");
  if (strcmp(s, "*t") == 0) {
    lua_createtable(L, 0, 9);  
    setallfields(L, stm);
  }
  else {
    char cc[4];  
    luaL_Buffer b;
    cc[0] = '%';
    luaL_buffinit(L, &b);
    while (s < se) {
      if (*s != '%')  
        luaL_addchar(&b, *s++);
      else {
        size_t reslen;
        char *buff = luaL_prepbuffsize(&b, SIZETIMEFMT);
        s++;  
        s = checkoption(L, s, se - s, cc + 1);  
        reslen = strftime(buff, SIZETIMEFMT, cc, stm);
        luaL_addsize(&b, reslen);
      }
    }
    luaL_pushresult(&b);
  }
  return 1;
}
static int os_time (lua_State *L) {
  time_t t;
  if (lua_isnoneornil(L, 1))  
    t = time(NULL);  
  else {
    struct tm ts;
    luaL_checktype(L, 1, LUA_TTABLE);
    lua_settop(L, 1);  
    ts.tm_sec = getfield(L, "sec", 0, 0);
    ts.tm_min = getfield(L, "min", 0, 0);
    ts.tm_hour = getfield(L, "hour", 12, 0);
    ts.tm_mday = getfield(L, "day", -1, 0);
    ts.tm_mon = getfield(L, "month", -1, 1);
    ts.tm_year = getfield(L, "year", -1, 1900);
    ts.tm_isdst = getboolfield(L, "isdst");
    t = mktime(&ts);
    setallfields(L, &ts);  
  }
  if (t != (time_t)(l_timet)t || t == (time_t)(-1))
    return luaL_error(L,
                  "time result cannot be represented in this installation");
  l_pushtime(L, t);
  return 1;
}
static int os_difftime (lua_State *L) {
  time_t t1 = l_checktime(L, 1);
  time_t t2 = l_checktime(L, 2);
  lua_pushnumber(L, (lua_Number)difftime(t1, t2));
  return 1;
}
static int os_setlocale (lua_State *L) {
  static const int cat[] = {LC_ALL, LC_COLLATE, LC_CTYPE, LC_MONETARY,
                      LC_NUMERIC, LC_TIME};
  static const char *const catnames[] = {"all", "collate", "ctype", "monetary",
     "numeric", "time", NULL};
  const char *l = luaL_optstring(L, 1, NULL);
  int op = luaL_checkoption(L, 2, "all", catnames);
  lua_pushstring(L, setlocale(cat[op], l));
  return 1;
}
static int os_exit (lua_State *L) {
  int status;
  if (lua_isboolean(L, 1))
    status = (lua_toboolean(L, 1) ? EXIT_SUCCESS : EXIT_FAILURE);
  else
    status = (int)luaL_optinteger(L, 1, EXIT_SUCCESS);
  if (lua_toboolean(L, 2))
    lua_close(L);
  if (L) exit(status);  
  return 0;
}
static const luaL_Reg syslib[] = {
  {"clock",     os_clock},
  {"date",      os_date},
  {"difftime",  os_difftime},
  {"execute",   os_execute},
  {"exit",      os_exit},
  {"getenv",    os_getenv},
  {"remove",    os_remove},
  {"rename",    os_rename},
  {"setlocale", os_setlocale},
  {"time",      os_time},
  {"tmpname",   os_tmpname},
  {NULL, NULL}
};
LUAMOD_API int luaopen_os (lua_State *L) {
  luaL_newlib(L, syslib);
  return 1;
}
#define lstrlib_c
#define LUA_LIB
#include <ctype.h>
#include <float.h>
#include <limits.h>
#include <locale.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if !defined(LUA_MAXCAPTURES)
#define LUA_MAXCAPTURES		32
#endif
#define uchar(c)	((unsigned char)(c))
#define MAX_SIZET	((size_t)(~(size_t)0))
#define MAXSIZE  \
	(sizeof(size_t) < sizeof(int) ? MAX_SIZET : (size_t)(INT_MAX))
static int str_len (lua_State *L) {
  size_t l;
  luaL_checklstring(L, 1, &l);
  lua_pushinteger(L, (lua_Integer)l);
  return 1;
}
static lua_Integer posrelat (lua_Integer pos, size_t len) {
  if (pos >= 0) return pos;
  else if (0u - (size_t)pos > len) return 0;
  else return (lua_Integer)len + pos + 1;
}
static int str_sub (lua_State *L) {
  size_t l;
  const char *s = luaL_checklstring(L, 1, &l);
  lua_Integer start = posrelat(luaL_checkinteger(L, 2), l);
  lua_Integer end = posrelat(luaL_optinteger(L, 3, -1), l);
  if (start < 1) start = 1;
  if (end > (lua_Integer)l) end = l;
  if (start <= end)
    lua_pushlstring(L, s + start - 1, (size_t)(end - start) + 1);
  else lua_pushliteral(L, "");
  return 1;
}
static int str_reverse (lua_State *L) {
  size_t l, i;
  luaL_Buffer b;
  const char *s = luaL_checklstring(L, 1, &l);
  char *p = luaL_buffinitsize(L, &b, l);
  for (i = 0; i < l; i++)
    p[i] = s[l - i - 1];
  luaL_pushresultsize(&b, l);
  return 1;
}
static int str_lower (lua_State *L) {
  size_t l;
  size_t i;
  luaL_Buffer b;
  const char *s = luaL_checklstring(L, 1, &l);
  char *p = luaL_buffinitsize(L, &b, l);
  for (i=0; i<l; i++)
    p[i] = tolower(uchar(s[i]));
  luaL_pushresultsize(&b, l);
  return 1;
}
static int str_upper (lua_State *L) {
  size_t l;
  size_t i;
  luaL_Buffer b;
  const char *s = luaL_checklstring(L, 1, &l);
  char *p = luaL_buffinitsize(L, &b, l);
  for (i=0; i<l; i++)
    p[i] = toupper(uchar(s[i]));
  luaL_pushresultsize(&b, l);
  return 1;
}
static int str_rep (lua_State *L) {
  size_t l, lsep;
  const char *s = luaL_checklstring(L, 1, &l);
  lua_Integer n = luaL_checkinteger(L, 2);
  const char *sep = luaL_optlstring(L, 3, "", &lsep);
  if (n <= 0) lua_pushliteral(L, "");
  else if (l + lsep < l || l + lsep > MAXSIZE / n)  
    return luaL_error(L, "resulting string too large");
  else {
    size_t totallen = (size_t)n * l + (size_t)(n - 1) * lsep;
    luaL_Buffer b;
    char *p = luaL_buffinitsize(L, &b, totallen);
    while (n-- > 1) {  
      memcpy(p, s, l * sizeof(char)); p += l;
      if (lsep > 0) {  
        memcpy(p, sep, lsep * sizeof(char));
        p += lsep;
      }
    }
    memcpy(p, s, l * sizeof(char));  
    luaL_pushresultsize(&b, totallen);
  }
  return 1;
}
static int str_byte (lua_State *L) {
  size_t l;
  const char *s = luaL_checklstring(L, 1, &l);
  lua_Integer posi = posrelat(luaL_optinteger(L, 2, 1), l);
  lua_Integer pose = posrelat(luaL_optinteger(L, 3, posi), l);
  int n, i;
  if (posi < 1) posi = 1;
  if (pose > (lua_Integer)l) pose = l;
  if (posi > pose) return 0;  
  if (pose - posi >= INT_MAX)  
    return luaL_error(L, "string slice too long");
  n = (int)(pose -  posi) + 1;
  luaL_checkstack(L, n, "string slice too long");
  for (i=0; i<n; i++)
    lua_pushinteger(L, uchar(s[posi+i-1]));
  return n;
}
static int str_char (lua_State *L) {
  int n = lua_gettop(L);  
  int i;
  luaL_Buffer b;
  char *p = luaL_buffinitsize(L, &b, n);
  for (i=1; i<=n; i++) {
    lua_Integer c = luaL_checkinteger(L, i);
    luaL_argcheck(L, uchar(c) == c, i, "value out of range");
    p[i - 1] = uchar(c);
  }
  luaL_pushresultsize(&b, n);
  return 1;
}
static int writer (lua_State *L, const void *b, size_t size, void *B) {
  (void)L;
  luaL_addlstring((luaL_Buffer *) B, (const char *)b, size);
  return 0;
}
static int str_dump (lua_State *L) {
  luaL_Buffer b;
  int strip = lua_toboolean(L, 2);
  luaL_checktype(L, 1, LUA_TFUNCTION);
  lua_settop(L, 1);
  luaL_buffinit(L,&b);
  if (lua_dump(L, writer, &b, strip) != 0)
    return luaL_error(L, "unable to dump given function");
  luaL_pushresult(&b);
  return 1;
}
#define CAP_UNFINISHED	(-1)
#define CAP_POSITION	(-2)
typedef struct MatchState {
  const char *src_init;  
  const char *src_end;  
  const char *p_end;  
  lua_State *L;
  int matchdepth;  
  unsigned char level;  
  struct {
    const char *init;
    ptrdiff_t len;
  } capture[LUA_MAXCAPTURES];
} MatchState;
static const char *match (MatchState *ms, const char *s, const char *p);
#if !defined(MAXCCALLS)
#define MAXCCALLS	200
#endif
#define L_ESC		'%'
#define SPECIALS	"^$*+?.([%-"
static int check_capture (MatchState *ms, int l) {
  l -= '1';
  if (l < 0 || l >= ms->level || ms->capture[l].len == CAP_UNFINISHED)
    return luaL_error(ms->L, "invalid capture index %%%d", l + 1);
  return l;
}
static int capture_to_close (MatchState *ms) {
  int level = ms->level;
  for (level--; level>=0; level--)
    if (ms->capture[level].len == CAP_UNFINISHED) return level;
  return luaL_error(ms->L, "invalid pattern capture");
}
static const char *classend (MatchState *ms, const char *p) {
  switch (*p++) {
    case L_ESC: {
      if (p == ms->p_end)
        luaL_error(ms->L, "malformed pattern (ends with '%%')");
      return p+1;
    }
    case '[': {
      if (*p == '^') p++;
      do {  
        if (p == ms->p_end)
          luaL_error(ms->L, "malformed pattern (missing ']')");
        if (*(p++) == L_ESC && p < ms->p_end)
          p++;  
      } while (*p != ']');
      return p+1;
    }
    default: {
      return p;
    }
  }
}
static int match_class (int c, int cl) {
  int res;
  switch (tolower(cl)) {
    case 'a' : res = isalpha(c); break;
    case 'c' : res = iscntrl(c); break;
    case 'd' : res = isdigit(c); break;
    case 'g' : res = isgraph(c); break;
    case 'l' : res = islower(c); break;
    case 'p' : res = ispunct(c); break;
    case 's' : res = isspace(c); break;
    case 'u' : res = isupper(c); break;
    case 'w' : res = isalnum(c); break;
    case 'x' : res = isxdigit(c); break;
    case 'z' : res = (c == 0); break;  
    default: return (cl == c);
  }
  return (islower(cl) ? res : !res);
}
static int matchbracketclass (int c, const char *p, const char *ec) {
  int sig = 1;
  if (*(p+1) == '^') {
    sig = 0;
    p++;  
  }
  while (++p < ec) {
    if (*p == L_ESC) {
      p++;
      if (match_class(c, uchar(*p)))
        return sig;
    }
    else if ((*(p+1) == '-') && (p+2 < ec)) {
      p+=2;
      if (uchar(*(p-2)) <= c && c <= uchar(*p))
        return sig;
    }
    else if (uchar(*p) == c) return sig;
  }
  return !sig;
}
static int singlematch (MatchState *ms, const char *s, const char *p,
                        const char *ep) {
  if (s >= ms->src_end)
    return 0;
  else {
    int c = uchar(*s);
    switch (*p) {
      case '.': return 1;  
      case L_ESC: return match_class(c, uchar(*(p+1)));
      case '[': return matchbracketclass(c, p, ep-1);
      default:  return (uchar(*p) == c);
    }
  }
}
static const char *matchbalance (MatchState *ms, const char *s,
                                   const char *p) {
  if (p >= ms->p_end - 1)
    luaL_error(ms->L, "malformed pattern (missing arguments to '%%b')");
  if (*s != *p) return NULL;
  else {
    int b = *p;
    int e = *(p+1);
    int cont = 1;
    while (++s < ms->src_end) {
      if (*s == e) {
        if (--cont == 0) return s+1;
      }
      else if (*s == b) cont++;
    }
  }
  return NULL;  
}
static const char *max_expand (MatchState *ms, const char *s,
                                 const char *p, const char *ep) {
  ptrdiff_t i = 0;  
  while (singlematch(ms, s + i, p, ep))
    i++;
  while (i>=0) {
    const char *res = match(ms, (s+i), ep+1);
    if (res) return res;
    i--;  
  }
  return NULL;
}
static const char *min_expand (MatchState *ms, const char *s,
                                 const char *p, const char *ep) {
  for (;;) {
    const char *res = match(ms, s, ep+1);
    if (res != NULL)
      return res;
    else if (singlematch(ms, s, p, ep))
      s++;  
    else return NULL;
  }
}
static const char *start_capture (MatchState *ms, const char *s,
                                    const char *p, int what) {
  const char *res;
  int level = ms->level;
  if (level >= LUA_MAXCAPTURES) luaL_error(ms->L, "too many captures");
  ms->capture[level].init = s;
  ms->capture[level].len = what;
  ms->level = level+1;
  if ((res=match(ms, s, p)) == NULL)  
    ms->level--;  
  return res;
}
static const char *end_capture (MatchState *ms, const char *s,
                                  const char *p) {
  int l = capture_to_close(ms);
  const char *res;
  ms->capture[l].len = s - ms->capture[l].init;  
  if ((res = match(ms, s, p)) == NULL)  
    ms->capture[l].len = CAP_UNFINISHED;  
  return res;
}
static const char *match_capture (MatchState *ms, const char *s, int l) {
  size_t len;
  l = check_capture(ms, l);
  len = ms->capture[l].len;
  if ((size_t)(ms->src_end-s) >= len &&
      memcmp(ms->capture[l].init, s, len) == 0)
    return s+len;
  else return NULL;
}
static const char *match (MatchState *ms, const char *s, const char *p) {
  if (ms->matchdepth-- == 0)
    luaL_error(ms->L, "pattern too complex");
  init: 
  if (p != ms->p_end) {  
    switch (*p) {
      case '(': {  
        if (*(p + 1) == ')')  
          s = start_capture(ms, s, p + 2, CAP_POSITION);
        else
          s = start_capture(ms, s, p + 1, CAP_UNFINISHED);
        break;
      }
      case ')': {  
        s = end_capture(ms, s, p + 1);
        break;
      }
      case '$': {
        if ((p + 1) != ms->p_end)  
          goto dflt;  
        s = (s == ms->src_end) ? s : NULL;  
        break;
      }
      case L_ESC: {  
        switch (*(p + 1)) {
          case 'b': {  
            s = matchbalance(ms, s, p + 2);
            if (s != NULL) {
              p += 4; goto init;  
            }  
            break;
          }
          case 'f': {  
            const char *ep; char previous;
            p += 2;
            if (*p != '[')
              luaL_error(ms->L, "missing '[' after '%%f' in pattern");
            ep = classend(ms, p);  
            previous = (s == ms->src_init) ? '\0' : *(s - 1);
            if (!matchbracketclass(uchar(previous), p, ep - 1) &&
               matchbracketclass(uchar(*s), p, ep - 1)) {
              p = ep; goto init;  
            }
            s = NULL;  
            break;
          }
          case '0': case '1': case '2': case '3':
          case '4': case '5': case '6': case '7':
          case '8': case '9': {  
            s = match_capture(ms, s, uchar(*(p + 1)));
            if (s != NULL) {
              p += 2; goto init;  
            }
            break;
          }
          default: goto dflt;
        }
        break;
      }
      default: dflt: {  
        const char *ep = classend(ms, p);  
        if (!singlematch(ms, s, p, ep)) {
          if (*ep == '*' || *ep == '?' || *ep == '-') {  
            p = ep + 1; goto init;  
          }
          else  
            s = NULL;  
        }
        else {  
          switch (*ep) {  
            case '?': {  
              const char *res;
              if ((res = match(ms, s + 1, ep + 1)) != NULL)
                s = res;
              else {
                p = ep + 1; goto init;  
              }
              break;
            }
            case '+':  
              s++;  
            case '*':  
              s = max_expand(ms, s, p, ep);
              break;
            case '-':  
              s = min_expand(ms, s, p, ep);
              break;
            default:  
              s++; p = ep; goto init;  
          }
        }
        break;
      }
    }
  }
  ms->matchdepth++;
  return s;
}
static const char *lmemfind (const char *s1, size_t l1,
                               const char *s2, size_t l2) {
  if (l2 == 0) return s1;  
  else if (l2 > l1) return NULL;  
  else {
    const char *init;  
    l2--;  
    l1 = l1-l2;  
    while (l1 > 0 && (init = (const char *)memchr(s1, *s2, l1)) != NULL) {
      init++;   
      if (memcmp(init, s2+1, l2) == 0)
        return init-1;
      else {  
        l1 -= init-s1;
        s1 = init;
      }
    }
    return NULL;  
  }
}
static void push_onecapture (MatchState *ms, int i, const char *s,
                                                    const char *e) {
  if (i >= ms->level) {
    if (i == 0)  
      lua_pushlstring(ms->L, s, e - s);  
    else
      luaL_error(ms->L, "invalid capture index %%%d", i + 1);
  }
  else {
    ptrdiff_t l = ms->capture[i].len;
    if (l == CAP_UNFINISHED) luaL_error(ms->L, "unfinished capture");
    if (l == CAP_POSITION)
      lua_pushinteger(ms->L, (ms->capture[i].init - ms->src_init) + 1);
    else
      lua_pushlstring(ms->L, ms->capture[i].init, l);
  }
}
static int push_captures (MatchState *ms, const char *s, const char *e) {
  int i;
  int nlevels = (ms->level == 0 && s) ? 1 : ms->level;
  luaL_checkstack(ms->L, nlevels, "too many captures");
  for (i = 0; i < nlevels; i++)
    push_onecapture(ms, i, s, e);
  return nlevels;  
}
static int nospecials (const char *p, size_t l) {
  size_t upto = 0;
  do {
    if (strpbrk(p + upto, SPECIALS))
      return 0;  
    upto += strlen(p + upto) + 1;  
  } while (upto <= l);
  return 1;  
}
static void prepstate (MatchState *ms, lua_State *L,
                       const char *s, size_t ls, const char *p, size_t lp) {
  ms->L = L;
  ms->matchdepth = MAXCCALLS;
  ms->src_init = s;
  ms->src_end = s + ls;
  ms->p_end = p + lp;
}
static void reprepstate (MatchState *ms) {
  ms->level = 0;
  lua_assert(ms->matchdepth == MAXCCALLS);
}
static int str_find_aux (lua_State *L, int find) {
  size_t ls, lp;
  const char *s = luaL_checklstring(L, 1, &ls);
  const char *p = luaL_checklstring(L, 2, &lp);
  lua_Integer init = posrelat(luaL_optinteger(L, 3, 1), ls);
  if (init < 1) init = 1;
  else if (init > (lua_Integer)ls + 1) {  
    lua_pushnil(L);  
    return 1;
  }
  if (find && (lua_toboolean(L, 4) || nospecials(p, lp))) {
    const char *s2 = lmemfind(s + init - 1, ls - (size_t)init + 1, p, lp);
    if (s2) {
      lua_pushinteger(L, (s2 - s) + 1);
      lua_pushinteger(L, (s2 - s) + lp);
      return 2;
    }
  }
  else {
    MatchState ms;
    const char *s1 = s + init - 1;
    int anchor = (*p == '^');
    if (anchor) {
      p++; lp--;  
    }
    prepstate(&ms, L, s, ls, p, lp);
    do {
      const char *res;
      reprepstate(&ms);
      if ((res=match(&ms, s1, p)) != NULL) {
        if (find) {
          lua_pushinteger(L, (s1 - s) + 1);  
          lua_pushinteger(L, res - s);   
          return push_captures(&ms, NULL, 0) + 2;
        }
        else
          return push_captures(&ms, s1, res);
      }
    } while (s1++ < ms.src_end && !anchor);
  }
  lua_pushnil(L);  
  return 1;
}
static int str_find (lua_State *L) {
  return str_find_aux(L, 1);
}
static int str_match (lua_State *L) {
  return str_find_aux(L, 0);
}
typedef struct GMatchState {
  const char *src;  
  const char *p;  
  const char *lastmatch;  
  MatchState ms;  
} GMatchState;
static int gmatch_aux (lua_State *L) {
  GMatchState *gm = (GMatchState *)lua_touserdata(L, lua_upvalueindex(3));
  const char *src;
  gm->ms.L = L;
  for (src = gm->src; src <= gm->ms.src_end; src++) {
    const char *e;
    reprepstate(&gm->ms);
    if ((e = match(&gm->ms, src, gm->p)) != NULL && e != gm->lastmatch) {
      gm->src = gm->lastmatch = e;
      return push_captures(&gm->ms, src, e);
    }
  }
  return 0;  
}
static int gmatch (lua_State *L) {
  size_t ls, lp;
  const char *s = luaL_checklstring(L, 1, &ls);
  const char *p = luaL_checklstring(L, 2, &lp);
  GMatchState *gm;
  lua_settop(L, 2);  
  gm = (GMatchState *)lua_newuserdata(L, sizeof(GMatchState));
  prepstate(&gm->ms, L, s, ls, p, lp);
  gm->src = s; gm->p = p; gm->lastmatch = NULL;
  lua_pushcclosure(L, gmatch_aux, 3);
  return 1;
}
static void add_s (MatchState *ms, luaL_Buffer *b, const char *s,
                                                   const char *e) {
  size_t l, i;
  lua_State *L = ms->L;
  const char *news = lua_tolstring(L, 3, &l);
  for (i = 0; i < l; i++) {
    if (news[i] != L_ESC)
      luaL_addchar(b, news[i]);
    else {
      i++;  
      if (!isdigit(uchar(news[i]))) {
        if (news[i] != L_ESC)
          luaL_error(L, "invalid use of '%c' in replacement string", L_ESC);
        luaL_addchar(b, news[i]);
      }
      else if (news[i] == '0')
          luaL_addlstring(b, s, e - s);
      else {
        push_onecapture(ms, news[i] - '1', s, e);
        luaL_tolstring(L, -1, NULL);  
        lua_remove(L, -2);  
        luaL_addvalue(b);  
      }
    }
  }
}
static void add_value (MatchState *ms, luaL_Buffer *b, const char *s,
                                       const char *e, int tr) {
  lua_State *L = ms->L;
  switch (tr) {
    case LUA_TFUNCTION: {
      int n;
      lua_pushvalue(L, 3);
      n = push_captures(ms, s, e);
      lua_call(L, n, 1);
      break;
    }
    case LUA_TTABLE: {
      push_onecapture(ms, 0, s, e);
      lua_gettable(L, 3);
      break;
    }
    default: {  
      add_s(ms, b, s, e);
      return;
    }
  }
  if (!lua_toboolean(L, -1)) {  
    lua_pop(L, 1);
    lua_pushlstring(L, s, e - s);  
  }
  else if (!lua_isstring(L, -1))
    luaL_error(L, "invalid replacement value (a %s)", luaL_typename(L, -1));
  luaL_addvalue(b);  
}
static int str_gsub (lua_State *L) {
  size_t srcl, lp;
  const char *src = luaL_checklstring(L, 1, &srcl);  
  const char *p = luaL_checklstring(L, 2, &lp);  
  const char *lastmatch = NULL;  
  int tr = lua_type(L, 3);  
  lua_Integer max_s = luaL_optinteger(L, 4, srcl + 1);  
  int anchor = (*p == '^');
  lua_Integer n = 0;  
  MatchState ms;
  luaL_Buffer b;
  luaL_argcheck(L, tr == LUA_TNUMBER || tr == LUA_TSTRING ||
                   tr == LUA_TFUNCTION || tr == LUA_TTABLE, 3,
                      "string/function/table expected");
  luaL_buffinit(L, &b);
  if (anchor) {
    p++; lp--;  
  }
  prepstate(&ms, L, src, srcl, p, lp);
  while (n < max_s) {
    const char *e;
    reprepstate(&ms);  
    if ((e = match(&ms, src, p)) != NULL && e != lastmatch) {  
      n++;
      add_value(&ms, &b, src, e, tr);  
      src = lastmatch = e;
    }
    else if (src < ms.src_end)  
      luaL_addchar(&b, *src++);
    else break;  
    if (anchor) break;
  }
  luaL_addlstring(&b, src, ms.src_end-src);
  luaL_pushresult(&b);
  lua_pushinteger(L, n);  
  return 2;
}
#if !defined(lua_number2strx)	
#include <math.h>
#define SIZELENMOD	(sizeof(LUA_NUMBER_FRMLEN)/sizeof(char))
#define L_NBFD		((l_mathlim(MANT_DIG) - 1)%4 + 1)
static lua_Number adddigit (char *buff, int n, lua_Number x) {
  lua_Number dd = l_mathop(floor)(x);  
  int d = (int)dd;
  buff[n] = (d < 10 ? d + '0' : d - 10 + 'a');  
  return x - dd;  
}
static int num2straux (char *buff, int sz, lua_Number x) {
  if (x != x || x == (lua_Number)HUGE_VAL || x == -(lua_Number)HUGE_VAL)
    return l_sprintf(buff, sz, LUA_NUMBER_FMT, (LUAI_UACNUMBER)x);
  else if (x == 0) {  
    return l_sprintf(buff, sz, LUA_NUMBER_FMT "x0p+0", (LUAI_UACNUMBER)x);
  }
  else {
    int e;
    lua_Number m = l_mathop(frexp)(x, &e);  
    int n = 0;  
    if (m < 0) {  
      buff[n++] = '-';  
      m = -m;  
    }
    buff[n++] = '0'; buff[n++] = 'x';  
    m = adddigit(buff, n++, m * (1 << L_NBFD));  
    e -= L_NBFD;  
    if (m > 0) {  
      buff[n++] = lua_getlocaledecpoint();  
      do {  
        m = adddigit(buff, n++, m * 16);
      } while (m > 0);
    }
    n += l_sprintf(buff + n, sz - n, "p%+d", e);  
    lua_assert(n < sz);
    return n;
  }
}
static int lua_number2strx (lua_State *L, char *buff, int sz,
                            const char *fmt, lua_Number x) {
  int n = num2straux(buff, sz, x);
  if (fmt[SIZELENMOD] == 'A') {
    int i;
    for (i = 0; i < n; i++)
      buff[i] = toupper(uchar(buff[i]));
  }
  else if (fmt[SIZELENMOD] != 'a')
    return luaL_error(L, "modifiers for format '%%a'/'%%A' not implemented");
  return n;
}
#endif				
#define MAX_ITEM        (120 + l_mathlim(MAX_10_EXP))
#define FLAGS	"-+ #0"
#define MAX_FORMAT	32
static void addquoted (luaL_Buffer *b, const char *s, size_t len) {
  luaL_addchar(b, '"');
  while (len--) {
    if (*s == '"' || *s == '\\' || *s == '\n') {
      luaL_addchar(b, '\\');
      luaL_addchar(b, *s);
    }
    else if (iscntrl(uchar(*s))) {
      char buff[10];
      if (!isdigit(uchar(*(s+1))))
        l_sprintf(buff, sizeof(buff), "\\%d", (int)uchar(*s));
      else
        l_sprintf(buff, sizeof(buff), "\\%03d", (int)uchar(*s));
      luaL_addstring(b, buff);
    }
    else
      luaL_addchar(b, *s);
    s++;
  }
  luaL_addchar(b, '"');
}
static void checkdp (char *buff, int nb) {
  if (memchr(buff, '.', nb) == NULL) {  
    char point = lua_getlocaledecpoint();  
    char *ppoint = (char *)memchr(buff, point, nb);
    if (ppoint) *ppoint = '.';  
  }
}
static void addliteral (lua_State *L, luaL_Buffer *b, int arg) {
  switch (lua_type(L, arg)) {
    case LUA_TSTRING: {
      size_t len;
      const char *s = lua_tolstring(L, arg, &len);
      addquoted(b, s, len);
      break;
    }
    case LUA_TNUMBER: {
      char *buff = luaL_prepbuffsize(b, MAX_ITEM);
      int nb;
      if (!lua_isinteger(L, arg)) {  
        lua_Number n = lua_tonumber(L, arg);  
        nb = lua_number2strx(L, buff, MAX_ITEM, "%" LUA_NUMBER_FRMLEN "a", n);
        checkdp(buff, nb);  
      }
      else {  
        lua_Integer n = lua_tointeger(L, arg);
        const char *format = (n == LUA_MININTEGER)  
                           ? "0x%" LUA_INTEGER_FRMLEN "x"  
                           : LUA_INTEGER_FMT;  
        nb = l_sprintf(buff, MAX_ITEM, format, (LUAI_UACINT)n);
      }
      luaL_addsize(b, nb);
      break;
    }
    case LUA_TNIL: case LUA_TBOOLEAN: {
      luaL_tolstring(L, arg, NULL);
      luaL_addvalue(b);
      break;
    }
    default: {
      luaL_argerror(L, arg, "value has no literal form");
    }
  }
}
static const char *scanformat (lua_State *L, const char *strfrmt, char *form) {
  const char *p = strfrmt;
  while (*p != '\0' && strchr(FLAGS, *p) != NULL) p++;  
  if ((size_t)(p - strfrmt) >= sizeof(FLAGS)/sizeof(char))
    luaL_error(L, "invalid format (repeated flags)");
  if (isdigit(uchar(*p))) p++;  
  if (isdigit(uchar(*p))) p++;  
  if (*p == '.') {
    p++;
    if (isdigit(uchar(*p))) p++;  
    if (isdigit(uchar(*p))) p++;  
  }
  if (isdigit(uchar(*p)))
    luaL_error(L, "invalid format (width or precision too long)");
  *(form++) = '%';
  memcpy(form, strfrmt, ((p - strfrmt) + 1) * sizeof(char));
  form += (p - strfrmt) + 1;
  *form = '\0';
  return p;
}
static void addlenmod (char *form, const char *lenmod) {
  size_t l = strlen(form);
  size_t lm = strlen(lenmod);
  char spec = form[l - 1];
  strcpy(form + l - 1, lenmod);
  form[l + lm - 1] = spec;
  form[l + lm] = '\0';
}
static int str_format (lua_State *L) {
  int top = lua_gettop(L);
  int arg = 1;
  size_t sfl;
  const char *strfrmt = luaL_checklstring(L, arg, &sfl);
  const char *strfrmt_end = strfrmt+sfl;
  luaL_Buffer b;
  luaL_buffinit(L, &b);
  while (strfrmt < strfrmt_end) {
    if (*strfrmt != L_ESC)
      luaL_addchar(&b, *strfrmt++);
    else if (*++strfrmt == L_ESC)
      luaL_addchar(&b, *strfrmt++);  
    else { 
      char form[MAX_FORMAT];  
      char *buff = luaL_prepbuffsize(&b, MAX_ITEM);  
      int nb = 0;  
      if (++arg > top)
        luaL_argerror(L, arg, "no value");
      strfrmt = scanformat(L, strfrmt, form);
      switch (*strfrmt++) {
        case 'c': {
          nb = l_sprintf(buff, MAX_ITEM, form, (int)luaL_checkinteger(L, arg));
          break;
        }
        case 'd': case 'i':
        case 'o': case 'u': case 'x': case 'X': {
          lua_Integer n = luaL_checkinteger(L, arg);
          addlenmod(form, LUA_INTEGER_FRMLEN);
          nb = l_sprintf(buff, MAX_ITEM, form, (LUAI_UACINT)n);
          break;
        }
        case 'a': case 'A':
          addlenmod(form, LUA_NUMBER_FRMLEN);
          nb = lua_number2strx(L, buff, MAX_ITEM, form,
                                  luaL_checknumber(L, arg));
          break;
        case 'e': case 'E': case 'f':
        case 'g': case 'G': {
          lua_Number n = luaL_checknumber(L, arg);
          addlenmod(form, LUA_NUMBER_FRMLEN);
          nb = l_sprintf(buff, MAX_ITEM, form, (LUAI_UACNUMBER)n);
          break;
        }
        case 'q': {
          addliteral(L, &b, arg);
          break;
        }
        case 's': {
          size_t l;
          const char *s = luaL_tolstring(L, arg, &l);
          if (form[2] == '\0')  
            luaL_addvalue(&b);  
          else {
            luaL_argcheck(L, l == strlen(s), arg, "string contains zeros");
            if (!strchr(form, '.') && l >= 100) {
              luaL_addvalue(&b);  
            }
            else {  
              nb = l_sprintf(buff, MAX_ITEM, form, s);
              lua_pop(L, 1);  
            }
          }
          break;
        }
        default: {  
          return luaL_error(L, "invalid option '%%%c' to 'format'",
                               *(strfrmt - 1));
        }
      }
      lua_assert(nb < MAX_ITEM);
      luaL_addsize(&b, nb);
    }
  }
  luaL_pushresult(&b);
  return 1;
}
#if !defined(LUAL_PACKPADBYTE)
#define LUAL_PACKPADBYTE		0x00
#endif
#define MAXINTSIZE	16
#define NB	CHAR_BIT
#define MC	((1 << NB) - 1)
#define SZINT	((int)sizeof(lua_Integer))
static const union {
  int dummy;
  char little;  
} nativeendian = {1};
struct cD {
  char c;
  union { double d; void *p; lua_Integer i; lua_Number n; } u;
};
#define MAXALIGN	(offsetof(struct cD, u))
typedef union Ftypes {
  float f;
  double d;
  lua_Number n;
  char buff[5 * sizeof(lua_Number)];  
} Ftypes;
typedef struct Header {
  lua_State *L;
  int islittle;
  int maxalign;
} Header;
typedef enum KOption {
  Kint,		
  Kuint,	
  Kfloat,	
  Kchar,	
  Kstring,	
  Kzstr,	
  Kpadding,	
  Kpaddalign,	
  Knop		
} KOption;
static int digit (int c) { return '0' <= c && c <= '9'; }
static int getnum (const char **fmt, int df) {
  if (!digit(**fmt))  
    return df;  
  else {
    int a = 0;
    do {
      a = a*10 + (*((*fmt)++) - '0');
    } while (digit(**fmt) && a <= ((int)MAXSIZE - 9)/10);
    return a;
  }
}
static int getnumlimit (Header *h, const char **fmt, int df) {
  int sz = getnum(fmt, df);
  if (sz > MAXINTSIZE || sz <= 0)
    return luaL_error(h->L, "integral size (%d) out of limits [1,%d]",
                            sz, MAXINTSIZE);
  return sz;
}
static void initheader (lua_State *L, Header *h) {
  h->L = L;
  h->islittle = nativeendian.little;
  h->maxalign = 1;
}
static KOption getoption (Header *h, const char **fmt, int *size) {
  int opt = *((*fmt)++);
  *size = 0;  
  switch (opt) {
    case 'b': *size = sizeof(char); return Kint;
    case 'B': *size = sizeof(char); return Kuint;
    case 'h': *size = sizeof(short); return Kint;
    case 'H': *size = sizeof(short); return Kuint;
    case 'l': *size = sizeof(long); return Kint;
    case 'L': *size = sizeof(long); return Kuint;
    case 'j': *size = sizeof(lua_Integer); return Kint;
    case 'J': *size = sizeof(lua_Integer); return Kuint;
    case 'T': *size = sizeof(size_t); return Kuint;
    case 'f': *size = sizeof(float); return Kfloat;
    case 'd': *size = sizeof(double); return Kfloat;
    case 'n': *size = sizeof(lua_Number); return Kfloat;
    case 'i': *size = getnumlimit(h, fmt, sizeof(int)); return Kint;
    case 'I': *size = getnumlimit(h, fmt, sizeof(int)); return Kuint;
    case 's': *size = getnumlimit(h, fmt, sizeof(size_t)); return Kstring;
    case 'c':
      *size = getnum(fmt, -1);
      if (*size == -1)
        luaL_error(h->L, "missing size for format option 'c'");
      return Kchar;
    case 'z': return Kzstr;
    case 'x': *size = 1; return Kpadding;
    case 'X': return Kpaddalign;
    case ' ': break;
    case '<': h->islittle = 1; break;
    case '>': h->islittle = 0; break;
    case '=': h->islittle = nativeendian.little; break;
    case '!': h->maxalign = getnumlimit(h, fmt, MAXALIGN); break;
    default: luaL_error(h->L, "invalid format option '%c'", opt);
  }
  return Knop;
}
static KOption getdetails (Header *h, size_t totalsize,
                           const char **fmt, int *psize, int *ntoalign) {
  KOption opt = getoption(h, fmt, psize);
  int align = *psize;  
  if (opt == Kpaddalign) {  
    if (**fmt == '\0' || getoption(h, fmt, &align) == Kchar || align == 0)
      luaL_argerror(h->L, 1, "invalid next option for option 'X'");
  }
  if (align <= 1 || opt == Kchar)  
    *ntoalign = 0;
  else {
    if (align > h->maxalign)  
      align = h->maxalign;
    if ((align & (align - 1)) != 0)  
      luaL_argerror(h->L, 1, "format asks for alignment not power of 2");
    *ntoalign = (align - (int)(totalsize & (align - 1))) & (align - 1);
  }
  return opt;
}
static void packint (luaL_Buffer *b, lua_Unsigned n,
                     int islittle, int size, int neg) {
  char *buff = luaL_prepbuffsize(b, size);
  int i;
  buff[islittle ? 0 : size - 1] = (char)(n & MC);  
  for (i = 1; i < size; i++) {
    n >>= NB;
    buff[islittle ? i : size - 1 - i] = (char)(n & MC);
  }
  if (neg && size > SZINT) {  
    for (i = SZINT; i < size; i++)  
      buff[islittle ? i : size - 1 - i] = (char)MC;
  }
  luaL_addsize(b, size);  
}
static void copywithendian (volatile char *dest, volatile const char *src,
                            int size, int islittle) {
  if (islittle == nativeendian.little) {
    while (size-- != 0)
      *(dest++) = *(src++);
  }
  else {
    dest += size - 1;
    while (size-- != 0)
      *(dest--) = *(src++);
  }
}
static int str_pack (lua_State *L) {
  luaL_Buffer b;
  Header h;
  const char *fmt = luaL_checkstring(L, 1);  
  int arg = 1;  
  size_t totalsize = 0;  
  initheader(L, &h);
  lua_pushnil(L);  
  luaL_buffinit(L, &b);
  while (*fmt != '\0') {
    int size, ntoalign;
    KOption opt = getdetails(&h, totalsize, &fmt, &size, &ntoalign);
    totalsize += ntoalign + size;
    while (ntoalign-- > 0)
     luaL_addchar(&b, LUAL_PACKPADBYTE);  
    arg++;
    switch (opt) {
      case Kint: {  
        lua_Integer n = luaL_checkinteger(L, arg);
        if (size < SZINT) {  
          lua_Integer lim = (lua_Integer)1 << ((size * NB) - 1);
          luaL_argcheck(L, -lim <= n && n < lim, arg, "integer overflow");
        }
        packint(&b, (lua_Unsigned)n, h.islittle, size, (n < 0));
        break;
      }
      case Kuint: {  
        lua_Integer n = luaL_checkinteger(L, arg);
        if (size < SZINT)  
          luaL_argcheck(L, (lua_Unsigned)n < ((lua_Unsigned)1 << (size * NB)),
                           arg, "unsigned overflow");
        packint(&b, (lua_Unsigned)n, h.islittle, size, 0);
        break;
      }
      case Kfloat: {  
        volatile Ftypes u;
        char *buff = luaL_prepbuffsize(&b, size);
        lua_Number n = luaL_checknumber(L, arg);  
        if (size == sizeof(u.f)) u.f = (float)n;  
        else if (size == sizeof(u.d)) u.d = (double)n;
        else u.n = n;
        copywithendian(buff, u.buff, size, h.islittle);
        luaL_addsize(&b, size);
        break;
      }
      case Kchar: {  
        size_t len;
        const char *s = luaL_checklstring(L, arg, &len);
        luaL_argcheck(L, len <= (size_t)size, arg,
                         "string longer than given size");
        luaL_addlstring(&b, s, len);  
        while (len++ < (size_t)size)  
          luaL_addchar(&b, LUAL_PACKPADBYTE);
        break;
      }
      case Kstring: {  
        size_t len;
        const char *s = luaL_checklstring(L, arg, &len);
        luaL_argcheck(L, size >= (int)sizeof(size_t) ||
                         len < ((size_t)1 << (size * NB)),
                         arg, "string length does not fit in given size");
        packint(&b, (lua_Unsigned)len, h.islittle, size, 0);  
        luaL_addlstring(&b, s, len);
        totalsize += len;
        break;
      }
      case Kzstr: {  
        size_t len;
        const char *s = luaL_checklstring(L, arg, &len);
        luaL_argcheck(L, strlen(s) == len, arg, "string contains zeros");
        luaL_addlstring(&b, s, len);
        luaL_addchar(&b, '\0');  
        totalsize += len + 1;
        break;
      }
      case Kpadding: luaL_addchar(&b, LUAL_PACKPADBYTE);  
      case Kpaddalign: case Knop:
        arg--;  
        break;
    }
  }
  luaL_pushresult(&b);
  return 1;
}
static int str_packsize (lua_State *L) {
  Header h;
  const char *fmt = luaL_checkstring(L, 1);  
  size_t totalsize = 0;  
  initheader(L, &h);
  while (*fmt != '\0') {
    int size, ntoalign;
    KOption opt = getdetails(&h, totalsize, &fmt, &size, &ntoalign);
    size += ntoalign;  
    luaL_argcheck(L, totalsize <= MAXSIZE - size, 1,
                     "format result too large");
    totalsize += size;
    switch (opt) {
      case Kstring:  
      case Kzstr:    
        luaL_argerror(L, 1, "variable-length format");
      default:  break;
    }
  }
  lua_pushinteger(L, (lua_Integer)totalsize);
  return 1;
}
static lua_Integer unpackint (lua_State *L, const char *str,
                              int islittle, int size, int issigned) {
  lua_Unsigned res = 0;
  int i;
  int limit = (size  <= SZINT) ? size : SZINT;
  for (i = limit - 1; i >= 0; i--) {
    res <<= NB;
    res |= (lua_Unsigned)(unsigned char)str[islittle ? i : size - 1 - i];
  }
  if (size < SZINT) {  
    if (issigned) {  
      lua_Unsigned mask = (lua_Unsigned)1 << (size*NB - 1);
      res = ((res ^ mask) - mask);  
    }
  }
  else if (size > SZINT) {  
    int mask = (!issigned || (lua_Integer)res >= 0) ? 0 : MC;
    for (i = limit; i < size; i++) {
      if ((unsigned char)str[islittle ? i : size - 1 - i] != mask)
        luaL_error(L, "%d-byte integer does not fit into Lua Integer", size);
    }
  }
  return (lua_Integer)res;
}
static int str_unpack (lua_State *L) {
  Header h;
  const char *fmt = luaL_checkstring(L, 1);
  size_t ld;
  const char *data = luaL_checklstring(L, 2, &ld);
  size_t pos = (size_t)posrelat(luaL_optinteger(L, 3, 1), ld) - 1;
  int n = 0;  
  luaL_argcheck(L, pos <= ld, 3, "initial position out of string");
  initheader(L, &h);
  while (*fmt != '\0') {
    int size, ntoalign;
    KOption opt = getdetails(&h, pos, &fmt, &size, &ntoalign);
    if ((size_t)ntoalign + size > ~pos || pos + ntoalign + size > ld)
      luaL_argerror(L, 2, "data string too short");
    pos += ntoalign;  
    luaL_checkstack(L, 2, "too many results");
    n++;
    switch (opt) {
      case Kint:
      case Kuint: {
        lua_Integer res = unpackint(L, data + pos, h.islittle, size,
                                       (opt == Kint));
        lua_pushinteger(L, res);
        break;
      }
      case Kfloat: {
        volatile Ftypes u;
        lua_Number num;
        copywithendian(u.buff, data + pos, size, h.islittle);
        if (size == sizeof(u.f)) num = (lua_Number)u.f;
        else if (size == sizeof(u.d)) num = (lua_Number)u.d;
        else num = u.n;
        lua_pushnumber(L, num);
        break;
      }
      case Kchar: {
        lua_pushlstring(L, data + pos, size);
        break;
      }
      case Kstring: {
        size_t len = (size_t)unpackint(L, data + pos, h.islittle, size, 0);
        luaL_argcheck(L, pos + len + size <= ld, 2, "data string too short");
        lua_pushlstring(L, data + pos + size, len);
        pos += len;  
        break;
      }
      case Kzstr: {
        size_t len = (int)strlen(data + pos);
        lua_pushlstring(L, data + pos, len);
        pos += len + 1;  
        break;
      }
      case Kpaddalign: case Kpadding: case Knop:
        n--;  
        break;
    }
    pos += size;
  }
  lua_pushinteger(L, pos + 1);  
  return n + 1;
}
static const luaL_Reg strlib[] = {
  {"byte", str_byte},
  {"char", str_char},
  {"dump", str_dump},
  {"find", str_find},
  {"format", str_format},
  {"gmatch", gmatch},
  {"gsub", str_gsub},
  {"len", str_len},
  {"lower", str_lower},
  {"match", str_match},
  {"rep", str_rep},
  {"reverse", str_reverse},
  {"sub", str_sub},
  {"upper", str_upper},
  {"pack", str_pack},
  {"packsize", str_packsize},
  {"unpack", str_unpack},
  {NULL, NULL}
};
static void createmetatable (lua_State *L) {
  lua_createtable(L, 0, 1);  
  lua_pushliteral(L, "");  
  lua_pushvalue(L, -2);  
  lua_setmetatable(L, -2);  
  lua_pop(L, 1);  
  lua_pushvalue(L, -2);  
  lua_setfield(L, -2, "__index");  
  lua_pop(L, 1);  
}
LUAMOD_API int luaopen_string (lua_State *L) {
  luaL_newlib(L, strlib);
  createmetatable(L);
  return 1;
}
#define ltablib_c
#define LUA_LIB
#include <limits.h>
#include <stddef.h>
#include <string.h>
#define TAB_R	1			
#define TAB_W	2			
#define TAB_L	4			
#define TAB_RW	(TAB_R | TAB_W)		
#define aux_getn(L,n,w)	(checktab(L, n, (w) | TAB_L), luaL_len(L, n))
static int checkfield (lua_State *L, const char *key, int n) {
  lua_pushstring(L, key);
  return (lua_rawget(L, -n) != LUA_TNIL);
}
static void checktab (lua_State *L, int arg, int what) {
  if (lua_type(L, arg) != LUA_TTABLE) {  
    int n = 1;  
    if (lua_getmetatable(L, arg) &&  
        (!(what & TAB_R) || checkfield(L, "__index", ++n)) &&
        (!(what & TAB_W) || checkfield(L, "__newindex", ++n)) &&
        (!(what & TAB_L) || checkfield(L, "__len", ++n))) {
      lua_pop(L, n);  
    }
    else
      luaL_checktype(L, arg, LUA_TTABLE);  
  }
}
#if defined(LUA_COMPAT_MAXN)
static int maxn (lua_State *L) {
  lua_Number max = 0;
  luaL_checktype(L, 1, LUA_TTABLE);
  lua_pushnil(L);  
  while (lua_next(L, 1)) {
    lua_pop(L, 1);  
    if (lua_type(L, -1) == LUA_TNUMBER) {
      lua_Number v = lua_tonumber(L, -1);
      if (v > max) max = v;
    }
  }
  lua_pushnumber(L, max);
  return 1;
}
#endif
static int tinsert (lua_State *L) {
  lua_Integer e = aux_getn(L, 1, TAB_RW) + 1;  
  lua_Integer pos;  
  switch (lua_gettop(L)) {
    case 2: {  
      pos = e;  
      break;
    }
    case 3: {
      lua_Integer i;
      pos = luaL_checkinteger(L, 2);  
      luaL_argcheck(L, 1 <= pos && pos <= e, 2, "position out of bounds");
      for (i = e; i > pos; i--) {  
        lua_geti(L, 1, i - 1);
        lua_seti(L, 1, i);  
      }
      break;
    }
    default: {
      return luaL_error(L, "wrong number of arguments to 'insert'");
    }
  }
  lua_seti(L, 1, pos);  
  return 0;
}
static int tremove (lua_State *L) {
  lua_Integer size = aux_getn(L, 1, TAB_RW);
  lua_Integer pos = luaL_optinteger(L, 2, size);
  if (pos != size)  
    luaL_argcheck(L, 1 <= pos && pos <= size + 1, 1, "position out of bounds");
  lua_geti(L, 1, pos);  
  for ( ; pos < size; pos++) {
    lua_geti(L, 1, pos + 1);
    lua_seti(L, 1, pos);  
  }
  lua_pushnil(L);
  lua_seti(L, 1, pos);  
  return 1;
}
static int tmove (lua_State *L) {
  lua_Integer f = luaL_checkinteger(L, 2);
  lua_Integer e = luaL_checkinteger(L, 3);
  lua_Integer t = luaL_checkinteger(L, 4);
  int tt = !lua_isnoneornil(L, 5) ? 5 : 1;  
  checktab(L, 1, TAB_R);
  checktab(L, tt, TAB_W);
  if (e >= f) {  
    lua_Integer n, i;
    luaL_argcheck(L, f > 0 || e < LUA_MAXINTEGER + f, 3,
                  "too many elements to move");
    n = e - f + 1;  
    luaL_argcheck(L, t <= LUA_MAXINTEGER - n + 1, 4,
                  "destination wrap around");
    if (t > e || t <= f || (tt != 1 && !lua_compare(L, 1, tt, LUA_OPEQ))) {
      for (i = 0; i < n; i++) {
        lua_geti(L, 1, f + i);
        lua_seti(L, tt, t + i);
      }
    }
    else {
      for (i = n - 1; i >= 0; i--) {
        lua_geti(L, 1, f + i);
        lua_seti(L, tt, t + i);
      }
    }
  }
  lua_pushvalue(L, tt);  
  return 1;
}
static void addfield (lua_State *L, luaL_Buffer *b, lua_Integer i) {
  lua_geti(L, 1, i);
  if (!lua_isstring(L, -1))
    luaL_error(L, "invalid value (%s) at index %d in table for 'concat'",
                  luaL_typename(L, -1), i);
  luaL_addvalue(b);
}
static int tconcat (lua_State *L) {
  luaL_Buffer b;
  lua_Integer last = aux_getn(L, 1, TAB_R);
  size_t lsep;
  const char *sep = luaL_optlstring(L, 2, "", &lsep);
  lua_Integer i = luaL_optinteger(L, 3, 1);
  last = luaL_optinteger(L, 4, last);
  luaL_buffinit(L, &b);
  for (; i < last; i++) {
    addfield(L, &b, i);
    luaL_addlstring(&b, sep, lsep);
  }
  if (i == last)  
    addfield(L, &b, i);
  luaL_pushresult(&b);
  return 1;
}
static int pack (lua_State *L) {
  int i;
  int n = lua_gettop(L);  
  lua_createtable(L, n, 1);  
  lua_insert(L, 1);  
  for (i = n; i >= 1; i--)  
    lua_seti(L, 1, i);
  lua_pushinteger(L, n);
  lua_setfield(L, 1, "n");  
  return 1;  
}
static int unpack (lua_State *L) {
  lua_Unsigned n;
  lua_Integer i = luaL_optinteger(L, 2, 1);
  lua_Integer e = luaL_opt(L, luaL_checkinteger, 3, luaL_len(L, 1));
  if (i > e) return 0;  
  n = (lua_Unsigned)e - i;  
  if (n >= (unsigned int)INT_MAX  || !lua_checkstack(L, (int)(++n)))
    return luaL_error(L, "too many results to unpack");
  for (; i < e; i++) {  
    lua_geti(L, 1, i);
  }
  lua_geti(L, 1, e);  
  return (int)n;
}
typedef unsigned int IdxT;
#if !defined(l_randomizePivot)		
#include <time.h>
#define sof(e)		(sizeof(e) / sizeof(unsigned int))
static unsigned int l_randomizePivot (void) {
  clock_t c = clock();
  time_t t = time(NULL);
  unsigned int buff[sof(c) + sof(t)];
  unsigned int i, rnd = 0;
  memcpy(buff, &c, sof(c) * sizeof(unsigned int));
  memcpy(buff + sof(c), &t, sof(t) * sizeof(unsigned int));
  for (i = 0; i < sof(buff); i++)
    rnd += buff[i];
  return rnd;
}
#endif					
#define RANLIMIT	100u
static void set2 (lua_State *L, IdxT i, IdxT j) {
  lua_seti(L, 1, i);
  lua_seti(L, 1, j);
}
static int sort_comp (lua_State *L, int a, int b) {
  if (lua_isnil(L, 2))  
    return lua_compare(L, a, b, LUA_OPLT);  
  else {  
    int res;
    lua_pushvalue(L, 2);    
    lua_pushvalue(L, a-1);  
    lua_pushvalue(L, b-2);  
    lua_call(L, 2, 1);      
    res = lua_toboolean(L, -1);  
    lua_pop(L, 1);          
    return res;
  }
}
static IdxT partition (lua_State *L, IdxT lo, IdxT up) {
  IdxT i = lo;  
  IdxT j = up - 1;  
  for (;;) {
    while (lua_geti(L, 1, ++i), sort_comp(L, -1, -2)) {
      if (i == up - 1)  
        luaL_error(L, "invalid order function for sorting");
      lua_pop(L, 1);  
    }
    while (lua_geti(L, 1, --j), sort_comp(L, -3, -1)) {
      if (j < i)  
        luaL_error(L, "invalid order function for sorting");
      lua_pop(L, 1);  
    }
    if (j < i) {  
      lua_pop(L, 1);  
      set2(L, up - 1, i);
      return i;
    }
    set2(L, i, j);
  }
}
static IdxT choosePivot (IdxT lo, IdxT up, unsigned int rnd) {
  IdxT r4 = (up - lo) / 4;  
  IdxT p = rnd % (r4 * 2) + (lo + r4);
  lua_assert(lo + r4 <= p && p <= up - r4);
  return p;
}
static void auxsort (lua_State *L, IdxT lo, IdxT up,
                                   unsigned int rnd) {
  while (lo < up) {  
    IdxT p;  
    IdxT n;  
    lua_geti(L, 1, lo);
    lua_geti(L, 1, up);
    if (sort_comp(L, -1, -2))  
      set2(L, lo, up);  
    else
      lua_pop(L, 2);  
    if (up - lo == 1)  
      return;  
    if (up - lo < RANLIMIT || rnd == 0)  
      p = (lo + up)/2;  
    else  
      p = choosePivot(lo, up, rnd);
    lua_geti(L, 1, p);
    lua_geti(L, 1, lo);
    if (sort_comp(L, -2, -1))  
      set2(L, p, lo);  
    else {
      lua_pop(L, 1);  
      lua_geti(L, 1, up);
      if (sort_comp(L, -1, -2))  
        set2(L, p, up);  
      else
        lua_pop(L, 2);
    }
    if (up - lo == 2)  
      return;  
    lua_geti(L, 1, p);  
    lua_pushvalue(L, -1);  
    lua_geti(L, 1, up - 1);  
    set2(L, p, up - 1);  
    p = partition(L, lo, up);
    if (p - lo < up - p) {  
      auxsort(L, lo, p - 1, rnd);  
      n = p - lo;  
      lo = p + 1;  
    }
    else {
      auxsort(L, p + 1, up, rnd);  
      n = up - p;  
      up = p - 1;  
    }
    if ((up - lo) / 128 > n) 
      rnd = l_randomizePivot();  
  }  
}
static int sort (lua_State *L) {
  lua_Integer n = aux_getn(L, 1, TAB_RW);
  if (n > 1) {  
    luaL_argcheck(L, n < INT_MAX, 1, "array too big");
    if (!lua_isnoneornil(L, 2))  
      luaL_checktype(L, 2, LUA_TFUNCTION);  
    lua_settop(L, 2);  
    auxsort(L, 1, (IdxT)n, 0);
  }
  return 0;
}
static const luaL_Reg tab_funcs[] = {
  {"concat", tconcat},
#if defined(LUA_COMPAT_MAXN)
  {"maxn", maxn},
#endif
  {"insert", tinsert},
  {"pack", pack},
  {"unpack", unpack},
  {"remove", tremove},
  {"move", tmove},
  {"sort", sort},
  {NULL, NULL}
};
LUAMOD_API int luaopen_table (lua_State *L) {
  luaL_newlib(L, tab_funcs);
#if defined(LUA_COMPAT_UNPACK)
  lua_getfield(L, -1, "unpack");
  lua_setglobal(L, "unpack");
#endif
  return 1;
}
#define lutf8lib_c
#define LUA_LIB
#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#define MAXUNICODE	0x10FFFF
#define iscont(p)	((*(p) & 0xC0) == 0x80)
static lua_Integer u_posrelat (lua_Integer pos, size_t len) {
  if (pos >= 0) return pos;
  else if (0u - (size_t)pos > len) return 0;
  else return (lua_Integer)len + pos + 1;
}
static const char *utf8_decode (const char *o, int *val) {
  static const unsigned int limits[] = {0xFF, 0x7F, 0x7FF, 0xFFFF};
  const unsigned char *s = (const unsigned char *)o;
  unsigned int c = s[0];
  unsigned int res = 0;  
  if (c < 0x80)  
    res = c;
  else {
    int count = 0;  
    while (c & 0x40) {  
      int cc = s[++count];  
      if ((cc & 0xC0) != 0x80)  
        return NULL;  
      res = (res << 6) | (cc & 0x3F);  
      c <<= 1;  
    }
    res |= ((c & 0x7F) << (count * 5));  
    if (count > 3 || res > MAXUNICODE || res <= limits[count])
      return NULL;  
    s += count;  
  }
  if (val) *val = res;
  return (const char *)s + 1;  
}
static int utflen (lua_State *L) {
  int n = 0;
  size_t len;
  const char *s = luaL_checklstring(L, 1, &len);
  lua_Integer posi = u_posrelat(luaL_optinteger(L, 2, 1), len);
  lua_Integer posj = u_posrelat(luaL_optinteger(L, 3, -1), len);
  luaL_argcheck(L, 1 <= posi && --posi <= (lua_Integer)len, 2,
                   "initial position out of string");
  luaL_argcheck(L, --posj < (lua_Integer)len, 3,
                   "final position out of string");
  while (posi <= posj) {
    const char *s1 = utf8_decode(s + posi, NULL);
    if (s1 == NULL) {  
      lua_pushnil(L);  
      lua_pushinteger(L, posi + 1);  
      return 2;
    }
    posi = s1 - s;
    n++;
  }
  lua_pushinteger(L, n);
  return 1;
}
static int codepoint (lua_State *L) {
  size_t len;
  const char *s = luaL_checklstring(L, 1, &len);
  lua_Integer posi = u_posrelat(luaL_optinteger(L, 2, 1), len);
  lua_Integer pose = u_posrelat(luaL_optinteger(L, 3, posi), len);
  int n;
  const char *se;
  luaL_argcheck(L, posi >= 1, 2, "out of range");
  luaL_argcheck(L, pose <= (lua_Integer)len, 3, "out of range");
  if (posi > pose) return 0;  
  if (pose - posi >= INT_MAX)  
    return luaL_error(L, "string slice too long");
  n = (int)(pose -  posi) + 1;
  luaL_checkstack(L, n, "string slice too long");
  n = 0;
  se = s + pose;
  for (s += posi - 1; s < se;) {
    int code;
    s = utf8_decode(s, &code);
    if (s == NULL)
      return luaL_error(L, "invalid UTF-8 code");
    lua_pushinteger(L, code);
    n++;
  }
  return n;
}
static void pushutfchar (lua_State *L, int arg) {
  lua_Integer code = luaL_checkinteger(L, arg);
  luaL_argcheck(L, 0 <= code && code <= MAXUNICODE, arg, "value out of range");
  lua_pushfstring(L, "%U", (long)code);
}
static int utfchar (lua_State *L) {
  int n = lua_gettop(L);  
  if (n == 1)  
    pushutfchar(L, 1);
  else {
    int i;
    luaL_Buffer b;
    luaL_buffinit(L, &b);
    for (i = 1; i <= n; i++) {
      pushutfchar(L, i);
      luaL_addvalue(&b);
    }
    luaL_pushresult(&b);
  }
  return 1;
}
static int byteoffset (lua_State *L) {
  size_t len;
  const char *s = luaL_checklstring(L, 1, &len);
  lua_Integer n  = luaL_checkinteger(L, 2);
  lua_Integer posi = (n >= 0) ? 1 : len + 1;
  posi = u_posrelat(luaL_optinteger(L, 3, posi), len);
  luaL_argcheck(L, 1 <= posi && --posi <= (lua_Integer)len, 3,
                   "position out of range");
  if (n == 0) {
    while (posi > 0 && iscont(s + posi)) posi--;
  }
  else {
    if (iscont(s + posi))
      return luaL_error(L, "initial position is a continuation byte");
    if (n < 0) {
       while (n < 0 && posi > 0) {  
         do {  
           posi--;
         } while (posi > 0 && iscont(s + posi));
         n++;
       }
     }
     else {
       n--;  
       while (n > 0 && posi < (lua_Integer)len) {
         do {  
           posi++;
         } while (iscont(s + posi));  
         n--;
       }
     }
  }
  if (n == 0)  
    lua_pushinteger(L, posi + 1);
  else  
    lua_pushnil(L);
  return 1;
}
static int iter_aux (lua_State *L) {
  size_t len;
  const char *s = luaL_checklstring(L, 1, &len);
  lua_Integer n = lua_tointeger(L, 2) - 1;
  if (n < 0)  
    n = 0;  
  else if (n < (lua_Integer)len) {
    n++;  
    while (iscont(s + n)) n++;  
  }
  if (n >= (lua_Integer)len)
    return 0;  
  else {
    int code;
    const char *next = utf8_decode(s + n, &code);
    if (next == NULL || iscont(next))
      return luaL_error(L, "invalid UTF-8 code");
    lua_pushinteger(L, n + 1);
    lua_pushinteger(L, code);
    return 2;
  }
}
static int iter_codes (lua_State *L) {
  luaL_checkstring(L, 1);
  lua_pushcfunction(L, iter_aux);
  lua_pushvalue(L, 1);
  lua_pushinteger(L, 0);
  return 3;
}
#define UTF8PATT	"[\0-\x7F\xC2-\xF4][\x80-\xBF]*"
static const luaL_Reg funcs[] = {
  {"offset", byteoffset},
  {"codepoint", codepoint},
  {"char", utfchar},
  {"len", utflen},
  {"codes", iter_codes},
  {"charpattern", NULL},
  {NULL, NULL}
};
LUAMOD_API int luaopen_utf8 (lua_State *L) {
  luaL_newlib(L, funcs);
  lua_pushlstring(L, UTF8PATT, sizeof(UTF8PATT)/sizeof(char) - 1);
  lua_setfield(L, -2, "charpattern");
  return 1;
}
#ifdef LUA_USE_LMATHX
#include <math.h>
#define LMATHX_NAME		"mathx"
#define LMATHX_VERSION	LMATHX_NAME " library for " LUA_VERSION " / Jun 2015"
#define A(i)	luaL_checknumber(L,i)
#define I(i)	((int)luaL_checkinteger(L,i))
#undef PI
#define PI	(l_mathop(3.141592653589793238462643383279502884))
#define	rad(x)	((x)*(PI/l_mathop(180.0)))
#define	deg(x)	((x)*(l_mathop(180.0)/PI))
static int Lfmax(lua_State *L)			
{
 int i,n=lua_gettop(L);
 lua_Number m=A(1);
 for (i=2; i<=n; i++) m=l_mathop(fmax)(m,A(i));
 lua_pushnumber(L,m);
 return 1;
}
static int Lfmin(lua_State *L)			
{
 int i,n=lua_gettop(L);
 lua_Number m=A(1);
 for (i=2; i<=n; i++) m=l_mathop(fmin)(m,A(i));
 lua_pushnumber(L,m);
 return 1;
}
static int Lfrexp(lua_State *L)			
{
 int e;
 lua_pushnumber(L,l_mathop(frexp)(A(1),&e));
 lua_pushinteger(L,e);
 return 2;
}
static int Lldexp(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(ldexp)(A(1),I(2)));
 return 1;
}
static int Lmodf(lua_State *L)			
{
 lua_Number ip;
 lua_Number fp=l_mathop(modf)(A(1),&ip);
 lua_pushnumber(L,ip);
 lua_pushnumber(L,fp);
 return 2;
}
static int Lfabs(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(fabs)(A(1)));
 return 1;
}
static int Lacos(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(acos)(A(1)));
 return 1;
}
static int Lacosh(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(acosh)(A(1)));
 return 1;
}
static int Lasin(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(asin)(A(1)));
 return 1;
}
static int Lasinh(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(asinh)(A(1)));
 return 1;
}
static int Latan(lua_State *L)			
{
 int n=lua_gettop(L);
 if (n==1)
  lua_pushnumber(L,l_mathop(atan)(A(1)));
 else
  lua_pushnumber(L,l_mathop(atan2)(A(1),A(2)));
 return 1;
}
static int Latan2(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(atan2)(A(1),A(2)));
 return 1;
}
static int Latanh(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(atanh)(A(1)));
 return 1;
}
static int Lcbrt(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(cbrt)(A(1)));
 return 1;
}
static int Lceil(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(ceil)(A(1)));
 return 1;
}
static int Lcopysign(lua_State *L)		
{
 lua_pushnumber(L,l_mathop(copysign)(A(1),A(2)));
 return 1;
}
static int Lcos(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(cos)(A(1)));
 return 1;
}
static int Lcosh(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(cosh)(A(1)));
 return 1;
}
static int Ldeg(lua_State *L)			
{
 lua_pushnumber(L,deg(A(1)));
 return 1;
}
static int Lerf(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(erf)(A(1)));
 return 1;
}
static int Lerfc(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(erfc)(A(1)));
 return 1;
}
static int Lexp(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(exp)(A(1)));
 return 1;
}
static int Lexp2(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(exp2)(A(1)));
 return 1;
}
static int Lexpm1(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(expm1)(A(1)));
 return 1;
}
static int Lfdim(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(fdim)(A(1),A(2)));
 return 1;
}
static int Lfloor(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(floor)(A(1)));
 return 1;
}
static int Lfma(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(fma)(A(1),A(2),A(3)));
 return 1;
}
static int Lfmod(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(fmod)(A(1),A(2)));
 return 1;
}
static int Lgamma(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(tgamma)(A(1)));
 return 1;
}
static int Lhypot(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(hypot)(A(1),A(2)));
 return 1;
}
static int Lisfinite(lua_State *L)		
{
 lua_pushboolean(L,isfinite(A(1)));
 return 1;
}
static int Lisinf(lua_State *L)			
{
 lua_pushboolean(L,isinf(A(1)));
 return 1;
}
static int Lisnan(lua_State *L)			
{
 lua_pushboolean(L,isnan(A(1)));
 return 1;
}
static int Lisnormal(lua_State *L)		
{
 lua_pushboolean(L,isnormal(A(1)));
 return 1;
}
static int Llgamma(lua_State *L)		
{
 lua_pushnumber(L,l_mathop(lgamma)(A(1)));
 return 1;
}
static int Llog(lua_State *L)			
{
 int n=lua_gettop(L);
 if (n==1)
  lua_pushnumber(L,l_mathop(log)(A(1)));
 else
 {
  lua_Number b=A(2);
  if (b==10.0)
   lua_pushnumber(L,l_mathop(log10)(A(1)));
  else if (b==2.0)
   lua_pushnumber(L,l_mathop(log2)(A(1)));
  else
   lua_pushnumber(L,l_mathop(log)(A(1))/l_mathop(log)(b));
 }
 return 1;
}
static int Llog10(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(log10)(A(1)));
 return 1;
}
static int Llog1p(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(log1p)(A(1)));
 return 1;
}
static int Llog2(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(log2)(A(1)));
 return 1;
}
static int Llogb(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(logb)(A(1)));
 return 1;
}
static int Lnearbyint(lua_State *L)		
{
 lua_pushnumber(L,l_mathop(nearbyint)(A(1)));
 return 1;
}
static int Lnextafter(lua_State *L)		
{
 lua_pushnumber(L,l_mathop(nextafter)(A(1),A(2)));
 return 1;
}
static int Lpow(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(pow)(A(1),A(2)));
 return 1;
}
static int Lrad(lua_State *L)			
{
 lua_pushnumber(L,rad(A(1)));
 return 1;
}
static int Lremainder(lua_State *L)		
{
 lua_pushnumber(L,l_mathop(remainder)(A(1),A(2)));
 return 1;
}
static int Lround(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(round)(A(1)));
 return 1;
}
static int Lscalbn(lua_State *L)		
{
 lua_pushnumber(L,l_mathop(scalbn)(A(1),A(2)));
 return 1;
}
static int Lsin(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(sin)(A(1)));
 return 1;
}
static int Lsinh(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(sinh)(A(1)));
 return 1;
}
static int Lsqrt(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(sqrt)(A(1)));
 return 1;
}
static int Ltan(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(tan)(A(1)));
 return 1;
}
static int Ltanh(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(tanh)(A(1)));
 return 1;
}
static int Ltrunc(lua_State *L)			
{
 lua_pushnumber(L,l_mathop(trunc)(A(1)));
 return 1;
}
static const luaL_Reg R[] =
{
	{ "fabs",	Lfabs },
	{ "acos",	Lacos },
	{ "acosh",	Lacosh },
	{ "asin",	Lasin },
	{ "asinh",	Lasinh },
	{ "atan",	Latan },
	{ "atan2",	Latan2 },
	{ "atanh",	Latanh },
	{ "cbrt",	Lcbrt },
	{ "ceil",	Lceil },
	{ "copysign",	Lcopysign },
	{ "cos",	Lcos },
	{ "cosh",	Lcosh },
	{ "deg",	Ldeg },
	{ "erf",	Lerf },
	{ "erfc",	Lerfc },
	{ "exp",	Lexp },
	{ "exp2",	Lexp2 },
	{ "expm1",	Lexpm1 },
	{ "fdim",	Lfdim },
	{ "floor",	Lfloor },
	{ "fma",	Lfma },
	{ "fmax",	Lfmax },
	{ "fmin",	Lfmin },
	{ "fmod",	Lfmod },
	{ "frexp",	Lfrexp },
	{ "gamma",	Lgamma },
	{ "hypot",	Lhypot },
	{ "isfinite",	Lisfinite },
	{ "isinf",	Lisinf },
	{ "isnan",	Lisnan },
	{ "isnormal",	Lisnormal },
	{ "ldexp",	Lldexp },
	{ "lgamma",	Llgamma },
	{ "log",	Llog },
	{ "log10",	Llog10 },
	{ "log1p",	Llog1p },
	{ "log2",	Llog2 },
	{ "logb",	Llogb },
	{ "modf",	Lmodf },
	{ "nearbyint",	Lnearbyint },
	{ "nextafter",	Lnextafter },
	{ "pow",	Lpow },
	{ "rad",	Lrad },
	{ "remainder",	Lremainder },
	{ "round",	Lround },
	{ "scalbn",	Lscalbn },
	{ "sin",	Lsin },
	{ "sinh",	Lsinh },
	{ "sqrt",	Lsqrt },
	{ "tan",	Ltan },
	{ "tanh",	Ltanh },
	{ "trunc",	Ltrunc },
	{ NULL,	NULL }
};
LUALIB_API int luaopen_mathx(lua_State *L)
{
 luaL_newlib(L,R);
 lua_pushliteral(L,"version");			
 lua_pushliteral(L,LMATHX_VERSION);
 lua_settable(L,-3);
 lua_pushnumber(L,INFINITY);	lua_setfield(L,-2,"inf");
 lua_pushnumber(L,NAN);		lua_setfield(L,-2,"nan");
 lua_pushnumber(L,PI);		lua_setfield(L,-2,"pi");
 return 1;
}
#endif
#ifdef LUA_USE_LFS
#ifdef NO_CHDIR
  #define chdir(p)  (-1)
  #define chdir_error "Function 'chdir' not provided by system"
#else
  #define chdir_error strerror(errno)
#endif
#ifdef _WIN32
  #ifndef fileno
    #define fileno(f) (_fileno(f))
  #endif
#else
#endif
#ifndef LFS_DO_NOT_USE_LARGE_FILE
#ifndef _WIN32
#ifndef _AIX
#define _FILE_OFFSET_BITS 64    
#else
#define _LARGE_FILES 1          
#endif
#endif
#endif
#ifdef _WIN32
#endif
#define _LARGEFILE64_SOURCE 1
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#ifdef _WIN32
#include <direct.h>
#include <windows.h>
#include <io.h>
#include <sys/locking.h>
#ifdef __BORLANDC__
#include <utime.h>
#else
#include <sys/utime.h>
#endif
#include <fcntl.h>
#define LFS_MAXPATHLEN MAX_PATH
#else
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>
#include <utime.h>
#include <sys/param.h>          
#ifdef MAXPATHLEN
#define LFS_MAXPATHLEN MAXPATHLEN
#else
#include <limits.h>             
#define LFS_MAXPATHLEN _POSIX_PATH_MAX
#endif
#endif
#define LFS_VERSION "1.8.0"
#define LFS_LIBNAME "lfs"
#if LUA_VERSION_NUM >= 503      
#ifndef luaL_optlong
#define luaL_optlong luaL_optinteger
#endif
#endif
#if LUA_VERSION_NUM >= 502
#define new_lib(L, l) (luaL_newlib(L, l))
#else
#define new_lib(L, l) (lua_newtable(L), luaL_register(L, NULL, l))
#endif
#ifdef NO_STRERROR
#define strerror(_)     "System unable to describe the error"
#endif
#define DIR_METATABLE "directory metatable"
typedef struct dir_data {
  int closed;
#ifdef _WIN32
  intptr_t hFile;
  char pattern[MAX_PATH + 1];
#else
  DIR *dir;
#endif
} dir_data;
#define LOCK_METATABLE "lock metatable"
#ifdef _WIN32
#ifdef __BORLANDC__
#define lfs_setmode(file, m)   (setmode(_fileno(file), m))
#define STAT_STRUCT struct stati64
#else
#define lfs_setmode(file, m)   (_setmode(_fileno(file), m))
#define STAT_STRUCT struct _stati64
#endif
#ifndef _S_IFLNK
#define _S_IFLNK 0x400
#endif
#ifndef S_ISDIR
#define S_ISDIR(mode)  (mode&_S_IFDIR)
#endif
#ifndef S_ISREG
#define S_ISREG(mode)  (mode&_S_IFREG)
#endif
#ifndef S_ISLNK
#define S_ISLNK(mode)  (mode&_S_IFLNK)
#endif
#ifndef S_ISSOCK
#define S_ISSOCK(mode)  (0)
#endif
#ifndef S_ISFIFO
#define S_ISFIFO(mode)  (0)
#endif
#ifndef S_ISCHR
#define S_ISCHR(mode)  (mode&_S_IFCHR)
#endif
#ifndef S_ISBLK
#define S_ISBLK(mode)  (0)
#endif
#define STAT_FUNC _stati64
#define LSTAT_FUNC lfs_win32_lstat
#else
#define _O_TEXT               0
#define _O_BINARY             0
#define lfs_setmode(file, m)   ((void)file, (void)m, 0)
#define STAT_STRUCT struct stat
#define STAT_FUNC stat
#define LSTAT_FUNC lstat
#endif
#ifdef _WIN32
#define lfs_mkdir _mkdir
#else
#define lfs_mkdir(path) (mkdir((path), \
    S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH))
#endif
#ifdef _WIN32
int lfs_win32_pusherror(lua_State * L)
{
  int en = GetLastError();
  lua_pushnil(L);
  if (en == ERROR_FILE_EXISTS || en == ERROR_SHARING_VIOLATION)
    lua_pushstring(L, "File exists");
  else
    lua_pushstring(L, strerror(en));
  return 2;
}
#define TICKS_PER_SECOND 10000000
#define EPOCH_DIFFERENCE 11644473600LL
time_t windowsToUnixTime(FILETIME ft)
{
  ULARGE_INTEGER uli;
  uli.LowPart = ft.dwLowDateTime;
  uli.HighPart = ft.dwHighDateTime;
  return (time_t) (uli.QuadPart / TICKS_PER_SECOND - EPOCH_DIFFERENCE);
}
int lfs_win32_lstat(const char *path, STAT_STRUCT * buffer)
{
  WIN32_FILE_ATTRIBUTE_DATA win32buffer;
  if (GetFileAttributesEx(path, GetFileExInfoStandard, &win32buffer)) {
    if (!(win32buffer.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT)) {
      return STAT_FUNC(path, buffer);
    }
    buffer->st_mode = _S_IFLNK;
    buffer->st_dev = 0;
    buffer->st_ino = 0;
    buffer->st_nlink = 0;
    buffer->st_uid = 0;
    buffer->st_gid = 0;
    buffer->st_rdev = 0;
    buffer->st_atime = windowsToUnixTime(win32buffer.ftLastAccessTime);
    buffer->st_mtime = windowsToUnixTime(win32buffer.ftLastWriteTime);
    buffer->st_ctime = windowsToUnixTime(win32buffer.ftCreationTime);
    buffer->st_size = 0;
    return 0;
  } else {
    return 1;
  }
}
#endif
static int _lfs_pusherror(lua_State * L, const char *info)
{
  lua_pushnil(L);
  if (info == NULL)
    lua_pushstring(L, strerror(errno));
  else
    lua_pushfstring(L, "%s: %s", info, strerror(errno));
  lua_pushinteger(L, errno);
  return 3;
}
static int _lfs_pushresult(lua_State * L, int res, const char *info)
{
  if (res == -1) {
    return _lfs_pusherror(L, info);
  } else {
    lua_pushboolean(L, 1);
    return 1;
  }
}
static int lfs_change_dir(lua_State * L)
{
  const char *path = luaL_checkstring(L, 1);
  if (chdir(path)) {
    lua_pushnil(L);
    lua_pushfstring(L, "Unable to change working directory to '%s'\n%s\n",
                    path, chdir_error);
    return 2;
  } else {
    lua_pushboolean(L, 1);
    return 1;
  }
}
static int lfs_get_dir(lua_State * L)
{
#ifdef NO_GETCWD
  lua_pushnil(L);
  lua_pushstring(L, "Function 'getcwd' not provided by system");
  return 2;
#else
  char *path = NULL;
  size_t size = LFS_MAXPATHLEN; 
  int result;
  while (1) {
    char *path2 = (char*) realloc(path, size);
    if (!path2) {               
      result = _lfs_pusherror(L, "lfs_get_dir realloc() failed");
      break;
    }
    path = path2;
    if (getcwd(path, size) != NULL) {
      lua_pushstring(L, path);
      result = 1;
      break;
    }
    if (errno != ERANGE) {      
      result = _lfs_pusherror(L, "lfs_get_dir getcwd() failed");
      break;
    }
    size *= 2;
  }
  free(path);
  return result;
#endif
}
static FILE *_lfs_check_file(lua_State * L, int idx, const char *funcname)
{
#if LUA_VERSION_NUM == 501
  FILE **fh = (FILE **) luaL_checkudata(L, idx, "FILE*");
  if (*fh == NULL) {
    luaL_error(L, "%s: closed file", funcname);
    return 0;
  } else
    return *fh;
#elif LUA_VERSION_NUM >= 502 && LUA_VERSION_NUM <= 504
  luaL_Stream *fh = (luaL_Stream *) luaL_checkudata(L, idx, "FILE*");
  if (fh->closef == 0 || fh->f == NULL) {
    luaL_error(L, "%s: closed file", funcname);
    return 0;
  } else
    return fh->f;
#else
#error unsupported Lua version
#endif
}
static int _lfs_file_lock(lua_State * L, FILE * fh, const char *mode,
                      const long start, long len, const char *funcname)
{
  int code;
#ifdef _WIN32
  int lkmode;
  switch (*mode) {
  case 'r':
    lkmode = LK_NBLCK;
    break;
  case 'w':
    lkmode = LK_NBLCK;
    break;
  case 'u':
    lkmode = LK_UNLCK;
    break;
  default:
    return luaL_error(L, "%s: invalid mode", funcname);
  }
  if (!len) {
    fseek(fh, 0L, SEEK_END);
    len = ftell(fh);
  }
  fseek(fh, start, SEEK_SET);
#ifdef __BORLANDC__
  code = locking(fileno(fh), lkmode, len);
#else
  code = _locking(fileno(fh), lkmode, len);
#endif
#else
  struct flock f;
  switch (*mode) {
  case 'w':
    f.l_type = F_WRLCK;
    break;
  case 'r':
    f.l_type = F_RDLCK;
    break;
  case 'u':
    f.l_type = F_UNLCK;
    break;
  default:
    return luaL_error(L, "%s: invalid mode", funcname);
  }
  f.l_whence = SEEK_SET;
  f.l_start = (off_t) start;
  f.l_len = (off_t) len;
  code = fcntl(fileno(fh), F_SETLK, &f);
#endif
  return (code != -1);
}
#ifdef _WIN32
typedef struct lfs_Lock {
  HANDLE fd;
} lfs_Lock;
static int lfs_lock_dir(lua_State * L)
{
  size_t pathl;
  HANDLE fd;
  lfs_Lock *lock;
  char *ln;
  const char *lockfile = "/lockfile.lfs";
  const char *path = luaL_checklstring(L, 1, &pathl);
  ln = (char *) malloc(pathl + strlen(lockfile) + 1);
  if (!ln) {
    lua_pushnil(L);
    lua_pushstring(L, strerror(errno));
    return 2;
  }
  strcpy(ln, path);
  strcat(ln, lockfile);
  fd = CreateFile(ln, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_DELETE_ON_CLOSE, NULL);
  free(ln);
  if (fd == INVALID_HANDLE_VALUE) {
    return lfs_win32_pusherror(L);
  }
  lock = (lfs_Lock *) lua_newuserdata(L, sizeof(lfs_Lock));
  lock->fd = fd;
  luaL_getmetatable(L, LOCK_METATABLE);
  lua_setmetatable(L, -2);
  return 1;
}
static int lfs_unlock_dir(lua_State * L)
{
  lfs_Lock *lock = (lfs_Lock *) luaL_checkudata(L, 1, LOCK_METATABLE);
  if (lock->fd != INVALID_HANDLE_VALUE) {
    CloseHandle(lock->fd);
    lock->fd = INVALID_HANDLE_VALUE;
  }
  return 0;
}
#else
typedef struct lfs_Lock {
  char *ln;
} lfs_Lock;
static int lfs_lock_dir(lua_State * L)
{
  lfs_Lock *lock;
  size_t pathl;
  char *ln;
  const char *lockfile = "/lockfile.lfs";
  const char *path = luaL_checklstring(L, 1, &pathl);
  lock = (lfs_Lock *) lua_newuserdata(L, sizeof(lfs_Lock));
  ln = (char *) malloc(pathl + strlen(lockfile) + 1);
  if (!ln) {
    lua_pushnil(L);
    lua_pushstring(L, strerror(errno));
    return 2;
  }
  strcpy(ln, path);
  strcat(ln, lockfile);
  if (symlink("lock", ln) == -1) {
    free(ln);
    lua_pushnil(L);
    lua_pushstring(L, strerror(errno));
    return 2;
  }
  lock->ln = ln;
  luaL_getmetatable(L, LOCK_METATABLE);
  lua_setmetatable(L, -2);
  return 1;
}
static int lfs_unlock_dir(lua_State * L)
{
  lfs_Lock *lock = (lfs_Lock *) luaL_checkudata(L, 1, LOCK_METATABLE);
  if (lock->ln) {
    unlink(lock->ln);
    free(lock->ln);
    lock->ln = NULL;
  }
  return 0;
}
#endif
static int lfs_g_setmode(lua_State * L, FILE * f, int arg)
{
  static const int mode[] = { _O_BINARY, _O_TEXT };
  static const char *const modenames[] = { "binary", "text", NULL };
  int op = luaL_checkoption(L, arg, NULL, modenames);
  int res = lfs_setmode(f, mode[op]);
  if (res != -1) {
    int i;
    lua_pushboolean(L, 1);
    for (i = 0; modenames[i] != NULL; i++) {
      if (mode[i] == res) {
        lua_pushstring(L, modenames[i]);
        return 2;
      }
    }
    lua_pushnil(L);
    return 2;
  } else {
    return _lfs_pusherror(L, NULL);
  }
}
static int lfs_f_setmode(lua_State * L)
{
  return lfs_g_setmode(L, _lfs_check_file(L, 1, "setmode"), 2);
}
static int file_lock(lua_State * L)
{
  FILE *fh = _lfs_check_file(L, 1, "lock");
  const char *mode = luaL_checkstring(L, 2);
  const long start = (long) luaL_optinteger(L, 3, 0);
  long len = (long) luaL_optinteger(L, 4, 0);
  if (_lfs_file_lock(L, fh, mode, start, len, "lock")) {
    lua_pushboolean(L, 1);
    return 1;
  } else {
    lua_pushnil(L);
    lua_pushfstring(L, "%s", strerror(errno));
    return 2;
  }
}
static int file_unlock(lua_State * L)
{
  FILE *fh = _lfs_check_file(L, 1, "unlock");
  const long start = (long) luaL_optinteger(L, 2, 0);
  long len = (long) luaL_optinteger(L, 3, 0);
  if (_lfs_file_lock(L, fh, "u", start, len, "unlock")) {
    lua_pushboolean(L, 1);
    return 1;
  } else {
    lua_pushnil(L);
    lua_pushfstring(L, "%s", strerror(errno));
    return 2;
  }
}
static int make_link(lua_State * L)
{
  const char *oldpath = luaL_checkstring(L, 1);
  const char *newpath = luaL_checkstring(L, 2);
#ifndef _WIN32
  return _lfs_pushresult(L,
                    (lua_toboolean(L, 3) ? symlink : link) (oldpath,
                                                            newpath),
                    NULL);
#else
  int symbolic = lua_toboolean(L, 3);
  STAT_STRUCT oldpathinfo;
  int is_dir = 0;
  if (STAT_FUNC(oldpath, &oldpathinfo) == 0) {
    is_dir = S_ISDIR(oldpathinfo.st_mode) != 0;
  }
  if (!symbolic && is_dir) {
    lua_pushnil(L);
    lua_pushstring(L,
                   "hard links to directories are not supported on Windows");
    return 2;
  }
  int result = symbolic ? CreateSymbolicLink(newpath, oldpath, is_dir)
      : CreateHardLink(newpath, oldpath, NULL);
  if (result) {
    return _lfs_pushresult(L, result, NULL);
  } else {
    lua_pushnil(L);
    lua_pushstring(L, symbolic ? "make_link CreateSymbolicLink() failed"
                   : "make_link CreateHardLink() failed");
    return 2;
  }
#endif
}
static int make_dir(lua_State * L)
{
  const char *path = luaL_checkstring(L, 1);
  return _lfs_pushresult(L, lfs_mkdir(path), NULL);
}
static int remove_dir(lua_State * L)
{
  const char *path = luaL_checkstring(L, 1);
  return _lfs_pushresult(L, rmdir(path), NULL);
}
static int dir_iter(lua_State * L)
{
#ifdef _WIN32
  struct _finddata_t c_file;
#else
  struct dirent *entry;
#endif
  dir_data *d = (dir_data *) luaL_checkudata(L, 1, DIR_METATABLE);
  luaL_argcheck(L, d->closed == 0, 1, "closed directory");
#ifdef _WIN32
  if (d->hFile == 0L) {         
    if ((d->hFile = _findfirst(d->pattern, &c_file)) == -1L) {
      lua_pushnil(L);
      lua_pushstring(L, strerror(errno));
      d->closed = 1;
      return 2;
    } else {
      lua_pushstring(L, c_file.name);
      return 1;
    }
  } else {                      
    if (_findnext(d->hFile, &c_file) == -1L) {
      _findclose(d->hFile);
      d->closed = 1;
      return 0;
    } else {
      lua_pushstring(L, c_file.name);
      return 1;
    }
  }
#else
  if ((entry = readdir(d->dir)) != NULL) {
    lua_pushstring(L, entry->d_name);
    return 1;
  } else {
    closedir(d->dir);
    d->closed = 1;
    return 0;
  }
#endif
}
static int dir_close(lua_State * L)
{
  dir_data *d = (dir_data *) lua_touserdata(L, 1);
#ifdef _WIN32
  if (!d->closed && d->hFile) {
    _findclose(d->hFile);
  }
#else
  if (!d->closed && d->dir) {
    closedir(d->dir);
  }
#endif
  d->closed = 1;
  return 0;
}
static int dir_iter_factory(lua_State * L)
{
  const char *path = luaL_checkstring(L, 1);
  dir_data *d;
  lua_pushcfunction(L, dir_iter);
  d = (dir_data *) lua_newuserdata(L, sizeof(dir_data));
  luaL_getmetatable(L, DIR_METATABLE);
  lua_setmetatable(L, -2);
  d->closed = 0;
#ifdef _WIN32
  d->hFile = 0L;
  if (strlen(path) > MAX_PATH - 2)
    luaL_error(L, "path too long: %s", path);
  else
    sprintf(d->pattern, "%s/*", path);
#else
  d->dir = opendir(path);
  if (d->dir == NULL)
    luaL_error(L, "cannot open %s: %s", path, strerror(errno));
#endif
#if LUA_VERSION_NUM >= 504
  lua_pushnil(L);
  lua_pushvalue(L, -2);
  return 4;
#else
  return 2;
#endif
}
static int dir_create_meta(lua_State * L)
{
  luaL_newmetatable(L, DIR_METATABLE);
  lua_newtable(L);
  lua_pushcfunction(L, dir_iter);
  lua_setfield(L, -2, "next");
  lua_pushcfunction(L, dir_close);
  lua_setfield(L, -2, "close");
  lua_setfield(L, -2, "__index");
  lua_pushcfunction(L, dir_close);
  lua_setfield(L, -2, "__gc");
#if LUA_VERSION_NUM >= 504
  lua_pushcfunction(L, dir_close);
  lua_setfield(L, -2, "__close");
#endif
  return 1;
}
static int lock_create_meta(lua_State * L)
{
  luaL_newmetatable(L, LOCK_METATABLE);
  lua_newtable(L);
  lua_pushcfunction(L, lfs_unlock_dir);
  lua_setfield(L, -2, "free");
  lua_setfield(L, -2, "__index");
  lua_pushcfunction(L, lfs_unlock_dir);
  lua_setfield(L, -2, "__gc");
  return 1;
}
#ifdef _WIN32
static const char *mode2string(unsigned short mode)
{
#else
static const char *mode2string(mode_t mode)
{
#endif
  if (S_ISREG(mode))
    return "file";
  else if (S_ISDIR(mode))
    return "directory";
  else if (S_ISLNK(mode))
    return "link";
  else if (S_ISSOCK(mode))
    return "socket";
  else if (S_ISFIFO(mode))
    return "named pipe";
  else if (S_ISCHR(mode))
    return "char device";
  else if (S_ISBLK(mode))
    return "block device";
  else
    return "other";
}
static int file_utime(lua_State * L)
{
  const char *file = luaL_checkstring(L, 1);
  struct utimbuf utb, *buf;
  if (lua_gettop(L) == 1)       
    buf = NULL;
  else {
    utb.actime = (time_t) luaL_optnumber(L, 2, 0);
    utb.modtime = (time_t) luaL_optinteger(L, 3, utb.actime);
    buf = &utb;
  }
  return _lfs_pushresult(L, utime(file, buf), NULL);
}
static void push_st_mode(lua_State * L, STAT_STRUCT * info)
{
  lua_pushstring(L, mode2string(info->st_mode));
}
static void push_st_dev(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_dev);
}
static void push_st_ino(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_ino);
}
static void push_st_nlink(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_nlink);
}
static void push_st_uid(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_uid);
}
static void push_st_gid(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_gid);
}
static void push_st_rdev(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_rdev);
}
static void push_st_atime(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_atime);
}
static void push_st_mtime(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_mtime);
}
static void push_st_ctime(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_ctime);
}
static void push_st_size(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_size);
}
#ifndef _WIN32
static void push_st_blocks(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_blocks);
}
static void push_st_blksize(lua_State * L, STAT_STRUCT * info)
{
  lua_pushinteger(L, (lua_Integer) info->st_blksize);
}
#endif
#ifdef _WIN32
static const char *perm2string(unsigned short mode)
{
  static char perms[10] = "---------";
  int i;
  for (i = 0; i < 9; i++)
    perms[i] = '-';
  if (mode & _S_IREAD) {
    perms[0] = 'r';
    perms[3] = 'r';
    perms[6] = 'r';
  }
  if (mode & _S_IWRITE) {
    perms[1] = 'w';
    perms[4] = 'w';
    perms[7] = 'w';
  }
  if (mode & _S_IEXEC) {
    perms[2] = 'x';
    perms[5] = 'x';
    perms[8] = 'x';
  }
  return perms;
}
#else
static const char *perm2string(mode_t mode)
{
  static char perms[10] = "---------";
  int i;
  for (i = 0; i < 9; i++)
    perms[i] = '-';
  if (mode & S_IRUSR)
    perms[0] = 'r';
  if (mode & S_IWUSR)
    perms[1] = 'w';
  if (mode & S_IXUSR)
    perms[2] = 'x';
  if (mode & S_IRGRP)
    perms[3] = 'r';
  if (mode & S_IWGRP)
    perms[4] = 'w';
  if (mode & S_IXGRP)
    perms[5] = 'x';
  if (mode & S_IROTH)
    perms[6] = 'r';
  if (mode & S_IWOTH)
    perms[7] = 'w';
  if (mode & S_IXOTH)
    perms[8] = 'x';
  return perms;
}
#endif
static void push_st_perm(lua_State * L, STAT_STRUCT * info)
{
  lua_pushstring(L, perm2string(info->st_mode));
}
typedef void (*_push_function)(lua_State * L, STAT_STRUCT * info);
struct _stat_members {
  const char *name;
  _push_function push;
};
struct _stat_members members[] = {
  { "mode", push_st_mode },
  { "dev", push_st_dev },
  { "ino", push_st_ino },
  { "nlink", push_st_nlink },
  { "uid", push_st_uid },
  { "gid", push_st_gid },
  { "rdev", push_st_rdev },
  { "access", push_st_atime },
  { "modification", push_st_mtime },
  { "change", push_st_ctime },
  { "size", push_st_size },
  { "permissions", push_st_perm },
#ifndef _WIN32
  { "blocks", push_st_blocks },
  { "blksize", push_st_blksize },
#endif
  { NULL, NULL }
};
static int _file_info_(lua_State * L,
                       int (*st)(const char *, STAT_STRUCT *))
{
  STAT_STRUCT info;
  const char *file = luaL_checkstring(L, 1);
  int i;
  if (st(file, &info)) {
    lua_pushnil(L);
    lua_pushfstring(L, "cannot obtain information from file '%s': %s",
                    file, strerror(errno));
    lua_pushinteger(L, errno);
    return 3;
  }
  if (lua_isstring(L, 2)) {
    const char *member = lua_tostring(L, 2);
    for (i = 0; members[i].name; i++) {
      if (strcmp(members[i].name, member) == 0) {
        members[i].push(L, &info);
        return 1;
      }
    }
    return luaL_error(L, "invalid attribute name '%s'", member);
  }
  lua_settop(L, 2);
  if (!lua_istable(L, 2)) {
    lua_newtable(L);
  }
  for (i = 0; members[i].name; i++) {
    lua_pushstring(L, members[i].name);
    members[i].push(L, &info);
    lua_rawset(L, -3);
  }
  return 1;
}
static int file_info(lua_State * L)
{
  return _file_info_(L, STAT_FUNC);
}
static int push_link_target(lua_State * L)
{
  const char *file = luaL_checkstring(L, 1);
#ifdef _WIN32
  HANDLE h = CreateFile(file, GENERIC_READ,
                        FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (h == INVALID_HANDLE_VALUE) {
    return lfs_win32_pusherror(L);
  }
#endif
  char *target = NULL;
  int tsize, size = 256;        
  int ok = 0;
  while (!ok) {
    char *target2 = (char*) realloc(target, size);
    if (!target2) {             
      break;
    }
    target = target2;
#ifdef _WIN32
    tsize = GetFinalPathNameByHandle(h, target, size, FILE_NAME_OPENED);
#else
    tsize = readlink(file, target, size);
#endif
    if (tsize < 0) {            
      break;
    }
    if (tsize < size) {
#ifdef _WIN32
      if (tsize > 4 && strncmp(target, "\\\\?\\", 4) == 0) {
        memmove_s(target, tsize - 3, target + 4, tsize - 3);
        tsize -= 4;
      }
#endif
      ok = 1;
      break;
    }
    size *= 2;
  }
  if (ok) {
    target[tsize] = '\0';
    lua_pushlstring(L, target, tsize);
  }
#ifdef _WIN32
  CloseHandle(h);
#endif
  free(target);
  return ok;
}
static int link_info(lua_State * L)
{
  int ret;
  if (lua_isstring(L, 2) && (strcmp(lua_tostring(L, 2), "target") == 0)) {
    int ok = push_link_target(L);
    return ok ? 1 : _lfs_pusherror(L, "could not obtain link target");
  }
  ret = _file_info_(L, LSTAT_FUNC);
  if (ret == 1 && lua_type(L, -1) == LUA_TTABLE) {
    int ok = push_link_target(L);
    if (ok) {
      lua_setfield(L, -2, "target");
    }
  }
  return ret;
}
static void set_info(lua_State * L)
{
  lua_pushliteral(L, "Copyright (C) 2003-2017 Kepler Project");
  lua_setfield(L, -2, "_COPYRIGHT");
  lua_pushliteral(L,
                  "LuaFileSystem is a Lua library developed to complement "
                  "the set of functions related to file systems offered by "
                  "the standard Lua distribution");
  lua_setfield(L, -2, "_DESCRIPTION");
  lua_pushliteral(L, "LuaFileSystem " LFS_VERSION);
  lua_setfield(L, -2, "_VERSION");
}
static const struct luaL_Reg fslib[] = {
  { "attributes", file_info },
  { "chdir", lfs_change_dir },
  { "currentdir", lfs_get_dir },
  { "dir", dir_iter_factory },
  { "link", make_link },
  { "lock", file_lock },
  { "mkdir", make_dir },
  { "rmdir", remove_dir },
  { "symlinkattributes", link_info },
  { "setmode", lfs_f_setmode },
  { "touch", file_utime },
  { "unlock", file_unlock },
  { "lock_dir", lfs_lock_dir },
  { NULL, NULL },
};
int luaopen_lfs(lua_State * L)
{
  dir_create_meta(L);
  lock_create_meta(L);
  new_lib(L, fslib);
  lua_pushvalue(L, -1);
  lua_setglobal(L, LFS_LIBNAME);
  set_info(L);
  return 1;
}
#endif
#ifdef LUA_USE_LSQLITE3
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define LUA_LIB
#include "lua536.h"
#if LUA_VERSION_NUM > 501
#ifndef lua_strlen
#define lua_strlen lua_rawlen
#endif
#define luaL_typerror(L,ndx,str) luaL_error(L,"bad argument %d (%s expected, got nil)",ndx,str)
#define luaL_register(L,name,reg) lua_newtable(L);luaL_setfuncs(L,reg,0)
#define luaL_openlib(L,name,reg,nup) luaL_setfuncs(L,reg,nup)
#if LUA_VERSION_NUM > 502
#define luaL_checkint(L,n)  ((int)luaL_checkinteger(L, (n)))
#endif
#endif
#include "sqlite3.h"
#if !defined(SQLITE_OMIT_PROGRESS_CALLBACK)
    #define SQLITE_OMIT_PROGRESS_CALLBACK 0
#endif
#if !defined(LSQLITE_OMIT_UPDATE_HOOK)
    #define LSQLITE_OMIT_UPDATE_HOOK 0
#endif
#if defined(LSQLITE_OMIT_OPEN_V2)
    #define SQLITE3_OPEN(L,filename,flags) sqlite3_open(L,filename)
#else
    #define SQLITE3_OPEN(L,filename,flags) sqlite3_open_v2(L,filename,flags,NULL)
#endif
typedef struct sdb sdb;
typedef struct sdb_vm sdb_vm;
typedef struct sdb_bu sdb_bu;
typedef struct sdb_func sdb_func;
struct sdb_func {
    int fn_step;
    int fn_finalize;
    int udata;
    sdb *db;
    char aggregate;
    sdb_func *next;
};
struct sdb {
    lua_State *L;
    sqlite3 *db;
    sdb_func *func;         
    int busy_cb;        
    int busy_udata;
    int progress_cb;    
    int progress_udata;
    int trace_cb;       
    int trace_udata;
#if !defined(LSQLITE_OMIT_UPDATE_HOOK) || !LSQLITE_OMIT_UPDATE_HOOK
    int update_hook_cb; 
    int update_hook_udata;
    int commit_hook_cb; 
    int commit_hook_udata;
    int rollback_hook_cb; 
    int rollback_hook_udata;
#endif
};
static const char *sqlite_meta      = ":sqlite3";
static const char *sqlite_vm_meta   = ":sqlite3:vm";
static const char *sqlite_bu_meta   = ":sqlite3:bu";
static const char *sqlite_ctx_meta  = ":sqlite3:ctx";
static int sqlite_ctx_meta_ref;
#if LUA_VERSION_NUM > 502
#define PUSH_INT64(L,i64in,fallback) \
    do { \
        sqlite_int64 i64 = i64in; \
        lua_Integer i = (lua_Integer )i64; \
        if (i == i64) lua_pushinteger(L, i);\
        else { \
            lua_Number n = (lua_Number)i64; \
            if (n == i64) lua_pushnumber(L, n); \
            else fallback; \
        } \
    } while (0)
#else
#define PUSH_INT64(L,i64in,fallback) \
    do { \
        sqlite_int64 i64 = i64in; \
        lua_Number n = (lua_Number)i64; \
        if (n == i64) lua_pushnumber(L, n); \
        else fallback; \
    } while (0)
#endif
static void vm_push_column(lua_State *L, sqlite3_stmt *vm, int idx) {
    switch (sqlite3_column_type(vm, idx)) {
        case SQLITE_INTEGER:
            PUSH_INT64(L, sqlite3_column_int64(vm, idx)
                     , lua_pushlstring(L, (const char*)sqlite3_column_text(vm, idx)
                                        , sqlite3_column_bytes(vm, idx)));
            break;
        case SQLITE_FLOAT:
            lua_pushnumber(L, sqlite3_column_double(vm, idx));
            break;
        case SQLITE_TEXT:
            lua_pushlstring(L, (const char*) sqlite3_column_text(vm, idx), sqlite3_column_bytes(vm, idx));
            break;
        case SQLITE_BLOB:
            lua_pushlstring(L, (const char*) sqlite3_column_blob(vm, idx), sqlite3_column_bytes(vm, idx));
            break;
        case SQLITE_NULL:
            lua_pushnil(L);
            break;
        default:
            lua_pushnil(L);
            break;
    }
}
struct sdb_vm {
    sdb *db;                
    sqlite3_stmt *vm;       
    int columns;            
    char has_values;        
    char temp;              
};
static sdb_vm *newvm(lua_State *L, sdb *db) {
    sdb_vm *svm = (sdb_vm*)lua_newuserdata(L, sizeof(sdb_vm)); 
    luaL_getmetatable(L, sqlite_vm_meta);
    lua_setmetatable(L, -2);        
    svm->db = db;
    svm->columns = 0;
    svm->has_values = 0;
    svm->vm = NULL;
    svm->temp = 0;
    lua_pushlightuserdata(L, db);     
    lua_rawget(L, LUA_REGISTRYINDEX); 
    lua_pushlightuserdata(L, svm);    
    lua_pushvalue(L, -5);             
    lua_rawset(L, -3);                
    lua_pop(L, 1);                    
    return svm;
}
static int cleanupvm(lua_State *L, sdb_vm *svm) {
    lua_pushlightuserdata(L, svm->db);
    lua_rawget(L, LUA_REGISTRYINDEX);
    lua_pushlightuserdata(L, svm);
    lua_pushnil(L);
    lua_rawset(L, -3);
    lua_pop(L, 1);
    svm->columns = 0;
    svm->has_values = 0;
    if (!svm->vm) return 0;
    lua_pushinteger(L, sqlite3_finalize(svm->vm));
    svm->vm = NULL;
    return 1;
}
static int stepvm(lua_State *L, sdb_vm *svm) {
	(void)L;
    return sqlite3_step(svm->vm);
}
static sdb_vm *lsqlite_getvm(lua_State *L, int index) {
    sdb_vm *svm = (sdb_vm*)luaL_checkudata(L, index, sqlite_vm_meta);
    if (svm == NULL) luaL_argerror(L, index, "bad sqlite virtual machine");
    return svm;
}
static sdb_vm *lsqlite_checkvm(lua_State *L, int index) {
    sdb_vm *svm = lsqlite_getvm(L, index);
    if (svm->vm == NULL) luaL_argerror(L, index, "attempt to use closed sqlite virtual machine");
    return svm;
}
static int dbvm_isopen(lua_State *L) {
    sdb_vm *svm = lsqlite_getvm(L, 1);
    lua_pushboolean(L, svm->vm != NULL ? 1 : 0);
    return 1;
}
static int dbvm_tostring(lua_State *L) {
    char buff[39];
    sdb_vm *svm = lsqlite_getvm(L, 1);
    if (svm->vm == NULL)
        strcpy(buff, "closed");
    else
        sprintf(buff, "%p", svm);
    lua_pushfstring(L, "sqlite virtual machine (%s)", buff);
    return 1;
}
static int dbvm_gc(lua_State *L) {
    sdb_vm *svm = lsqlite_getvm(L, 1);
    if (svm->vm != NULL)  
        cleanupvm(L, svm);
    return 0;
}
static int dbvm_step(lua_State *L) {
    int result;
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    result = stepvm(L, svm);
    svm->has_values = result == SQLITE_ROW ? 1 : 0;
    svm->columns = sqlite3_data_count(svm->vm);
    lua_pushinteger(L, result);
    return 1;
}
static int dbvm_finalize(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    return cleanupvm(L, svm);
}
static int dbvm_reset(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_reset(svm->vm);
    lua_pushinteger(L, sqlite3_errcode(svm->db->db));
    return 1;
}
static void dbvm_check_contents(lua_State *L, sdb_vm *svm) {
    if (!svm->has_values) {
        luaL_error(L, "misuse of function");
    }
}
static void dbvm_check_index(lua_State *L, sdb_vm *svm, int index) {
    if (index < 0 || index >= svm->columns) {
        luaL_error(L, "index out of range [0..%d]", svm->columns - 1);
    }
}
static void dbvm_check_bind_index(lua_State *L, sdb_vm *svm, int index) {
    if (index < 1 || index > sqlite3_bind_parameter_count(svm->vm)) {
        luaL_error(L, "bind index out of range [1..%d]", sqlite3_bind_parameter_count(svm->vm));
    }
}
static int dbvm_last_insert_rowid(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite_int64 rowid = sqlite3_last_insert_rowid(svm->db->db);
    PUSH_INT64(L, rowid, lua_pushfstring(L, "%ll", rowid));
    return 1;
}
static int dbvm_columns(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    lua_pushinteger(L, sqlite3_column_count(svm->vm));
    return 1;
}
static int dbvm_get_value(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    int index = luaL_checkint(L, 2);
    dbvm_check_contents(L, svm);
    dbvm_check_index(L, svm, index);
    vm_push_column(L, svm->vm, index);
    return 1;
}
static int dbvm_get_name(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    int index = luaL_checknumber(L, 2);
    dbvm_check_index(L, svm, index);
    lua_pushstring(L, sqlite3_column_name(svm->vm, index));
    return 1;
}
static int dbvm_get_type(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    int index = luaL_checknumber(L, 2);
    dbvm_check_index(L, svm, index);
    lua_pushstring(L, sqlite3_column_decltype(svm->vm, index));
    return 1;
}
static int dbvm_get_values(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int columns = svm->columns;
    int n;
    dbvm_check_contents(L, svm);
    lua_createtable(L, columns, 0);
    for (n = 0; n < columns;) {
        vm_push_column(L, vm, n++);
        lua_rawseti(L, -2, n);
    }
    return 1;
}
static int dbvm_get_names(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int columns = sqlite3_column_count(vm); 
    int n;
    lua_createtable(L, columns, 0);
    for (n = 0; n < columns;) {
        lua_pushstring(L, sqlite3_column_name(vm, n++));
        lua_rawseti(L, -2, n);
    }
    return 1;
}
static int dbvm_get_types(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int columns = sqlite3_column_count(vm); 
    int n;
    lua_createtable(L, columns, 0);
    for (n = 0; n < columns;) {
        lua_pushstring(L, sqlite3_column_decltype(vm, n++));
        lua_rawseti(L, -2, n);
    }
    return 1;
}
static int dbvm_get_uvalues(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int columns = svm->columns;
    int n;
    dbvm_check_contents(L, svm);
    lua_checkstack(L, columns);
    for (n = 0; n < columns; ++n)
        vm_push_column(L, vm, n);
    return columns;
}
static int dbvm_get_unames(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int columns = sqlite3_column_count(vm); 
    int n;
    lua_checkstack(L, columns);
    for (n = 0; n < columns; ++n)
        lua_pushstring(L, sqlite3_column_name(vm, n));
    return columns;
}
static int dbvm_get_utypes(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int columns = sqlite3_column_count(vm); 
    int n;
    lua_checkstack(L, columns);
    for (n = 0; n < columns; ++n)
        lua_pushstring(L, sqlite3_column_decltype(vm, n));
    return columns;
}
static int dbvm_get_named_values(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int columns = svm->columns;
    int n;
    dbvm_check_contents(L, svm);
    lua_createtable(L, 0, columns);
    for (n = 0; n < columns; ++n) {
        lua_pushstring(L, sqlite3_column_name(vm, n));
        vm_push_column(L, vm, n);
        lua_rawset(L, -3);
    }
    return 1;
}
static int dbvm_get_named_types(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int columns = sqlite3_column_count(vm);
    int n;
    lua_createtable(L, 0, columns);
    for (n = 0; n < columns; ++n) {
        lua_pushstring(L, sqlite3_column_name(vm, n));
        lua_pushstring(L, sqlite3_column_decltype(vm, n));
        lua_rawset(L, -3);
    }
    return 1;
}
static int dbvm_bind_index(lua_State *L, sqlite3_stmt *vm, int index, int lindex) {
    switch (lua_type(L, lindex)) {
        case LUA_TSTRING:
            return sqlite3_bind_text(vm, index, lua_tostring(L, lindex), lua_strlen(L, lindex), SQLITE_TRANSIENT);
        case LUA_TNUMBER:
#if LUA_VERSION_NUM > 502
            if (lua_isinteger(L, lindex))
                return sqlite3_bind_int64(vm, index, lua_tointeger(L, lindex));
#endif
            return sqlite3_bind_double(vm, index, lua_tonumber(L, lindex));
        case LUA_TBOOLEAN:
            return sqlite3_bind_int(vm, index, lua_toboolean(L, lindex) ? 1 : 0);
        case LUA_TNONE:
        case LUA_TNIL:
            return sqlite3_bind_null(vm, index);
        default:
            luaL_error(L, "index (%d) - invalid data type for bind (%s)", index, lua_typename(L, lua_type(L, lindex)));
            return SQLITE_MISUSE; 
    }
}
static int dbvm_bind_parameter_count(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    lua_pushinteger(L, sqlite3_bind_parameter_count(svm->vm));
    return 1;
}
static int dbvm_bind_parameter_name(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    int index = luaL_checknumber(L, 2);
    dbvm_check_bind_index(L, svm, index);
    lua_pushstring(L, sqlite3_bind_parameter_name(svm->vm, index));
    return 1;
}
static int dbvm_bind(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int index = luaL_checkint(L, 2);
    int result;
    dbvm_check_bind_index(L, svm, index);
    result = dbvm_bind_index(L, vm, index, 3);
    lua_pushinteger(L, result);
    return 1;
}
static int dbvm_bind_blob(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    int index = luaL_checkint(L, 2);
    const char *value = luaL_checkstring(L, 3);
    int len = lua_strlen(L, 3);
    lua_pushinteger(L, sqlite3_bind_blob(svm->vm, index, value, len, SQLITE_TRANSIENT));
    return 1;
}
static int dbvm_bind_values(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int top = lua_gettop(L);
    int result, n;
    if (top - 1 != sqlite3_bind_parameter_count(vm))
        luaL_error(L,
            "incorrect number of parameters to bind (%d given, %d to bind)",
            top - 1,
            sqlite3_bind_parameter_count(vm)
        );
    for (n = 2; n <= top; ++n) {
        if ((result = dbvm_bind_index(L, vm, n - 1, n)) != SQLITE_OK) {
            lua_pushinteger(L, result);
            return 1;
        }
    }
    lua_pushinteger(L, SQLITE_OK);
    return 1;
}
static int dbvm_bind_table_fields (lua_State *L, int idx, int n, sqlite3_stmt *vm) {
    const char *name;
    int result, i;
    for ( i = 1; i <= n; ++i ) {
        name = sqlite3_bind_parameter_name(vm, i );
        if (name && (name[0] == ':' || name[0] == '$')) {
            lua_pushstring(L, ++name);
            lua_gettable(L, idx);
            result = dbvm_bind_index(L, vm, i, -1);
            lua_pop(L, 1);
        }
        else {
            lua_pushinteger(L, i );
            lua_gettable(L, idx);
            result = dbvm_bind_index(L, vm, i, -1);
            lua_pop(L, 1);
        }
        if (result != SQLITE_OK) {
            return result;
        }
    }
    return SQLITE_OK;
}
static int dbvm_bind_names(lua_State *L) {
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm = svm->vm;
    int count = sqlite3_bind_parameter_count(vm);
    int result;
    luaL_checktype(L, 2, LUA_TTABLE);
    result = dbvm_bind_table_fields (L, 2, count, vm);
    lua_pushinteger(L, result);
    return 1;
}
static sdb *newdb (lua_State *L) {
    sdb *db = (sdb*)lua_newuserdata(L, sizeof(sdb));
    db->L = L;
    db->db = NULL;  
    db->func = NULL;
    db->busy_cb =
    db->busy_udata =
    db->progress_cb =
    db->progress_udata =
    db->trace_cb =
    db->trace_udata =
#if !defined(LSQLITE_OMIT_UPDATE_HOOK) || !LSQLITE_OMIT_UPDATE_HOOK
    db->update_hook_cb =
    db->update_hook_udata =
    db->commit_hook_cb =
    db->commit_hook_udata =
    db->rollback_hook_cb =
    db->rollback_hook_udata =
#endif
     LUA_NOREF;
    luaL_getmetatable(L, sqlite_meta);
    lua_setmetatable(L, -2);        
    lua_pushlightuserdata(L, db);
    lua_newtable(L);
    lua_rawset(L, LUA_REGISTRYINDEX);
    return db;
}
static int cleanupdb(lua_State *L, sdb *db) {
    sdb_func *func;
    sdb_func *func_next;
    int top;
    int result;
    lua_pushlightuserdata(L, db);
    lua_rawget(L, LUA_REGISTRYINDEX);
    top = lua_gettop(L);
    lua_pushnil(L);
    while (lua_next(L, -2)) {
        sdb_vm *svm = (sdb_vm*) lua_touserdata(L, -2); 
        cleanupvm(L, svm);
        lua_settop(L, top);
        lua_pushnil(L);
    }
    lua_pop(L, 1); 
    lua_pushlightuserdata(L, db);
    lua_pushnil(L);
    lua_rawset(L, LUA_REGISTRYINDEX);
    luaL_unref(L, LUA_REGISTRYINDEX, db->busy_cb);
    luaL_unref(L, LUA_REGISTRYINDEX, db->busy_udata);
    luaL_unref(L, LUA_REGISTRYINDEX, db->progress_cb);
    luaL_unref(L, LUA_REGISTRYINDEX, db->progress_udata);
    luaL_unref(L, LUA_REGISTRYINDEX, db->trace_cb);
    luaL_unref(L, LUA_REGISTRYINDEX, db->trace_udata);
#if !defined(LSQLITE_OMIT_UPDATE_HOOK) || !LSQLITE_OMIT_UPDATE_HOOK
    luaL_unref(L, LUA_REGISTRYINDEX, db->update_hook_cb);
    luaL_unref(L, LUA_REGISTRYINDEX, db->update_hook_udata);
    luaL_unref(L, LUA_REGISTRYINDEX, db->commit_hook_cb);
    luaL_unref(L, LUA_REGISTRYINDEX, db->commit_hook_udata);
    luaL_unref(L, LUA_REGISTRYINDEX, db->rollback_hook_cb);
    luaL_unref(L, LUA_REGISTRYINDEX, db->rollback_hook_udata);
#endif
    result = sqlite3_close(db->db);
    db->db = NULL;
    func = db->func;
    while (func) {
        func_next = func->next;
        luaL_unref(L, LUA_REGISTRYINDEX, func->fn_step);
        luaL_unref(L, LUA_REGISTRYINDEX, func->fn_finalize);
        luaL_unref(L, LUA_REGISTRYINDEX, func->udata);
        free(func);
        func = func_next;
    }
    db->func = NULL;
    return result;
}
static sdb *lsqlite_getdb(lua_State *L, int index) {
    sdb *db = (sdb*)luaL_checkudata(L, index, sqlite_meta);
    if (db == NULL) luaL_typerror(L, index, "sqlite database");
    return db;
}
static sdb *lsqlite_checkdb(lua_State *L, int index) {
    sdb *db = lsqlite_getdb(L, index);
    if (db->db == NULL) luaL_argerror(L, index, "attempt to use closed sqlite database");
    return db;
}
typedef struct {
    sqlite3_context *ctx;
    int ud;
} lcontext;
static lcontext *lsqlite_make_context(lua_State *L) {
    lcontext *ctx = (lcontext*)lua_newuserdata(L, sizeof(lcontext));
    lua_rawgeti(L, LUA_REGISTRYINDEX, sqlite_ctx_meta_ref);
    lua_setmetatable(L, -2);
    ctx->ctx = NULL;
    ctx->ud = LUA_NOREF;
    return ctx;
}
static lcontext *lsqlite_getcontext(lua_State *L, int index) {
    lcontext *ctx = (lcontext*)luaL_checkudata(L, index, sqlite_ctx_meta);
    if (ctx == NULL) luaL_typerror(L, index, "sqlite context");
    return ctx;
}
static lcontext *lsqlite_checkcontext(lua_State *L, int index) {
    lcontext *ctx = lsqlite_getcontext(L, index);
    if (ctx->ctx == NULL) luaL_argerror(L, index, "invalid sqlite context");
    return ctx;
}
static int lcontext_tostring(lua_State *L) {
    char buff[39];
    lcontext *ctx = lsqlite_getcontext(L, 1);
    if (ctx->ctx == NULL)
        strcpy(buff, "closed");
    else
        sprintf(buff, "%p", ctx->ctx);
    lua_pushfstring(L, "sqlite function context (%s)", buff);
    return 1;
}
static void lcontext_check_aggregate(lua_State *L, lcontext *ctx) {
    sdb_func *func = (sdb_func*)sqlite3_user_data(ctx->ctx);
    if (!func->aggregate) {
        luaL_error(L, "attempt to call aggregate method from scalar function");
    }
}
static int lcontext_user_data(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    sdb_func *func = (sdb_func*)sqlite3_user_data(ctx->ctx);
    lua_rawgeti(L, LUA_REGISTRYINDEX, func->udata);
    return 1;
}
static int lcontext_get_aggregate_context(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    lcontext_check_aggregate(L, ctx);
    lua_rawgeti(L, LUA_REGISTRYINDEX, ctx->ud);
    return 1;
}
static int lcontext_set_aggregate_context(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    lcontext_check_aggregate(L, ctx);
    lua_settop(L, 2);
    luaL_unref(L, LUA_REGISTRYINDEX, ctx->ud);
    ctx->ud = luaL_ref(L, LUA_REGISTRYINDEX);
    return 0;
}
static int lcontext_aggregate_count(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    lcontext_check_aggregate(L, ctx);
    lua_pushinteger(L, sqlite3_aggregate_count(ctx->ctx));
    return 1;
}
#if 0
void *sqlite3_get_auxdata(sqlite3_context*, int);
void sqlite3_set_auxdata(sqlite3_context*, int, void*, void (*)(void*));
#endif
static int lcontext_result(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    switch (lua_type(L, 2)) {
        case LUA_TNUMBER:
#if LUA_VERSION_NUM > 502
            if (lua_isinteger(L, 2))
                sqlite3_result_int64(ctx->ctx, luaL_checkinteger(L, 2));
            else
#endif
            sqlite3_result_double(ctx->ctx, luaL_checknumber(L, 2));
            break;
        case LUA_TSTRING:
            sqlite3_result_text(ctx->ctx, luaL_checkstring(L, 2), lua_strlen(L, 2), SQLITE_TRANSIENT);
            break;
        case LUA_TNIL:
        case LUA_TNONE:
            sqlite3_result_null(ctx->ctx);
            break;
        default:
            luaL_error(L, "invalid result type %s", lua_typename(L, 2));
            break;
    }
    return 0;
}
static int lcontext_result_blob(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    const char *blob = luaL_checkstring(L, 2);
    int size = lua_strlen(L, 2);
    sqlite3_result_blob(ctx->ctx, (const void*)blob, size, SQLITE_TRANSIENT);
    return 0;
}
static int lcontext_result_double(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    double d = luaL_checknumber(L, 2);
    sqlite3_result_double(ctx->ctx, d);
    return 0;
}
static int lcontext_result_error(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    const char *err = luaL_checkstring(L, 2);
    int size = lua_strlen(L, 2);
    sqlite3_result_error(ctx->ctx, err, size);
    return 0;
}
static int lcontext_result_int(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    int i = luaL_checkint(L, 2);
    sqlite3_result_int(ctx->ctx, i);
    return 0;
}
static int lcontext_result_null(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    sqlite3_result_null(ctx->ctx);
    return 0;
}
static int lcontext_result_text(lua_State *L) {
    lcontext *ctx = lsqlite_checkcontext(L, 1);
    const char *text = luaL_checkstring(L, 2);
    int size = lua_strlen(L, 2);
    sqlite3_result_text(ctx->ctx, text, size, SQLITE_TRANSIENT);
    return 0;
}
static int db_isopen(lua_State *L) {
    sdb *db = lsqlite_getdb(L, 1);
    lua_pushboolean(L, db->db != NULL ? 1 : 0);
    return 1;
}
static int db_last_insert_rowid(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    sqlite_int64 rowid = sqlite3_last_insert_rowid(db->db);
    PUSH_INT64(L, rowid, lua_pushfstring(L, "%ll", rowid));
    return 1;
}
static int db_changes(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    lua_pushinteger(L, sqlite3_changes(db->db));
    return 1;
}
static int db_total_changes(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    lua_pushinteger(L, sqlite3_total_changes(db->db));
    return 1;
}
static int db_errcode(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    lua_pushinteger(L, sqlite3_errcode(db->db));
    return 1;
}
static int db_errmsg(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    lua_pushstring(L, sqlite3_errmsg(db->db));
    return 1;
}
static int db_interrupt(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    sqlite3_interrupt(db->db);
    return 0;
}
static int db_db_filename(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    const char *db_name = luaL_checkstring(L, 2);
    lua_pushstring(L, sqlite3_db_filename(db->db, db_name));
    return 1;
}
static void db_push_value(lua_State *L, sqlite3_value *value) {
    switch (sqlite3_value_type(value)) {
        case SQLITE_TEXT:
            lua_pushlstring(L, (const char*)sqlite3_value_text(value), sqlite3_value_bytes(value));
            break;
        case SQLITE_INTEGER:
            PUSH_INT64(L, sqlite3_value_int64(value)
                        , lua_pushlstring(L, (const char*)sqlite3_value_text(value)
                                            , sqlite3_value_bytes(value)));
            break;
        case SQLITE_FLOAT:
            lua_pushnumber(L, sqlite3_value_double(value));
            break;
        case SQLITE_BLOB:
            lua_pushlstring(L, (const char*) sqlite3_value_blob(value), sqlite3_value_bytes(value));
            break;
        case SQLITE_NULL:
            lua_pushnil(L);
            break;
        default:
            lua_pushnil(L);
            break;
    }
}
static void db_sql_normal_function(sqlite3_context *context, int argc, sqlite3_value **argv) {
    sdb_func *func = (sdb_func*)sqlite3_user_data(context);
    lua_State *L = func->db->L;
    int n;
    lcontext *ctx;
    int top = lua_gettop(L);
    lua_checkstack(L, argc + 3);
    lua_rawgeti(L, LUA_REGISTRYINDEX, func->fn_step);   
    if (!func->aggregate) {
        ctx = lsqlite_make_context(L); 
    }
    else {
        void *p = sqlite3_aggregate_context(context, 1);
        lua_pushlightuserdata(L, p);
        lua_rawget(L, LUA_REGISTRYINDEX);       
        if (lua_isnil(L, -1)) { 
            lua_pop(L, 1);
            ctx = lsqlite_make_context(L);
            lua_pushlightuserdata(L, p);
            lua_pushvalue(L, -2);
            lua_rawset(L, LUA_REGISTRYINDEX);
        }
        else
            ctx = lsqlite_getcontext(L, -1);
    }
    for (n = 0; n < argc; ++n) {
        db_push_value(L, argv[n]);
    }
    ctx->ctx = context;
    if (lua_pcall(L, argc + 1, 0, 0)) {
        const char *errmsg = lua_tostring(L, -1);
        int size = lua_strlen(L, -1);
        sqlite3_result_error(context, errmsg, size);
    }
    ctx->ctx = NULL;
    if (!func->aggregate) {
        luaL_unref(L, LUA_REGISTRYINDEX, ctx->ud);
    }
    lua_settop(L, top);
}
static void db_sql_finalize_function(sqlite3_context *context) {
    sdb_func *func = (sdb_func*)sqlite3_user_data(context);
    lua_State *L = func->db->L;
    void *p = sqlite3_aggregate_context(context, 1); 
    lcontext *ctx;
    int top = lua_gettop(L);
    lua_rawgeti(L, LUA_REGISTRYINDEX, func->fn_finalize);   
    lua_pushlightuserdata(L, p);
    lua_rawget(L, LUA_REGISTRYINDEX);       
    if (lua_isnil(L, -1)) { 
        lua_pop(L, 1);
        ctx = lsqlite_make_context(L);
        lua_pushlightuserdata(L, p);
        lua_pushvalue(L, -2);
        lua_rawset(L, LUA_REGISTRYINDEX);
    }
    else
        ctx = lsqlite_getcontext(L, -1);
    ctx->ctx = context;
    if (lua_pcall(L, 1, 0, 0)) {
        sqlite3_result_error(context, lua_tostring(L, -1), -1);
    }
    ctx->ctx = NULL;
    luaL_unref(L, LUA_REGISTRYINDEX, ctx->ud);
    lua_pushlightuserdata(L, p);
    lua_pushnil(L);
    lua_rawset(L, LUA_REGISTRYINDEX);
    lua_settop(L, top);
}
static int db_register_function(lua_State *L, int aggregate) {
    sdb *db = lsqlite_checkdb(L, 1);
    const char *name;
    int args;
    int result;
    sdb_func *func;
    if (aggregate) aggregate = 1;
    name = luaL_checkstring(L, 2);
    args = luaL_checkint(L, 3);
    luaL_checktype(L, 4, LUA_TFUNCTION);
    if (aggregate) luaL_checktype(L, 5, LUA_TFUNCTION);
    func = (sdb_func*)malloc(sizeof(sdb_func));
    if (func == NULL) {
        luaL_error(L, "out of memory");
    }
    result = sqlite3_create_function(
        db->db, name, args, SQLITE_UTF8, func,
        aggregate ? NULL : db_sql_normal_function,
        aggregate ? db_sql_normal_function : NULL,
        aggregate ? db_sql_finalize_function : NULL
    );
    if (result == SQLITE_OK) {
        lua_settop(L, 5 + aggregate);
        func->db = db;
        func->aggregate = aggregate;
        func->next = db->func;
        db->func = func;
        lua_pushvalue(L, 4);
        func->fn_step = luaL_ref(L, LUA_REGISTRYINDEX);
        lua_pushvalue(L, 5+aggregate);
        func->udata = luaL_ref(L, LUA_REGISTRYINDEX);
        if (aggregate) {
            lua_pushvalue(L, 5);
            func->fn_finalize = luaL_ref(L, LUA_REGISTRYINDEX);
        }
        else
            func->fn_finalize = LUA_NOREF;
    }
    else {
        free(func);
    }
    lua_pushboolean(L, result == SQLITE_OK ? 1 : 0);
    return 1;
}
static int db_create_function(lua_State *L) {
    return db_register_function(L, 0);
}
static int db_create_aggregate(lua_State *L) {
    return db_register_function(L, 1);
}
typedef struct {
    lua_State *L;
    int ref;
} scc;
static int collwrapper(scc *co,int l1,const void *p1,
                        int l2,const void *p2) {
    int res=0;
    lua_State *L=co->L;
    lua_rawgeti(L,LUA_REGISTRYINDEX,co->ref);
    lua_pushlstring(L, (const char*) p1, l1);
    lua_pushlstring(L, (const char*) p2, l2);
    if (lua_pcall(L,2,1,0)==0) res=(int)lua_tonumber(L,-1);
    lua_pop(L,1);
    return res;
}
static void collfree(scc *co) {
    if (co) {
        luaL_unref(co->L,LUA_REGISTRYINDEX,co->ref);
        free(co);
    }
}
static int db_create_collation(lua_State *L) {
    sdb *db=lsqlite_checkdb(L,1);
    const char *collname=luaL_checkstring(L,2);
    scc *co=NULL;
    int (*collfunc)(scc *,int,const void *,int,const void *)=NULL;
    lua_settop(L,3); 
    if (lua_isfunction(L,3)) collfunc=collwrapper;
    else if (!lua_isnil(L,3))
        luaL_error(L,"create_collation: function or nil expected");
    if (collfunc != NULL) {
        co=(scc *)malloc(sizeof(scc)); 
        if (co) {
            co->L=L;
            co->ref=luaL_ref(L,LUA_REGISTRYINDEX);
        }
        else luaL_error(L,"create_collation: could not allocate callback");
    }
    sqlite3_create_collation_v2(db->db, collname, SQLITE_UTF8,
        (void *)co,
        (int(*)(void*,int,const void*,int,const void*))collfunc,
        (void(*)(void*))collfree);
    return 0;
}
static int db_load_extension(lua_State *L) {
    sdb *db=lsqlite_checkdb(L,1);
    const char *extname=luaL_optstring(L,2,NULL);
    const char *entrypoint=luaL_optstring(L,3,NULL);
    int result;
    char *errmsg = NULL;
    if (extname == NULL) {
        result = sqlite3_enable_load_extension(db->db,0); 
    }
    else {
        sqlite3_enable_load_extension(db->db,1); 
        result = sqlite3_load_extension(db->db,extname,entrypoint,&errmsg);
    }
    if (result == SQLITE_OK) {
        lua_pushboolean(L,1);
        return 1;
    }
    lua_pushboolean(L,0); 
    lua_pushstring(L,errmsg);
    sqlite3_free(errmsg);
    return 2;
}
static void db_trace_callback(void *user, const char *sql) {
    sdb *db = (sdb*)user;
    lua_State *L = db->L;
    int top = lua_gettop(L);
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->trace_cb);    
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->trace_udata); 
    lua_pushstring(L, sql); 
    lua_pcall(L, 2, 0, 0);
    lua_settop(L, top);
}
static int db_trace(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    if (lua_gettop(L) < 2 || lua_isnil(L, 2)) {
        luaL_unref(L, LUA_REGISTRYINDEX, db->trace_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->trace_udata);
        db->trace_cb =
        db->trace_udata = LUA_NOREF;
        sqlite3_trace(db->db, NULL, NULL);
    }
    else {
        luaL_checktype(L, 2, LUA_TFUNCTION);
        lua_settop(L, 3);
        luaL_unref(L, LUA_REGISTRYINDEX, db->trace_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->trace_udata);
        db->trace_udata = luaL_ref(L, LUA_REGISTRYINDEX);
        db->trace_cb = luaL_ref(L, LUA_REGISTRYINDEX);
        sqlite3_trace(db->db, db_trace_callback, db);
    }
    return 0;
}
#if !defined(LSQLITE_OMIT_UPDATE_HOOK) || !LSQLITE_OMIT_UPDATE_HOOK
static void db_update_hook_callback(void *user, int op, char const *dbname, char const *tblname, sqlite3_int64 rowid) {
    sdb *db = (sdb*)user;
    lua_State *L = db->L;
    int top = lua_gettop(L);
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->update_hook_cb);    
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->update_hook_udata); 
    lua_pushinteger(L, op);
    lua_pushstring(L, dbname); 
    lua_pushstring(L, tblname); 
    PUSH_INT64(L, rowid, lua_pushfstring(L, "%ll", rowid));
    lua_pcall(L, 5, 0, 0);
    lua_settop(L, top);
}
static int db_update_hook(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    if (lua_gettop(L) < 2 || lua_isnil(L, 2)) {
        luaL_unref(L, LUA_REGISTRYINDEX, db->update_hook_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->update_hook_udata);
        db->update_hook_cb =
        db->update_hook_udata = LUA_NOREF;
        sqlite3_update_hook(db->db, NULL, NULL);
    }
    else {
        luaL_checktype(L, 2, LUA_TFUNCTION);
        lua_settop(L, 3);
        luaL_unref(L, LUA_REGISTRYINDEX, db->update_hook_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->update_hook_udata);
        db->update_hook_udata = luaL_ref(L, LUA_REGISTRYINDEX);
        db->update_hook_cb = luaL_ref(L, LUA_REGISTRYINDEX);
        sqlite3_update_hook(db->db, db_update_hook_callback, db);
    }
    return 0;
}
static int db_commit_hook_callback(void *user) {
    sdb *db = (sdb*)user;
    lua_State *L = db->L;
    int top = lua_gettop(L);
    int rollback = 0;
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->commit_hook_cb);    
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->commit_hook_udata); 
    if (!lua_pcall(L, 1, 1, 0))
        rollback = lua_toboolean(L, -1); 
    lua_settop(L, top);
    return rollback;
}
static int db_commit_hook(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    if (lua_gettop(L) < 2 || lua_isnil(L, 2)) {
        luaL_unref(L, LUA_REGISTRYINDEX, db->commit_hook_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->commit_hook_udata);
        db->commit_hook_cb =
        db->commit_hook_udata = LUA_NOREF;
        sqlite3_commit_hook(db->db, NULL, NULL);
    }
    else {
        luaL_checktype(L, 2, LUA_TFUNCTION);
        lua_settop(L, 3);
        luaL_unref(L, LUA_REGISTRYINDEX, db->commit_hook_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->commit_hook_udata);
        db->commit_hook_udata = luaL_ref(L, LUA_REGISTRYINDEX);
        db->commit_hook_cb = luaL_ref(L, LUA_REGISTRYINDEX);
        sqlite3_commit_hook(db->db, db_commit_hook_callback, db);
    }
    return 0;
}
static void db_rollback_hook_callback(void *user) {
    sdb *db = (sdb*)user;
    lua_State *L = db->L;
    int top = lua_gettop(L);
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->rollback_hook_cb);    
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->rollback_hook_udata); 
    lua_pcall(L, 1, 0, 0);
    lua_settop(L, top);
}
static int db_rollback_hook(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    if (lua_gettop(L) < 2 || lua_isnil(L, 2)) {
        luaL_unref(L, LUA_REGISTRYINDEX, db->rollback_hook_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->rollback_hook_udata);
        db->rollback_hook_cb =
        db->rollback_hook_udata = LUA_NOREF;
        sqlite3_rollback_hook(db->db, NULL, NULL);
    }
    else {
        luaL_checktype(L, 2, LUA_TFUNCTION);
        lua_settop(L, 3);
        luaL_unref(L, LUA_REGISTRYINDEX, db->rollback_hook_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->rollback_hook_udata);
        db->rollback_hook_udata = luaL_ref(L, LUA_REGISTRYINDEX);
        db->rollback_hook_cb = luaL_ref(L, LUA_REGISTRYINDEX);
        sqlite3_rollback_hook(db->db, db_rollback_hook_callback, db);
    }
    return 0;
}
#endif 
#if !defined(SQLITE_OMIT_PROGRESS_CALLBACK) || !SQLITE_OMIT_PROGRESS_CALLBACK
static int db_progress_callback(void *user) {
    int result = 1; 
    sdb *db = (sdb*)user;
    lua_State *L = db->L;
    int top = lua_gettop(L);
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->progress_cb);
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->progress_udata);
    if (!lua_pcall(L, 1, 1, 0))
        result = lua_toboolean(L, -1);
    lua_settop(L, top);
    return result;
}
static int db_progress_handler(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    if (lua_gettop(L) < 2 || lua_isnil(L, 2)) {
        luaL_unref(L, LUA_REGISTRYINDEX, db->progress_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->progress_udata);
        db->progress_cb =
        db->progress_udata = LUA_NOREF;
        sqlite3_progress_handler(db->db, 0, NULL, NULL);
    }
    else {
        int nop = luaL_checkint(L, 2);  
        luaL_checktype(L, 3, LUA_TFUNCTION);
        lua_settop(L, 4);
        luaL_unref(L, LUA_REGISTRYINDEX, db->progress_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->progress_udata);
        db->progress_udata = luaL_ref(L, LUA_REGISTRYINDEX);
        db->progress_cb = luaL_ref(L, LUA_REGISTRYINDEX);
        sqlite3_progress_handler(db->db, nop, db_progress_callback, db);
    }
    return 0;
}
#else 
static int db_progress_handler(lua_State *L) {
    lua_pushliteral(L, "progress callback support disabled at compile time");
    lua_error(L);
    return 0;
}
#endif 
#if 0
sqlite3_backup *sqlite3_backup_init(
  sqlite3 *pDest,                        
  const char *zDestName,                 
  sqlite3 *pSource,                      
  const char *zSourceName                
);
int sqlite3_backup_step(sqlite3_backup *p, int nPage);
int sqlite3_backup_finish(sqlite3_backup *p);
int sqlite3_backup_remaining(sqlite3_backup *p);
int sqlite3_backup_pagecount(sqlite3_backup *p);
#endif
struct sdb_bu {
    sqlite3_backup *bu;     
};
static int cleanupbu(lua_State *L, sdb_bu *sbu) {
    if (!sbu->bu) return 0; 
    lua_pushlightuserdata(L, sbu->bu);
    lua_pushnil(L);
    lua_rawset(L, LUA_REGISTRYINDEX);
    lua_pushinteger(L, sqlite3_backup_finish(sbu->bu));
    sbu->bu = NULL;
    return 1;
}
static int lsqlite_backup_init(lua_State *L) {
    sdb *target_db = lsqlite_checkdb(L, 1);
    const char *target_nm = luaL_checkstring(L, 2);
    sdb *source_db = lsqlite_checkdb(L, 3);
    const char *source_nm = luaL_checkstring(L, 4);
    sqlite3_backup *bu = sqlite3_backup_init(target_db->db, target_nm, source_db->db, source_nm);
    if (NULL != bu) {
        sdb_bu *sbu = (sdb_bu*)lua_newuserdata(L, sizeof(sdb_bu));
        luaL_getmetatable(L, sqlite_bu_meta);
        lua_setmetatable(L, -2);        
        sbu->bu = bu;
        lua_pushlightuserdata(L, bu);
        lua_createtable(L, 2, 0);
        lua_pushvalue(L, 1); 
        lua_rawseti(L, -2, 1);
        lua_pushvalue(L, 3); 
        lua_rawseti(L, -2, 2);
        lua_rawset(L, LUA_REGISTRYINDEX);
        return 1;
    }
    else {
        return 0;
    }
}
static sdb_bu *lsqlite_getbu(lua_State *L, int index) {
    sdb_bu *sbu = (sdb_bu*)luaL_checkudata(L, index, sqlite_bu_meta);
    if (sbu == NULL) luaL_typerror(L, index, "sqlite database backup");
    return sbu;
}
static sdb_bu *lsqlite_checkbu(lua_State *L, int index) {
    sdb_bu *sbu = lsqlite_getbu(L, index);
    if (sbu->bu == NULL) luaL_argerror(L, index, "attempt to use closed sqlite database backup");
    return sbu;
}
static int dbbu_gc(lua_State *L) {
    sdb_bu *sbu = lsqlite_getbu(L, 1);
    if (sbu->bu != NULL) {
        cleanupbu(L, sbu);
        lua_pop(L, 1);
    }
    return 0;
}
static int dbbu_step(lua_State *L) {
    sdb_bu *sbu = lsqlite_checkbu(L, 1);
    int nPage = luaL_checkint(L, 2);
    lua_pushinteger(L, sqlite3_backup_step(sbu->bu, nPage));
    return 1;
}
static int dbbu_remaining(lua_State *L) {
    sdb_bu *sbu = lsqlite_checkbu(L, 1);
    lua_pushinteger(L, sqlite3_backup_remaining(sbu->bu));
    return 1;
}
static int dbbu_pagecount(lua_State *L) {
    sdb_bu *sbu = lsqlite_checkbu(L, 1);
    lua_pushinteger(L, sqlite3_backup_pagecount(sbu->bu));
    return 1;
}
static int dbbu_finish(lua_State *L) {
    sdb_bu *sbu = lsqlite_checkbu(L, 1);
    return cleanupbu(L, sbu);
}
static int db_busy_callback(void *user, int tries) {
    int retry = 0; 
    sdb *db = (sdb*)user;
    lua_State *L = db->L;
    int top = lua_gettop(L);
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->busy_cb);
    lua_rawgeti(L, LUA_REGISTRYINDEX, db->busy_udata);
    lua_pushinteger(L, tries);
    if (!lua_pcall(L, 2, 1, 0))
        retry = lua_toboolean(L, -1);
    lua_settop(L, top);
    return retry;
}
static int db_busy_handler(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    if (lua_gettop(L) < 2 || lua_isnil(L, 2)) {
        luaL_unref(L, LUA_REGISTRYINDEX, db->busy_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->busy_udata);
        db->busy_cb =
        db->busy_udata = LUA_NOREF;
        sqlite3_busy_handler(db->db, NULL, NULL);
    }
    else {
        luaL_checktype(L, 2, LUA_TFUNCTION);
        lua_settop(L, 3);
        luaL_unref(L, LUA_REGISTRYINDEX, db->busy_cb);
        luaL_unref(L, LUA_REGISTRYINDEX, db->busy_udata);
        db->busy_udata = luaL_ref(L, LUA_REGISTRYINDEX);
        db->busy_cb = luaL_ref(L, LUA_REGISTRYINDEX);
        sqlite3_busy_handler(db->db, db_busy_callback, db);
    }
    return 0;
}
static int db_busy_timeout(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    int timeout = luaL_checkint(L, 2);
    sqlite3_busy_timeout(db->db, timeout);
    luaL_unref(L, LUA_REGISTRYINDEX, db->busy_cb);
    luaL_unref(L, LUA_REGISTRYINDEX, db->busy_udata);
    db->busy_cb =
    db->busy_udata = LUA_NOREF;
    return 0;
}
static int db_exec_callback(void* user, int columns, char **data, char **names) {
    int result = SQLITE_ABORT; 
    lua_State *L = (lua_State*)user;
    int n;
    int top = lua_gettop(L);
    lua_pushvalue(L, 3); 
    lua_pushvalue(L, 4); 
    lua_pushinteger(L, columns); 
    lua_pushvalue(L, 6);
    for (n = 0; n < columns;) {
        lua_pushstring(L, data[n++]);
        lua_rawseti(L, -2, n);
    }
    lua_pushvalue(L, 5);
    if (lua_isnil(L, -1)) {
        lua_pop(L, 1);
        lua_createtable(L, columns, 0);
        lua_pushvalue(L, -1);
        lua_replace(L, 5);
        for (n = 0; n < columns;) {
            lua_pushstring(L, names[n++]);
            lua_rawseti(L, -2, n);
        }
    }
    if (!lua_pcall(L, 4, 1, 0)) {
#if LUA_VERSION_NUM > 502
        if (lua_isinteger(L, -1))
            result = lua_tointeger(L, -1);
        else
#endif
        if (lua_isnumber(L, -1))
            result = lua_tonumber(L, -1);
    }
    lua_settop(L, top);
    return result;
}
static int db_exec(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    const char *sql = luaL_checkstring(L, 2);
    int result;
    if (!lua_isnoneornil(L, 3)) {
        luaL_checktype(L, 3, LUA_TFUNCTION);
        lua_settop(L, 4);   
        lua_pushnil(L);     
        lua_newtable(L);    
        result = sqlite3_exec(db->db, sql, db_exec_callback, L, NULL);
    }
    else {
        result = sqlite3_exec(db->db, sql, NULL, NULL, NULL);
    }
    lua_pushinteger(L, result);
    return 1;
}
static int db_prepare(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    const char *sql = luaL_checkstring(L, 2);
    int sql_len = lua_strlen(L, 2);
    const char *sqltail;
    sdb_vm *svm;
    lua_settop(L,2); 
    svm = newvm(L, db);
    if (sqlite3_prepare_v2(db->db, sql, sql_len, &svm->vm, &sqltail) != SQLITE_OK) {
        lua_pushnil(L);
        lua_pushinteger(L, sqlite3_errcode(db->db));
        if (cleanupvm(L, svm) == 1)
            lua_pop(L, 1); 
        return 2;
    }
    lua_pushstring(L, sqltail);
    return 2;
}
static int db_do_next_row(lua_State *L, int packed) {
    int result;
    sdb_vm *svm = lsqlite_checkvm(L, 1);
    sqlite3_stmt *vm;
    int columns;
    int i;
    result = stepvm(L, svm);
    vm = svm->vm; 
    svm->has_values = result == SQLITE_ROW ? 1 : 0;
    svm->columns = columns = sqlite3_data_count(vm);
    if (result == SQLITE_ROW) {
        if (packed) {
            if (packed == 1) {
                lua_createtable(L, columns, 0);
                for (i = 0; i < columns;) {
                    vm_push_column(L, vm, i);
                    lua_rawseti(L, -2, ++i);
                }
            }
            else {
                lua_createtable(L, 0, columns);
                for (i = 0; i < columns; ++i) {
                    lua_pushstring(L, sqlite3_column_name(vm, i));
                    vm_push_column(L, vm, i);
                    lua_rawset(L, -3);
                }
            }
            return 1;
        }
        else {
            lua_checkstack(L, columns);
            for (i = 0; i < columns; ++i)
                vm_push_column(L, vm, i);
            return svm->columns;
        }
    }
    if (svm->temp) {
        result = sqlite3_finalize(vm);
        svm->vm = NULL;
        cleanupvm(L, svm);
    }
    else if (result == SQLITE_DONE) {
        result = sqlite3_reset(vm);
    }
    if (result != SQLITE_OK) {
        lua_pushstring(L, sqlite3_errmsg(svm->db->db));
        lua_error(L);
    }
    return 0;
}
static int db_next_row(lua_State *L) {
    return db_do_next_row(L, 0);
}
static int db_next_packed_row(lua_State *L) {
    return db_do_next_row(L, 1);
}
static int db_next_named_row(lua_State *L) {
    return db_do_next_row(L, 2);
}
static int dbvm_do_rows(lua_State *L, int(*f)(lua_State *)) {
    lsqlite_checkvm(L, 1);
    lua_pushvalue(L,1);
    lua_pushcfunction(L, f);
    lua_insert(L, -2);
    return 2;
}
static int dbvm_rows(lua_State *L) {
    return dbvm_do_rows(L, db_next_packed_row);
}
static int dbvm_nrows(lua_State *L) {
    return dbvm_do_rows(L, db_next_named_row);
}
static int dbvm_urows(lua_State *L) {
    return dbvm_do_rows(L, db_next_row);
}
static int db_do_rows(lua_State *L, int(*f)(lua_State *)) {
    sdb *db = lsqlite_checkdb(L, 1);
    const char *sql = luaL_checkstring(L, 2);
    sdb_vm *svm;
    int nargs = lua_gettop(L) - 2;
    if (nargs > 0) {
        lua_pushvalue(L, 1);
        lua_pushvalue(L, 2);    
    }
    svm = newvm(L, db);
    svm->temp = 1;
    if (sqlite3_prepare_v2(db->db, sql, -1, &svm->vm, NULL) != SQLITE_OK) {
        lua_pushstring(L, sqlite3_errmsg(svm->db->db));
        if (cleanupvm(L, svm) == 1)
            lua_pop(L, 1); 
        lua_error(L);
    }
    if (nargs > 0) {
        lua_replace(L, 1);
        lua_remove(L, 2);  
        if (nargs == 1 && lua_istable(L, 2)) {
            int result;
            if ((result = dbvm_bind_table_fields (L, 2, nargs, svm->vm)) != SQLITE_OK) {
                lua_pushstring(L, sqlite3_errstr(result));
                cleanupvm(L, svm);
                lua_error(L);
            }
        } else if (nargs == sqlite3_bind_parameter_count(svm->vm)) {
            int result, i;
            for (i = 1; i <= nargs; i++) {
                if ((result = dbvm_bind_index(L, svm->vm, i, i + 1)) != SQLITE_OK) {
                    lua_pushstring(L, sqlite3_errstr(result));
                    cleanupvm(L, svm);
                    lua_error(L);
                }
            }
        } else {
            luaL_error(L, "Required either %d parameters or a single table, got %d.",
                sqlite3_bind_parameter_count(svm->vm), nargs);
        }
        lua_pop(L, nargs);
        lua_pushvalue(L, 1);
    }
    lua_pushcfunction(L, f);
    lua_insert(L, -2);
    return 2;
}
static int db_rows(lua_State *L) {
    return db_do_rows(L, db_next_packed_row);
}
static int db_nrows(lua_State *L) {
    return db_do_rows(L, db_next_named_row);
}
static int db_urows(lua_State *L) {
    return db_do_rows(L, db_next_row);
}
static int db_tostring(lua_State *L) {
    char buff[32];
    sdb *db = lsqlite_getdb(L, 1);
    if (db->db == NULL)
        strcpy(buff, "closed");
    else
        sprintf(buff, "%p", lua_touserdata(L, 1));
    lua_pushfstring(L, "sqlite database (%s)", buff);
    return 1;
}
static int db_close(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    lua_pushinteger(L, cleanupdb(L, db));
    return 1;
}
static int db_close_vm(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    int temp = lua_toboolean(L, 2);
    lua_pushlightuserdata(L, db);
    lua_rawget(L, LUA_REGISTRYINDEX);
    lua_pushnil(L);
    while (lua_next(L, -2)) {
        sdb_vm *svm = (sdb_vm*) lua_touserdata(L, -2); 
        if ((!temp || svm->temp) && svm->vm)
        {
            sqlite3_finalize(svm->vm);
            svm->vm = NULL;
        }
        lua_pop(L, 1);
    }
    return 0;
}
static int db_get_ptr(lua_State *L) {
    sdb *db = lsqlite_checkdb(L, 1);
    lua_pushlightuserdata(L, db->db);
    return 1;
}
static int db_gc(lua_State *L) {
    sdb *db = lsqlite_getdb(L, 1);
    if (db->db != NULL)  
        cleanupdb(L, db);
    return 0;
}
static int lsqlite_version(lua_State *L) {
    lua_pushstring(L, sqlite3_libversion());
    return 1;
}
static int lsqlite_complete(lua_State *L) {
    const char *sql = luaL_checkstring(L, 1);
    lua_pushboolean(L, sqlite3_complete(sql));
    return 1;
}
#ifndef _WIN32
static int lsqlite_temp_directory(lua_State *L) {
    const char *oldtemp = sqlite3_temp_directory;
    if (!lua_isnone(L, 1)) {
        const char *temp = luaL_optstring(L, 1, NULL);
        if (sqlite3_temp_directory) {
            sqlite3_free((char*)sqlite3_temp_directory);
        }
        if (temp) {
            sqlite3_temp_directory = sqlite3_mprintf("%s", temp);
        }
        else {
            sqlite3_temp_directory = NULL;
        }
    }
    lua_pushstring(L, oldtemp);
    return 1;
}
#endif
static int lsqlite_do_open(lua_State *L, const char *filename, int flags) {
    sdb *db = newdb(L); 
    if (SQLITE3_OPEN(filename, &db->db, flags) == SQLITE_OK) {
        return 1;
    }
    lua_pushnil(L);                             
    lua_pushinteger(L, sqlite3_errcode(db->db));
    lua_pushstring(L, sqlite3_errmsg(db->db));  
    cleanupdb(L, db);
    return 3;
}
static int lsqlite_open(lua_State *L) {
    const char *filename = luaL_checkstring(L, 1);
    int flags = luaL_optinteger(L, 2, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE);
    return lsqlite_do_open(L, filename, flags);
}
static int lsqlite_open_memory(lua_State *L) {
    return lsqlite_do_open(L, ":memory:", SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE);
}
static int lsqlite_open_ptr(lua_State *L) {
    sqlite3 *db_ptr;
    sdb *db;
    int rc;
    luaL_checktype(L, 1, LUA_TLIGHTUSERDATA);
    db_ptr = (sqlite3*) lua_touserdata(L, 1);
    rc = sqlite3_exec(db_ptr, NULL, NULL, NULL, NULL);
    if (rc != SQLITE_OK)
        luaL_argerror(L, 1, "not a valid SQLite3 pointer");
    db = newdb(L); 
    db->db = db_ptr;
    return 1;
}
static int lsqlite_newindex(lua_State *L) {
    lua_pushliteral(L, "attempt to change readonly table");
    lua_error(L);
    return 0;
}
#ifndef LSQLITE_VERSION
#define LSQLITE_VERSION "2021-08-24"
#endif
static int lsqlite_lversion(lua_State *L) {
    lua_pushstring(L, LSQLITE_VERSION);
    return 1;
}
#define SC(s)   { #s, SQLITE_ ## s },
#define LSC(s)  { #s, LSQLITE_ ## s },
static const struct {
    const char* name;
    int value;
} sqlite_constants[] = {
    SC(OK)          SC(ERROR)       SC(INTERNAL)    SC(PERM)
    SC(ABORT)       SC(BUSY)        SC(LOCKED)      SC(NOMEM)
    SC(READONLY)    SC(INTERRUPT)   SC(IOERR)       SC(CORRUPT)
    SC(NOTFOUND)    SC(FULL)        SC(CANTOPEN)    SC(PROTOCOL)
    SC(EMPTY)       SC(SCHEMA)      SC(TOOBIG)      SC(CONSTRAINT)
    SC(MISMATCH)    SC(MISUSE)      SC(NOLFS)
    SC(FORMAT)      SC(NOTADB)
    SC(RANGE)       SC(ROW)         SC(DONE)
    SC(INTEGER)     SC(FLOAT)       SC(TEXT)        SC(BLOB)
    SC(NULL)
    SC(CREATE_INDEX       )
    SC(CREATE_TABLE       )
    SC(CREATE_TEMP_INDEX  )
    SC(CREATE_TEMP_TABLE  )
    SC(CREATE_TEMP_TRIGGER)
    SC(CREATE_TEMP_VIEW   )
    SC(CREATE_TRIGGER     )
    SC(CREATE_VIEW        )
    SC(DELETE             )
    SC(DROP_INDEX         )
    SC(DROP_TABLE         )
    SC(DROP_TEMP_INDEX    )
    SC(DROP_TEMP_TABLE    )
    SC(DROP_TEMP_TRIGGER  )
    SC(DROP_TEMP_VIEW     )
    SC(DROP_TRIGGER       )
    SC(DROP_VIEW          )
    SC(INSERT             )
    SC(PRAGMA             )
    SC(READ               )
    SC(SELECT             )
    SC(TRANSACTION        )
    SC(UPDATE             )
    SC(ATTACH             )
    SC(DETACH             )
    SC(ALTER_TABLE        )
    SC(REINDEX            )
    SC(ANALYZE            )
    SC(CREATE_VTABLE      )
    SC(DROP_VTABLE        )
    SC(FUNCTION           )
    SC(SAVEPOINT          )
    SC(OPEN_READONLY)
    SC(OPEN_READWRITE)
    SC(OPEN_CREATE)
    SC(OPEN_URI)
    SC(OPEN_MEMORY)
    SC(OPEN_NOMUTEX)
    SC(OPEN_FULLMUTEX)
    SC(OPEN_SHAREDCACHE)
    SC(OPEN_PRIVATECACHE)
    { NULL, 0 }
};
static const luaL_Reg lsqlite3_dblib[] = {
    {"isopen",              db_isopen               },
    {"last_insert_rowid",   db_last_insert_rowid    },
    {"changes",             db_changes              },
    {"total_changes",       db_total_changes        },
    {"errcode",             db_errcode              },
    {"error_code",          db_errcode              },
    {"errmsg",              db_errmsg               },
    {"error_message",       db_errmsg               },
    {"interrupt",           db_interrupt            },
    {"db_filename",         db_db_filename          },
    {"create_function",     db_create_function      },
    {"create_aggregate",    db_create_aggregate     },
    {"create_collation",    db_create_collation     },
    {"load_extension",      db_load_extension       },
    {"trace",               db_trace                },
    {"progress_handler",    db_progress_handler     },
    {"busy_timeout",        db_busy_timeout         },
    {"busy_handler",        db_busy_handler         },
#if !defined(LSQLITE_OMIT_UPDATE_HOOK) || !LSQLITE_OMIT_UPDATE_HOOK
    {"update_hook",         db_update_hook          },
    {"commit_hook",         db_commit_hook          },
    {"rollback_hook",       db_rollback_hook        },
#endif
    {"prepare",             db_prepare              },
    {"rows",                db_rows                 },
    {"urows",               db_urows                },
    {"nrows",               db_nrows                },
    {"exec",                db_exec                 },
    {"execute",             db_exec                 },
    {"close",               db_close                },
    {"close_vm",            db_close_vm             },
    {"get_ptr",             db_get_ptr              },
    {"__tostring",          db_tostring             },
    {"__gc",                db_gc                   },
    {NULL, NULL}
};
static const luaL_Reg vmlib[] = {
    {"isopen",              dbvm_isopen             },
    {"step",                dbvm_step               },
    {"reset",               dbvm_reset              },
    {"finalize",            dbvm_finalize           },
    {"columns",             dbvm_columns            },
    {"bind",                dbvm_bind               },
    {"bind_values",         dbvm_bind_values        },
    {"bind_names",          dbvm_bind_names         },
    {"bind_blob",           dbvm_bind_blob          },
    {"bind_parameter_count",dbvm_bind_parameter_count},
    {"bind_parameter_name", dbvm_bind_parameter_name},
    {"get_value",           dbvm_get_value          },
    {"get_values",          dbvm_get_values         },
    {"get_name",            dbvm_get_name           },
    {"get_names",           dbvm_get_names          },
    {"get_type",            dbvm_get_type           },
    {"get_types",           dbvm_get_types          },
    {"get_uvalues",         dbvm_get_uvalues        },
    {"get_unames",          dbvm_get_unames         },
    {"get_utypes",          dbvm_get_utypes         },
    {"get_named_values",    dbvm_get_named_values   },
    {"get_named_types",     dbvm_get_named_types    },
    {"rows",                dbvm_rows               },
    {"urows",               dbvm_urows              },
    {"nrows",               dbvm_nrows              },
    {"last_insert_rowid",   dbvm_last_insert_rowid  },
    {"idata",               dbvm_get_values         },
    {"inames",              dbvm_get_names          },
    {"itypes",              dbvm_get_types          },
    {"data",                dbvm_get_named_values   },
    {"type",                dbvm_get_named_types    },
    {"__tostring",          dbvm_tostring           },
    {"__gc",                dbvm_gc                 },
    { NULL, NULL }
};
static const luaL_Reg ctxlib[] = {
    {"user_data",               lcontext_user_data              },
    {"get_aggregate_data",      lcontext_get_aggregate_context  },
    {"set_aggregate_data",      lcontext_set_aggregate_context  },
    {"aggregate_count",         lcontext_aggregate_count        },
    {"result",                  lcontext_result                 },
    {"result_null",             lcontext_result_null            },
    {"result_number",           lcontext_result_double          },
    {"result_double",           lcontext_result_double          },
    {"result_int",              lcontext_result_int             },
    {"result_text",             lcontext_result_text            },
    {"result_blob",             lcontext_result_blob            },
    {"result_error",            lcontext_result_error           },
    {"__tostring",              lcontext_tostring               },
    {NULL, NULL}
};
static const luaL_Reg dbbulib[] = {
    {"step",        dbbu_step       },
    {"remaining",   dbbu_remaining  },
    {"pagecount",   dbbu_pagecount  },
    {"finish",      dbbu_finish     },
    {"__gc",        dbbu_gc         },
    {NULL, NULL}
};
static const luaL_Reg sqlitelib[] = {
    {"lversion",        lsqlite_lversion        },
    {"version",         lsqlite_version         },
    {"complete",        lsqlite_complete        },
#ifndef _WIN32
    {"temp_directory",  lsqlite_temp_directory  },
#endif
    {"open",            lsqlite_open            },
    {"open_memory",     lsqlite_open_memory     },
    {"open_ptr",        lsqlite_open_ptr        },
    {"backup_init",     lsqlite_backup_init     },
    {"__newindex",      lsqlite_newindex        },
    {NULL, NULL}
};
static void create_meta(lua_State *L, const char *name, const luaL_Reg *lib) {
    luaL_newmetatable(L, name);
    lua_pushstring(L, "__index");
    lua_pushvalue(L, -2);               
    lua_rawset(L, -3);                  
    luaL_openlib(L, NULL, lib, 0);
    lua_pop(L, 1);
}
LUALIB_API int luaopen_lsqlite3(lua_State *L) {
    create_meta(L, sqlite_meta, lsqlite3_dblib);
    create_meta(L, sqlite_vm_meta, vmlib);
    create_meta(L, sqlite_bu_meta, dbbulib);
    create_meta(L, sqlite_ctx_meta, ctxlib);
    luaL_getmetatable(L, sqlite_ctx_meta);
    sqlite_ctx_meta_ref = luaL_ref(L, LUA_REGISTRYINDEX);
    luaL_register(L, "sqlite3", sqlitelib);
    {
        int i = 0;
        while (sqlite_constants[i].name) {
            lua_pushstring(L, sqlite_constants[i].name);
            lua_pushinteger(L, sqlite_constants[i].value);
            lua_rawset(L, -3);
            ++i;
        }
    }
    lua_pushvalue(L, -1);
    lua_setmetatable(L, -2);
    return 1;
}
#endif
#ifdef LUA_USE_LPEG
#if !defined(lptypes_h)
#define lptypes_h
#include <assert.h>
#include <limits.h>
#define VERSION         "1.0.2"
#define PATTERN_T	"lpeg-pattern"
#define MAXSTACKIDX	"lpeg-maxstack"
#if (LUA_VERSION_NUM == 501)
#define lp_equal	lua_equal
#define lua_getuservalue	lua_getfenv
#define lua_setuservalue	lua_setfenv
#define lua_rawlen		lua_objlen
#define luaL_setfuncs(L,f,n)	luaL_register(L,NULL,f)
#define luaL_newlib(L,f)	luaL_register(L,"lpeg",f)
#endif
#if !defined(lp_equal)
#define lp_equal(L,idx1,idx2)  lua_compare(L,(idx1),(idx2),LUA_OPEQ)
#endif
#if !defined(MAXBACK)
#define MAXBACK         400
#endif
#if !defined(MAXRULES)
#define MAXRULES        250
#endif
#define INITCAPSIZE	32
#define SUBJIDX		2
#define FIXEDARGS	3
#define caplistidx(ptop)	((ptop) + 2)
#define ktableidx(ptop)		((ptop) + 3)
#define stackidx(ptop)	((ptop) + 4)
typedef unsigned char byte;
#define BITSPERCHAR		8
#define CHARSETSIZE		((UCHAR_MAX/BITSPERCHAR) + 1)
typedef struct Charset {
  byte cs[CHARSETSIZE];
} Charset;
#define loopset(v,b)    { int v; for (v = 0; v < CHARSETSIZE; v++) {b;} }
#define treebuffer(t)      ((byte *)((t) + 1))
#define bytes2slots(n)  (((n) - 1) / sizeof(TTree) + 1)
#define setchar(cs,b)   ((cs)[(b) >> 3] |= (1 << ((b) & 7)))
#define getkind(op)		((op)->i.aux & 0xF)
#define getoff(op)		(((op)->i.aux >> 4) & 0xF)
#define joinkindoff(k,o)	((k) | ((o) << 4))
#define MAXOFF		0xF
#define MAXAUX		0xFF
#define MAXBEHIND	MAXAUX
#define MAXPATTSIZE	(SHRT_MAX - 10)
#define instsize(l)  (((l) + sizeof(Instruction2) - 1)/sizeof(Instruction2) + 1)
#define CHARSETINSTSIZE		instsize(CHARSETSIZE)
#define funcinstsize(p)		((p)->i.aux + 2)
#define testchar(st,c)	(((int)(st)[((c) >> 3)] & (1 << ((c) & 7))))
#endif
#endif
#ifdef LUA_USE_LPEG
#if !defined(lpcap_h)
#define lpcap_h
typedef enum CapKind {
  Cclose,  
  Cposition,
  Cconst,  
  Cbackref,  
  Carg,  
  Csimple,  
  Ctable,  
  Cfunction,  
  Cquery,  
  Cstring,  
  Cnum,  
  Csubst,  
  Cfold,  
  Cruntime,  
  Cgroup  
} CapKind;
typedef struct Capture {
  const char *s;  
  unsigned short idx;  
  byte kind;  
  byte siz;  
} Capture;
typedef struct CapState {
  Capture *cap;  
  Capture *ocap;  
  lua_State *L;
  int ptop;  
  const char *s;  
  int valuecached;  
  int reclevel;  
} CapState;
int runtimecap (CapState *cs, Capture *close, const char *s, int *rem);
int getcaptures (lua_State *L, const char *s, const char *r, int ptop);
int finddyncap (Capture *cap, Capture *last);
#endif
#endif
#ifdef LUA_USE_LPEG
#if !defined(lptree_h)
#define lptree_h
typedef enum TTag {
  TChar = 0,  
  TSet,  
  TAny,
  TTrue,
  TFalse,
  TRep,  
  TSeq,  
  TChoice,  
  TNot,  
  TAnd,  
  TCall,  
  TOpenCall,  
  TRule,  
  TGrammar,  
  TBehind,  
  TCapture,  
  TRunTime  
} TTag;
typedef struct TTree {
  byte tag;
  byte cap;  
  unsigned short key;  
  union {
    int ps;  
    int n;  
  } u;
} TTree;
typedef struct Pattern {
  union Instruction2 *code;
  int codesize;
  TTree tree[1];
} Pattern;
extern const byte numsiblings[];
#define sib1(t)         ((t) + 1)
#define sib2(t)         ((t) + (t)->u.ps)
#endif
#endif
#ifdef LUA_USE_LPEG
#if !defined(lpvm_h)
#define lpvm_h
typedef enum Opcode {
  IAny, 
  IChar,  
  ISet,  
  ITestAny,  
  ITestChar,  
  ITestSet,  
  ISpan,  
  IBehind,  
  IRet,  
  IEnd,  
  IChoice,  
  IJmp,  
  ICall,  
  IOpenCall,  
  ICommit,  
  IPartialCommit,  
  IBackCommit,  
  IFailTwice,  
  IFail,  
  IGiveup,  
  IFullCapture,  
  IOpenCapture,  
  ICloseCapture,
  ICloseRunTime
} Opcode;
typedef union Instruction2 {
  struct Inst {
    byte code;
    byte aux;
    short key;
  } i;
  int offset;
  byte buff[1];
} Instruction2;
void printpatt (Instruction2 *p, int n);
const char *lpeg_match (lua_State *L, const char *o, const char *s, const char *e,
                   Instruction2 *op, Capture *capture, int ptop);
#endif
#endif
#ifdef LUA_USE_LPEG
#if !defined(lpcode_h)
#define lpcode_h
int tocharset (TTree *tree, Charset *cs);
int checkaux (TTree *tree, int pred);
int fixedlen (TTree *tree);
int hascaptures (TTree *tree);
int lp_gc (lua_State *L);
Instruction2 *compile (lua_State *L, Pattern *p);
void realloccode (lua_State *L, Pattern *p, int nsize);
int sizei (const Instruction2 *i);
#define PEnullable      0
#define PEnofail        1
#define nofail(t)	checkaux(t, PEnofail)
#define nullable(t)	checkaux(t, PEnullable)
#endif
#endif
#ifdef LUA_USE_LPEG
#if !defined(lpprint_h)
#define lpprint_h
#if defined(LPEG_DEBUG)
void printpatt (Instruction2 *p, int n);
void printtree (TTree *tree, int ident);
void printktable (lua_State *L, int idx);
void printcharset (const byte *st);
void printcaplist (Capture *cap, Capture *limit);
void printinst (const Instruction2 *op, const Instruction2 *p);
#else
#define printktable(L,idx)  \
	luaL_error(L, "function only implemented in debug mode")
#define printtree(tree,i)  \
	luaL_error(L, "function only implemented in debug mode")
#define printpatt(p,n)  \
	luaL_error(L, "function only implemented in debug mode")
#endif
#endif
#endif
#ifdef LUA_USE_LPEG
#define captype(cap)	((cap)->kind)
#define isclosecap(cap)	(captype(cap) == Cclose)
#define closeaddr(c)	((c)->s + (c)->siz - 1)
#define isfullcap(cap)	((cap)->siz != 0)
#define getfromktable(cs,v)	lua_rawgeti((cs)->L, ktableidx((cs)->ptop), v)
#define pushluaval(cs)		getfromktable(cs, (cs)->cap->idx)
static int updatecache (CapState *cs, int v) {
  int idx = cs->ptop + 1;  
  if (v != cs->valuecached) {  
    getfromktable(cs, v);  
    lua_replace(cs->L, idx);  
    cs->valuecached = v;  
  }
  return idx;
}
static int pushcapture (CapState *cs);
static Capture *findopen (Capture *cap) {
  int n = 0;  
  for (;;) {
    cap--;
    if (isclosecap(cap)) n++;  
    else if (!isfullcap(cap))
      if (n-- == 0) return cap;
  }
}
static void nextcap (CapState *cs) {
  Capture *cap = cs->cap;
  if (!isfullcap(cap)) {  
    int n = 0;  
    for (;;) {  
      cap++;
      if (isclosecap(cap)) {
        if (n-- == 0) break;
      }
      else if (!isfullcap(cap)) n++;
    }
  }
  cs->cap = cap + 1;  
}
static int pushnestedvalues (CapState *cs, int addextra) {
  Capture *co = cs->cap;
  if (isfullcap(cs->cap++)) {  
    lua_pushlstring(cs->L, co->s, co->siz - 1);  
    return 1;  
  }
  else {
    int n = 0;
    while (!isclosecap(cs->cap))  
      n += pushcapture(cs);
    if (addextra || n == 0) {  
      lua_pushlstring(cs->L, co->s, cs->cap->s - co->s);  
      n++;
    }
    cs->cap++;  
    return n;
  }
}
static void pushonenestedvalue (CapState *cs) {
  int n = pushnestedvalues(cs, 0);
  if (n > 1)
    lua_pop(cs->L, n - 1);  
}
static Capture *findback (CapState *cs, Capture *cap) {
  lua_State *L = cs->L;
  while (cap-- > cs->ocap) {  
    if (isclosecap(cap))
      cap = findopen(cap);  
    else if (!isfullcap(cap))
      continue; 
    if (captype(cap) == Cgroup) {
      getfromktable(cs, cap->idx);  
      if (lp_equal(L, -2, -1)) {  
        lua_pop(L, 2);  
        return cap;
      }
      else lua_pop(L, 1);  
    }
  }
  luaL_error(L, "back reference '%s' not found", lua_tostring(L, -1));
  return NULL;  
}
static int backrefcap (CapState *cs) {
  int n;
  Capture *curr = cs->cap;
  pushluaval(cs);  
  cs->cap = findback(cs, curr);  
  n = pushnestedvalues(cs, 0);  
  cs->cap = curr + 1;
  return n;
}
static int tablecap (CapState *cs) {
  lua_State *L = cs->L;
  int n = 0;
  lua_newtable(L);
  if (isfullcap(cs->cap++))
    return 1;  
  while (!isclosecap(cs->cap)) {
    if (captype(cs->cap) == Cgroup && cs->cap->idx != 0) {  
      pushluaval(cs);  
      pushonenestedvalue(cs);
      lua_settable(L, -3);
    }
    else {  
      int i;
      int k = pushcapture(cs);
      for (i = k; i > 0; i--)  
        lua_rawseti(L, -(i + 1), n + i);
      n += k;
    }
  }
  cs->cap++;  
  return 1;  
}
static int querycap (CapState *cs) {
  int idx = cs->cap->idx;
  pushonenestedvalue(cs);  
  lua_gettable(cs->L, updatecache(cs, idx));  
  if (!lua_isnil(cs->L, -1))
    return 1;
  else {  
    lua_pop(cs->L, 1);  
    return 0;
  }
}
static int foldcap (CapState *cs) {
  int n;
  lua_State *L = cs->L;
  int idx = cs->cap->idx;
  if (isfullcap(cs->cap++) ||  
      isclosecap(cs->cap) ||  
      (n = pushcapture(cs)) == 0)  
    return luaL_error(L, "no initial value for fold capture");
  if (n > 1)
    lua_pop(L, n - 1);  
  while (!isclosecap(cs->cap)) {
    lua_pushvalue(L, updatecache(cs, idx));  
    lua_insert(L, -2);  
    n = pushcapture(cs);  
    lua_call(L, n + 1, 1);  
  }
  cs->cap++;  
  return 1;  
}
static int functioncap (CapState *cs) {
  int n;
  int top = lua_gettop(cs->L);
  pushluaval(cs);  
  n = pushnestedvalues(cs, 0);  
  lua_call(cs->L, n, LUA_MULTRET);  
  return lua_gettop(cs->L) - top;  
}
static int numcap (CapState *cs) {
  int idx = cs->cap->idx;  
  if (idx == 0) {  
    nextcap(cs);  
    return 0;  
  }
  else {
    int n = pushnestedvalues(cs, 0);
    if (n < idx)  
      return luaL_error(cs->L, "no capture '%d'", idx);
    else {
      lua_pushvalue(cs->L, -(n - idx + 1));  
      lua_replace(cs->L, -(n + 1));  
      lua_pop(cs->L, n - 1);  
      return 1;
    }
  }
}
int finddyncap (Capture *cap, Capture *last) {
  for (; cap < last; cap++) {
    if (cap->kind == Cruntime)
      return cap->idx;  
  }
  return 0;  
}
int runtimecap (CapState *cs, Capture *close, const char *s, int *rem) {
  int n, id;
  lua_State *L = cs->L;
  int otop = lua_gettop(L);
  Capture *open = findopen(close);  
  assert(captype(open) == Cgroup);
  id = finddyncap(open, close);  
  close->kind = Cclose;  
  close->s = s;
  cs->cap = open; cs->valuecached = 0;  
  luaL_checkstack(L, 4, "too many runtime captures");
  pushluaval(cs);  
  lua_pushvalue(L, SUBJIDX);  
  lua_pushinteger(L, s - cs->s + 1);  
  n = pushnestedvalues(cs, 0);  
  lua_call(L, n + 2, LUA_MULTRET);  
  if (id > 0) {  
    int i;
    for (i = id; i <= otop; i++)
      lua_remove(L, id);  
    *rem = otop - id + 1;  
  }
  else
    *rem = 0;  
  return close - open - 1;  
}
typedef struct StrAux {
  int isstring;  
  union {
    Capture *cp;  
    struct {  
      const char *s;  
      const char *e;  
    } s;
  } u;
} StrAux;
#define MAXSTRCAPS	10
static int getstrcaps (CapState *cs, StrAux *cps, int n) {
  int k = n++;
  cps[k].isstring = 1;  
  cps[k].u.s.s = cs->cap->s;  
  if (!isfullcap(cs->cap++)) {  
    while (!isclosecap(cs->cap)) {  
      if (n >= MAXSTRCAPS)  
        nextcap(cs);  
      else if (captype(cs->cap) == Csimple)  
        n = getstrcaps(cs, cps, n);  
      else {
        cps[n].isstring = 0;  
        cps[n].u.cp = cs->cap;  
        nextcap(cs);
        n++;
      }
    }
    cs->cap++;  
  }
  cps[k].u.s.e = closeaddr(cs->cap - 1);  
  return n;
}
static int addonestring (luaL_Buffer *b, CapState *cs, const char *what);
static void stringcap (luaL_Buffer *b, CapState *cs) {
  StrAux cps[MAXSTRCAPS];
  int n;
  size_t len, i;
  const char *fmt;  
  fmt = lua_tolstring(cs->L, updatecache(cs, cs->cap->idx), &len);
  n = getstrcaps(cs, cps, 0) - 1;  
  for (i = 0; i < len; i++) {  
    if (fmt[i] != '%')  
      luaL_addchar(b, fmt[i]);  
    else if (fmt[++i] < '0' || fmt[i] > '9')  
      luaL_addchar(b, fmt[i]);  
    else {
      int l = fmt[i] - '0';  
      if (l > n)
        luaL_error(cs->L, "invalid capture index (%d)", l);
      else if (cps[l].isstring)
        luaL_addlstring(b, cps[l].u.s.s, cps[l].u.s.e - cps[l].u.s.s);
      else {
        Capture *curr = cs->cap;
        cs->cap = cps[l].u.cp;  
        if (!addonestring(b, cs, "capture"))
          luaL_error(cs->L, "no values in capture index %d", l);
        cs->cap = curr;  
      }
    }
  }
}
static void substcap (luaL_Buffer *b, CapState *cs) {
  const char *curr = cs->cap->s;
  if (isfullcap(cs->cap))  
    luaL_addlstring(b, curr, cs->cap->siz - 1);  
  else {
    cs->cap++;  
    while (!isclosecap(cs->cap)) {  
      const char *next = cs->cap->s;
      luaL_addlstring(b, curr, next - curr);  
      if (addonestring(b, cs, "replacement"))
        curr = closeaddr(cs->cap - 1);  
      else  
        curr = next;  
    }
    luaL_addlstring(b, curr, cs->cap->s - curr);  
  }
  cs->cap++;  
}
static int addonestring (luaL_Buffer *b, CapState *cs, const char *what) {
  switch (captype(cs->cap)) {
    case Cstring:
      stringcap(b, cs);  
      return 1;
    case Csubst:
      substcap(b, cs);  
      return 1;
    default: {
      lua_State *L = cs->L;
      int n = pushcapture(cs);
      if (n > 0) {
        if (n > 1) lua_pop(L, n - 1);  
        if (!lua_isstring(L, -1))
          luaL_error(L, "invalid %s value (a %s)", what, luaL_typename(L, -1));
        luaL_addvalue(b);
      }
      return n;
    }
  }
}
#if !defined(MAXRECLEVEL)
#define MAXRECLEVEL	200
#endif
static int pushcapture (CapState *cs) {
  lua_State *L = cs->L;
  int res;
  luaL_checkstack(L, 4, "too many captures");
  if (cs->reclevel++ > MAXRECLEVEL)
    return luaL_error(L, "subcapture nesting too deep");
  switch (captype(cs->cap)) {
    case Cposition: {
      lua_pushinteger(L, cs->cap->s - cs->s + 1);
      cs->cap++;
      res = 1;
      break;
    }
    case Cconst: {
      pushluaval(cs);
      cs->cap++;
      res = 1;
      break;
    }
    case Carg: {
      int arg = (cs->cap++)->idx;
      if (arg + FIXEDARGS > cs->ptop)
        return luaL_error(L, "reference to absent extra argument #%d", arg);
      lua_pushvalue(L, arg + FIXEDARGS);
      res = 1;
      break;
    }
    case Csimple: {
      int k = pushnestedvalues(cs, 1);
      lua_insert(L, -k);  
      res = k;
      break;
    }
    case Cruntime: {
      lua_pushvalue(L, (cs->cap++)->idx);  
      res = 1;
      break;
    }
    case Cstring: {
      luaL_Buffer b;
      luaL_buffinit(L, &b);
      stringcap(&b, cs);
      luaL_pushresult(&b);
      res = 1;
      break;
    }
    case Csubst: {
      luaL_Buffer b;
      luaL_buffinit(L, &b);
      substcap(&b, cs);
      luaL_pushresult(&b);
      res = 1;
      break;
    }
    case Cgroup: {
      if (cs->cap->idx == 0)  
        res = pushnestedvalues(cs, 0);  
      else {  
        nextcap(cs);  
        res = 0;
      }
      break;
    }
    case Cbackref: res = backrefcap(cs); break;
    case Ctable: res = tablecap(cs); break;
    case Cfunction: res = functioncap(cs); break;
    case Cnum: res = numcap(cs); break;
    case Cquery: res = querycap(cs); break;
    case Cfold: res = foldcap(cs); break;
    default: assert(0); res = 0;
  }
  cs->reclevel--;
  return res;
}
int getcaptures (lua_State *L, const char *s, const char *r, int ptop) {
  Capture *capture = (Capture *)lua_touserdata(L, caplistidx(ptop));
  int n = 0;
  if (!isclosecap(capture)) {  
    CapState cs;
    cs.ocap = cs.cap = capture; cs.L = L; cs.reclevel = 0;
    cs.s = s; cs.valuecached = 0; cs.ptop = ptop;
    do {  
      n += pushcapture(&cs);
    } while (!isclosecap(cs.cap));
  }
  if (n == 0) {  
    lua_pushinteger(L, r - s + 1);  
    n = 1;
  }
  return n;
}
#endif
#ifdef LUA_USE_LPEG
#include <limits.h>
#define NOINST		-1
static const Charset fullset_ =
  {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}};
static const Charset *fullset = &fullset_;
static Opcode charsettype (const byte *cs, int *c) {
  int count = 0;  
  int i;
  int candidate = -1;  
  for (i = 0; i < CHARSETSIZE; i++) {  
    int b = cs[i];
    if (b == 0) {  
      if (count > 1)  
        return ISet;  
    }
    else if (b == 0xFF) {  
      if (count < (i * BITSPERCHAR))  
        return ISet;  
      else count += BITSPERCHAR;  
    }
    else if ((b & (b - 1)) == 0) {  
      if (count > 0)  
        return ISet;  
      else {  
        count++;
        candidate = i;
      }
    }
    else return ISet;  
  }
  switch (count) {
    case 0: return IFail;  
    case 1: {  
      int b = cs[candidate];
      *c = candidate * BITSPERCHAR;
      if ((b & 0xF0) != 0) { *c += 4; b >>= 4; }
      if ((b & 0x0C) != 0) { *c += 2; b >>= 2; }
      if ((b & 0x02) != 0) { *c += 1; }
      return IChar;
    }
    default: {
       assert(count == CHARSETSIZE * BITSPERCHAR);  
       return IAny;
    }
  }
}
static void cs_complement (Charset *cs) {
  loopset(i, cs->cs[i] = ~cs->cs[i]);
}
static int cs_equal (const byte *cs1, const byte *cs2) {
  loopset(i, if (cs1[i] != cs2[i]) return 0);
  return 1;
}
static int cs_disjoint (const Charset *cs1, const Charset *cs2) {
  loopset(i, if ((cs1->cs[i] & cs2->cs[i]) != 0) return 0;)
  return 1;
}
int tocharset (TTree *tree, Charset *cs) {
  switch (tree->tag) {
    case TSet: {  
      loopset(i, cs->cs[i] = treebuffer(tree)[i]);
      return 1;
    }
    case TChar: {  
      assert(0 <= tree->u.n && tree->u.n <= UCHAR_MAX);
      loopset(i, cs->cs[i] = 0);  
      setchar(cs->cs, tree->u.n);  
      return 1;
    }
    case TAny: {
      loopset(i, cs->cs[i] = 0xFF);  
      return 1;
    }
    default: return 0;
  }
}
static int callrecursive (TTree *tree, int f (TTree *t), int def) {
  int key = tree->key;
  assert(tree->tag == TCall);
  assert(sib2(tree)->tag == TRule);
  if (key == 0)  
    return def;  
  else {  
    int result;
    tree->key = 0;  
    result = f(sib2(tree));  
    tree->key = key;  
    return result;
  }
}
int hascaptures (TTree *tree) {
 tailcall:
  switch (tree->tag) {
    case TCapture: case TRunTime:
      return 1;
    case TCall:
      return callrecursive(tree, hascaptures, 0);
    case TRule:  
      tree = sib1(tree); goto tailcall;
    case TOpenCall: assert(0);
    default: {
      switch (numsiblings[tree->tag]) {
        case 1:  
          tree = sib1(tree); goto tailcall;
        case 2:
          if (hascaptures(sib1(tree)))
            return 1;
          tree = sib2(tree); goto tailcall;
        default: assert(numsiblings[tree->tag] == 0); return 0;
      }
    }
  }
}
int checkaux (TTree *tree, int pred) {
 tailcall:
  switch (tree->tag) {
    case TChar: case TSet: case TAny:
    case TFalse: case TOpenCall:
      return 0;  
    case TRep: case TTrue:
      return 1;  
    case TNot: case TBehind:  
      if (pred == PEnofail) return 0;
      else return 1;  
    case TAnd:  
      if (pred == PEnullable) return 1;
      tree = sib1(tree); goto tailcall;
    case TRunTime:  
      if (pred == PEnofail) return 0;
      tree = sib1(tree); goto tailcall;
    case TSeq:
      if (!checkaux(sib1(tree), pred)) return 0;
      tree = sib2(tree); goto tailcall;
    case TChoice:
      if (checkaux(sib2(tree), pred)) return 1;
      tree = sib1(tree); goto tailcall;
    case TCapture: case TGrammar: case TRule:
      tree = sib1(tree); goto tailcall;
    case TCall:  
      tree = sib2(tree); goto tailcall;
    default: assert(0); return 0;
  }
}
int fixedlen (TTree *tree) {
  int len = 0;  
 tailcall:
  switch (tree->tag) {
    case TChar: case TSet: case TAny:
      return len + 1;
    case TFalse: case TTrue: case TNot: case TAnd: case TBehind:
      return len;
    case TRep: case TRunTime: case TOpenCall:
      return -1;
    case TCapture: case TRule: case TGrammar:
      tree = sib1(tree); goto tailcall;
    case TCall: {
      int n1 = callrecursive(tree, fixedlen, -1);
      if (n1 < 0)
        return -1;
      else
        return len + n1;
    }
    case TSeq: {
      int n1 = fixedlen(sib1(tree));
      if (n1 < 0)
        return -1;
      len += n1; tree = sib2(tree); goto tailcall;
    }
    case TChoice: {
      int n1 = fixedlen(sib1(tree));
      int n2 = fixedlen(sib2(tree));
      if (n1 != n2 || n1 < 0)
        return -1;
      else
        return len + n1;
    }
    default: assert(0); return 0;
  };
}
static int getfirst (TTree *tree, const Charset *follow, Charset *firstset) {
 tailcall:
  switch (tree->tag) {
    case TChar: case TSet: case TAny: {
      tocharset(tree, firstset);
      return 0;
    }
    case TTrue: {
      loopset(i, firstset->cs[i] = follow->cs[i]);
      return 1;  
    }
    case TFalse: {
      loopset(i, firstset->cs[i] = 0);
      return 0;
    }
    case TChoice: {
      Charset csaux;
      int e1 = getfirst(sib1(tree), follow, firstset);
      int e2 = getfirst(sib2(tree), follow, &csaux);
      loopset(i, firstset->cs[i] |= csaux.cs[i]);
      return e1 | e2;
    }
    case TSeq: {
      if (!nullable(sib1(tree))) {
        tree = sib1(tree); follow = fullset; goto tailcall;
      }
      else {  
        Charset csaux;
        int e2 = getfirst(sib2(tree), follow, &csaux);
        int e1 = getfirst(sib1(tree), &csaux, firstset);
        if (e1 == 0) return 0;  
        else if ((e1 | e2) & 2)  
          return 2;  
        else return e2;  
      }
    }
    case TRep: {
      getfirst(sib1(tree), follow, firstset);
      loopset(i, firstset->cs[i] |= follow->cs[i]);
      return 1;  
    }
    case TCapture: case TGrammar: case TRule: {
      tree = sib1(tree); goto tailcall;
    }
    case TRunTime: {  
      int e = getfirst(sib1(tree), fullset, firstset);
      if (e) return 2;  
      else return 0;  
    }
    case TCall: {
      tree = sib2(tree); goto tailcall;
    }
    case TAnd: {
      int e = getfirst(sib1(tree), follow, firstset);
      loopset(i, firstset->cs[i] &= follow->cs[i]);
      return e;
    }
    case TNot: {
      if (tocharset(sib1(tree), firstset)) {
        cs_complement(firstset);
        return 1;
      }
    }
    case TBehind: {  
      int e = getfirst(sib1(tree), follow, firstset);
      loopset(i, firstset->cs[i] = follow->cs[i]);  
      return e | 1;  
    }
    default: assert(0); return 0;
  }
}
static int headfail (TTree *tree) {
 tailcall:
  switch (tree->tag) {
    case TChar: case TSet: case TAny: case TFalse:
      return 1;
    case TTrue: case TRep: case TRunTime: case TNot:
    case TBehind:
      return 0;
    case TCapture: case TGrammar: case TRule: case TAnd:
      tree = sib1(tree); goto tailcall;  
    case TCall:
      tree = sib2(tree); goto tailcall;  
    case TSeq:
      if (!nofail(sib2(tree))) return 0;
      tree = sib1(tree); goto tailcall;
    case TChoice:
      if (!headfail(sib1(tree))) return 0;
      tree = sib2(tree); goto tailcall;
    default: assert(0); return 0;
  }
}
static int needfollow (TTree *tree) {
 tailcall:
  switch (tree->tag) {
    case TChar: case TSet: case TAny:
    case TFalse: case TTrue: case TAnd: case TNot:
    case TRunTime: case TGrammar: case TCall: case TBehind:
      return 0;
    case TChoice: case TRep:
      return 1;
    case TCapture:
      tree = sib1(tree); goto tailcall;
    case TSeq:
      tree = sib2(tree); goto tailcall;
    default: assert(0); return 0;
  }
}
int sizei (const Instruction2 *i) {
  switch((Opcode)i->i.code) {
    case ISet: case ISpan: return CHARSETINSTSIZE;
    case ITestSet: return CHARSETINSTSIZE + 1;
    case ITestChar: case ITestAny: case IChoice: case IJmp: case ICall:
    case IOpenCall: case ICommit: case IPartialCommit: case IBackCommit:
      return 2;
    default: return 1;
  }
}
typedef struct CompileState {
  Pattern *p;  
  int ncode;  
  lua_State *L;
} CompileState;
static void codegen (CompileState *compst, TTree *tree, int opt, int tt,
                     const Charset *fl);
void realloccode (lua_State *L, Pattern *p, int nsize) {
  void *ud;
  lua_Alloc f = lua_getallocf(L, &ud);
  void *newblock = f(ud, p->code, p->codesize * sizeof(Instruction2),
                                  nsize * sizeof(Instruction2));
  if (newblock == NULL && nsize > 0)
    luaL_error(L, "not enough memory");
  p->code = (Instruction2 *)newblock;
  p->codesize = nsize;
}
static int nextinstruction (CompileState *compst) {
  int size = compst->p->codesize;
  if (compst->ncode >= size)
    realloccode(compst->L, compst->p, size * 2);
  return compst->ncode++;
}
#define getinstr(cs,i)		((cs)->p->code[i])
static int addinstruction (CompileState *compst, Opcode op, int aux) {
  int i = nextinstruction(compst);
  getinstr(compst, i).i.code = op;
  getinstr(compst, i).i.aux = aux;
  return i;
}
static int addoffsetinst (CompileState *compst, Opcode op) {
  int i = addinstruction(compst, op, 0);  
  addinstruction(compst, (Opcode)0, 0);  
  assert(op == ITestSet || sizei(&getinstr(compst, i)) == 2);
  return i;
}
static void setoffset (CompileState *compst, int instruction, int offset) {
  getinstr(compst, instruction + 1).offset = offset;
}
static int addinstcap (CompileState *compst, Opcode op, int cap, int key,
                       int aux) {
  int i = addinstruction(compst, op, joinkindoff(cap, aux));
  getinstr(compst, i).i.key = key;
  return i;
}
#define gethere(compst) 	((compst)->ncode)
#define target(code,i)		((i) + code[i + 1].offset)
static void jumptothere (CompileState *compst, int instruction, int target) {
  if (instruction >= 0)
    setoffset(compst, instruction, target - instruction);
}
static void jumptohere (CompileState *compst, int instruction) {
  jumptothere(compst, instruction, gethere(compst));
}
static void codechar (CompileState *compst, int c, int tt) {
  if (tt >= 0 && getinstr(compst, tt).i.code == ITestChar &&
                 getinstr(compst, tt).i.aux == c)
    addinstruction(compst, IAny, 0);
  else
    addinstruction(compst, IChar, c);
}
static void addcharset (CompileState *compst, const byte *cs) {
  int p = gethere(compst);
  int i;
  for (i = 0; i < (int)CHARSETINSTSIZE - 1; i++)
    nextinstruction(compst);  
  loopset(j, getinstr(compst, p).buff[j] = cs[j]);
}
static void codecharset (CompileState *compst, const byte *cs, int tt) {
  int c = 0;  
  Opcode op = charsettype(cs, &c);
  switch (op) {
    case IChar: codechar(compst, c, tt); break;
    case ISet: {  
      if (tt >= 0 && getinstr(compst, tt).i.code == ITestSet &&
          cs_equal(cs, getinstr(compst, tt + 2).buff))
        addinstruction(compst, IAny, 0);
      else {
        addinstruction(compst, ISet, 0);
        addcharset(compst, cs);
      }
      break;
    }
    default: addinstruction(compst, op, c); break;
  }
}
static int codetestset (CompileState *compst, Charset *cs, int e) {
  if (e) return NOINST;  
  else {
    int c = 0;
    Opcode op = charsettype(cs->cs, &c);
    switch (op) {
      case IFail: return addoffsetinst(compst, IJmp);  
      case IAny: return addoffsetinst(compst, ITestAny);
      case IChar: {
        int i = addoffsetinst(compst, ITestChar);
        getinstr(compst, i).i.aux = c;
        return i;
      }
      case ISet: {
        int i = addoffsetinst(compst, ITestSet);
        addcharset(compst, cs->cs);
        return i;
      }
      default: assert(0); return 0;
    }
  }
}
static int finaltarget (Instruction2 *code, int i) {
  while (code[i].i.code == IJmp)
    i = target(code, i);
  return i;
}
static int finallabel (Instruction2 *code, int i) {
  return finaltarget(code, target(code, i));
}
static void codebehind (CompileState *compst, TTree *tree) {
  if (tree->u.n > 0)
    addinstruction(compst, IBehind, tree->u.n);
  codegen(compst, sib1(tree), 0, NOINST, fullset);
}
static void codechoice (CompileState *compst, TTree *p1, TTree *p2, int opt,
                        const Charset *fl) {
  int emptyp2 = (p2->tag == TTrue);
  Charset cs1, cs2;
  int e1 = getfirst(p1, fullset, &cs1);
  if (headfail(p1) ||
      (!e1 && (getfirst(p2, fl, &cs2), cs_disjoint(&cs1, &cs2)))) {
    int test = codetestset(compst, &cs1, 0);
    int jmp = NOINST;
    codegen(compst, p1, 0, test, fl);
    if (!emptyp2)
      jmp = addoffsetinst(compst, IJmp);
    jumptohere(compst, test);
    codegen(compst, p2, opt, NOINST, fl);
    jumptohere(compst, jmp);
  }
  else if (opt && emptyp2) {
    jumptohere(compst, addoffsetinst(compst, IPartialCommit));
    codegen(compst, p1, 1, NOINST, fullset);
  }
  else {
    int pcommit;
    int test = codetestset(compst, &cs1, e1);
    int pchoice = addoffsetinst(compst, IChoice);
    codegen(compst, p1, emptyp2, test, fullset);
    pcommit = addoffsetinst(compst, ICommit);
    jumptohere(compst, pchoice);
    jumptohere(compst, test);
    codegen(compst, p2, opt, NOINST, fl);
    jumptohere(compst, pcommit);
  }
}
static void codeand (CompileState *compst, TTree *tree, int tt) {
  int n = fixedlen(tree);
  if (n >= 0 && n <= MAXBEHIND && !hascaptures(tree)) {
    codegen(compst, tree, 0, tt, fullset);
    if (n > 0)
      addinstruction(compst, IBehind, n);
  }
  else {  
    int pcommit;
    int pchoice = addoffsetinst(compst, IChoice);
    codegen(compst, tree, 0, tt, fullset);
    pcommit = addoffsetinst(compst, IBackCommit);
    jumptohere(compst, pchoice);
    addinstruction(compst, IFail, 0);
    jumptohere(compst, pcommit);
  }
}
static void codecapture (CompileState *compst, TTree *tree, int tt,
                         const Charset *fl) {
  int len = fixedlen(sib1(tree));
  if (len >= 0 && len <= MAXOFF && !hascaptures(sib1(tree))) {
    codegen(compst, sib1(tree), 0, tt, fl);
    addinstcap(compst, IFullCapture, tree->cap, tree->key, len);
  }
  else {
    addinstcap(compst, IOpenCapture, tree->cap, tree->key, 0);
    codegen(compst, sib1(tree), 0, tt, fl);
    addinstcap(compst, ICloseCapture, Cclose, 0, 0);
  }
}
static void coderuntime (CompileState *compst, TTree *tree, int tt) {
  addinstcap(compst, IOpenCapture, Cgroup, tree->key, 0);
  codegen(compst, sib1(tree), 0, tt, fullset);
  addinstcap(compst, ICloseRunTime, Cclose, 0, 0);
}
static void coderep (CompileState *compst, TTree *tree, int opt,
                     const Charset *fl) {
  Charset st;
  if (tocharset(tree, &st)) {
    addinstruction(compst, ISpan, 0);
    addcharset(compst, st.cs);
  }
  else {
    int e1 = getfirst(tree, fullset, &st);
    if (headfail(tree) || (!e1 && cs_disjoint(&st, fl))) {
      int jmp;
      int test = codetestset(compst, &st, 0);
      codegen(compst, tree, 0, test, fullset);
      jmp = addoffsetinst(compst, IJmp);
      jumptohere(compst, test);
      jumptothere(compst, jmp, test);
    }
    else {
      int commit, l2;
      int test = codetestset(compst, &st, e1);
      int pchoice = NOINST;
      if (opt)
        jumptohere(compst, addoffsetinst(compst, IPartialCommit));
      else
        pchoice = addoffsetinst(compst, IChoice);
      l2 = gethere(compst);
      codegen(compst, tree, 0, NOINST, fullset);
      commit = addoffsetinst(compst, IPartialCommit);
      jumptothere(compst, commit, l2);
      jumptohere(compst, pchoice);
      jumptohere(compst, test);
    }
  }
}
static void lpeg_codenot (CompileState *compst, TTree *tree) {
  Charset st;
  int e = getfirst(tree, fullset, &st);
  int test = codetestset(compst, &st, e);
  if (headfail(tree))  
    addinstruction(compst, IFail, 0);
  else {
    int pchoice = addoffsetinst(compst, IChoice);
    codegen(compst, tree, 0, NOINST, fullset);
    addinstruction(compst, IFailTwice, 0);
    jumptohere(compst, pchoice);
  }
  jumptohere(compst, test);
}
static void correctcalls (CompileState *compst, int *positions,
                          int from, int to) {
  int i;
  Instruction2 *code = compst->p->code;
  for (i = from; i < to; i += sizei(&code[i])) {
    if (code[i].i.code == IOpenCall) {
      int n = code[i].i.key;  
      int rule = positions[n];  
      assert(rule == from || code[rule - 1].i.code == IRet);
      if (code[finaltarget(code, i + 2)].i.code == IRet)  
        code[i].i.code = IJmp;  
      else
        code[i].i.code = ICall;
      jumptothere(compst, i, rule);  
    }
  }
  assert(i == to);
}
static void codegrammar (CompileState *compst, TTree *grammar) {
  int positions[MAXRULES];
  int rulenumber = 0;
  TTree *rule;
  int firstcall = addoffsetinst(compst, ICall);  
  int jumptoend = addoffsetinst(compst, IJmp);  
  int start = gethere(compst);  
  jumptohere(compst, firstcall);
  for (rule = sib1(grammar); rule->tag == TRule; rule = sib2(rule)) {
    positions[rulenumber++] = gethere(compst);  
    codegen(compst, sib1(rule), 0, NOINST, fullset);  
    addinstruction(compst, IRet, 0);
  }
  assert(rule->tag == TTrue);
  jumptohere(compst, jumptoend);
  correctcalls(compst, positions, start, gethere(compst));
}
static void codecall (CompileState *compst, TTree *call) {
  int c = addoffsetinst(compst, IOpenCall);  
  getinstr(compst, c).i.key = sib2(call)->cap;  
  assert(sib2(call)->tag == TRule);
}
static int codeseq1 (CompileState *compst, TTree *p1, TTree *p2,
                     int tt, const Charset *fl) {
  if (needfollow(p1)) {
    Charset fl1;
    getfirst(p2, fl, &fl1);  
    codegen(compst, p1, 0, tt, &fl1);
  }
  else  
    codegen(compst, p1, 0, tt, fullset);
  if (fixedlen(p1) != 0)  
    return  NOINST;  
  else return tt;  
}
static void codegen (CompileState *compst, TTree *tree, int opt, int tt,
                     const Charset *fl) {
 tailcall:
  switch (tree->tag) {
    case TChar: codechar(compst, tree->u.n, tt); break;
    case TAny: addinstruction(compst, IAny, 0); break;
    case TSet: codecharset(compst, treebuffer(tree), tt); break;
    case TTrue: break;
    case TFalse: addinstruction(compst, IFail, 0); break;
    case TChoice: codechoice(compst, sib1(tree), sib2(tree), opt, fl); break;
    case TRep: coderep(compst, sib1(tree), opt, fl); break;
    case TBehind: codebehind(compst, tree); break;
    case TNot: lpeg_codenot(compst, sib1(tree)); break;
    case TAnd: codeand(compst, sib1(tree), tt); break;
    case TCapture: codecapture(compst, tree, tt, fl); break;
    case TRunTime: coderuntime(compst, tree, tt); break;
    case TGrammar: codegrammar(compst, tree); break;
    case TCall: codecall(compst, tree); break;
    case TSeq: {
      tt = codeseq1(compst, sib1(tree), sib2(tree), tt, fl);  
      tree = sib2(tree); goto tailcall;
    }
    default: assert(0);
  }
}
static void peephole (CompileState *compst) {
  Instruction2 *code = compst->p->code;
  int i;
  for (i = 0; i < compst->ncode; i += sizei(&code[i])) {
   redo:
    switch (code[i].i.code) {
      case IChoice: case ICall: case ICommit: case IPartialCommit:
      case IBackCommit: case ITestChar: case ITestSet:
      case ITestAny: {  
        jumptothere(compst, i, finallabel(code, i));  
        break;
      }
      case IJmp: {
        int ft = finaltarget(code, i);
        switch (code[ft].i.code) {  
          case IRet: case IFail: case IFailTwice:
          case IEnd: {  
            code[i] = code[ft];  
            code[i + 1].i.code = IAny;  
            break;
          }
          case ICommit: case IPartialCommit:
          case IBackCommit: {  
            int fft = finallabel(code, ft);
            code[i] = code[ft];  
            jumptothere(compst, i, fft);  
            goto redo;  
          }
          default: {
            jumptothere(compst, i, ft);  
            break;
          }
        }
        break;
      }
      default: break;
    }
  }
  assert(code[i - 1].i.code == IEnd);
}
Instruction2 *compile (lua_State *L, Pattern *p) {
  CompileState compst;
  compst.p = p;  compst.ncode = 0;  compst.L = L;
  realloccode(L, p, 2);  
  codegen(&compst, p->tree, 0, NOINST, fullset);
  addinstruction(&compst, IEnd, 0);
  realloccode(L, p, compst.ncode);  
  peephole(&compst);
  return p->code;
}
#endif
#ifdef LUA_USE_LPEG
#if defined(LPEG_DEBUG)
void printcharset (const byte *st) {
  int i;
  printf("[");
  for (i = 0; i <= UCHAR_MAX; i++) {
    int first = i;
    while (testchar(st, i) && i <= UCHAR_MAX) i++;
    if (i - 1 == first)  
      printf("(%02x)", first);
    else if (i - 1 > first)  
      printf("(%02x-%02x)", first, i - 1);
  }
  printf("]");
}
static const char *capkind (int kind) {
  const char *const modes[] = {
    "close", "position", "constant", "backref",
    "argument", "simple", "table", "function",
    "query", "string", "num", "substitution", "fold",
    "runtime", "group"};
  return modes[kind];
}
static void printjmp (const Instruction2 *op, const Instruction2 *p) {
  printf("-> %d", (int)(p + (p + 1)->offset - op));
}
void printinst (const Instruction2 *op, const Instruction2 *p) {
  const char *const names[] = {
    "any", "char", "set",
    "testany", "testchar", "testset",
    "span", "behind",
    "ret", "end",
    "choice", "jmp", "call", "open_call",
    "commit", "partial_commit", "back_commit", "failtwice", "fail", "giveup",
     "fullcapture", "opencapture", "closecapture", "closeruntime"
  };
  printf("%02ld: %s ", (long)(p - op), names[p->i.code]);
  switch ((Opcode)p->i.code) {
    case IChar: {
      printf("'%c'", p->i.aux);
      break;
    }
    case ITestChar: {
      printf("'%c'", p->i.aux); printjmp(op, p);
      break;
    }
    case IFullCapture: {
      printf("%s (size = %d)  (idx = %d)",
             capkind(getkind(p)), getoff(p), p->i.key);
      break;
    }
    case IOpenCapture: {
      printf("%s (idx = %d)", capkind(getkind(p)), p->i.key);
      break;
    }
    case ISet: {
      printcharset((p+1)->buff);
      break;
    }
    case ITestSet: {
      printcharset((p+2)->buff); printjmp(op, p);
      break;
    }
    case ISpan: {
      printcharset((p+1)->buff);
      break;
    }
    case IOpenCall: {
      printf("-> %d", (p + 1)->offset);
      break;
    }
    case IBehind: {
      printf("%d", p->i.aux);
      break;
    }
    case IJmp: case ICall: case ICommit: case IChoice:
    case IPartialCommit: case IBackCommit: case ITestAny: {
      printjmp(op, p);
      break;
    }
    default: break;
  }
  printf("\n");
}
void printpatt (Instruction2 *p, int n) {
  Instruction2 *op = p;
  while (p < op + n) {
    printinst(op, p);
    p += sizei(p);
  }
}
#if defined(LPEG_DEBUG)
static void printcap (Capture *cap) {
  printf("%s (idx: %d - size: %d) -> %p\n",
         capkind(cap->kind), cap->idx, cap->siz, cap->s);
}
void printcaplist (Capture *cap, Capture *limit) {
  printf(">======\n");
  for (; cap->s && (limit == NULL || cap < limit); cap++)
    printcap(cap);
  printf("=======\n");
}
#endif
static const char *tagnames[] = {
  "char", "set", "any",
  "true", "false",
  "rep",
  "seq", "choice",
  "not", "and",
  "call", "opencall", "rule", "grammar",
  "behind",
  "capture", "run-time"
};
void printtree (TTree *tree, int ident) {
  int i;
  for (i = 0; i < ident; i++) printf(" ");
  printf("%s", tagnames[tree->tag]);
  switch (tree->tag) {
    case TChar: {
      int c = tree->u.n;
      if (isprint(c))
        printf(" '%c'\n", c);
      else
        printf(" (%02X)\n", c);
      break;
    }
    case TSet: {
      printcharset(treebuffer(tree));
      printf("\n");
      break;
    }
    case TOpenCall: case TCall: {
      assert(sib2(tree)->tag == TRule);
      printf(" key: %d  (rule: %d)\n", tree->key, sib2(tree)->cap);
      break;
    }
    case TBehind: {
      printf(" %d\n", tree->u.n);
        printtree(sib1(tree), ident + 2);
      break;
    }
    case TCapture: {
      printf(" kind: '%s'  key: %d\n", capkind(tree->cap), tree->key);
      printtree(sib1(tree), ident + 2);
      break;
    }
    case TRule: {
      printf(" n: %d  key: %d\n", tree->cap, tree->key);
      printtree(sib1(tree), ident + 2);
      break;  
    }
    case TGrammar: {
      TTree *rule = sib1(tree);
      printf(" %d\n", tree->u.n);  
      for (i = 0; i < tree->u.n; i++) {
        printtree(rule, ident + 2);
        rule = sib2(rule);
      }
      assert(rule->tag == TTrue);  
      break;
    }
    default: {
      int sibs = numsiblings[tree->tag];
      printf("\n");
      if (sibs >= 1) {
        printtree(sib1(tree), ident + 2);
        if (sibs >= 2)
          printtree(sib2(tree), ident + 2);
      }
      break;
    }
  }
}
void printktable (lua_State *L, int idx) {
  int n, i;
  lua_getuservalue(L, idx);
  if (lua_isnil(L, -1))  
    return;
  n = lua_rawlen(L, -1);
  printf("[");
  for (i = 1; i <= n; i++) {
    printf("%d = ", i);
    lua_rawgeti(L, -1, i);
    if (lua_isstring(L, -1))
      printf("%s  ", lua_tostring(L, -1));
    else
      printf("%s  ", lua_typename(L, lua_type(L, -1)));
    lua_pop(L, 1);
  }
  printf("]\n");
}
#endif
#endif
#ifdef LUA_USE_LPEG
const byte numsiblings[] = {
  0, 0, 0,	
  0, 0,		
  1,		
  2, 2,		
  1, 1,		
  0, 0, 2, 1,  
  1,  
  1, 1  
};
static TTree *newgrammar (lua_State *L, int arg);
static const char *val2str (lua_State *L, int idx) {
  const char *k = lua_tostring(L, idx);
  if (k != NULL)
    return lua_pushfstring(L, "%s", k);
  else
    return lua_pushfstring(L, "(a %s)", luaL_typename(L, idx));
}
static void fixonecall (lua_State *L, int postable, TTree *g, TTree *t) {
  int n;
  lua_rawgeti(L, -1, t->key);  
  lua_gettable(L, postable);  
  n = lua_tonumber(L, -1);  
  lua_pop(L, 1);  
  if (n == 0) {  
    lua_rawgeti(L, -1, t->key);  
    luaL_error(L, "rule '%s' undefined in given grammar", val2str(L, -1));
  }
  t->tag = TCall;
  t->u.ps = n - (t - g);  
  assert(sib2(t)->tag == TRule);
  sib2(t)->key = t->key;  
}
static void correctassociativity (TTree *tree) {
  TTree *t1 = sib1(tree);
  assert(tree->tag == TChoice || tree->tag == TSeq);
  while (t1->tag == tree->tag) {
    int n1size = tree->u.ps - 1;  
    int n11size = t1->u.ps - 1;
    int n12size = n1size - n11size - 1;
    memmove(sib1(tree), sib1(t1), n11size * sizeof(TTree)); 
    tree->u.ps = n11size + 1;
    sib2(tree)->tag = tree->tag;
    sib2(tree)->u.ps = n12size + 1;
  }
}
static void finalfix (lua_State *L, int postable, TTree *g, TTree *t) {
 tailcall:
  switch (t->tag) {
    case TGrammar:  
      return;
    case TOpenCall: {
      if (g != NULL)  
        fixonecall(L, postable, g, t);
      else {  
        lua_rawgeti(L, -1, t->key);
        luaL_error(L, "rule '%s' used outside a grammar", val2str(L, -1));
      }
      break;
    }
    case TSeq: case TChoice:
      correctassociativity(t);
      break;
  }
  switch (numsiblings[t->tag]) {
    case 1: 
      t = sib1(t); goto tailcall;
    case 2:
      finalfix(L, postable, g, sib1(t));
      t = sib2(t); goto tailcall;  
    default: assert(numsiblings[t->tag] == 0); break;
  }
}
static void newktable (lua_State *L, int n) {
  lua_createtable(L, n, 0);  
  lua_setuservalue(L, -2);  
}
static int addtoktable (lua_State *L, int idx) {
  if (lua_isnil(L, idx))  
    return 0;
  else {
    int n;
    lua_getuservalue(L, -1);  
    n = lua_rawlen(L, -1);
    if (n >= USHRT_MAX)
      luaL_error(L, "too many Lua values in pattern");
    lua_pushvalue(L, idx);  
    lua_rawseti(L, -2, ++n);
    lua_pop(L, 1);  
    return n;
  }
}
static int ktablelen (lua_State *L, int idx) {
  if (!lua_istable(L, idx)) return 0;
  else return lua_rawlen(L, idx);
}
static int concattable (lua_State *L, int idx1, int idx2) {
  int i;
  int n1 = ktablelen(L, idx1);
  int n2 = ktablelen(L, idx2);
  if (n1 + n2 > USHRT_MAX)
    luaL_error(L, "too many Lua values in pattern");
  if (n1 == 0) return 0;  
  for (i = 1; i <= n1; i++) {
    lua_rawgeti(L, idx1, i);
    lua_rawseti(L, idx2 - 1, n2 + i);  
  }
  return n2;
}
static void correctkeys (TTree *tree, int n) {
  if (n == 0) return;  
 tailcall:
  switch (tree->tag) {
    case TOpenCall: case TCall: case TRunTime: case TRule: {
      if (tree->key > 0)
        tree->key += n;
      break;
    }
    case TCapture: {
      if (tree->key > 0 && tree->cap != Carg && tree->cap != Cnum)
        tree->key += n;
      break;
    }
    default: break;
  }
  switch (numsiblings[tree->tag]) {
    case 1:  
      tree = sib1(tree); goto tailcall;
    case 2:
      correctkeys(sib1(tree), n);
      tree = sib2(tree); goto tailcall;  
    default: assert(numsiblings[tree->tag] == 0); break;
  }
}
static void joinktables (lua_State *L, int p1, TTree *t2, int p2) {
  int n1, n2;
  lua_getuservalue(L, p1);  
  lua_getuservalue(L, p2);
  n1 = ktablelen(L, -2);
  n2 = ktablelen(L, -1);
  if (n1 == 0 && n2 == 0)  
    lua_pop(L, 2);  
  else if (n2 == 0 || lp_equal(L, -2, -1)) {  
    lua_pop(L, 1);  
    lua_setuservalue(L, -2);  
  }
  else if (n1 == 0) {  
    lua_setuservalue(L, -3);  
    lua_pop(L, 1);  
  }
  else {
    lua_createtable(L, n1 + n2, 0);  
    concattable(L, -3, -1);  
    concattable(L, -2, -1);  
    lua_setuservalue(L, -4);  
    lua_pop(L, 2);  
    correctkeys(t2, n1);  
  }
}
static void copyktable (lua_State *L, int idx) {
  lua_getuservalue(L, idx);
  lua_setuservalue(L, -2);
}
static void mergektable (lua_State *L, int idx, TTree *stree) {
  int n;
  lua_getuservalue(L, -1);  
  lua_getuservalue(L, idx);
  n = concattable(L, -1, -2);
  lua_pop(L, 2);  
  correctkeys(stree, n);
}
static int addtonewktable (lua_State *L, int p, int idx) {
  newktable(L, 1);
  if (p)
    mergektable(L, p, NULL);
  return addtoktable(L, idx);
}
static int testpattern (lua_State *L, int idx) {
  if (lua_touserdata(L, idx)) {  
    if (lua_getmetatable(L, idx)) {  
      luaL_getmetatable(L, PATTERN_T);
      if (lua_rawequal(L, -1, -2)) {  
        lua_pop(L, 2);  
        return 1;
      }
    }
  }
  return 0;
}
static Pattern *getpattern (lua_State *L, int idx) {
  return (Pattern *)luaL_checkudata(L, idx, PATTERN_T);
}
static int getsize (lua_State *L, int idx) {
  return (lua_rawlen(L, idx) - sizeof(Pattern)) / sizeof(TTree) + 1;
}
static TTree *gettree (lua_State *L, int idx, int *len) {
  Pattern *p = getpattern(L, idx);
  if (len)
    *len = getsize(L, idx);
  return p->tree;
}
static TTree *newtree (lua_State *L, int len) {
  size_t size = (len - 1) * sizeof(TTree) + sizeof(Pattern);
  Pattern *p = (Pattern *)lua_newuserdata(L, size);
  luaL_getmetatable(L, PATTERN_T);
  lua_pushvalue(L, -1);
  lua_setuservalue(L, -3);
  lua_setmetatable(L, -2);
  p->code = NULL;  p->codesize = 0;
  return p->tree;
}
static TTree *newleaf (lua_State *L, int tag) {
  TTree *tree = newtree(L, 1);
  tree->tag = tag;
  return tree;
}
static TTree *newcharset (lua_State *L) {
  TTree *tree = newtree(L, bytes2slots(CHARSETSIZE) + 1);
  tree->tag = TSet;
  loopset(i, treebuffer(tree)[i] = 0);
  return tree;
}
static TTree *seqaux (TTree *tree, TTree *sib, int sibsize) {
  tree->tag = TSeq; tree->u.ps = sibsize + 1;
  memcpy(sib1(tree), sib, sibsize * sizeof(TTree));
  return sib2(tree);
}
static void fillseq (TTree *tree, int tag, int n, const char *s) {
  int i;
  for (i = 0; i < n - 1; i++) {  
    tree->tag = TSeq; tree->u.ps = 2;
    sib1(tree)->tag = tag;
    sib1(tree)->u.n = s ? (byte)s[i] : 0;
    tree = sib2(tree);
  }
  tree->tag = tag;  
  tree->u.n = s ? (byte)s[i] : 0;
}
static TTree *numtree (lua_State *L, int n) {
  if (n == 0)
    return newleaf(L, TTrue);
  else {
    TTree *tree, *nd;
    if (n > 0)
      tree = nd = newtree(L, 2 * n - 1);
    else {  
      n = -n;
      tree = newtree(L, 2 * n);
      tree->tag = TNot;
      nd = sib1(tree);
    }
    fillseq(nd, TAny, n, NULL);  
    return tree;
  }
}
static TTree *getpatt (lua_State *L, int idx, int *len) {
  TTree *tree;
  switch (lua_type(L, idx)) {
    case LUA_TSTRING: {
      size_t slen;
      const char *s = lua_tolstring(L, idx, &slen);  
      if (slen == 0)  
        tree = newleaf(L, TTrue);  
      else {
        tree = newtree(L, 2 * (slen - 1) + 1);
        fillseq(tree, TChar, slen, s);  
      }
      break;
    }
    case LUA_TNUMBER: {
      int n = lua_tointeger(L, idx);
      tree = numtree(L, n);
      break;
    }
    case LUA_TBOOLEAN: {
      tree = (lua_toboolean(L, idx) ? newleaf(L, TTrue) : newleaf(L, TFalse));
      break;
    }
    case LUA_TTABLE: {
      tree = newgrammar(L, idx);
      break;
    }
    case LUA_TFUNCTION: {
      tree = newtree(L, 2);
      tree->tag = TRunTime;
      tree->key = addtonewktable(L, 0, idx);
      sib1(tree)->tag = TTrue;
      break;
    }
    default: {
      return gettree(L, idx, len);
    }
  }
  lua_replace(L, idx);  
  if (len)
    *len = getsize(L, idx);
  return tree;
}
static TTree *newroot1sib (lua_State *L, int tag) {
  int s1;
  TTree *tree1 = getpatt(L, 1, &s1);
  TTree *tree = newtree(L, 1 + s1);  
  tree->tag = tag;
  memcpy(sib1(tree), tree1, s1 * sizeof(TTree));
  copyktable(L, 1);
  return tree;
}
static TTree *newroot2sib (lua_State *L, int tag) {
  int s1, s2;
  TTree *tree1 = getpatt(L, 1, &s1);
  TTree *tree2 = getpatt(L, 2, &s2);
  TTree *tree = newtree(L, 1 + s1 + s2);  
  tree->tag = tag;
  tree->u.ps =  1 + s1;
  memcpy(sib1(tree), tree1, s1 * sizeof(TTree));
  memcpy(sib2(tree), tree2, s2 * sizeof(TTree));
  joinktables(L, 1, sib2(tree), 2);
  return tree;
}
static int lp_P (lua_State *L) {
  luaL_checkany(L, 1);
  getpatt(L, 1, NULL);
  lua_settop(L, 1);
  return 1;
}
static int lp_seq (lua_State *L) {
  TTree *tree1 = getpatt(L, 1, NULL);
  TTree *tree2 = getpatt(L, 2, NULL);
  if (tree1->tag == TFalse || tree2->tag == TTrue)
    lua_pushvalue(L, 1);  
  else if (tree1->tag == TTrue)
    lua_pushvalue(L, 2);  
  else
    newroot2sib(L, TSeq);
  return 1;
}
static int lp_choice (lua_State *L) {
  Charset st1, st2;
  TTree *t1 = getpatt(L, 1, NULL);
  TTree *t2 = getpatt(L, 2, NULL);
  if (tocharset(t1, &st1) && tocharset(t2, &st2)) {
    TTree *t = newcharset(L);
    loopset(i, treebuffer(t)[i] = st1.cs[i] | st2.cs[i]);
  }
  else if (nofail(t1) || t2->tag == TFalse)
    lua_pushvalue(L, 1);  
  else if (t1->tag == TFalse)
    lua_pushvalue(L, 2);  
  else
    newroot2sib(L, TChoice);
  return 1;
}
static int lp_star (lua_State *L) {
  int size1;
  int n = (int)luaL_checkinteger(L, 2);
  TTree *tree1 = getpatt(L, 1, &size1);
  if (n >= 0) {  
    TTree *tree = newtree(L, (n + 1) * (size1 + 1));
    if (nullable(tree1))
      luaL_error(L, "loop body may accept empty string");
    while (n--)  
      tree = seqaux(tree, tree1, size1);
    tree->tag = TRep;
    memcpy(sib1(tree), tree1, size1 * sizeof(TTree));
  }
  else {  
    TTree *tree;
    n = -n;
    tree = newtree(L, n * (size1 + 3) - 1);
    for (; n > 1; n--) {  
      tree->tag = TChoice; tree->u.ps = n * (size1 + 3) - 2;
      sib2(tree)->tag = TTrue;
      tree = sib1(tree);
      tree = seqaux(tree, tree1, size1);
    }
    tree->tag = TChoice; tree->u.ps = size1 + 1;
    sib2(tree)->tag = TTrue;
    memcpy(sib1(tree), tree1, size1 * sizeof(TTree));
  }
  copyktable(L, 1);
  return 1;
}
static int lp_and (lua_State *L) {
  newroot1sib(L, TAnd);
  return 1;
}
static int lp_not (lua_State *L) {
  newroot1sib(L, TNot);
  return 1;
}
static int lp_sub (lua_State *L) {
  Charset st1, st2;
  int s1, s2;
  TTree *t1 = getpatt(L, 1, &s1);
  TTree *t2 = getpatt(L, 2, &s2);
  if (tocharset(t1, &st1) && tocharset(t2, &st2)) {
    TTree *t = newcharset(L);
    loopset(i, treebuffer(t)[i] = st1.cs[i] & ~st2.cs[i]);
  }
  else {
    TTree *tree = newtree(L, 2 + s1 + s2);
    tree->tag = TSeq;  
    tree->u.ps =  2 + s2;
    sib1(tree)->tag = TNot;  
    memcpy(sib1(sib1(tree)), t2, s2 * sizeof(TTree));  
    memcpy(sib2(tree), t1, s1 * sizeof(TTree));  
    joinktables(L, 1, sib1(tree), 2);
  }
  return 1;
}
static int lp_set (lua_State *L) {
  size_t l;
  const char *s = luaL_checklstring(L, 1, &l);
  TTree *tree = newcharset(L);
  while (l--) {
    setchar(treebuffer(tree), (byte)(*s));
    s++;
  }
  return 1;
}
static int lp_range (lua_State *L) {
  int arg;
  int top = lua_gettop(L);
  TTree *tree = newcharset(L);
  for (arg = 1; arg <= top; arg++) {
    int c;
    size_t l;
    const char *r = luaL_checklstring(L, arg, &l);
    luaL_argcheck(L, l == 2, arg, "range must have two characters");
    for (c = (byte)r[0]; c <= (byte)r[1]; c++)
      setchar(treebuffer(tree), c);
  }
  return 1;
}
static int lp_behind (lua_State *L) {
  TTree *tree;
  TTree *tree1 = getpatt(L, 1, NULL);
  int n = fixedlen(tree1);
  luaL_argcheck(L, n >= 0, 1, "pattern may not have fixed length");
  luaL_argcheck(L, !hascaptures(tree1), 1, "pattern have captures");
  luaL_argcheck(L, n <= MAXBEHIND, 1, "pattern too long to look behind");
  tree = newroot1sib(L, TBehind);
  tree->u.n = n;
  return 1;
}
static int lp_V (lua_State *L) {
  TTree *tree = newleaf(L, TOpenCall);
  luaL_argcheck(L, !lua_isnoneornil(L, 1), 1, "non-nil value expected");
  tree->key = addtonewktable(L, 0, 1);
  return 1;
}
static int capture_aux (lua_State *L, int cap, int labelidx) {
  TTree *tree = newroot1sib(L, TCapture);
  tree->cap = cap;
  tree->key = (labelidx == 0) ? 0 : addtonewktable(L, 1, labelidx);
  return 1;
}
static TTree *auxemptycap (TTree *tree, int cap) {
  tree->tag = TCapture;
  tree->cap = cap;
  sib1(tree)->tag = TTrue;
  return tree;
}
static TTree *newemptycap (lua_State *L, int cap, int key) {
  TTree *tree = auxemptycap(newtree(L, 2), cap);
  tree->key = key;
  return tree;
}
static TTree *newemptycapkey (lua_State *L, int cap, int idx) {
  TTree *tree = auxemptycap(newtree(L, 2), cap);
  tree->key = addtonewktable(L, 0, idx);
  return tree;
}
static int lp_divcapture (lua_State *L) {
  switch (lua_type(L, 2)) {
    case LUA_TFUNCTION: return capture_aux(L, Cfunction, 2);
    case LUA_TTABLE: return capture_aux(L, Cquery, 2);
    case LUA_TSTRING: return capture_aux(L, Cstring, 2);
    case LUA_TNUMBER: {
      int n = lua_tointeger(L, 2);
      TTree *tree = newroot1sib(L, TCapture);
      luaL_argcheck(L, 0 <= n && n <= SHRT_MAX, 1, "invalid number");
      tree->cap = Cnum;
      tree->key = n;
      return 1;
    }
    default: return luaL_argerror(L, 2, "invalid replacement value");
  }
}
static int lp_substcapture (lua_State *L) {
  return capture_aux(L, Csubst, 0);
}
static int lp_tablecapture (lua_State *L) {
  return capture_aux(L, Ctable, 0);
}
static int lp_groupcapture (lua_State *L) {
  if (lua_isnoneornil(L, 2))
    return capture_aux(L, Cgroup, 0);
  else
    return capture_aux(L, Cgroup, 2);
}
static int lp_foldcapture (lua_State *L) {
  luaL_checktype(L, 2, LUA_TFUNCTION);
  return capture_aux(L, Cfold, 2);
}
static int lp_simplecapture (lua_State *L) {
  return capture_aux(L, Csimple, 0);
}
static int lp_poscapture (lua_State *L) {
  newemptycap(L, Cposition, 0);
  return 1;
}
static int lp_argcapture (lua_State *L) {
  int n = (int)luaL_checkinteger(L, 1);
  luaL_argcheck(L, 0 < n && n <= SHRT_MAX, 1, "invalid argument index");
  newemptycap(L, Carg, n);
  return 1;
}
static int lp_backref (lua_State *L) {
  luaL_checkany(L, 1);
  newemptycapkey(L, Cbackref, 1);
  return 1;
}
static int lp_constcapture (lua_State *L) {
  int i;
  int n = lua_gettop(L);  
  if (n == 0)  
    newleaf(L, TTrue);  
  else if (n == 1)
    newemptycapkey(L, Cconst, 1);  
  else {  
    TTree *tree = newtree(L, 1 + 3 * (n - 1) + 2);
    newktable(L, n);  
    tree->tag = TCapture;
    tree->cap = Cgroup;
    tree->key = 0;
    tree = sib1(tree);
    for (i = 1; i <= n - 1; i++) {
      tree->tag = TSeq;
      tree->u.ps = 3;  
      auxemptycap(sib1(tree), Cconst);
      sib1(tree)->key = addtoktable(L, i);
      tree = sib2(tree);
    }
    auxemptycap(tree, Cconst);
    tree->key = addtoktable(L, i);
  }
  return 1;
}
static int lp_matchtime (lua_State *L) {
  TTree *tree;
  luaL_checktype(L, 2, LUA_TFUNCTION);
  tree = newroot1sib(L, TRunTime);
  tree->key = addtonewktable(L, 1, 2);
  return 1;
}
static void getfirstrule (lua_State *L, int arg, int postab) {
  lua_rawgeti(L, arg, 1);  
  if (lua_isstring(L, -1)) {  
    lua_pushvalue(L, -1);  
    lua_gettable(L, arg);  
  }
  else {
    lua_pushinteger(L, 1);  
    lua_insert(L, -2);  
  }
  if (!testpattern(L, -1)) {  
    if (lua_isnil(L, -1))
      luaL_error(L, "grammar has no initial rule");
    else
      luaL_error(L, "initial rule '%s' is not a pattern", lua_tostring(L, -2));
  }
  lua_pushvalue(L, -2);  
  lua_pushinteger(L, 1);  
  lua_settable(L, postab);  
}
static int collectrules (lua_State *L, int arg, int *totalsize) {
  int n = 1;  
  int postab = lua_gettop(L) + 1;  
  int size;  
  lua_newtable(L);  
  getfirstrule(L, arg, postab);
  size = 2 + getsize(L, postab + 2);  
  lua_pushnil(L);  
  while (lua_next(L, arg) != 0) {
    if (lua_tonumber(L, -2) == 1 ||
        lp_equal(L, -2, postab + 1)) {  
      lua_pop(L, 1);  
      continue;
    }
    if (!testpattern(L, -1))  
      luaL_error(L, "rule '%s' is not a pattern", val2str(L, -2));
    luaL_checkstack(L, LUA_MINSTACK, "grammar has too many rules");
    lua_pushvalue(L, -2);  
    lua_pushinteger(L, size);
    lua_settable(L, postab);
    size += 1 + getsize(L, -1);  
    lua_pushvalue(L, -2);  
    n++;
  }
  *totalsize = size + 1;  
  return n;
}
static void buildgrammar (lua_State *L, TTree *grammar, int frule, int n) {
  int i;
  TTree *nd = sib1(grammar);  
  for (i = 0; i < n; i++) {  
    int ridx = frule + 2*i + 1;  
    int rulesize;
    TTree *rn = gettree(L, ridx, &rulesize);
    nd->tag = TRule;
    nd->key = 0;  
    nd->cap = i;  
    nd->u.ps = rulesize + 1;  
    memcpy(sib1(nd), rn, rulesize * sizeof(TTree));  
    mergektable(L, ridx, sib1(nd));  
    nd = sib2(nd);  
  }
  nd->tag = TTrue;  
}
static int checkloops (TTree *tree) {
 tailcall:
  if (tree->tag == TRep && nullable(sib1(tree)))
    return 1;
  else if (tree->tag == TGrammar)
    return 0;  
  else {
    switch (numsiblings[tree->tag]) {
      case 1:  
        tree = sib1(tree); goto tailcall;
      case 2:
        if (checkloops(sib1(tree))) return 1;
        tree = sib2(tree); goto tailcall;
      default: assert(numsiblings[tree->tag] == 0); return 0;
    }
  }
}
static int verifyerror (lua_State *L, int *passed, int npassed) {
  int i, j;
  for (i = npassed - 1; i >= 0; i--) {  
    for (j = i - 1; j >= 0; j--) {
      if (passed[i] == passed[j]) {
        lua_rawgeti(L, -1, passed[i]);  
        return luaL_error(L, "rule '%s' may be left recursive", val2str(L, -1));
      }
    }
  }
  return luaL_error(L, "too many left calls in grammar");
}
static int verifyrule (lua_State *L, TTree *tree, int *passed, int npassed,
                       int nb) {
 tailcall:
  switch (tree->tag) {
    case TChar: case TSet: case TAny:
    case TFalse:
      return nb;  
    case TTrue:
    case TBehind:  
      return 1;
    case TNot: case TAnd: case TRep:
      tree = sib1(tree); nb = 1; goto tailcall;
    case TCapture: case TRunTime:
      tree = sib1(tree); goto tailcall;
    case TCall:
      tree = sib2(tree); goto tailcall;
    case TSeq:  
      if (!verifyrule(L, sib1(tree), passed, npassed, 0))
        return nb;
      tree = sib2(tree); goto tailcall;
    case TChoice:  
      nb = verifyrule(L, sib1(tree), passed, npassed, nb);
      tree = sib2(tree); goto tailcall;
    case TRule:
      if (npassed >= MAXRULES)
        return verifyerror(L, passed, npassed);
      else {
        passed[npassed++] = tree->key;
        tree = sib1(tree); goto tailcall;
      }
    case TGrammar:
      return nullable(tree);  
    default: assert(0); return 0;
  }
}
static void verifygrammar (lua_State *L, TTree *grammar) {
  int passed[MAXRULES];
  TTree *rule;
  for (rule = sib1(grammar); rule->tag == TRule; rule = sib2(rule)) {
    if (rule->key == 0) continue;  
    verifyrule(L, sib1(rule), passed, 0, 0);
  }
  assert(rule->tag == TTrue);
  for (rule = sib1(grammar); rule->tag == TRule; rule = sib2(rule)) {
    if (rule->key == 0) continue;  
    if (checkloops(sib1(rule))) {
      lua_rawgeti(L, -1, rule->key);  
      luaL_error(L, "empty loop in rule '%s'", val2str(L, -1));
    }
  }
  assert(rule->tag == TTrue);
}
static void initialrulename (lua_State *L, TTree *grammar, int frule) {
  if (sib1(grammar)->key == 0) {  
    int n = lua_rawlen(L, -1) + 1;  
    lua_pushvalue(L, frule);  
    lua_rawseti(L, -2, n);  
    sib1(grammar)->key = n;
  }
}
static TTree *newgrammar (lua_State *L, int arg) {
  int treesize;
  int frule = lua_gettop(L) + 2;  
  int n = collectrules(L, arg, &treesize);
  TTree *g = newtree(L, treesize);
  luaL_argcheck(L, n <= MAXRULES, arg, "grammar has too many rules");
  g->tag = TGrammar;  g->u.n = n;
  lua_newtable(L);  
  lua_setuservalue(L, -2);
  buildgrammar(L, g, frule, n);
  lua_getuservalue(L, -1);  
  finalfix(L, frule - 1, g, sib1(g));
  initialrulename(L, g, frule);
  verifygrammar(L, g);
  lua_pop(L, 1);  
  lua_insert(L, -(n * 2 + 2));  
  lua_pop(L, n * 2 + 1);  
  return g;  
}
static Instruction2 *prepcompile (lua_State *L, Pattern *p, int idx) {
  lua_getuservalue(L, idx);  
  finalfix(L, 0, NULL, p->tree);
  lua_pop(L, 1);  
  return compile(L, p);
}
static int lp_printtree (lua_State *L) {
  TTree *tree = getpatt(L, 1, NULL);
  int c = lua_toboolean(L, 2);
  if (c) {
    lua_getuservalue(L, 1);  
    finalfix(L, 0, NULL, tree);
    lua_pop(L, 1);  
  }
  printktable(L, 1);
  printtree(tree, 0);
  return 0;
}
static int lp_printcode (lua_State *L) {
  Pattern *p = getpattern(L, 1);
  printktable(L, 1);
  if (p->code == NULL)  
    prepcompile(L, p, 1);
  printpatt(p->code, p->codesize);
  return 0;
}
static size_t initposition (lua_State *L, size_t len) {
  lua_Integer ii = luaL_optinteger(L, 3, 1);
  if (ii > 0) {  
    if ((size_t)ii <= len)  
      return (size_t)ii - 1;  
    else return len;  
  }
  else {  
    if ((size_t)(-ii) <= len)  
      return len - ((size_t)(-ii));  
    else return 0;  
  }
}
static int lp_match (lua_State *L) {
  Capture capture[INITCAPSIZE];
  const char *r;
  size_t l;
  Pattern *p = (getpatt(L, 1, NULL), getpattern(L, 1));
  Instruction2 *code = (p->code != NULL) ? p->code : prepcompile(L, p, 1);
  const char *s = luaL_checklstring(L, SUBJIDX, &l);
  size_t i = initposition(L, l);
  int ptop = lua_gettop(L);
  lua_pushnil(L);  
  lua_pushlightuserdata(L, capture);  
  lua_getuservalue(L, 1);  
  r = lpeg_match(L, s, s + i, s + l, code, capture, ptop);
  if (r == NULL) {
    lua_pushnil(L);
    return 1;
  }
  return getcaptures(L, s, r, ptop);
}
#define MAXLIM		(INT_MAX / 100)
static int lp_setmax (lua_State *L) {
  lua_Integer lim = luaL_checkinteger(L, 1);
  luaL_argcheck(L, 0 < lim && lim <= MAXLIM, 1, "out of range");
  lua_settop(L, 1);
  lua_setfield(L, LUA_REGISTRYINDEX, MAXSTACKIDX);
  return 0;
}
static int lp_version (lua_State *L) {
  lua_pushstring(L, VERSION);
  return 1;
}
static int lp_type (lua_State *L) {
  if (testpattern(L, 1))
    lua_pushliteral(L, "pattern");
  else
    lua_pushnil(L);
  return 1;
}
int lp_gc (lua_State *L) {
  Pattern *p = getpattern(L, 1);
  realloccode(L, p, 0);  
  return 0;
}
static void createcat (lua_State *L, const char *catname, int (catf) (int)) {
  TTree *t = newcharset(L);
  int i;
  for (i = 0; i <= UCHAR_MAX; i++)
    if (catf(i)) setchar(treebuffer(t), i);
  lua_setfield(L, -2, catname);
}
static int lp_locale (lua_State *L) {
  if (lua_isnoneornil(L, 1)) {
    lua_settop(L, 0);
    lua_createtable(L, 0, 12);
  }
  else {
    luaL_checktype(L, 1, LUA_TTABLE);
    lua_settop(L, 1);
  }
  createcat(L, "alnum", isalnum);
  createcat(L, "alpha", isalpha);
  createcat(L, "cntrl", iscntrl);
  createcat(L, "digit", isdigit);
  createcat(L, "graph", isgraph);
  createcat(L, "lower", islower);
  createcat(L, "print", isprint);
  createcat(L, "punct", ispunct);
  createcat(L, "space", isspace);
  createcat(L, "upper", isupper);
  createcat(L, "xdigit", isxdigit);
  return 1;
}
static struct luaL_Reg pattreg[] = {
  {"ptree", lp_printtree},
  {"pcode", lp_printcode},
  {"match", lp_match},
  {"B", lp_behind},
  {"V", lp_V},
  {"C", lp_simplecapture},
  {"Cc", lp_constcapture},
  {"Cmt", lp_matchtime},
  {"Cb", lp_backref},
  {"Carg", lp_argcapture},
  {"Cp", lp_poscapture},
  {"Cs", lp_substcapture},
  {"Ct", lp_tablecapture},
  {"Cf", lp_foldcapture},
  {"Cg", lp_groupcapture},
  {"P", lp_P},
  {"S", lp_set},
  {"R", lp_range},
  {"locale", lp_locale},
  {"version", lp_version},
  {"setmaxstack", lp_setmax},
  {"type", lp_type},
  {NULL, NULL}
};
static struct luaL_Reg metareg[] = {
  {"__mul", lp_seq},
  {"__add", lp_choice},
  {"__pow", lp_star},
  {"__gc", lp_gc},
  {"__len", lp_and},
  {"__div", lp_divcapture},
  {"__unm", lp_not},
  {"__sub", lp_sub},
  {NULL, NULL}
};
int luaopen_lpeg (lua_State *L);
int luaopen_lpeg (lua_State *L) {
  luaL_newmetatable(L, PATTERN_T);
  lua_pushnumber(L, MAXBACK);  
  lua_setfield(L, LUA_REGISTRYINDEX, MAXSTACKIDX);
  luaL_setfuncs(L, metareg, 0);
  luaL_newlib(L, pattreg);
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "__index");
  return 1;
}
#endif
#ifdef LUA_USE_LPEG
#if !defined(INITBACK)
#define INITBACK	MAXBACK
#endif
#define getoffset(p)	(((p) + 1)->offset)
static const Instruction2 giveup = {{IGiveup, 0, 0}};
typedef struct Stack {
  const char *s;  
  const Instruction2 *p;  
  int caplevel;
} Stack;
#define getstackbase(L, ptop)	((Stack *)lua_touserdata(L, stackidx(ptop)))
static Capture *growcap (lua_State *L, Capture *capture, int *capsize,
                                       int captop, int n, int ptop) {
  if (*capsize - captop > n)
    return capture;  
  else {  
    Capture *newc;
    int newsize = captop + n + 1;  
    if (newsize < INT_MAX/((int)sizeof(Capture) * 2))
      newsize *= 2;  
    else if (newsize >= INT_MAX/((int)sizeof(Capture)))
      luaL_error(L, "too many captures");
    newc = (Capture *)lua_newuserdata(L, newsize * sizeof(Capture));
    memcpy(newc, capture, captop * sizeof(Capture));
    *capsize = newsize;
    lua_replace(L, caplistidx(ptop));
    return newc;
  }
}
static Stack *doublestack (lua_State *L, Stack **stacklimit, int ptop) {
  Stack *stack = getstackbase(L, ptop);
  Stack *newstack;
  int n = *stacklimit - stack;  
  int max, newn;
  lua_getfield(L, LUA_REGISTRYINDEX, MAXSTACKIDX);
  max = lua_tointeger(L, -1);  
  lua_pop(L, 1);
  if (n >= max)  
    luaL_error(L, "backtrack stack overflow (current limit is %d)", max);
  newn = 2 * n;  
  if (newn > max) newn = max;
  newstack = (Stack *)lua_newuserdata(L, newn * sizeof(Stack));
  memcpy(newstack, stack, n * sizeof(Stack));
  lua_replace(L, stackidx(ptop));
  *stacklimit = newstack + newn;
  return newstack + n;  
}
static int resdyncaptures (lua_State *L, int fr, int curr, int limit) {
  lua_Integer res;
  if (!lua_toboolean(L, fr)) {  
    lua_settop(L, fr - 1);  
    return -1;  
  }
  else if (lua_isboolean(L, fr))  
    res = curr;  
  else {
    res = lua_tointeger(L, fr) - 1;  
    if (res < curr || res > limit)
      luaL_error(L, "invalid position returned by match-time capture");
  }
  lua_remove(L, fr);  
  return res;
}
static void adddyncaptures (const char *s, Capture *capture, int n, int fd) {
  int i;
  assert(capture[-1].kind == Cgroup && capture[-1].siz == 0);
  capture[-1].idx = 0;  
  for (i = 0; i < n; i++) {  
    capture[i].kind = Cruntime;
    capture[i].siz = 1;  
    capture[i].idx = fd + i;  
    capture[i].s = s;
  }
  capture[n].kind = Cclose;  
  capture[n].siz = 1;
  capture[n].s = s;
}
static int removedyncap (lua_State *L, Capture *capture,
                         int level, int last) {
  int id = finddyncap(capture + level, capture + last);  
  int top = lua_gettop(L);
  if (id == 0) return 0;  
  lua_settop(L, id - 1);  
  return top - id + 1;  
}
const char *lpeg_match (lua_State *L, const char *o, const char *s, const char *e,
                   Instruction2 *op, Capture *capture, int ptop) {
  Stack stackbase[INITBACK];
  Stack *stacklimit = stackbase + INITBACK;
  Stack *stack = stackbase;  
  int capsize = INITCAPSIZE;
  int captop = 0;  
  int ndyncap = 0;  
  const Instruction2 *p = op;  
  stack->p = &giveup; stack->s = s; stack->caplevel = 0; stack++;
  lua_pushlightuserdata(L, stackbase);
  for (;;) {
#if defined(LPEG_DEBUG)
      printf("-------------------------------------\n");
      printcaplist(capture, capture + captop);
      printf("s: |%s| stck:%d, dyncaps:%d, caps:%d  ",
             s, (int)(stack - getstackbase(L, ptop)), ndyncap, captop);
      printinst(op, p);
#endif
    assert(stackidx(ptop) + ndyncap == lua_gettop(L) && ndyncap <= captop);
    switch ((Opcode)p->i.code) {
      case IEnd: {
        assert(stack == getstackbase(L, ptop) + 1);
        capture[captop].kind = Cclose;
        capture[captop].s = NULL;
        return s;
      }
      case IGiveup: {
        assert(stack == getstackbase(L, ptop));
        return NULL;
      }
      case IRet: {
        assert(stack > getstackbase(L, ptop) && (stack - 1)->s == NULL);
        p = (--stack)->p;
        continue;
      }
      case IAny: {
        if (s < e) { p++; s++; }
        else goto fail;
        continue;
      }
      case ITestAny: {
        if (s < e) p += 2;
        else p += getoffset(p);
        continue;
      }
      case IChar: {
        if ((byte)*s == p->i.aux && s < e) { p++; s++; }
        else goto fail;
        continue;
      }
      case ITestChar: {
        if ((byte)*s == p->i.aux && s < e) p += 2;
        else p += getoffset(p);
        continue;
      }
      case ISet: {
        int c = (byte)*s;
        if (testchar((p+1)->buff, c) && s < e)
          { p += CHARSETINSTSIZE; s++; }
        else goto fail;
        continue;
      }
      case ITestSet: {
        int c = (byte)*s;
        if (testchar((p + 2)->buff, c) && s < e)
          p += 1 + CHARSETINSTSIZE;
        else p += getoffset(p);
        continue;
      }
      case IBehind: {
        int n = p->i.aux;
        if (n > s - o) goto fail;
        s -= n; p++;
        continue;
      }
      case ISpan: {
        for (; s < e; s++) {
          int c = (byte)*s;
          if (!testchar((p+1)->buff, c)) break;
        }
        p += CHARSETINSTSIZE;
        continue;
      }
      case IJmp: {
        p += getoffset(p);
        continue;
      }
      case IChoice: {
        if (stack == stacklimit)
          stack = doublestack(L, &stacklimit, ptop);
        stack->p = p + getoffset(p);
        stack->s = s;
        stack->caplevel = captop;
        stack++;
        p += 2;
        continue;
      }
      case ICall: {
        if (stack == stacklimit)
          stack = doublestack(L, &stacklimit, ptop);
        stack->s = NULL;
        stack->p = p + 2;  
        stack++;
        p += getoffset(p);
        continue;
      }
      case ICommit: {
        assert(stack > getstackbase(L, ptop) && (stack - 1)->s != NULL);
        stack--;
        p += getoffset(p);
        continue;
      }
      case IPartialCommit: {
        assert(stack > getstackbase(L, ptop) && (stack - 1)->s != NULL);
        (stack - 1)->s = s;
        (stack - 1)->caplevel = captop;
        p += getoffset(p);
        continue;
      }
      case IBackCommit: {
        assert(stack > getstackbase(L, ptop) && (stack - 1)->s != NULL);
        s = (--stack)->s;
        captop = stack->caplevel;
        p += getoffset(p);
        continue;
      }
      case IFailTwice:
        assert(stack > getstackbase(L, ptop));
        stack--;
      case IFail:
      fail: { 
        do {  
          assert(stack > getstackbase(L, ptop));
          s = (--stack)->s;
        } while (s == NULL);
        if (ndyncap > 0)  
          ndyncap -= removedyncap(L, capture, stack->caplevel, captop);
        captop = stack->caplevel;
        p = stack->p;
#if defined(LPEG_DEBUG)
        printf("**FAIL**\n");
#endif
        continue;
      }
      case ICloseRunTime: {
        CapState cs;
        int rem, res, n;
        int fr = lua_gettop(L) + 1;  
        cs.reclevel = 0; cs.L = L;
        cs.s = o; cs.ocap = capture; cs.ptop = ptop;
        n = runtimecap(&cs, capture + captop, s, &rem);  
        captop -= n;  
        ndyncap -= rem;  
        fr -= rem;  
        res = resdyncaptures(L, fr, s - o, e - o);  
        if (res == -1)  
          goto fail;
        s = o + res;  
        n = lua_gettop(L) - fr + 1;  
        ndyncap += n;  
        if (n == 0)  
          captop--;  
        else {  
          if (fr + n >= SHRT_MAX)
            luaL_error(L, "too many results in match-time capture");
          capture = growcap(L, capture, &capsize, captop, n + 1, ptop);
          adddyncaptures(s, capture + captop, n, fr);
          captop += n + 1;  
        }
        p++;
        continue;
      }
      case ICloseCapture: {
        const char *s1 = s;
        assert(captop > 0);
        if (capture[captop - 1].siz == 0 &&
            s1 - capture[captop - 1].s < UCHAR_MAX) {
          capture[captop - 1].siz = s1 - capture[captop - 1].s + 1;
          p++;
          continue;
        }
        else {
          capture[captop].siz = 1;  
          capture[captop].s = s;
          goto pushcapture;
        }
      }
      case IOpenCapture:
        capture[captop].siz = 0;  
        capture[captop].s = s;
        goto pushcapture;
      case IFullCapture:
        capture[captop].siz = getoff(p) + 1;  
        capture[captop].s = s - getoff(p);
      pushcapture: {
        capture[captop].idx = p->i.key;
        capture[captop].kind = getkind(p);
        captop++;
        capture = growcap(L, capture, &capsize, captop, 0, ptop);
        p++;
        continue;
      }
      default: assert(0); return NULL;
    }
  }
}
#endif
#ifdef LUA_USE_DKJSON
static const size_t DKJSON_SIZE = 12179;
static const unsigned char DKJSON[12180] = {
27,76,117,97,83,0,25,147,13,10,26,10,4,8,4,8,8,120,86,0,0,0,0,0,0,0,0,0,0,0,40,119,64,1,0,0,0,0,0,0,0,0,0,0,1,53,126,0,0,0,3,0,128,0,67,0,0,0,129,0,0,0,198,64,64,
0,6,129,64,0,70,193,64,0,134,1,65,0,198,65,65,0,6,130,65,0,70,194,65,0,134,2,66,0,198,66,66,0,6,131,66,0,70,195,66,0,134,3,67,0,135,67,67,7,198,3,67,0,199,131,195,7,6,196,67,0,7,4,68,
8,70,196,67,0,71,68,196,8,134,196,67,0,135,132,68,9,198,196,67,0,199,196,196,9,6,197,67,0,7,5,69,10,70,197,67,0,71,69,197,10,134,197,67,0,135,133,69,11,198,197,67,0,199,197,197,11,6,198,67,0,7,6,70,
12,70,70,70,0,71,134,198,12,139,70,0,0,138,6,199,141,8,128,6,128,98,0,0,0,30,128,0,128,134,70,71,0,198,6,64,0,138,198,6,1,128,6,0,6,236,6,0,0,164,70,0,1,134,6,64,0,192,6,0,4,11,7,0,
0,75,71,0,0,172,71,0,0,74,135,135,143,228,134,128,1,138,198,6,143,172,134,0,0,203,198,1,0,202,70,72,144,202,198,72,145,202,70,73,146,202,198,73,147,202,70,74,148,202,198,74,149,202,70,75,150,44,199,0,0,108,7,1,
0,172,71,1,0,198,7,64,0,202,135,7,151,236,135,1,0,4,8,128,0,172,200,1,0,192,8,0,17,228,72,128,0,236,8,2,0,44,73,2,0,108,137,2,0,134,9,64,0,236,201,2,0,138,201,137,151,132,9,0,0,236,9,3,
0,44,74,3,0,108,138,3,0,134,10,64,0,236,202,3,0,138,202,10,152,172,10,4,0,128,9,0,21,134,10,64,0,236,74,4,0,138,202,138,152,172,138,4,0,236,202,4,0,44,11,5,0,75,11,2,0,74,139,76,153,74,139,72,
145,74,203,204,153,74,11,73,154,74,139,201,154,74,203,77,155,74,139,74,156,74,11,203,156,172,75,5,0,236,139,5,0,4,12,0,0,108,204,5,0,172,12,6,0,0,12,0,25,172,76,6,0,198,12,64,0,44,141,6,0,202,12,13,
157,198,12,64,0,44,205,6,0,202,12,141,157,34,0,0,0,30,192,0,128,192,12,0,6,6,13,79,0,7,205,78,26,228,76,0,1,38,0,128,0,61,0,0,0,4,7,100,107,106,115,111,110,4,6,112,97,105,114,115,4,5,116,121,
112,101,4,9,116,111,115,116,114,105,110,103,4,9,116,111,110,117,109,98,101,114,4,13,103,101,116,109,101,116,97,116,97,98,108,101,4,13,115,101,116,109,101,116,97,116,97,98,108,101,4,7,114,97,119,115,101,116,4,6,101,114,114,111,
114,4,8,114,101,113,117,105,114,101,4,6,112,99,97,108,108,4,7,115,101,108,101,99,116,4,5,109,97,116,104,4,6,102,108,111,111,114,4,5,104,117,103,101,4,7,115,116,114,105,110,103,4,4,114,101,112,4,5,103,115,117,98,4,
4,115,117,98,4,5,98,121,116,101,4,5,99,104,97,114,4,5,102,105,110,100,4,4,108,101,110,4,7,102,111,114,109,97,116,4,6,109,97,116,99,104,4,6,116,97,98,108,101,4,7,99,111,110,99,97,116,4,8,118,101,114,115,105,
111,110,4,11,100,107,106,115,111,110,32,50,46,53,4,3,95,71,4,5,110,117,108,108,4,9,95,95,116,111,106,115,111,110,4,2,34,4,3,92,34,4,2,92,4,3,92,92,4,2,8,4,3,92,98,4,2,12,4,3,92,102,4,2,
10,4,3,92,110,4,2,13,4,3,92,114,4,2,9,4,3,92,116,4,12,113,117,111,116,101,115,116,114,105,110,103,4,11,97,100,100,110,101,119,108,105,110,101,4,16,101,110,99,111,100,101,101,120,99,101,112,116,105,111,110,4,7,101,
110,99,111,100,101,4,2,34,4,2,47,4,2,98,4,2,102,4,2,110,4,2,10,4,2,114,4,2,116,4,7,100,101,99,111,100,101,4,9,117,115,101,95,108,112,101,103,4,7,100,107,106,115,111,110,1,0,0,0,1,0,28,0,0,
0,0,17,0,0,0,20,0,0,0,0,0,2,8,0,0,0,5,0,0,0,65,0,0,0,36,128,0,1,7,64,64,0,34,0,0,0,30,0,0,128,9,0,128,0,38,0,128,0,2,0,0,0,4,6,100,101,98,117,103,4,13,103,101,
116,109,101,116,97,116,97,98,108,101,2,0,0,0,1,11,1,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,22,0,0,0,22,0,0,0,0,0,2,3,0,0,0,1,0,0,0,38,0,0,1,38,0,128,0,1,0,
0,0,4,5,110,117,108,108,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,0,0,0,46,0,0,0,1,0,11,52,0,0,0,65,0,0,0,129,0,0,0,193,0,0,0,5,1,0,0,64,1,0,0,
36,1,1,1,30,64,7,128,31,64,192,3,30,64,2,128,69,2,128,0,128,2,0,4,100,130,0,1,31,128,192,4,30,0,1,128,192,0,0,4,32,0,130,0,30,192,4,128,64,0,0,4,30,64,4,128,69,2,128,0,128,2,128,3,
100,130,0,1,31,128,192,4,30,128,1,128,96,192,192,3,30,0,1,128,69,2,0,1,128,2,128,3,100,130,0,1,95,192,129,4,30,64,0,128,67,2,0,0,102,2,0,1,32,192,129,0,30,0,0,128,64,0,128,3,141,192,64,1,
41,129,0,0,170,193,247,127,32,64,0,130,30,128,1,128,32,64,128,1,30,0,1,128,15,65,65,1,32,64,0,2,30,64,0,128,3,1,0,0,38,1,0,1,3,1,128,0,64,1,128,0,38,1,128,1,38,0,128,0,6,0,0,0,
19,0,0,0,0,0,0,0,0,4,2,110,4,7,110,117,109,98,101,114,19,1,0,0,0,0,0,0,0,19,10,0,0,0,0,0,0,0,19,2,0,0,0,0,0,0,0,3,0,0,0,1,3,1,4,1,14,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,53,0,0,0,80,0,0,0,1,0,12,105,0,0,0,70,0,0,0,98,0,0,0,30,0,0,128,102,0,0,1,133,0,128,0,192,0,0,0,1,1,0,0,65,65,0,0,164,64,1,2,163,65,0,
1,30,0,0,128,129,129,0,0,227,65,128,1,30,0,0,128,193,129,0,0,35,66,0,2,30,0,0,128,1,130,0,0,98,65,0,0,30,0,0,128,65,129,0,0,0,1,0,4,192,0,128,3,128,0,0,3,33,192,64,1,30,64,0,
128,64,0,0,1,30,64,12,128,33,128,0,130,30,0,2,128,33,64,65,1,30,128,1,128,33,192,0,131,30,0,1,128,142,1,65,1,143,193,65,3,141,193,0,3,78,128,65,3,30,128,9,128,33,128,0,132,30,64,3,128,33,64,66,
1,30,192,2,128,33,192,0,131,30,64,2,128,33,0,1,131,30,192,1,128,142,1,66,1,143,193,65,3,141,193,0,3,142,129,65,3,143,193,65,3,141,1,1,3,78,128,65,3,30,128,5,128,33,128,0,133,30,128,4,128,33,192,66,
1,30,0,4,128,33,192,0,131,30,128,3,128,33,0,1,131,30,0,3,128,33,64,1,131,30,128,2,128,142,129,66,1,143,193,65,3,141,193,0,3,142,129,65,3,143,193,65,3,141,1,1,3,142,129,65,3,143,193,65,3,141,65,1,
3,78,128,65,3,30,64,0,128,129,1,3,0,166,1,0,1,33,64,195,0,30,64,1,128,133,1,0,1,193,129,3,0,0,2,128,0,165,1,128,1,166,1,0,0,30,64,4,128,33,192,195,0,30,64,3,128,78,0,196,0,133,1,128,
1,210,65,196,0,164,129,0,1,141,129,1,137,208,65,196,0,205,193,129,137,5,2,0,1,65,2,5,0,128,2,0,3,192,2,128,3,37,2,0,2,38,2,0,0,30,64,0,128,129,1,3,0,166,1,0,1,38,0,128,0,21,0,0,
0,19,1,0,0,0,0,0,0,0,19,4,0,0,0,0,0,0,0,19,0,0,0,0,0,0,0,0,19,127,0,0,0,0,0,0,0,19,192,0,0,0,0,0,0,0,19,223,0,0,0,0,0,0,0,19,128,0,0,0,0,0,0,0,
19,64,0,0,0,0,0,0,0,19,224,0,0,0,0,0,0,0,19,239,0,0,0,0,0,0,0,19,240,0,0,0,0,0,0,0,19,247,0,0,0,0,0,0,0,4,1,19,255,255,0,0,0,0,0,0,4,7,92,117,37,46,52,120,
19,255,255,16,0,0,0,0,0,19,0,0,1,0,0,0,0,0,19,0,4,0,0,0,0,0,0,19,0,216,0,0,0,0,0,0,19,0,220,0,0,0,0,0,0,4,13,92,117,37,46,52,120,92,117,37,46,52,120,4,0,0,0,1,
27,1,19,1,23,1,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,81,0,0,0,87,0,0,0,3,0,7,15,0,0,0,197,0,0,0,0,1,0,0,64,1,128,0,228,128,128,1,226,0,0,0,30,128,1,128,197,
0,128,0,0,1,0,0,64,1,128,0,128,1,0,1,229,0,0,2,230,0,0,0,30,0,0,128,38,0,0,1,38,0,128,0,0,0,0,0,2,0,0,0,1,21,1,17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
88,0,0,0,101,0,0,0,1,0,5,66,0,0,0,69,0,0,0,128,0,0,0,193,0,0,0,5,1,128,0,100,128,0,2,0,0,128,0,69,0,0,1,128,0,0,0,193,64,0,0,100,128,128,1,98,0,0,0,30,192,11,128,69,
0,0,0,128,0,0,0,193,128,0,0,5,1,128,0,100,128,0,2,0,0,128,0,69,0,0,0,128,0,0,0,193,192,0,0,5,1,128,0,100,128,0,2,0,0,128,0,69,0,0,0,128,0,0,0,193,0,1,0,5,1,128,0,100,
128,0,2,0,0,128,0,69,0,0,0,128,0,0,0,193,64,1,0,5,1,128,0,100,128,0,2,0,0,128,0,69,0,0,0,128,0,0,0,193,128,1,0,5,1,128,0,100,128,0,2,0,0,128,0,69,0,0,0,128,0,0,0,193,
192,1,0,5,1,128,0,100,128,0,2,0,0,128,0,69,0,0,0,128,0,0,0,193,0,2,0,5,1,128,0,100,128,0,2,0,0,128,0,69,0,0,0,128,0,0,0,193,64,2,0,5,1,128,0,100,128,0,2,0,0,128,0,65,
128,2,0,128,0,0,0,193,128,2,0,93,192,128,0,102,0,0,1,38,0,128,0,11,0,0,0,4,11,91,37,122,1,45,31,34,92,127,93,4,9,91,194,216,220,225,226,239,93,4,8,194,91,128,45,159,173,93,4,7,216,91,128,45,
132,93,4,3,220,143,4,7,225,158,91,180,181,93,4,11,226,128,91,140,45,143,168,45,175,93,4,8,226,129,91,160,45,175,93,4,4,239,187,191,4,8,239,191,91,176,45,191,93,4,2,34,3,0,0,0,1,29,1,28,1,21,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,103,0,0,0,110,0,0,0,3,0,11,24,0,0,0,197,0,0,0,0,1,0,0,64,1,128,0,129,1,0,0,195,1,128,0,228,192,128,2,226,0,0,0,30,64,3,128,69,1,
128,0,128,1,0,0,193,1,0,0,14,2,192,1,100,129,0,2,128,1,0,1,197,1,128,0,0,2,0,0,77,2,64,2,129,66,0,0,228,129,0,2,93,193,129,2,102,1,0,1,30,0,0,128,38,0,0,1,38,0,128,0,2,0,
0,0,19,1,0,0,0,0,0,0,0,19,255,255,255,255,255,255,255,255,2,0,0,0,1,21,1,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,113,0,0,0,116,0,0,0,0,0,5,17,0,0,0,5,0,128,0,
69,0,0,1,129,0,0,0,100,128,0,1,129,64,0,0,36,128,128,1,9,0,0,0,1,128,0,0,69,0,0,2,133,0,0,0,193,192,0,0,1,1,1,0,100,128,0,2,129,64,1,0,29,128,0,0,9,0,128,1,38,0,128,0,
6,0,0,0,3,0,0,0,0,0,0,224,63,4,9,40,91,94,48,53,43,93,41,4,12,91,94,48,45,57,37,45,37,43,101,69,4,27,91,37,94,37,36,37,40,37,41,37,37,37,46,37,91,37,93,37,42,37,43,37,45,37,63,93,
4,5,37,37,37,48,4,3,93,43,5,0,0,0,1,32,1,24,1,5,1,33,1,17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,118,0,0,0,120,0,0,0,1,0,6,13,0,0,0,69,0,0,0,133,0,128,0,
197,0,0,1,0,1,0,0,228,128,0,1,5,1,128,1,65,1,0,0,164,128,0,2,197,0,0,2,1,65,0,0,101,0,0,2,102,0,0,0,38,0,128,0,2,0,0,0,4,1,4,2,46,5,0,0,0,1,31,1,29,1,5,1,
33,1,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,121,0,0,0,128,0,0,0,1,0,7,21,0,0,0,69,0,0,0,133,0,128,0,192,0,0,0,1,1,0,0,69,1,0,1,164,0,0,2,100,128,0,0,98,
64,0,0,30,64,2,128,133,0,128,1,164,64,128,0,133,0,0,0,197,0,128,0,0,1,0,0,65,1,0,0,133,1,0,1,228,0,0,2,164,128,0,0,64,0,0,1,102,0,0,1,38,0,128,0,1,0,0,0,4,2,46,4,0,
0,0,1,6,1,31,1,32,1,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,129,0,0,0,134,0,0,0,3,0,7,11,0,0,0,205,0,64,1,74,64,192,1,205,128,64,1,5,1,0,0,65,193,0,0,128,1,
0,0,36,129,128,1,74,0,129,1,141,128,64,1,166,0,0,1,38,0,128,0,4,0,0,0,19,1,0,0,0,0,0,0,0,4,2,10,19,2,0,0,0,0,0,0,0,4,3,32,32,1,0,0,0,1,16,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,135,0,0,0,140,0,0,0,1,0,5,17,0,0,0,71,0,64,0,98,0,0,0,30,0,3,128,69,0,0,0,135,128,64,0,162,64,0,0,30,0,0,128,129,192,0,0,199,0,65,0,7,65,64,
0,34,65,0,0,30,64,0,128,7,1,65,0,28,1,0,2,100,128,0,2,10,64,128,128,38,0,128,0,5,0,0,0,4,7,105,110,100,101,110,116,4,10,98,117,102,102,101,114,108,101,110,4,6,108,101,118,101,108,19,0,0,0,0,
0,0,0,0,4,7,98,117,102,102,101,114,1,0,0,0,1,37,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,142,0,0,0,157,0,0,0,10,0,20,44,0,0,0,133,2,0,0,192,2,0,0,164,130,0,1,95,0,
64,5,30,192,1,128,95,64,64,5,30,64,1,128,196,2,0,0,1,131,0,0,64,3,0,5,129,195,0,0,29,131,3,6,230,2,128,1,162,0,0,0,30,64,0,128,141,1,65,3,74,65,65,3,226,0,0,0,30,64,1,128,197,2,
128,0,0,3,0,2,64,3,128,2,128,3,0,3,228,130,0,2,128,1,128,5,205,2,65,3,5,3,0,1,64,3,0,0,36,131,0,1,74,1,131,5,205,130,65,3,74,193,193,5,197,2,128,1,0,3,128,0,64,3,128,1,128,3,
0,2,192,3,128,2,13,132,65,3,64,4,128,3,128,4,0,4,192,4,128,4,229,2,128,4,230,2,0,0,38,0,128,0,8,0,0,0,4,7,115,116,114,105,110,103,4,7,110,117,109,98,101,114,4,7,116,121,112,101,32,39,4,39,
39,32,105,115,32,110,111,116,32,115,117,112,112,111,114,116,101,100,32,97,115,32,97,32,107,101,121,32,98,121,32,100,107,106,115,111,110,46,19,1,0,0,0,0,0,0,0,4,2,44,19,2,0,0,0,0,0,0,0,4,2,58,4,0,
0,0,1,4,1,37,1,30,1,38,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,158,0,0,0,165,0,0,0,3,0,6,10,0,0,0,199,0,64,1,5,1,0,0,64,1,0,0,36,129,0,1,31,64,64,2,30,64,
0,128,205,128,192,1,74,0,128,1,230,0,0,1,38,0,128,0,3,0,0,0,4,10,98,117,102,102,101,114,108,101,110,4,7,115,116,114,105,110,103,19,1,0,0,0,0,0,0,0,1,0,0,0,1,4,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,166,0,0,0,177,0,0,0,6,0,13,31,0,0,0,98,65,0,0,30,0,0,128,64,1,0,0,135,1,64,1,162,65,0,0,30,192,0,128,196,1,0,0,0,2,128,2,230,1,128,1,30,192,4,128,
138,0,129,128,192,1,0,3,0,2,0,0,64,2,128,0,128,2,0,1,192,2,128,2,228,193,128,2,226,65,0,0,30,0,1,128,68,2,0,0,163,66,0,4,30,0,0,128,128,2,128,2,102,2,128,1,69,2,0,0,128,2,128,3,
192,2,128,1,0,3,0,1,101,2,0,2,102,2,0,0,38,0,128,0,2,0,0,0,4,10,101,120,99,101,112,116,105,111,110,4,10,98,117,102,102,101,114,108,101,110,1,0,0,0,1,40,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,178,0,0,0,180,0,0,0,4,0,8,8,0,0,0,5,1,0,0,65,1,0,0,128,1,128,1,193,65,0,0,93,193,129,2,37,1,0,1,38,1,0,0,38,0,128,0,2,0,0,0,4,2,60,4,2,62,1,0,
0,0,1,30,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,181,0,0,0,25,1,0,0,8,0,34,24,1,0,0,5,2,0,0,64,2,0,0,36,130,0,1,69,2,128,0,128,2,0,0,100,130,0,1,133,2,0,0,
192,2,128,4,164,130,0,1,31,0,64,5,30,0,0,128,30,64,0,128,67,66,0,0,67,2,128,0,163,2,128,4,30,0,0,128,135,66,192,4,162,2,0,0,30,192,8,128,199,2,128,2,226,2,0,0,30,192,1,128,197,2,0,1,
1,131,0,0,64,3,0,0,128,3,128,3,192,3,128,1,0,4,0,2,229,2,0,3,230,2,0,0,74,193,64,0,202,1,1,130,192,2,0,5,0,3,0,0,64,3,128,3,228,194,128,1,226,66,0,0,30,0,2,128,69,3,0,1,
129,67,1,0,192,3,0,0,0,4,128,3,64,4,128,1,128,4,0,2,192,4,0,6,101,3,128,3,102,3,0,0,74,129,65,0,69,3,128,1,128,3,128,5,192,3,128,1,0,4,128,3,100,131,0,2,0,1,128,6,30,128,55,128,
31,128,65,0,30,128,0,128,13,193,65,2,202,0,66,2,30,64,54,128,31,64,66,4,30,128,4,128,196,2,0,0,31,0,0,0,30,128,1,128,5,3,0,2,97,0,0,6,30,192,0,128,25,3,0,0,69,3,0,2,33,0,131,6,
30,64,0,128,193,2,2,0,30,192,0,128,5,3,128,2,64,3,0,0,36,131,0,1,192,2,0,6,13,193,65,2,202,192,2,2,30,0,49,128,31,128,66,4,30,0,2,128,13,193,65,2,34,0,0,0,30,128,0,128,193,194,2,0,
226,66,0,0,30,0,0,128,193,2,3,0,202,192,2,2,30,64,46,128,31,64,67,4,30,64,1,128,13,193,65,2,197,2,0,3,0,3,0,0,228,130,0,1,202,192,2,2,30,64,44,128,31,0,64,4,30,192,40,128,199,2,128,2,
226,2,0,0,30,192,1,128,197,2,0,1,1,131,0,0,64,3,0,0,128,3,128,3,192,3,128,1,0,4,0,2,229,2,0,3,230,2,0,0,74,193,64,0,141,192,65,1,197,2,128,3,0,3,0,0,228,194,0,1,31,128,67,6,
30,64,1,128,98,2,0,0,30,192,0,128,71,195,195,4,31,0,196,6,30,0,0,128,195,2,0,0,68,3,0,0,226,2,0,0,30,128,7,128,13,193,65,2,202,64,68,2,129,195,1,0,192,3,0,6,1,196,1,0,168,3,5,128,
133,4,0,4,199,68,4,0,0,5,128,0,64,5,0,1,128,5,128,1,192,5,0,2,0,6,128,2,64,6,0,3,128,6,128,3,164,196,128,4,64,3,128,9,0,1,0,9,34,65,0,0,30,128,0,128,132,4,0,0,192,4,128,6,
166,4,128,1,32,0,131,8,30,64,0,128,13,193,65,2,202,128,68,2,167,67,250,127,13,193,65,2,202,192,68,2,30,192,25,128,131,3,0,0,13,193,65,2,202,0,69,2,98,2,0,0,30,128,0,128,199,67,197,4,226,67,0,0,
30,0,0,128,192,3,0,3,226,3,0,0,30,0,14,128,11,4,0,0,28,3,128,7,65,196,1,0,128,4,0,6,193,196,1,0,104,196,4,128,71,5,133,7,135,69,5,0,162,5,0,0,30,192,3,128,10,196,192,10,197,5,128,4,
0,6,128,10,64,6,0,11,128,6,0,7,192,6,128,0,0,7,0,1,64,7,128,1,128,7,0,2,192,7,128,2,0,8,0,3,64,8,128,3,228,197,128,5,64,3,0,12,0,1,128,11,131,3,128,0,103,132,250,127,69,4,0,5,
128,4,0,0,100,4,1,1,30,128,5,128,135,5,5,8,162,69,0,0,30,192,4,128,133,5,128,4,192,5,0,10,0,6,128,10,64,6,0,7,128,6,128,0,192,6,0,1,0,7,128,1,64,7,0,2,128,7,128,2,192,7,0,3,
0,8,128,3,164,197,128,5,64,3,128,11,0,1,0,11,34,65,0,0,30,128,0,128,132,5,0,0,192,5,128,6,166,5,128,1,131,3,128,0,105,132,0,0,234,132,249,127,30,64,6,128,5,4,0,5,64,4,0,0,36,4,1,1,
30,192,4,128,69,5,128,4,128,5,128,9,192,5,0,10,0,6,0,7,64,6,128,0,128,6,0,1,192,6,128,1,0,7,0,2,64,7,128,2,128,7,0,3,192,7,128,3,100,197,128,5,64,3,0,11,0,1,128,10,34,65,0,0,
30,128,0,128,68,5,0,0,128,5,128,6,102,5,128,1,131,3,128,0,41,132,0,0,170,68,250,127,98,0,0,0,30,64,1,128,5,4,128,5,78,196,65,1,128,4,128,1,192,4,0,2,36,132,0,2,0,1,0,8,13,193,65,2,
202,128,69,2,74,129,65,0,30,192,2,128,197,2,0,1,1,195,5,0,64,3,0,0,128,3,128,3,192,3,128,1,0,4,0,2,65,4,6,0,128,4,0,4,193,68,6,0,93,196,132,8,229,2,128,3,230,2,0,0,38,1,0,1,
38,0,128,0,26,0,0,0,4,6,116,97,98,108,101,4,9,95,95,116,111,106,115,111,110,4,16,114,101,102,101,114,101,110,99,101,32,99,121,99,108,101,1,1,4,10,98,117,102,102,101,114,108,101,110,4,22,99,117,115,116,111,109,32,
101,110,99,111,100,101,114,32,102,97,105,108,101,100,0,19,1,0,0,0,0,0,0,0,4,5,110,117,108,108,4,7,110,117,109,98,101,114,4,8,98,111,111,108,101,97,110,4,5,116,114,117,101,4,6,102,97,108,115,101,4,7,115,116,
114,105,110,103,19,0,0,0,0,0,0,0,0,4,11,95,95,106,115,111,110,116,121,112,101,4,7,111,98,106,101,99,116,4,2,91,4,2,44,4,2,93,4,2,123,4,12,95,95,106,115,111,110,111,114,100,101,114,4,2,125,4,17,117,
110,115,117,112,112,111,114,116,101,100,32,116,121,112,101,4,7,116,121,112,101,32,39,4,30,39,32,105,115,32,110,111,116,32,115,117,112,112,111,114,116,101,100,32,98,121,32,100,107,106,115,111,110,46,12,0,0,0,1,4,1,7,1,41,
1,40,1,15,1,35,1,30,1,26,1,38,1,39,1,3,1,37,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,26,1,0,0,44,1,0,0,2,0,13,50,0,0,0,98,64,0,0,30,64,0,128,139,0,0,0,64,0,
0,1,135,0,192,0,227,64,0,1,30,0,0,128,203,0,0,0,74,192,0,128,5,1,0,0,36,65,128,0,5,1,128,0,64,1,0,0,135,65,192,0,199,129,192,0,226,65,0,0,30,0,0,128,193,193,0,0,0,2,128,1,71,2,
193,0,98,66,0,0,30,0,0,128,65,194,0,0,135,66,193,0,162,66,0,0,30,0,0,128,139,2,0,0,199,130,193,0,0,3,128,0,36,193,128,4,34,65,0,0,30,0,1,128,133,1,0,1,192,1,128,2,1,194,1,0,164,65,
128,1,30,192,2,128,31,192,0,1,30,192,0,128,74,0,1,130,131,1,128,0,166,1,0,1,30,64,1,128,74,0,66,130,74,0,66,128,133,1,128,1,192,1,128,1,165,1,0,1,166,1,0,0,38,0,128,0,9,0,0,0,4,7,
98,117,102,102,101,114,4,7,105,110,100,101,110,116,4,6,108,101,118,101,108,19,0,0,0,0,0,0,0,0,4,10,98,117,102,102,101,114,108,101,110,4,7,116,97,98,108,101,115,4,9,107,101,121,111,114,100,101,114,19,2,0,0,0,
0,0,0,0,0,4,0,0,0,1,34,1,38,1,10,1,25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,45,1,0,0,58,1,0,0,2,0,10,27,0,0,0,129,0,0,0,193,0,0,0,1,65,0,0,69,1,0,
0,128,1,0,0,193,129,0,0,0,2,128,1,67,2,128,0,100,129,128,2,192,0,128,2,226,0,0,0,30,192,1,128,32,64,128,1,30,64,1,128,141,0,64,1,0,1,128,1,205,0,192,1,30,0,252,127,30,0,0,128,30,128,251,
127,65,193,0,0,128,1,0,1,193,1,1,0,14,2,129,0,93,1,130,2,102,1,0,1,38,0,128,0,5,0,0,0,19,1,0,0,0,0,0,0,0,19,0,0,0,0,0,0,0,0,4,2,10,4,6,108,105,110,101,32,4,10,44,
32,99,111,108,117,109,110,32,1,0,0,0,1,21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,59,1,0,0,61,1,0,0,3,0,11,15,0,0,0,196,0,0,0,5,1,0,0,64,1,0,0,36,129,0,1,13,1,
64,2,65,65,0,0,128,1,128,0,193,129,0,0,5,2,128,0,64,2,0,0,128,2,0,1,36,130,128,1,93,1,130,2,230,0,0,2,38,0,128,0,3,0,0,0,19,1,0,0,0,0,0,0,0,4,14,117,110,116,101,114,109,105,
110,97,116,101,100,32,4,5,32,97,116,32,2,0,0,0,1,22,1,42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,62,1,0,0,80,1,0,0,2,0,7,56,0,0,0,133,0,0,0,192,0,0,0,1,1,0,0,
64,1,128,0,164,128,0,2,64,0,0,1,98,64,0,0,30,64,0,128,132,0,0,0,166,0,0,1,133,0,128,0,192,0,0,0,0,1,128,0,77,65,192,0,164,128,0,2,31,128,64,1,30,0,2,128,197,0,128,0,0,1,0,0,
77,193,192,0,141,193,192,0,228,128,0,2,31,0,193,1,30,64,0,128,77,64,193,0,30,64,249,127,31,128,65,1,30,128,2,128,197,0,0,0,0,1,0,0,65,193,1,0,141,193,192,0,228,128,0,2,64,0,128,1,98,64,0,0,
30,192,246,127,196,0,0,0,230,0,0,1,30,0,246,127,31,0,66,1,30,192,2,128,197,0,0,0,0,1,0,0,65,65,2,0,141,193,192,0,228,128,0,2,64,0,128,1,98,64,0,0,30,64,0,128,196,0,0,0,230,0,0,1,
77,192,192,0,30,128,242,127,102,0,0,1,30,0,242,127,38,0,128,0,10,0,0,0,4,3,37,83,19,1,0,0,0,0,0,0,0,4,3,239,187,19,2,0,0,0,0,0,0,0,4,2,191,19,3,0,0,0,0,0,0,0,4,3,
47,47,4,5,91,10,13,93,4,3,47,42,4,3,42,47,2,0,0,0,1,21,1,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,85,1,0,0,105,1,0,0,1,0,7,75,0,0,0,32,0,64,0,30,128,0,128,
68,0,0,0,102,0,0,1,30,0,17,128,33,64,64,0,30,0,1,128,69,0,0,0,128,0,0,0,101,0,0,1,102,0,0,0,30,64,15,128,33,128,64,0,30,0,3,128,69,0,0,0,133,0,128,0,210,192,64,0,164,128,0,1,
141,128,0,130,197,0,128,0,0,1,0,0,228,128,0,1,208,192,192,1,205,192,128,130,101,0,128,1,102,0,0,0,30,128,11,128,33,128,65,0,30,64,4,128,69,0,0,0,133,0,128,0,210,192,65,0,164,128,0,1,141,128,0,132,
197,0,128,0,18,193,64,0,228,128,0,1,208,192,192,1,205,192,128,130,5,1,128,0,64,1,0,0,36,129,0,1,16,193,64,2,13,1,129,130,101,0,0,2,102,0,0,0,30,128,6,128,33,64,66,0,30,128,5,128,69,0,0,0,
133,0,128,0,210,128,66,0,164,128,0,1,141,128,128,133,197,0,128,0,18,193,65,0,228,128,0,1,208,192,192,1,205,192,128,130,5,1,128,0,82,193,64,0,36,129,0,1,16,193,64,2,13,1,129,130,69,1,128,0,128,1,0,0,
100,129,0,1,80,193,192,2,77,65,129,130,101,0,128,2,102,0,0,0,30,64,0,128,68,0,0,0,102,0,0,1,38,0,128,0,12,0,0,0,19,0,0,0,0,0,0,0,0,19,127,0,0,0,0,0,0,0,19,255,7,0,0,0,
0,0,0,19,64,0,0,0,0,0,0,0,19,192,0,0,0,0,0,0,0,19,128,0,0,0,0,0,0,0,19,255,255,0,0,0,0,0,0,19,0,16,0,0,0,0,0,0,19,224,0,0,0,0,0,0,0,19,255,255,16,0,0,0,
0,0,19,0,0,4,0,0,0,0,0,19,240,0,0,0,0,0,0,0,2,0,0,0,1,20,1,14,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,107,1,0,0,164,1,0,0,2,0,14,129,0,0,0,141,0,192,0,
203,0,0,0,1,65,0,0,69,1,0,0,128,1,0,0,193,129,0,0,0,2,0,1,100,129,0,2,98,65,0,0,30,64,1,128,133,1,128,0,192,1,0,0,1,194,0,0,64,2,128,0,165,1,0,2,166,1,0,0,32,64,1,1,
30,128,1,128,13,1,64,2,133,1,0,1,192,1,0,0,0,2,0,1,78,2,192,2,164,129,0,2,202,128,1,2,133,1,0,1,192,1,0,0,0,2,128,2,64,2,128,2,164,129,0,2,31,0,65,3,30,128,0,128,141,0,192,2,
30,0,19,128,30,192,247,127,133,1,0,1,192,1,0,0,13,2,192,2,77,2,192,2,164,129,0,2,196,1,0,0,31,64,65,3,30,64,14,128,5,2,128,1,69,2,0,1,128,2,0,0,205,130,193,2,13,195,193,2,100,130,0,2,
129,2,2,0,36,130,128,1,192,1,0,4,226,1,0,0,30,128,11,128,4,2,0,0,33,192,129,132,30,128,7,128,33,128,194,3,30,0,7,128,69,2,0,1,128,2,0,0,205,194,194,2,13,3,195,2,100,130,0,2,31,64,195,4,
30,64,5,128,69,2,128,1,133,2,0,1,192,2,0,0,13,131,195,2,77,195,195,2,164,130,0,2,193,2,2,0,100,130,128,1,0,2,128,4,34,2,0,0,30,64,2,128,33,0,2,136,30,192,1,128,33,64,68,4,30,64,1,128,
78,66,194,3,79,130,196,4,142,2,68,4,77,130,130,4,205,193,196,4,30,0,0,128,4,2,0,0,226,1,0,0,30,192,0,128,69,2,0,2,128,2,128,3,100,130,0,1,192,1,128,4,226,1,0,0,30,0,1,128,34,2,0,0,
30,64,0,128,141,0,197,2,30,0,0,128,141,192,194,2,226,65,0,0,30,0,1,128,6,130,129,2,227,65,0,4,30,0,0,128,192,1,0,3,141,128,193,2,13,1,64,2,202,192,1,2,30,192,228,127,31,0,64,2,30,192,0,128,
71,1,192,1,128,1,0,1,102,1,128,1,30,128,2,128,32,0,1,128,30,64,1,128,69,1,0,3,128,1,128,1,100,129,0,1,128,1,0,1,102,1,128,1,30,128,0,128,65,65,5,0,128,1,0,1,102,1,128,1,38,0,128,0,
22,0,0,0,19,1,0,0,0,0,0,0,0,19,0,0,0,0,0,0,0,0,4,5,91,34,92,93,4,7,115,116,114,105,110,103,4,2,34,4,2,117,19,2,0,0,0,0,0,0,0,19,5,0,0,0,0,0,0,0,19,16,0,0,
0,0,0,0,0,19,0,216,0,0,0,0,0,0,19,255,219,0,0,0,0,0,0,19,6,0,0,0,0,0,0,0,19,7,0,0,0,0,0,0,0,4,3,92,117,19,8,0,0,0,0,0,0,0,19,11,0,0,0,0,0,0,0,19,
0,220,0,0,0,0,0,0,19,255,223,0,0,0,0,0,0,19,0,4,0,0,0,0,0,0,19,0,0,1,0,0,0,0,0,19,12,0,0,0,0,0,0,0,4,1,7,0,0,0,1,21,1,43,1,18,1,6,1,46,1,45,1,25,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,168,1,0,0,211,1,0,0,7,0,21,148,0,0,0,197,1,0,0,0,2,0,1,228,129,0,1,11,2,0,0,65,2,0,0,141,66,192,1,31,128,64,0,30,0,1,128,
197,2,128,0,0,3,0,4,64,3,128,2,228,66,128,1,30,192,0,128,197,2,128,0,0,3,0,4,64,3,0,3,228,66,128,1,197,2,0,1,0,3,0,1,64,3,0,5,228,130,128,1,128,2,128,5,162,66,0,0,30,64,1,128,
197,2,128,1,0,3,0,1,64,3,0,0,128,3,128,1,229,2,0,2,230,2,0,0,197,2,0,2,0,3,0,1,64,3,0,5,128,3,0,5,228,130,0,2,31,64,128,5,30,128,0,128,0,3,0,4,77,67,64,5,38,3,128,1,
4,3,128,0,133,3,128,2,192,3,0,1,0,4,0,5,64,4,0,2,128,4,128,2,192,4,0,3,164,3,1,3,64,3,0,8,128,2,128,7,0,3,0,7,98,3,0,0,30,192,0,128,132,3,0,0,192,3,0,5,0,4,128,6,
166,3,0,2,133,3,0,1,192,3,0,1,0,4,0,5,164,131,128,1,128,2,0,7,162,66,0,0,30,64,1,128,133,3,128,1,192,3,0,1,0,4,0,0,64,4,128,1,165,3,0,2,166,3,0,0,133,3,0,2,192,3,0,1,
0,4,0,5,64,4,0,5,164,131,0,2,192,2,0,7,31,192,192,5,30,128,15,128,31,0,65,6,30,64,2,128,132,3,0,0,192,3,0,5,1,68,1,0,69,4,0,3,128,4,0,1,192,4,0,5,100,132,128,1,129,132,1,0,
29,132,4,8,166,3,0,2,133,3,0,1,192,3,0,1,13,68,64,5,164,131,128,1,128,2,0,7,162,66,0,0,30,64,1,128,133,3,128,1,192,3,0,1,0,4,0,0,64,4,128,1,165,3,0,2,166,3,0,0,132,3,0,0,
197,3,128,2,0,4,0,1,64,4,0,5,128,4,0,2,192,4,128,2,0,5,0,3,228,3,1,3,64,3,128,8,128,2,0,8,128,3,128,7,98,3,0,0,30,192,0,128,196,3,0,0,0,4,0,5,64,4,128,6,230,3,0,2,
10,130,3,6,197,3,0,1,0,4,0,1,64,4,0,5,228,131,128,1,128,2,128,7,162,66,0,0,30,64,1,128,197,3,128,1,0,4,0,1,64,4,0,0,128,4,128,1,229,3,0,2,230,3,0,0,197,3,0,2,0,4,0,1,
64,4,0,5,128,4,0,5,228,131,0,2,192,2,128,7,30,64,0,128,77,66,192,4,10,2,131,4,31,192,193,5,30,192,223,127,141,66,64,5,30,64,223,127,38,0,128,0,8,0,0,0,19,0,0,0,0,0,0,0,0,19,1,0,
0,0,0,0,0,0,4,7,111,98,106,101,99,116,4,2,58,0,4,35,99,97,110,110,111,116,32,117,115,101,32,110,105,108,32,97,115,32,116,97,98,108,101,32,105,110,100,101,120,32,40,97,116,32,4,2,41,4,2,44,7,0,0,0,
1,22,1,8,1,44,1,43,1,18,1,48,1,42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,212,1,0,0,246,1,0,0,5,0,14,116,0,0,0,98,64,0,0,30,0,0,128,65,0,0,0,69,1,0,0,128,1,
0,0,192,1,128,0,100,129,128,1,64,0,128,2,98,64,0,0,30,128,1,128,68,1,0,0,133,1,128,0,192,1,0,0,164,129,0,1,141,1,64,3,193,65,0,0,102,1,0,2,69,1,0,1,128,1,0,0,192,1,128,0,0,2,
128,0,100,129,0,2,31,128,192,2,30,128,2,128,133,1,128,1,193,193,0,0,1,2,1,0,64,2,0,0,128,2,128,0,192,2,0,1,0,3,128,1,64,3,0,2,165,1,0,4,166,1,0,0,30,192,19,128,31,64,193,2,30,128,
2,128,133,1,128,1,193,129,1,0,1,194,1,0,64,2,0,0,128,2,128,0,192,2,0,1,0,3,128,1,64,3,0,2,165,1,0,4,166,1,0,0,30,128,16,128,31,0,194,2,30,64,1,128,133,1,0,2,192,1,0,0,0,2,
128,0,165,1,128,1,166,1,0,0,30,128,14,128,133,1,128,2,192,1,0,0,1,66,2,0,64,2,128,0,164,193,0,2,162,1,0,0,30,192,2,128,5,2,0,3,69,2,0,1,128,2,0,0,192,2,0,3,0,3,128,3,100,2,
0,2,36,130,0,0,34,2,0,0,30,128,0,128,64,2,0,4,141,2,192,3,102,2,128,1,5,2,128,2,64,2,0,0,129,130,2,0,192,2,128,0,36,194,0,2,192,1,128,4,128,1,0,4,162,1,0,0,30,64,5,128,5,2,
0,1,64,2,0,0,128,2,0,3,192,2,128,3,36,130,0,2,31,192,66,4,30,192,0,128,67,2,128,0,141,2,192,3,102,2,128,1,30,128,2,128,31,0,67,4,30,192,0,128,67,2,0,0,141,2,192,3,102,2,128,1,30,0,
1,128,31,64,67,4,30,128,0,128,64,2,0,1,141,2,192,3,102,2,128,1,4,2,0,0,64,2,128,0,129,130,3,0,197,2,128,3,0,3,0,0,64,3,128,0,228,130,128,1,157,194,2,5,38,2,0,2,38,0,128,0,15,0,
0,0,19,1,0,0,0,0,0,0,0,4,38,110,111,32,118,97,108,105,100,32,74,83,79,78,32,118,97,108,117,101,32,40,114,101,97,99,104,101,100,32,116,104,101,32,101,110,100,41,4,2,123,4,7,111,98,106,101,99,116,4,2,125,
4,2,91,4,6,97,114,114,97,121,4,2,93,4,2,34,4,27,94,37,45,63,91,37,100,37,46,93,43,91,101,69,93,63,91,37,43,37,45,93,63,37,100,42,4,7,94,37,97,37,119,42,4,5,116,114,117,101,4,6,102,97,108,115,
101,4,5,110,117,108,108,4,24,110,111,32,118,97,108,105,100,32,74,83,79,78,32,118,97,108,117,101,32,97,116,32,8,0,0,0,1,44,1,22,1,18,1,49,1,47,1,21,1,36,1,42,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,247,1,0,0,253,1,0,0,0,1,3,15,0,0,0,5,0,0,0,65,0,0,0,173,0,0,0,36,128,0,0,32,0,128,128,30,128,0,128,45,0,0,0,38,0,0,0,30,0,1,128,11,64,0,0,10,192,64,129,
75,64,0,0,74,0,65,129,38,0,128,1,38,0,128,0,5,0,0,0,4,2,35,19,0,0,0,0,0,0,0,0,4,11,95,95,106,115,111,110,116,121,112,101,4,7,111,98,106,101,99,116,4,6,97,114,114,97,121,1,0,0,0,1,
13,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,254,1,0,0,1,2,0,0,3,1,11,12,0,0,0,197,0,0,0,45,1,0,0,228,192,0,0,69,1,128,0,128,1,0,0,192,1,128,0,0,2,0,1,64,2,128,
1,128,2,0,2,101,1,0,3,102,1,0,0,38,0,128,0,0,0,0,0,2,0,0,0,1,50,1,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,94,2,0,0,0,0,38,35,1,0,0,5,0,0,
0,65,0,0,0,36,128,0,1,71,64,64,0,100,128,128,0,31,128,192,0,30,128,0,128,69,0,128,0,129,192,0,0,100,64,0,1,71,0,65,0,135,64,65,0,199,128,65,0,7,193,65,0,108,1,0,0,172,65,0,0,192,1,0,
1,1,2,2,0,228,129,0,1,0,2,128,1,65,66,2,0,36,130,0,1,14,2,2,133,17,194,66,4,207,1,130,3,0,2,0,1,65,2,3,0,36,130,0,1,64,2,0,1,129,66,3,0,100,130,0,1,78,66,2,133,81,194,194,
4,15,66,2,4,64,2,0,1,129,66,3,0,100,130,0,1,15,66,2,4,64,2,128,1,129,130,3,0,100,130,0,1,128,2,0,1,193,194,3,0,164,130,0,1,77,130,130,4,77,194,129,4,77,2,130,4,81,194,194,4,128,2,128,
1,193,2,4,0,164,130,0,1,142,130,2,133,192,2,0,1,1,67,4,0,228,130,0,1,7,131,68,0,64,3,128,1,129,195,4,0,100,131,0,1,128,3,0,3,193,3,5,0,164,131,0,1,77,131,131,6,36,131,0,1,207,2,131,
5,5,3,128,1,210,2,131,5,0,3,0,2,65,67,5,0,129,131,5,0,193,195,5,0,36,131,0,2,108,131,0,0,172,195,0,0,192,3,0,1,1,4,6,0,228,131,0,1,7,132,68,0,79,4,3,6,79,4,131,8,79,4,131,
8,36,132,0,1,207,3,132,7,7,68,70,0,79,196,131,7,128,4,128,6,36,132,128,1,82,132,131,7,13,68,4,8,77,196,2,8,77,132,130,8,128,4,0,1,193,132,6,0,164,132,0,1,199,196,70,0,17,197,194,8,228,132,0,
1,143,196,4,9,192,4,0,1,1,133,6,0,228,132,0,1,0,5,0,3,65,5,7,0,36,133,0,1,205,4,133,9,143,196,4,9,192,4,0,1,1,69,7,0,228,132,0,1,209,132,199,9,0,5,0,1,65,197,7,0,36,133,0,
1,64,5,0,2,129,5,8,0,100,133,0,1,128,5,0,2,193,69,5,0,164,133,0,1,145,197,66,11,79,133,133,10,13,69,5,10,207,4,133,9,0,5,0,1,65,69,8,0,36,133,0,1,64,5,0,2,129,69,5,0,100,133,0,
1,81,197,194,10,15,69,5,10,64,5,128,1,129,133,8,0,100,133,0,1,128,5,128,1,193,197,8,0,164,133,0,1,145,133,71,11,79,133,133,10,128,5,0,2,193,69,5,0,164,133,0,1,145,133,66,11,79,133,133,10,145,133,71,
10,143,133,133,9,209,133,199,10,143,197,5,11,197,5,0,3,146,197,5,11,192,5,0,1,1,6,9,0,228,133,0,1,7,70,73,0,67,6,128,0,36,134,0,1,207,5,134,11,0,6,0,1,65,134,9,0,36,134,0,1,71,70,73,
0,131,6,0,0,100,134,0,1,15,70,6,12,205,5,134,11,0,6,0,1,65,198,9,0,36,134,0,1,71,6,74,0,129,134,2,0,100,134,0,1,15,70,6,12,205,5,134,11,13,134,4,11,13,198,5,12,68,6,128,0,236,6,1,
0,44,71,1,0,64,7,0,1,129,71,10,0,100,135,0,1,135,71,70,0,199,7,74,0,1,136,10,0,228,135,0,1,7,8,74,0,65,200,10,0,36,136,0,1,207,7,136,15,0,8,128,13,164,135,128,1,79,135,135,14,79,71,130,
14,128,7,0,1,193,7,11,0,164,135,0,1,192,7,0,3,1,72,11,0,228,135,0,1,141,199,7,15,79,135,135,14,128,7,0,1,193,135,11,0,164,135,0,1,199,71,70,0,7,8,74,0,65,136,10,0,36,136,0,1,71,8,74,
0,129,200,10,0,100,136,0,1,15,72,8,16,64,8,0,14,228,135,128,1,143,199,7,15,143,71,2,15,192,7,0,1,1,200,11,0,228,135,0,1,0,8,0,3,65,8,12,0,36,136,0,1,205,7,136,15,143,199,7,15,205,135,135,
14,205,7,134,15,207,199,135,4,0,8,0,3,65,72,12,0,36,136,0,1,15,8,136,4,13,8,136,15,79,72,130,15,128,8,0,1,193,136,12,0,164,136,0,1,199,72,73,0,1,201,12,0,228,136,0,1,143,200,8,17,199,72,73,
0,1,9,13,0,228,136,0,1,141,200,8,17,79,136,136,16,135,72,77,0,164,136,128,0,79,134,136,16,71,136,77,0,143,136,132,4,143,72,2,17,192,8,0,1,1,201,13,0,228,136,0,1,0,9,0,3,65,9,14,0,36,137,0,
1,205,8,137,17,143,200,8,17,143,8,8,17,100,136,0,1,143,72,130,16,192,8,0,1,1,137,12,0,228,136,0,1,7,73,73,0,65,201,12,0,36,137,0,1,207,8,137,17,7,73,73,0,65,9,13,0,36,137,0,1,205,8,137,
17,143,200,8,17,199,72,77,0,228,136,128,0,143,198,8,17,135,72,77,0,164,136,128,0,143,136,8,16,198,72,78,4,44,137,1,0,202,8,9,157,198,72,78,4,44,201,1,0,202,8,137,157,198,8,79,4,202,136,207,158,198,8,79,
4,230,8,0,1,38,0,128,0,63,0,0,0,4,5,108,112,101,103,4,8,118,101,114,115,105,111,110,4,5,48,46,49,49,20,63,100,117,101,32,116,111,32,97,32,98,117,103,32,105,110,32,76,80,101,103,32,48,46,49,49,44,32,105,
116,32,99,97,110,110,111,116,32,98,101,32,117,115,101,100,32,102,111,114,32,74,83,79,78,32,109,97,116,99,104,105,110,103,4,6,109,97,116,99,104,4,2,80,4,2,83,4,2,82,4,3,47,47,4,3,10,13,19,1,0,0,0,0,
0,0,0,19,0,0,0,0,0,0,0,0,4,3,47,42,4,3,42,47,4,5,32,10,13,9,4,4,239,187,191,4,5,34,92,10,13,4,2,92,4,2,67,4,9,34,92,47,98,102,110,114,116,4,28,117,110,115,117,112,112,111,114,116,
101,100,32,101,115,99,97,112,101,32,115,101,113,117,101,110,99,101,4,3,48,57,4,3,97,102,4,3,65,70,4,3,92,117,4,4,67,109,116,4,2,34,4,3,67,115,4,20,117,110,116,101,114,109,105,110,97,116,101,100,32,115,116,114,
105,110,103,4,2,45,19,255,255,255,255,255,255,255,255,4,2,48,4,3,49,57,4,2,46,4,3,101,69,4,3,43,45,4,5,116,114,117,101,4,3,67,99,4,6,102,97,108,115,101,4,5,110,117,108,108,4,5,67,97,114,103,4,2,
91,19,1,0,0,0,0,0,0,0,19,2,0,0,0,0,0,0,0,4,2,93,4,13,39,93,39,32,101,120,112,101,99,116,101,100,4,2,123,4,2,125,4,13,39,125,39,32,101,120,112,101,99,116,101,100,4,15,118,97,108,117,101,32,
101,120,112,101,99,116,101,100,4,2,44,4,5,99,111,110,116,4,5,108,97,115,116,4,3,67,112,4,3,67,103,4,2,58,4,15,99,111,108,111,110,32,101,120,112,101,99,116,101,100,4,7,100,107,106,115,111,110,4,7,100,101,99,111,
100,101,4,9,117,115,101,95,108,112,101,103,4,7,100,107,106,115,111,110,4,11,117,115,105,110,103,95,108,112,101,103,1,1,10,0,0,0,1,11,1,10,1,42,1,45,1,6,1,46,1,36,1,8,0,0,1,50,8,0,0,0,0,9,
2,0,0,15,2,0,0,4,0,9,15,0,0,0,7,1,192,1,34,65,0,0,30,0,2,128,0,1,0,1,65,65,0,0,133,1,0,0,192,1,0,0,0,2,128,0,164,129,128,1,29,129,1,2,202,0,1,128,202,64,0,129,3,1,
0,0,38,1,0,1,38,0,128,0,3,0,0,0,4,4,109,115,103,4,5,32,97,116,32,4,4,112,111,115,1,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,2,0,0,18,2,0,0,1,0,5,
12,0,0,0,70,0,64,0,134,64,64,0,192,0,0,0,164,128,0,1,198,128,64,0,1,193,0,0,228,128,0,1,143,192,0,1,197,0,128,0,101,0,128,1,102,0,0,0,38,0,128,0,4,0,0,0,4,4,67,109,116,4,3,67,
99,4,5,67,97,114,103,19,2,0,0,0,0,0,0,0,2,0,0,0,1,0,1,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,26,2,0,0,33,2,0,0,4,0,8,31,0,0,0,5,1,0,0,64,1,0,1,
129,1,0,0,36,129,128,1,69,1,0,0,128,1,128,1,193,1,0,0,100,129,128,1,192,0,128,2,128,0,0,2,33,128,128,128,30,192,3,128,33,128,64,1,30,64,3,128,33,192,128,129,30,192,2,128,33,0,193,1,30,64,2,128,
3,1,128,0,69,1,128,0,142,65,64,1,143,65,65,3,206,193,192,1,141,193,1,3,141,129,65,3,100,1,0,1,38,1,0,0,30,64,0,128,3,1,0,0,38,1,0,1,38,0,128,0,7,0,0,0,19,16,0,0,0,0,0,0,
0,19,0,216,0,0,0,0,0,0,19,255,219,0,0,0,0,0,0,19,0,220,0,0,0,0,0,0,19,255,223,0,0,0,0,0,0,19,0,4,0,0,0,0,0,0,19,0,0,1,0,0,0,0,0,2,0,0,0,0,4,0,5,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,34,2,0,0,36,2,0,0,1,0,5,8,0,0,0,69,0,0,0,133,0,128,0,192,0,0,0,1,1,0,0,164,0,128,1,101,0,0,0,102,0,0,0,38,0,128,0,1,
0,0,0,19,16,0,0,0,0,0,0,0,2,0,0,0,0,5,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,2,0,0,60,2,0,0,4,0,15,27,0,0,0,4,1,0,1,203,1,0,0,1,2,0,0,
69,2,0,0,133,2,128,0,192,2,0,0,0,3,128,0,64,3,0,1,128,3,128,1,100,2,1,3,128,1,128,5,64,1,0,5,0,1,128,4,162,1,0,0,30,0,1,128,64,0,0,3,13,66,64,4,202,1,1,4,31,128,192,2,
30,128,251,127,64,2,128,0,133,2,0,1,192,2,128,3,7,195,192,1,164,2,128,1,102,2,0,0,38,0,128,0,4,0,0,0,19,0,0,0,0,0,0,0,0,19,1,0,0,0,0,0,0,0,4,5,108,97,115,116,4,10,97,114,
114,97,121,109,101,116,97,3,0,0,0,1,1,1,25,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,61,2,0,0,72,2,0,0,4,0,15,26,0,0,0,4,1,128,1,11,2,0,0,69,2,0,0,133,2,128,
0,192,2,0,0,0,3,128,0,64,3,0,1,128,3,128,1,100,66,1,3,192,1,0,6,128,1,128,5,0,1,0,5,64,1,128,4,226,1,0,0,30,192,0,128,64,0,128,3,10,2,129,2,31,0,64,3,30,128,251,127,64,2,128,
0,133,2,0,1,192,2,0,4,7,67,192,1,164,2,128,1,102,2,0,0,38,0,128,0,2,0,0,0,4,5,108,97,115,116,4,11,111,98,106,101,99,116,109,101,116,97,3,0,0,0,1,1,1,26,0,7,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,81,2,0,0,90,2,0,0,3,1,10,25,0,0,0,203,0,0,0,5,1,0,0,109,1,0,0,36,193,0,0,202,64,129,128,202,0,1,128,5,1,128,0,69,1,0,1,128,1,0,0,192,1,128,
0,0,2,0,1,64,2,128,1,36,193,0,3,135,129,192,1,162,1,0,0,30,0,1,128,132,1,0,0,199,193,192,1,7,130,192,1,166,1,0,2,30,128,0,128,128,1,0,2,192,1,128,2,166,1,128,1,38,0,128,0,4,0,0,
0,4,11,111,98,106,101,99,116,109,101,116,97,4,10,97,114,114,97,121,109,101,116,97,4,4,109,115,103,4,4,112,111,115,3,0,0,0,0,9,1,1,1,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,91,2,0,
0,91,2,0,0,0,0,2,3,0,0,0,6,0,64,0,38,0,0,1,38,0,128,0,1,0,0,0,4,7,100,107,106,115,111,110,1,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
#endif
#ifdef LUA_USE_XML2LUA
static const size_t XML2LUA_SIZE = 11123;
static const unsigned char XML2LUA[11124] = {
27,76,117,97,83,0,25,147,13,10,26,10,4,8,4,8,8,120,86,0,0,0,0,0,0,0,0,0,0,0,40,119,64,1,0,0,0,0,0,0,0,0,0,0,1,25,104,0,0,0,11,64,0,0,10,128,192,128,8,0,0,128,44,0,0,
0,108,64,0,0,139,128,4,0,138,0,193,129,138,128,193,130,138,0,194,131,138,128,194,132,138,0,195,133,138,128,195,134,138,0,196,135,138,128,196,136,138,0,197,137,138,128,197,138,138,0,198,139,138,128,198,140,138,0,199,141,138,128,199,
142,138,0,200,143,138,128,200,144,138,0,201,145,138,128,201,146,203,192,2,0,202,64,74,148,202,192,74,149,202,64,75,150,202,192,75,151,202,64,76,152,202,192,76,153,202,64,77,154,202,192,77,155,202,64,78,156,202,192,78,157,202,64,79,
158,138,192,128,147,203,192,1,0,202,0,208,159,202,128,208,160,202,0,209,161,202,128,209,162,202,0,210,163,202,0,128,164,202,64,0,165,138,192,0,159,236,128,0,0,138,192,128,165,236,192,0,0,44,1,1,0,108,65,1,0,172,129,1,
0,236,193,1,0,44,2,2,0,108,66,2,0,172,130,2,0,236,194,2,0,44,3,3,0,108,67,3,0,172,131,3,0,236,195,3,0,44,4,4,0,108,68,4,0,138,64,4,166,138,128,128,166,108,132,4,0,134,4,64,0,236,196,4,
0,138,196,4,167,134,4,64,0,236,4,5,0,138,196,132,167,134,4,64,0,236,68,5,0,138,196,4,168,134,68,84,0,236,132,5,0,138,196,4,169,172,196,5,0,236,4,6,0,44,69,6,0,70,197,84,0,172,133,6,0,74,133,5,
170,108,197,6,0,128,5,128,10,164,133,128,0,236,5,7,0,138,197,133,170,236,69,7,0,138,197,5,171,236,133,7,0,44,198,7,0,138,5,134,171,44,6,8,0,138,5,6,172,44,70,8,0,138,5,134,172,7,70,86,11,138,5,6,
173,138,133,133,173,6,198,84,0,10,134,5,174,38,0,128,0,93,0,0,0,4,8,120,109,108,50,108,117,97,4,9,95,86,69,82,83,73,79,78,4,6,49,46,53,45,50,4,5,95,88,77,76,4,28,94,40,91,94,60,93,42,41,60,
40,37,47,63,41,40,91,94,62,93,45,41,40,37,47,63,41,62,4,7,95,65,84,84,82,49,4,24,40,91,37,119,45,58,95,93,43,41,37,115,42,61,37,115,42,34,40,46,45,41,34,4,7,95,65,84,84,82,50,4,24,40,91,37,
119,45,58,95,93,43,41,37,115,42,61,37,115,42,39,40,46,45,41,39,4,7,95,67,68,65,84,65,4,22,60,37,33,37,91,67,68,65,84,65,37,91,40,46,45,41,37,93,37,93,62,4,4,95,80,73,4,11,60,37,63,40,46,45,
41,37,63,62,4,9,95,67,79,77,77,69,78,84,4,16,60,33,37,45,37,45,40,46,45,41,37,45,37,45,62,4,5,95,84,65,71,4,10,94,40,46,45,41,37,115,46,42,4,11,95,76,69,65,68,73,78,71,87,83,4,5,94,37,
115,43,4,12,95,84,82,65,73,76,73,78,71,87,83,4,5,37,115,43,36,4,4,95,87,83,4,6,94,37,115,42,36,4,6,95,68,84,68,49,20,56,60,33,68,79,67,84,89,80,69,37,115,43,40,46,45,41,37,115,43,40,83,89,
83,84,69,77,41,37,115,43,91,34,39,93,40,46,45,41,91,34,39,93,37,115,42,40,37,98,91,93,41,37,115,42,62,4,6,95,68,84,68,50,20,71,60,33,68,79,67,84,89,80,69,37,115,43,40,46,45,41,37,115,43,40,80,85,
66,76,73,67,41,37,115,43,91,34,39,93,40,46,45,41,91,34,39,93,37,115,43,91,34,39,93,40,46,45,41,91,34,39,93,37,115,42,40,37,98,91,93,41,37,115,42,62,4,6,95,68,84,68,51,4,15,60,33,68,79,67,84,89,
80,69,37,115,46,45,62,4,6,95,68,84,68,52,20,47,60,33,68,79,67,84,89,80,69,37,115,43,40,46,45,41,37,115,43,40,83,89,83,84,69,77,41,37,115,43,91,34,39,93,40,46,45,41,91,34,39,93,37,115,42,62,4,6,
95,68,84,68,53,20,62,60,33,68,79,67,84,89,80,69,37,115,43,40,46,45,41,37,115,43,40,80,85,66,76,73,67,41,37,115,43,91,34,39,93,40,46,45,41,91,34,39,93,37,115,43,91,34,39,93,40,46,45,41,91,34,39,93,
37,115,42,62,4,10,95,65,84,84,82,69,82,82,49,4,14,61,43,63,37,115,42,34,91,94,34,93,42,36,4,10,95,65,84,84,82,69,82,82,50,4,14,61,43,63,37,115,42,39,91,94,39,93,42,36,4,8,95,84,65,71,69,88,
84,4,7,40,37,47,63,41,62,4,8,95,101,114,114,115,116,114,4,7,120,109,108,69,114,114,4,18,69,114,114,111,114,32,80,97,114,115,105,110,103,32,88,77,76,4,8,100,101,99,108,69,114,114,4,22,69,114,114,111,114,32,80,97,
114,115,105,110,103,32,88,77,76,68,101,99,108,4,13,100,101,99,108,83,116,97,114,116,69,114,114,4,33,88,77,76,68,101,99,108,32,110,111,116,32,97,116,32,115,116,97,114,116,32,111,102,32,100,111,99,117,109,101,110,116,4,12,100,
101,99,108,65,116,116,114,69,114,114,4,27,73,110,118,97,108,105,100,32,88,77,76,68,101,99,108,32,97,116,116,114,105,98,117,116,101,115,4,6,112,105,69,114,114,4,37,69,114,114,111,114,32,80,97,114,115,105,110,103,32,80,114,111,
99,101,115,115,105,110,103,32,73,110,115,116,114,117,99,116,105,111,110,4,11,99,111,109,109,101,110,116,69,114,114,4,22,69,114,114,111,114,32,80,97,114,115,105,110,103,32,67,111,109,109,101,110,116,4,9,99,100,97,116,97,69,114,114,
4,20,69,114,114,111,114,32,80,97,114,115,105,110,103,32,67,68,65,84,65,4,7,100,116,100,69,114,114,4,18,69,114,114,111,114,32,80,97,114,115,105,110,103,32,68,84,68,4,10,101,110,100,84,97,103,69,114,114,4,27,69,110,100,
32,84,97,103,32,65,116,116,114,105,98,117,116,101,115,32,73,110,118,97,108,105,100,4,16,117,110,109,97,116,99,104,101,100,84,97,103,69,114,114,4,15,85,110,98,97,108,97,110,99,101,100,32,84,97,103,4,17,105,110,99,111,109,112,
108,101,116,101,88,109,108,69,114,114,4,24,73,110,99,111,109,112,108,101,116,101,32,88,77,76,32,68,111,99,117,109,101,110,116,4,10,95,69,78,84,73,84,73,69,83,4,5,38,108,116,59,4,2,60,4,5,38,103,116,59,4,2,62,
4,6,38,97,109,112,59,4,2,38,4,7,38,113,117,111,116,59,4,2,34,4,7,38,97,112,111,115,59,4,2,39,4,9,38,35,40,37,100,43,41,59,4,10,38,35,120,40,37,120,43,41,59,4,4,110,101,119,4,6,112,97,114,115,
101,4,8,95,95,105,110,100,101,120,4,7,112,97,114,115,101,114,4,10,112,114,105,110,116,97,98,108,101,4,9,116,111,83,116,114,105,110,103,4,8,120,109,108,50,108,117,97,4,9,108,111,97,100,70,105,108,101,4,8,120,109,108,50,
108,117,97,4,6,116,111,88,109,108,4,4,110,101,119,4,7,114,101,100,117,99,101,4,9,115,116,97,114,116,116,97,103,4,7,101,110,100,116,97,103,4,5,116,101,120,116,4,6,99,100,97,116,97,4,8,95,95,105,110,100,101,120,4,
5,116,114,101,101,1,0,0,0,1,0,34,0,0,0,0,3,0,0,0,10,0,0,0,1,0,5,18,0,0,0,70,0,64,0,128,0,0,0,100,128,0,1,33,64,128,128,30,128,1,128,32,128,192,0,30,0,1,128,134,192,64,0,135,
0,65,1,192,0,128,0,165,0,0,1,166,0,0,0,129,64,1,0,192,0,0,0,1,129,1,0,157,0,1,1,166,0,0,1,38,0,128,0,7,0,0,0,4,9,116,111,110,117,109,98,101,114,19,0,0,0,0,0,0,0,0,19,0,
1,0,0,0,0,0,0,4,7,115,116,114,105,110,103,4,5,99,104,97,114,4,3,38,35,4,2,59,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,0,0,0,19,0,0,0,1,0,5,19,0,
0,0,70,0,64,0,128,0,0,0,193,64,0,0,100,128,128,1,33,64,0,129,30,128,1,128,32,192,192,0,30,0,1,128,134,0,65,0,135,64,65,1,192,0,128,0,165,0,0,1,166,0,0,0,129,128,1,0,192,0,0,0,1,193,
1,0,157,0,1,1,166,0,0,1,38,0,128,0,8,0,0,0,4,9,116,111,110,117,109,98,101,114,19,16,0,0,0,0,0,0,0,19,0,0,0,0,0,0,0,0,19,0,1,0,0,0,0,0,0,4,7,115,116,114,105,110,103,4,
5,99,104,97,114,4,4,38,35,120,4,2,59,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,66,0,0,0,76,0,0,0,2,0,6,13,0,0,0,139,192,0,0,138,0,0,128,138,64,128,128,203,
0,0,0,138,192,0,129,198,192,64,0,0,1,0,1,69,1,128,0,228,64,128,1,197,0,128,0,138,192,0,130,166,0,0,1,38,0,128,0,5,0,0,0,4,8,104,97,110,100,108,101,114,4,8,111,112,116,105,111,110,115,4,7,95,
115,116,97,99,107,4,13,115,101,116,109,101,116,97,116,97,98,108,101,4,8,95,95,105,110,100,101,120,2,0,0,0,0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,78,0,0,0,88,0,0,0,2,0,5,
18,0,0,0,31,0,64,0,30,64,0,128,131,0,0,0,166,0,0,1,135,64,0,0,31,0,64,1,30,192,1,128,133,0,0,0,198,64,192,0,0,1,0,0,228,128,0,1,0,1,128,0,165,0,128,1,166,0,0,0,30,64,0,128,
131,0,128,0,166,0,0,1,38,0,128,0,2,0,0,0,0,4,13,103,101,116,109,101,116,97,116,97,98,108,101,2,0,0,0,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,90,0,0,0,94,0,0,0,
3,0,6,10,0,0,0,199,0,64,0,199,64,192,1,226,0,0,0,30,0,1,128,199,0,64,0,199,64,192,1,0,1,128,0,64,1,0,1,228,64,128,1,38,0,128,0,2,0,0,0,4,8,111,112,116,105,111,110,115,4,13,101,114,
114,111,114,72,97,110,100,108,101,114,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,96,0,0,0,102,0,0,0,2,0,6,20,0,0,0,135,0,64,0,135,64,64,1,162,0,0,0,30,64,3,128,134,128,
64,0,135,192,64,1,192,0,128,0,1,1,1,0,65,65,1,0,164,128,0,2,64,0,0,1,134,128,64,0,135,192,64,1,192,0,128,0,1,129,1,0,65,65,1,0,164,128,0,2,64,0,0,1,102,0,0,1,38,0,128,0,7,0,
0,0,4,8,111,112,116,105,111,110,115,4,8,115,116,114,105,112,87,83,4,7,115,116,114,105,110,103,4,5,103,115,117,98,4,5,94,37,115,43,4,1,4,5,37,115,43,36,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,104,0,0,0,112,0,0,0,2,0,11,19,0,0,0,135,0,64,0,135,64,64,1,162,0,0,0,30,0,3,128,134,128,64,0,199,192,64,0,164,0,1,1,30,128,1,128,198,1,65,0,199,65,193,3,0,2,
128,0,64,2,128,2,128,2,0,3,228,129,0,2,64,0,128,3,169,128,0,0,42,129,253,127,102,0,0,1,38,0,128,0,6,0,0,0,4,8,111,112,116,105,111,110,115,4,15,101,120,112,97,110,100,69,110,116,105,116,105,101,115,4,
6,112,97,105,114,115,4,10,95,69,78,84,73,84,73,69,83,4,7,115,116,114,105,110,103,4,5,103,115,117,98,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,114,0,0,0,135,0,0,0,2,0,
8,33,0,0,0,139,128,0,0,198,64,64,0,199,128,192,1,0,1,128,0,71,193,64,0,129,1,1,0,228,128,0,2,138,192,0,128,203,0,0,0,138,192,128,130,236,0,0,0,6,65,64,0,7,129,64,2,64,1,128,0,135,129,65,
0,192,1,128,1,36,65,0,2,6,65,64,0,7,129,64,2,64,1,128,0,135,193,65,0,192,1,128,1,36,65,0,2,7,1,66,1,7,65,66,2,34,1,0,0,30,128,0,128,7,1,66,1,10,129,194,132,30,0,0,128,138,128,66,
132,166,0,0,1,38,0,128,0,11,0,0,0,4,5,110,97,109,101,4,7,115,116,114,105,110,103,4,5,103,115,117,98,4,5,95,84,65,71,4,3,37,49,4,6,97,116,116,114,115,4,7,95,65,84,84,82,49,4,7,95,65,84,84,
82,50,4,6,97,116,116,114,115,4,2,95,0,2,0,0,0,0,0,1,6,1,0,0,0,0,120,0,0,0,123,0,0,0,2,0,6,9,0,0,0,134,0,64,0,197,0,128,0,5,1,0,1,64,1,128,0,228,128,128,1,138,192,0,
0,134,0,64,0,138,128,192,128,38,0,128,0,3,0,0,0,4,6,97,116,116,114,115,4,2,95,19,1,0,0,0,0,0,0,0,3,0,0,0,1,2,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,137,0,0,0,158,0,0,0,3,0,9,58,0,0,0,198,192,64,0,199,0,193,1,0,1,128,0,71,65,65,0,135,129,65,1,228,0,1,2,138,64,1,129,138,0,129,128,138,192,0,128,199,0,
64,1,226,64,0,0,30,64,1,128,197,0,128,0,0,1,0,0,71,193,65,0,71,1,194,2,135,129,65,1,228,64,0,2,199,0,64,1,95,64,194,1,30,64,1,128,197,0,128,0,0,1,0,0,71,193,65,0,71,129,194,2,135,129,
65,1,228,64,0,2,197,0,0,1,0,1,0,0,71,129,64,1,228,128,128,1,7,193,194,1,34,1,0,0,30,64,2,128,7,193,194,1,7,1,67,2,31,64,67,2,30,64,1,128,5,1,128,0,64,1,0,0,135,193,65,0,135,129,
67,3,199,129,65,1,36,65,0,2,5,1,128,1,71,193,67,0,129,1,4,0,36,129,128,1,34,1,0,0,30,64,1,128,7,193,67,0,12,1,68,2,128,1,128,1,199,1,64,1,7,66,64,1,36,65,128,2,230,0,0,1,38,0,
128,0,17,0,0,0,4,6,109,97,116,99,104,4,9,101,110,100,77,97,116,99,104,4,5,116,101,120,116,4,7,115,116,114,105,110,103,4,5,102,105,110,100,4,4,95,80,73,4,4,112,111,115,4,8,95,101,114,114,115,116,114,4,8,
100,101,99,108,69,114,114,19,1,0,0,0,0,0,0,0,4,13,100,101,99,108,83,116,97,114,116,69,114,114,4,6,97,116,116,114,115,4,8,118,101,114,115,105,111,110,0,4,12,100,101,99,108,65,116,116,114,69,114,114,4,8,104,97,
110,100,108,101,114,4,5,100,101,99,108,4,0,0,0,0,0,1,4,1,7,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,160,0,0,0,182,0,0,0,3,0,10,58,0,0,0,203,0,0,0,6,193,64,0,7,
1,65,2,64,1,128,0,135,65,65,0,199,129,65,1,36,1,1,2,138,128,1,129,138,64,129,128,138,0,1,128,7,1,64,1,34,65,0,0,30,64,1,128,5,1,128,0,64,1,0,0,135,193,65,0,135,1,66,3,199,129,65,1,36,
65,0,2,5,1,0,1,71,65,66,0,129,129,2,0,36,129,128,1,34,1,0,0,30,128,7,128,5,1,128,1,64,1,0,0,135,129,64,1,36,129,128,1,192,0,0,2,6,193,64,0,7,193,66,2,71,129,64,1,134,193,64,0,135,
1,67,3,199,65,195,1,164,129,0,1,141,129,67,3,36,129,128,1,95,192,67,2,30,0,2,128,71,1,196,1,98,1,0,0,30,128,0,128,71,1,196,1,74,1,129,136,30,128,0,128,75,65,0,0,74,1,129,136,202,64,1,136,71,
65,66,0,76,129,194,2,192,1,128,1,7,2,64,1,71,66,64,1,100,65,128,2,230,0,0,1,38,0,128,0,18,0,0,0,4,6,109,97,116,99,104,4,9,101,110,100,77,97,116,99,104,4,5,116,101,120,116,4,7,115,116,114,105,
110,103,4,5,102,105,110,100,4,4,95,80,73,4,4,112,111,115,4,8,95,101,114,114,115,116,114,4,6,112,105,69,114,114,4,8,104,97,110,100,108,101,114,4,3,112,105,4,4,115,117,98,4,4,108,101,110,4,5,110,97,109,101,19,
1,0,0,0,0,0,0,0,4,1,4,6,97,116,116,114,115,4,6,95,116,101,120,116,4,0,0,0,0,0,1,4,1,3,1,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,184,0,0,0,194,0,0,0,3,0,9,
40,0,0,0,198,192,64,0,199,0,193,1,0,1,128,0,71,65,65,0,135,129,65,1,228,0,1,2,138,64,1,129,138,0,129,128,138,192,0,128,199,0,64,1,226,64,0,0,30,64,1,128,197,0,128,0,0,1,0,0,71,193,65,0,
71,1,194,2,135,129,65,1,228,64,0,2,197,0,0,1,7,65,66,0,65,129,2,0,228,128,128,1,226,0,0,0,30,128,3,128,197,0,128,1,0,1,0,0,69,1,0,2,128,1,0,0,199,129,64,1,100,1,128,1,228,128,0,0,
138,192,0,129,199,64,66,0,204,128,194,1,71,129,64,1,134,193,66,0,199,1,64,1,7,66,64,1,228,64,0,3,38,0,128,0,12,0,0,0,4,6,109,97,116,99,104,4,9,101,110,100,77,97,116,99,104,4,5,116,101,120,116,4,
7,115,116,114,105,110,103,4,5,102,105,110,100,4,9,95,67,79,77,77,69,78,84,4,4,112,111,115,4,8,95,101,114,114,115,116,114,4,11,99,111,109,109,101,110,116,69,114,114,4,8,104,97,110,100,108,101,114,4,8,99,111,109,109,
101,110,116,4,5,110,101,120,116,5,0,0,0,0,0,1,4,1,3,1,6,1,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,196,0,0,0,207,0,0,0,3,0,19,33,0,0,0,203,0,128,2,7,1,64,0,71,
65,64,0,135,129,64,0,199,193,64,0,7,2,65,0,235,64,128,2,6,65,65,0,64,1,128,1,36,1,1,1,30,0,4,128,70,130,65,0,71,194,193,4,128,2,128,0,192,2,0,4,0,3,0,1,100,2,2,2,98,2,0,0,30,
0,2,128,0,4,128,4,64,4,0,5,139,68,1,0,138,196,2,132,138,4,131,132,138,68,3,133,138,132,131,133,138,196,3,134,38,4,0,2,41,129,0,0,170,1,251,127,4,1,0,0,38,1,0,1,38,0,128,0,13,0,0,0,4,
6,95,68,84,68,49,4,6,95,68,84,68,50,4,6,95,68,84,68,51,4,6,95,68,84,68,52,4,6,95,68,84,68,53,4,6,112,97,105,114,115,4,7,115,116,114,105,110,103,4,5,102,105,110,100,4,6,95,114,111,111,116,4,6,
95,116,121,112,101,4,6,95,110,97,109,101,4,5,95,117,114,105,4,10,95,105,110,116,101,114,110,97,108,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,209,0,0,0,219,0,0,0,3,0,9,41,
0,0,0,197,0,128,0,0,1,0,0,64,1,128,0,135,193,64,1,228,0,1,2,8,64,1,129,138,0,129,128,138,192,0,128,199,0,64,1,226,64,0,0,30,64,1,128,197,0,0,1,0,1,0,0,71,1,65,0,71,65,193,2,135,
193,64,1,228,64,0,2,197,0,128,1,7,129,65,0,65,193,1,0,228,128,128,1,226,0,0,0,30,0,4,128,203,128,0,0,202,64,66,132,6,193,66,0,7,1,67,2,64,1,128,0,135,1,64,1,141,65,67,3,199,65,64,1,206,
129,195,3,36,129,0,2,202,0,1,133,7,129,65,0,12,193,65,2,128,1,128,1,199,1,64,1,7,66,64,1,36,65,128,2,38,0,128,0,15,0,0,0,4,6,109,97,116,99,104,4,9,101,110,100,77,97,116,99,104,4,2,95,4,
4,112,111,115,4,8,95,101,114,114,115,116,114,4,7,100,116,100,69,114,114,4,8,104,97,110,100,108,101,114,4,4,100,116,100,4,5,110,97,109,101,4,8,68,79,67,84,89,80,69,4,6,118,97,108,117,101,4,7,115,116,114,105,110,
103,4,4,115,117,98,19,10,0,0,0,0,0,0,0,19,1,0,0,0,0,0,0,0,4,0,0,0,0,0,1,11,1,4,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,221,0,0,0,230,0,0,0,3,0,9,
32,0,0,0,198,192,64,0,199,0,193,1,0,1,128,0,71,65,65,0,135,129,65,1,228,0,1,2,138,64,1,129,138,0,129,128,138,192,0,128,199,0,64,1,226,64,0,0,30,64,1,128,197,0,128,0,0,1,0,0,71,193,65,0,
71,1,194,2,135,129,65,1,228,64,0,2,197,0,0,1,7,65,66,0,65,129,2,0,228,128,128,1,226,0,0,0,30,128,1,128,199,64,66,0,204,128,194,1,71,129,64,1,132,1,0,0,199,1,64,1,7,66,64,1,228,64,0,3,
38,0,128,0,11,0,0,0,4,6,109,97,116,99,104,4,9,101,110,100,77,97,116,99,104,4,5,116,101,120,116,4,7,115,116,114,105,110,103,4,5,102,105,110,100,4,7,95,67,68,65,84,65,4,4,112,111,115,4,8,95,101,114,114,
115,116,114,4,9,99,100,97,116,97,69,114,114,4,8,104,97,110,100,108,101,114,4,6,99,100,97,116,97,3,0,0,0,0,0,1,4,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,232,0,0,0,28,1,0,0,
3,0,10,142,0,0,0,198,128,64,0,199,192,192,1,7,1,65,1,71,65,65,0,228,192,128,1,138,0,129,128,138,192,0,128,199,64,64,1,31,128,193,1,30,64,2,128,198,128,64,0,199,192,192,1,7,1,65,1,71,193,65,0,228,
192,128,1,138,0,129,128,138,192,0,128,199,64,64,1,95,128,193,1,30,192,7,128,198,128,64,0,199,192,192,1,0,1,128,0,71,193,66,0,135,1,67,1,141,65,67,3,228,0,1,2,138,64,1,133,138,0,129,132,138,192,0,132,199,
0,65,1,6,129,64,0,7,129,67,2,64,1,128,0,135,1,67,1,199,65,66,1,206,65,195,3,36,129,0,2,221,0,129,1,138,192,0,130,199,192,67,1,226,64,0,0,30,64,1,128,197,0,128,0,0,1,0,0,71,1,68,0,71,
65,196,2,135,129,68,1,228,64,0,2,199,64,66,1,138,192,0,134,30,192,242,127,197,0,0,1,0,1,0,0,71,1,65,1,228,128,128,1,7,193,68,1,31,0,69,2,30,0,11,128,5,1,128,1,71,65,69,0,129,129,5,0,36,
129,128,1,34,1,0,0,30,128,18,128,7,193,197,1,34,1,0,0,30,128,2,128,5,1,128,0,64,1,0,0,134,129,64,0,135,1,70,3,193,65,6,0,7,2,68,0,7,130,70,4,71,194,198,1,164,129,0,2,199,129,68,1,36,
65,0,2,6,1,71,0,7,65,71,2,71,129,71,0,36,129,0,1,71,193,198,1,95,64,1,2,30,128,2,128,5,1,128,0,64,1,0,0,134,129,64,0,135,1,70,3,193,65,6,0,7,2,68,0,7,194,71,4,71,194,198,1,164,
129,0,2,199,129,68,1,36,65,0,2,7,65,69,0,12,129,69,2,128,1,128,1,199,193,67,1,7,2,67,1,36,65,128,2,30,192,8,128,6,1,71,0,7,1,72,2,71,129,71,0,135,193,198,1,36,65,128,1,5,1,128,1,71,
65,69,0,129,65,8,0,36,129,128,1,34,1,0,0,30,64,1,128,7,65,69,0,12,65,72,2,128,1,128,1,199,193,67,1,7,2,67,1,36,65,128,2,7,129,66,1,31,0,69,2,30,192,3,128,6,1,71,0,7,65,71,2,71,
129,71,0,36,65,0,1,5,1,128,1,71,65,69,0,129,129,5,0,36,129,128,1,34,1,0,0,30,64,1,128,7,65,69,0,12,129,69,2,128,1,128,1,199,193,67,1,7,2,67,1,36,65,128,2,230,0,0,1,38,0,128,0,34,
0,0,0,4,9,101,114,114,83,116,97,114,116,4,7,101,114,114,69,110,100,4,7,115,116,114,105,110,103,4,5,102,105,110,100,4,7,116,97,103,115,116,114,4,10,95,65,84,84,82,69,82,82,49,0,4,10,95,65,84,84,82,69,82,
82,50,4,9,101,120,116,83,116,97,114,116,4,7,101,120,116,69,110,100,4,6,101,110,100,116,50,4,8,95,84,65,71,69,88,84,4,9,101,110,100,77,97,116,99,104,19,1,0,0,0,0,0,0,0,4,4,115,117,98,4,6,109,97,
116,99,104,4,8,95,101,114,114,115,116,114,4,7,120,109,108,69,114,114,4,4,112,111,115,4,6,101,110,100,116,49,4,2,47,4,8,104,97,110,100,108,101,114,4,7,101,110,100,116,97,103,4,6,97,116,116,114,115,4,7,102,111,114,
109,97,116,4,9,37,115,32,40,47,37,115,41,4,10,101,110,100,84,97,103,69,114,114,4,5,110,97,109,101,4,6,116,97,98,108,101,4,7,114,101,109,111,118,101,4,7,95,115,116,97,99,107,4,16,117,110,109,97,116,99,104,101,100,
84,97,103,69,114,114,4,7,105,110,115,101,114,116,4,9,115,116,97,114,116,116,97,103,4,0,0,0,0,0,1,4,1,7,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,30,1,0,0,44,1,0,0,3,0,8,
80,0,0,0,198,0,64,0,199,64,192,1,6,1,64,0,7,129,64,2,71,193,64,1,129,1,1,0,193,65,1,0,36,129,0,2,65,129,1,0,228,128,128,1,226,0,0,0,30,64,1,128,197,0,128,0,0,1,0,0,64,1,128,0,
128,1,0,1,228,64,0,2,30,0,15,128,198,0,64,0,199,128,192,1,7,193,64,1,65,1,1,0,129,1,1,0,228,128,0,2,31,192,193,1,30,64,1,128,197,0,0,1,0,1,0,0,64,1,128,0,128,1,0,1,228,64,0,2,
30,128,11,128,198,0,64,0,199,128,192,1,7,193,64,1,65,1,1,0,129,1,2,0,228,128,0,2,31,64,194,1,30,64,1,128,197,0,128,1,0,1,0,0,64,1,128,0,128,1,0,1,228,64,0,2,30,0,8,128,198,0,64,0,
199,128,192,1,7,193,64,1,65,1,1,0,129,129,2,0,228,128,0,2,31,192,194,1,30,64,1,128,197,0,0,2,0,1,0,0,64,1,128,0,128,1,0,1,228,64,0,2,30,128,4,128,198,0,64,0,199,128,192,1,7,193,64,1,
65,1,1,0,129,129,2,0,228,128,0,2,31,0,195,1,30,64,1,128,197,0,128,2,0,1,0,0,64,1,128,0,128,1,0,1,228,64,0,2,30,0,1,128,197,0,0,3,0,1,0,0,64,1,128,0,128,1,0,1,228,64,0,2,
38,0,128,0,13,0,0,0,4,7,115,116,114,105,110,103,4,5,102,105,110,100,4,4,115,117,98,4,7,116,97,103,115,116,114,19,1,0,0,0,0,0,0,0,19,5,0,0,0,0,0,0,0,4,7,63,120,109,108,37,115,4,2,63,
19,3,0,0,0,0,0,0,0,4,4,33,45,45,19,8,0,0,0,0,0,0,0,4,9,33,68,79,67,84,89,80,69,4,9,33,91,67,68,65,84,65,91,7,0,0,0,0,0,1,8,1,9,1,10,1,12,1,13,1,14,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,46,1,0,0,67,1,0,0,3,0,9,65,0,0,0,198,128,65,0,199,192,193,1,0,1,128,0,71,1,66,0,135,65,66,1,228,192,1,2,138,0,130,130,138,192,1,130,138,128,129,
129,138,64,1,129,138,0,129,128,138,192,0,128,199,0,64,1,226,64,0,0,30,192,6,128,198,128,65,0,199,192,193,1,0,1,128,0,71,129,66,0,135,65,66,1,228,128,0,2,226,0,0,0,30,64,3,128,199,192,66,0,220,0,128,
1,95,0,195,1,30,128,1,128,197,0,128,0,0,1,0,0,71,65,67,0,71,129,195,2,135,65,66,1,228,64,0,2,30,0,2,128,195,0,0,0,230,0,0,1,30,64,1,128,197,0,128,0,0,1,0,0,71,65,67,0,71,193,195,
2,135,65,66,1,228,64,0,2,199,128,64,1,226,64,0,0,30,0,0,128,193,0,4,0,138,192,0,129,199,0,65,1,226,64,0,0,30,0,0,128,193,0,4,0,138,192,0,130,199,0,64,1,226,64,0,0,30,0,0,128,193,0,3,
0,138,192,0,128,199,64,64,1,31,64,196,1,30,0,0,128,195,64,0,0,195,0,128,0,230,0,0,1,38,0,128,0,18,0,0,0,4,6,109,97,116,99,104,4,9,101,110,100,77,97,116,99,104,4,5,116,101,120,116,4,6,101,110,
100,116,49,4,7,116,97,103,115,116,114,4,6,101,110,100,116,50,4,7,115,116,114,105,110,103,4,5,102,105,110,100,4,5,95,88,77,76,4,4,112,111,115,4,4,95,87,83,4,7,95,115,116,97,99,107,19,0,0,0,0,0,0,0,
0,4,8,95,101,114,114,115,116,114,4,17,105,110,99,111,109,112,108,101,116,101,88,109,108,69,114,114,4,7,120,109,108,69,114,114,4,1,0,2,0,0,0,0,0,1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
69,1,0,0,109,1,0,0,3,0,10,84,0,0,0,198,0,64,0,0,1,0,0,228,128,0,1,31,64,192,1,30,64,1,128,198,128,64,0,0,1,0,0,228,128,0,1,5,1,128,0,95,0,129,1,30,128,0,128,198,192,64,0,1,
1,1,0,228,64,0,1,31,64,65,1,30,0,0,128,131,0,128,0,199,128,65,0,202,128,128,131,203,192,0,0,202,64,66,132,202,64,66,133,202,0,195,133,7,1,194,1,34,1,0,0,30,0,14,128,5,1,0,1,64,1,0,0,128,
1,128,0,192,1,128,1,36,129,0,2,34,1,0,0,30,64,12,128,7,1,194,1,202,0,129,134,7,1,194,1,70,193,67,0,71,1,196,2,135,65,196,1,100,129,0,1,13,65,1,2,14,1,67,2,202,0,1,135,7,1,194,1,70,
193,67,0,71,1,196,2,135,65,196,1,100,129,0,1,13,65,1,2,202,0,1,132,5,1,128,1,64,1,0,0,133,1,0,2,192,1,0,0,7,66,196,1,164,1,128,1,36,129,0,0,202,0,129,136,7,65,196,1,95,128,68,2,30,
0,3,128,5,1,128,2,71,129,65,0,129,65,4,0,36,129,128,1,34,1,0,0,30,128,1,128,7,129,65,0,12,65,68,2,135,65,196,1,196,1,0,0,7,2,194,1,71,130,195,1,36,65,0,3,5,1,0,3,64,1,0,0,128,
1,128,0,192,1,128,1,36,65,0,2,7,129,194,1,13,1,67,2,202,0,129,133,30,192,240,127,38,0,128,0,19,0,0,0,4,5,116,121,112,101,4,6,116,97,98,108,101,4,13,103,101,116,109,101,116,97,116,97,98,108,101,4,6,
101,114,114,111,114,20,81,89,111,117,32,109,117,115,116,32,99,97,108,108,32,120,109,108,112,97,114,115,101,114,58,112,97,114,115,101,40,112,97,114,97,109,101,116,101,114,115,41,32,105,110,115,116,101,97,100,32,111,102,32,120,109,108,112,
97,114,115,101,114,46,112,97,114,115,101,40,112,97,114,97,109,101,116,101,114,115,41,0,4,8,104,97,110,100,108,101,114,4,16,112,97,114,115,101,65,116,116,114,105,98,117,116,101,115,4,6,109,97,116,99,104,19,0,0,0,0,0,0,
0,0,4,9,101,110,100,77,97,116,99,104,4,4,112,111,115,19,1,0,0,0,0,0,0,0,4,10,115,116,97,114,116,84,101,120,116,4,8,101,110,100,84,101,120,116,4,7,115,116,114,105,110,103,4,4,108,101,110,4,5,116,101,120,
116,4,1,7,0,0,0,0,0,1,2,1,16,1,6,1,5,1,3,1,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,113,1,0,0,128,1,0,0,2,0,13,40,0,0,0,31,0,64,0,30,0,0,128,38,0,128,
0,98,64,0,0,30,0,0,128,65,64,0,0,134,128,64,0,135,192,64,1,193,0,1,0,15,65,193,0,164,128,128,1,198,128,65,0,0,1,0,0,228,0,1,1,30,64,5,128,6,194,65,0,64,2,128,3,36,130,0,1,31,0,66,
4,30,64,2,128,6,66,66,0,64,2,0,1,128,2,0,3,93,130,130,4,36,66,0,1,5,2,128,0,64,2,128,3,141,66,192,0,36,66,128,1,30,128,1,128,6,66,66,0,64,2,0,1,128,2,0,3,193,130,2,0,0,3,128,
3,93,2,131,4,36,66,0,1,233,128,0,0,106,193,249,127,38,0,128,0,11,0,0,0,0,19,1,0,0,0,0,0,0,0,4,7,115,116,114,105,110,103,4,4,114,101,112,4,2,32,19,2,0,0,0,0,0,0,0,4,6,112,97,
105,114,115,4,5,116,121,112,101,4,6,116,97,98,108,101,4,6,112,114,105,110,116,4,2,61,2,0,0,0,0,0,1,17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,130,1,0,0,145,1,0,0,1,0,5,17,0,
0,0,70,0,64,0,31,64,0,0,30,128,0,128,70,64,64,0,129,128,0,0,100,64,0,1,75,192,0,0,74,0,193,129,74,0,193,130,172,0,0,0,74,128,0,131,134,192,193,0,192,0,0,0,0,1,128,0,165,0,128,1,166,0,
0,0,38,0,128,0,8,0,0,0,4,8,120,109,108,50,108,117,97,4,6,101,114,114,111,114,20,71,89,111,117,32,109,117,115,116,32,99,97,108,108,32,120,109,108,50,108,117,97,46,112,97,114,115,101,40,104,97,110,100,108,101,114,41,
32,105,110,115,116,101,97,100,32,111,102,32,120,109,108,50,108,117,97,58,112,97,114,115,101,40,104,97,110,100,108,101,114,41,4,8,115,116,114,105,112,87,83,19,1,0,0,0,0,0,0,0,4,15,101,120,112,97,110,100,69,110,116,105,
116,105,101,115,4,13,101,114,114,111,114,72,97,110,100,108,101,114,4,4,110,101,119,2,0,0,0,0,0,1,2,1,0,0,0,0,139,1,0,0,141,1,0,0,2,0,7,11,0,0,0,134,0,64,0,198,64,64,0,199,128,192,1,1,
193,0,0,99,65,0,0,30,0,0,128,65,1,1,0,128,1,128,0,228,0,0,2,164,64,0,0,38,0,128,0,5,0,0,0,4,6,101,114,114,111,114,4,7,115,116,114,105,110,103,4,7,102,111,114,109,97,116,4,14,37,115,32,91,
99,104,97,114,61,37,100,93,10,4,12,80,97,114,115,101,32,69,114,114,111,114,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,147,1,0,0,149,1,0,
0,1,0,3,4,0,0,0,69,0,0,0,128,0,0,0,100,64,0,1,38,0,128,0,0,0,0,0,1,0,0,0,1,17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,151,1,0,0,168,1,0,0,1,0,14,40,0,
0,0,65,0,0,0,129,0,0,0,198,64,64,0,0,1,0,0,228,128,0,1,95,128,192,1,30,0,0,128,38,0,0,1,198,192,64,0,0,1,0,0,228,0,1,1,30,192,4,128,6,66,64,0,64,2,128,3,36,130,0,1,31,128,
64,4,30,0,1,128,6,2,65,0,7,66,65,4,64,2,128,3,36,130,0,1,192,1,0,4,0,2,0,1,64,2,128,0,134,130,65,0,135,194,65,5,193,2,2,0,0,3,0,3,64,3,128,3,164,130,0,2,157,128,2,4,65,64,
2,0,233,128,0,0,106,65,250,127,193,128,2,0,0,1,0,1,65,193,2,0,157,64,129,1,166,0,0,1,38,0,128,0,12,0,0,0,4,1,4,5,116,121,112,101,4,6,116,97,98,108,101,4,6,112,97,105,114,115,4,8,120,109,
108,50,108,117,97,4,9,116,111,83,116,114,105,110,103,4,7,115,116,114,105,110,103,4,7,102,111,114,109,97,116,4,6,37,115,61,37,115,4,2,44,4,2,123,4,2,125,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,170,1,0,0,180,1,0,0,1,0,6,17,0,0,0,70,0,64,0,71,64,192,0,128,0,0,0,193,128,0,0,100,192,128,1,98,0,0,0,30,64,1,128,204,192,192,0,65,1,1,0,228,128,128,1,12,65,193,
0,36,65,0,1,230,0,0,1,198,128,65,0,0,1,0,1,228,64,0,1,38,0,128,0,7,0,0,0,4,3,105,111,4,5,111,112,101,110,4,2,114,4,5,114,101,97,100,4,3,42,97,4,6,99,108,111,115,101,4,6,101,114,114,
111,114,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,182,1,0,0,190,1,0,0,1,0,14,21,0,0,0,65,0,0,0,34,64,0,0,30,64,0,128,139,0,0,0,0,0,0,1,134,64,64,0,
192,0,0,0,164,0,1,1,30,192,1,128,192,1,128,0,1,130,0,0,64,2,128,2,129,194,0,0,193,2,1,0,0,3,0,3,65,3,1,0,93,64,131,3,169,128,0,0,42,65,253,127,102,0,0,1,38,0,128,0,5,0,0,0,
4,1,4,6,112,97,105,114,115,4,2,32,4,2,61,4,2,34,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,192,1,0,0,201,1,0,0,1,0,7,16,0,0,0,70,0,64,0,128,0,0,0,
100,128,0,1,31,64,192,0,30,0,2,128,70,128,64,0,128,0,0,0,100,0,1,1,30,0,0,128,38,1,0,1,105,128,0,0,234,0,255,127,68,0,0,0,102,0,0,1,38,0,0,1,38,0,128,0,3,0,0,0,4,5,116,121,
112,101,4,6,116,97,98,108,101,4,6,112,97,105,114,115,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,203,1,0,0,219,1,0,0,4,0,18,59,0,0,0,6,1,64,0,7,65,64,2,65,129,
0,0,143,193,192,1,36,129,128,1,65,1,1,0,129,1,1,0,198,65,65,0,0,2,0,1,228,129,0,1,31,128,193,3,30,192,6,128,197,1,128,0,7,194,65,1,228,129,0,1,128,1,128,3,138,0,194,131,220,1,0,1,31,64,
194,3,30,128,1,128,192,1,0,2,6,130,66,0,71,66,66,1,36,130,0,1,221,1,130,3,99,65,128,3,30,128,1,128,198,193,66,0,199,1,195,3,0,2,0,1,64,2,128,0,141,66,194,1,228,129,0,2,64,1,128,3,193,65,
3,0,0,2,128,2,65,66,3,0,128,2,0,2,93,129,130,3,30,192,0,128,198,129,66,0,0,2,0,1,228,129,0,1,64,1,128,3,198,129,65,0,199,129,195,3,0,2,0,0,64,2,0,2,129,194,3,0,192,2,128,0,0,3,
0,3,65,3,4,0,128,3,128,2,193,67,4,0,0,4,128,0,65,4,4,0,93,66,132,4,228,65,128,1,38,0,128,0,18,0,0,0,4,7,115,116,114,105,110,103,4,4,114,101,112,4,2,32,19,2,0,0,0,0,0,0,0,4,
1,4,5,116,121,112,101,4,6,116,97,98,108,101,4,6,95,97,116,116,114,0,19,1,0,0,0,0,0,0,0,4,9,116,111,115,116,114,105,110,103,4,8,120,109,108,50,108,117,97,4,6,116,111,88,109,108,4,2,10,4,7,105,110,
115,101,114,116,4,2,60,4,2,62,4,3,60,47,2,0,0,0,0,0,1,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,221,1,0,0,12,2,0,0,3,0,21,121,0,0,0,162,64,0,0,30,0,0,128,129,0,
0,0,192,0,0,1,98,64,0,0,30,0,0,128,65,64,0,0,95,64,192,0,30,0,3,128,31,0,64,1,30,128,2,128,11,1,128,0,65,129,0,0,128,1,128,0,197,1,0,0,7,194,64,0,228,129,0,1,1,2,1,0,93,1,
130,2,43,65,128,0,34,65,0,0,30,0,0,128,11,1,0,0,10,64,193,129,70,129,193,0,128,1,0,0,100,1,1,1,30,192,17,128,134,194,193,0,192,2,128,4,164,130,0,1,31,0,66,5,30,128,13,128,134,194,193,0,192,2,
0,4,164,130,0,1,31,64,66,5,30,128,1,128,133,2,0,1,192,2,0,2,0,3,128,0,64,3,128,4,128,3,0,1,164,66,128,2,30,128,13,128,141,0,64,1,134,194,193,0,197,2,128,1,0,3,128,4,228,2,0,1,164,130,
0,0,31,64,66,5,30,192,6,128,134,130,193,0,192,2,128,4,164,2,1,1,30,0,5,128,95,192,192,6,30,128,4,128,198,195,193,0,7,196,192,4,228,131,0,1,31,0,194,7,30,128,1,128,203,67,128,0,0,4,0,7,71,196,
192,4,202,67,132,129,235,67,128,0,226,67,0,0,30,0,0,128,192,3,0,7,5,4,0,1,64,4,0,2,128,4,0,4,192,4,128,7,0,5,0,1,36,68,128,2,169,130,0,0,42,3,250,127,30,128,4,128,133,2,0,1,192,2,
0,2,0,3,0,4,64,3,128,4,128,3,0,1,164,66,128,2,30,192,2,128,134,194,193,0,192,2,0,4,164,130,0,1,31,64,66,5,30,0,0,128,0,2,128,0,133,2,0,1,192,2,0,2,0,3,0,4,64,3,128,4,128,3,
0,1,164,66,128,2,105,129,0,0,234,65,237,127,95,64,192,0,30,64,2,128,31,0,192,1,30,192,1,128,70,1,194,0,71,129,194,2,128,1,0,2,193,193,2,0,0,2,128,0,65,2,3,0,221,65,130,3,100,65,128,1,70,1,
194,0,71,65,195,2,128,1,0,2,193,129,3,0,101,1,128,1,102,1,0,0,38,0,128,0,15,0,0,0,19,1,0,0,0,0,0,0,0,4,1,4,2,60,4,6,95,97,116,116,114,4,2,62,0,4,6,112,97,105,114,115,4,5,
116,121,112,101,4,6,116,97,98,108,101,4,7,110,117,109,98,101,114,4,7,105,110,115,101,114,116,4,3,60,47,4,3,62,10,4,7,99,111,110,99,97,116,4,2,10,4,0,0,0,1,18,0,0,1,20,1,19,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,14,2,0,0,22,2,0,0,0,0,3,13,0,0,0,11,128,0,0,75,0,0,0,10,64,0,128,75,64,0,0,139,0,0,0,74,128,0,129,10,64,128,128,75,0,128,0,135,0,64,0,107,64,
128,0,10,64,128,129,38,0,0,1,38,0,128,0,4,0,0,0,4,5,114,111,111,116,4,8,111,112,116,105,111,110,115,4,9,110,111,114,101,100,117,99,101,4,7,95,115,116,97,99,107,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,26,2,0,0,33,2,0,0,1,0,5,9,0,0,0,69,0,0,0,100,128,128,0,74,0,0,128,134,64,192,0,192,0,128,0,0,1,0,0,164,64,128,1,102,0,0,1,38,0,128,0,2,0,0,0,4,
8,95,95,105,110,100,101,120,4,13,115,101,116,109,101,116,97,116,97,98,108,101,2,0,0,0,1,21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,35,2,0,0,45,2,0,0,4,0,14,30,0,0,0,6,1,
64,0,64,1,128,0,36,1,1,1,30,64,2,128,70,66,64,0,128,2,0,4,100,130,0,1,31,128,192,4,30,0,1,128,76,194,64,0,192,2,0,4,0,3,128,3,64,3,128,0,100,66,128,2,41,129,0,0,170,193,252,127,28,1,
128,0,31,0,65,2,30,64,2,128,7,65,65,0,7,129,65,2,7,129,0,2,34,65,0,0,30,0,1,128,7,193,193,0,31,0,66,2,30,64,0,128,7,1,193,0,202,0,1,1,38,0,128,0,9,0,0,0,4,6,112,97,105,114,
115,4,5,116,121,112,101,4,6,116,97,98,108,101,4,7,114,101,100,117,99,101,19,1,0,0,0,0,0,0,0,4,8,111,112,116,105,111,110,115,4,9,110,111,114,101,100,117,99,101,4,6,95,97,116,116,114,0,1,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,47,2,0,0,55,2,0,0,1,0,5,12,0,0,0,92,0,0,0,31,0,192,0,30,128,1,128,75,0,0,0,134,64,64,0,135,128,64,1,192,0,128,0,0,1,0,0,
164,64,128,1,102,0,0,1,38,0,0,1,38,0,128,0,3,0,0,0,19,0,0,0,0,0,0,0,0,4,6,116,97,98,108,101,4,7,105,110,115,101,114,116,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,57,2,0,0,74,2,0,0,2,0,8,37,0,0,0,139,0,0,0,199,0,64,0,31,64,192,1,30,64,0,128,199,192,192,0,138,192,0,129,199,0,65,0,7,1,65,0,28,1,0,2,199,0,129,1,7,65,193,0,7,1,
129,1,34,1,0,0,30,192,2,128,5,1,0,0,71,65,193,0,71,65,129,1,36,129,0,1,70,129,193,0,71,193,193,2,128,1,0,2,192,1,0,1,100,65,128,1,71,65,193,0,202,0,129,2,30,0,1,128,7,65,193,0,75,1,
128,0,128,1,0,1,107,65,128,0,202,64,1,2,6,129,193,0,7,193,65,2,71,1,65,0,128,1,0,1,36,65,128,1,38,0,128,0,8,0,0,0,4,16,112,97,114,115,101,65,116,116,114,105,98,117,116,101,115,1,1,4,6,95,
97,116,116,114,4,6,97,116,116,114,115,4,7,95,115,116,97,99,107,4,5,110,97,109,101,4,6,116,97,98,108,101,4,7,105,110,115,101,114,116,2,0,0,0,1,23,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,76,2,0,0,87,2,0,0,3,0,10,29,0,0,0,199,0,64,0,7,1,64,0,28,1,0,2,14,65,64,2,199,0,129,1,7,129,192,0,7,1,129,1,34,65,0,0,30,192,1,128,6,193,64,0,65,1,1,0,128,1,0,1,
193,65,1,0,7,130,192,0,65,130,1,0,93,65,130,2,36,65,0,1,7,193,65,0,31,0,129,1,30,192,0,128,12,1,66,0,128,1,128,1,196,1,128,0,36,65,128,2,6,65,66,0,7,129,66,2,71,1,64,0,36,65,0,1,
38,0,128,0,11,0,0,0,4,7,95,115,116,97,99,107,19,1,0,0,0,0,0,0,0,4,5,110,97,109,101,4,6,101,114,114,111,114,4,28,88,77,76,32,69,114,114,111,114,32,45,32,85,110,109,97,116,99,104,101,100,32,84,97,
103,32,91,4,2,58,4,3,93,10,4,5,114,111,111,116,4,7,114,101,100,117,99,101,4,6,116,97,98,108,101,4,7,114,101,109,111,118,101,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,89,2,
0,0,92,2,0,0,2,0,6,10,0,0,0,135,0,64,0,199,0,64,0,220,0,128,1,135,192,0,1,198,64,64,0,199,128,192,1,0,1,0,1,64,1,128,0,228,64,128,1,38,0,128,0,3,0,0,0,4,7,95,115,116,97,99,
107,4,6,116,97,98,108,101,4,7,105,110,115,101,114,116,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
#endif
#define linit_c
#define LUA_LIB
#include <stddef.h>
static const luaL_Reg loadedlibs[] = {
    {"_G", luaopen_base},
    {LUA_LOADLIBNAME, luaopen_package},
    {LUA_COLIBNAME, luaopen_coroutine},
    {LUA_TABLIBNAME, luaopen_table},
    {LUA_IOLIBNAME, luaopen_io},
    {LUA_OSLIBNAME, luaopen_os},
    {LUA_STRLIBNAME, luaopen_string},
    {LUA_MATHLIBNAME, luaopen_math},
    {LUA_UTF8LIBNAME, luaopen_utf8},
    {LUA_DBLIBNAME, luaopen_debug},
#if defined(LUA_COMPAT_BITLIB)
    {LUA_BITLIBNAME, luaopen_bit32},
#endif
#ifdef LUA_USE_LMATHX
    {LMATHX_NAME, luaopen_mathx},
#endif
#ifdef LUA_USE_LFS
    {LFS_LIBNAME, luaopen_lfs},
#endif
#ifdef LUA_USE_LSQLITE3
    {"sqlite3", luaopen_lsqlite3},
#endif
#ifdef LUA_USE_LPEG
    {"lpeg", luaopen_lpeg},
#endif
    {NULL, NULL}
};
int linit_do_string(lua_State *L, const char* descr, const char* str) {
    const int ret = luaL_dostring(L, str);
    if (ret != LUA_OK) {
        fprintf(stderr, "error luaL_dostring: %s: %s\n", descr, lua_tostring(L, -1));
        lua_pop(L, 1);
        return 1;
    }
    return 0;
}
int linit_do_bytecode(lua_State *L, const char* descr, const char* str, size_t size) {
    int ret = luaL_loadbuffer (L, str, size, descr);
    if (ret != LUA_OK) {
        fprintf(stderr, "error luaL_loadbuffer: %s: %s\n", descr, lua_tostring(L, -1));
        lua_pop(L, 1);
        return 1;
    }
    if (lua_pcall(L, 0, 0, 0) != LUA_OK) {
        fprintf(stderr, "error lua_pcall: %s: %s\n", descr, lua_tostring(L, -1));
        lua_pop(L, 1);
        return 1;
    }
    return 0;
}
void luaL_openlibs (lua_State *L) {
    const luaL_Reg *lib;
    for (lib = loadedlibs; lib->func; lib++) {
        luaL_requiref(L, lib->name, lib->func, 1);
        lua_pop(L, 1);  
    }
#ifdef LUA_USE_XML2LUA
    linit_do_bytecode(L, "xml2lua", (const char*) XML2LUA, XML2LUA_SIZE);
#endif
#ifdef LUA_USE_DKJSON
    linit_do_bytecode(L, "dkjson", (const char*) DKJSON, DKJSON_SIZE);
#endif
}
