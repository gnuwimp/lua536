-- test mathx library

------------------------------------------------------------------------------
--local math=require"mathx"

print(mathx.version)

print""
function f(x)
	print(x,mathx.isfinite(x),mathx.isnan(x),mathx.isnormal(x))
end

print("x","finite","nan","normal")
f(0)
f(1/-mathx.inf)
f(1/0)
f(-1/0)
f(0/0)
f(mathx.inf)
--f(mathx.huge)
f(mathx.nan)
f(3.45)

print""
print("x","\t","finite","nan","normal")
x=1
while x~=0 do
	y,x=x,x/2
	if not mathx.isnormal(x) then break end
end
f(y)
f(x)
f(mathx.nextafter(x,1))
f(mathx.nextafter(0,1))
f(mathx.nextafter(0,-1))
f(mathx.nextafter(2,3)-2)
while x~=0 do
	y,x=x,x/2
end
f(y)

print""
print("x down","x","x up","diff down","diff up")
function f(x)
	local a=mathx.nextafter(x,-1/0)
	local b=mathx.nextafter(x, 1/0)
	print(a,x,b,x-a,b-x)
end
f(1)
f(0)

mathx.max=mathx.fmax
mathx.min=mathx.fmin

print""
print("max",mathx.fmax(1,mathx.nan,2), mathx.max(mathx.nan,1,2))
print("min",mathx.fmin(1,mathx.nan,2), mathx.min(mathx.nan,1,2))

print""
print("log2(32)",mathx.log2(32))
print("log(32,2)",mathx.log(32,2))
print("exp2(5)","",mathx.exp2(5))
print("cbrt(2)","",mathx.cbrt(2))
print("cbrt(64)",mathx.cbrt(64))
print("6!","",mathx.gamma(6+1))
print("hypot(5,12)",mathx.hypot(5,12))
print("fma(3,2,1)",mathx.fma(3,2,1))

print""
function f(x)
	print(x,mathx.floor(x),mathx.trunc(x),mathx.round(x),mathx.ceil(x))
end
print("x","floor","trunc","round","ceil")
f(-1.2)
f(-1.7)
f(1.2)
f(1.7)

print""
function f(x,y)
	print(x,y,x%y,mathx.remainder(x,y),mathx.fmod(x,y))
end
print("x","y","%","rem","fmod")
for x=0,10 do f(x,7) end

print""
print(mathx.version)
