-- local json = require ("dkjson")

local tbl = {
  animals = { "dog", "cat", "aardvark" },
  instruments = { "violin", "trombone", "theremin" },
  bugs = dkjson.null,
  trees = nil
}

local str = dkjson.encode (tbl, { indent = true })
print (str)

-- Output =
-- {
  -- "bugs":null,
  -- "instruments":["violin","trombone","theremin"],
  -- "animals":["dog","cat","aardvark"]
-- }
-- Decoding

-- local json = require ("dkjson")


local str = [[
{
  "numbers": [ 2, 3, -20.23e+2, -4 ],
  "currency": "\u20AC"
}
]]

local obj, pos, err = dkjson.decode (str, 1, nil)
if err then
  print ("Error:", err)
else
  print ("currency", obj.currency)
  for i = 1,#obj.numbers do
    print (i, obj.numbers[i])
  end
end
