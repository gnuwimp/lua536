---Shows how to parse more than one XML file in the same application.
--@author Manoel Campos da Silva Filho - http://manoelcampos.com

-- local xml2lua, handler = require("xml2lua")
print("xml2lua v" .. xml2lua._VERSION.."\n")

--Uses a handler that converts the XML to a Lua table
-- local handler = require("xmlhandler.tree")

-----------------------  people.xml parse code -----------------------
print("tests/people.xml")
local peopleHandler = xml2lua.tree:new()
local peopleParser = xml2lua.parser(peopleHandler)
peopleParser:parse(xml2lua.loadFile("tests/people.xml"))
xml2lua.printable(peopleHandler.root)

-----------------------  books.xml parse code -----------------------
print("\n\ntests/books.xml")
local booksHandler = xml2lua.tree:new()
local booksParser = xml2lua.parser(booksHandler)
booksParser:parse(xml2lua.loadFile("tests/books.xml"))
xml2lua.printable(booksHandler.root)

