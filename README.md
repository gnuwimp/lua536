# lua536

[Lua](https://www.lua.org/) source as one c file with some extensions.

Additions to the default lua are:
* patch for 'defer' keyword
* Lmathx - compile with -DLUA_USE_LMATHX
* luafilesystem - compile with -DLUA_USE_LFS
* lsqlite3 - compile with -DLUA_USE_LSQLITE3
* lpeg - compile with -DLUA_USE_LPEG
* dkjson - pure lua json parser - compile with -DLUA_USE_DKJSON
* xml2lua - pure lua xml parser - compile with -DLUA_USE_XML2LUA

Included compilable programs:
* lua-536 - the default lua executable with all options turned on
* luac-536 - the byte code compiler
* luas-536 - embed lua code in an executable
